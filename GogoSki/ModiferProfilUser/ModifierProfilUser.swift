//
//  ModifierProfilUser.swift
//  GogoSki
//
//  Created by Bouzid saif on 21/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import MapleBacon
class ModifierProfilUser: InternetCheckViewControllers,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,PassProtocolUser,UITextFieldDelegate {
    
    @IBOutlet weak var ajoutRoundBTN: UIButton!
    /*
     * profile picture that you can touch to go profil
     */
    @IBOutlet weak var imageToProfileNavBar: RoundedUIImageView!
    /*
     * sports tableview
     */
    @IBOutlet weak var TableSport: UITableView!
    /*
     * add sport view
     */
    @IBOutlet weak var ajouterSport: UIView!
    /*
     * date of birth textfield
     */
    @IBOutlet weak var date: UITextField!
    /*
     * title of this page
     */
    @IBOutlet weak var titre: UILabel!
    /*
     * description textview
     */
    @IBOutlet weak var desc: UITextView!
    /*
     * sport and level label
     */
    @IBOutlet weak var labelSportNiveau: UILabel!
    /*
     * validate modification button
     */
    @IBOutlet weak var btnValider: UIButton!
    /*
     * add sport label
     */
    @IBOutlet weak var labelAjouter: UILabel!
    /*
     * add sport label
     */
    @IBOutlet weak var btnAjouterSport: UILabel!
    /*
     * picture modify button
     */
    @IBOutlet weak var btnModifierPhoto: UIButton!
    /*
     * information label
     */
    @IBOutlet weak var labelInfo: UILabel!
    /*
     * profil picture
     */
    @IBOutlet weak var photo_profil: UIImageView!
    /*
     * date of birth textfield
     */
    @IBOutlet weak var data_naissance: PaddingTextField!
    /*
     * name textfield
     */
    @IBOutlet weak var nom: PaddingTextField!
    /*
     * last name textfield
     */
    @IBOutlet weak var prenom: PaddingTextField!
    /*
     * array of levels in french
     */
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     * array of levels in English
     */
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     * array of sports in French
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     * array of sports in English
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     * sports view contraints height
     */
    @IBOutlet weak var sport_height: NSLayoutConstraint!
    /*
     * sport object
     */
    var sports = [SportOBJUser]()
    /*
     * the new sport object added
     */
    var sportsNew = [SportOBJUser]()
    /*
     * initialize image picker
     */
    @objc let imagePicker = UIImagePickerController()
    /*
     * variable test if this is the first time to connect to gogoski
     */
    var Connex = ""
    /*
     * initialize datepicker
     */
    let datePicker = UIDatePicker()
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 10 {
            if range.location >= 12 {
                return false
            }else {
                return true
            }
        }else if textField.tag == 11 {
            if range.location >= 12 {
                return false
            }else {
                return true
            }
        }else {
            return true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prenom.delegate = self
        self.prenom.tag = 10
        self.nom.delegate = self
        self.nom.tag = 11
        //btnValider.isEnabled = false
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.goSports(_:)))
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(self.goSports(_:)))
        gesture.numberOfTapsRequired = 1
        gesture.numberOfTouchesRequired = 1
        gesture2.numberOfTapsRequired = 1
        gesture2.numberOfTouchesRequired = 1
        self.ajouterSport.addGestureRecognizer(gesture)
        self.ajoutRoundBTN.addGestureRecognizer(gesture2)
        self.ajoutRoundBTN.isUserInteractionEnabled = true
        self.ajouterSport.isUserInteractionEnabled = true
        let img = UIImageView()
        img.image =  UIImage(named: "asterix_saif")
        img.frame = CGRect(x: 40 , y: 5, width: 20, height: 20)
        data_naissance.leftView = img
        data_naissance.leftViewMode = .always
        desc.delegate = self
        self.photo_profil.contentMode = .scaleToFill
        self.photo_profil.layer.borderWidth = 0
        self.photo_profil.layer.cornerRadius = 44
        self.photo_profil.layer.masksToBounds = true
        print("height:",self.photo_profil.frame.height)
        print("widht:",self.photo_profil.frame.width)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let a = JSON(data: dataFromString!)
        if let urlImgUser = URL(string : a["user"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            
            self.photo_profil.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        desc.textContainerInset = UIEdgeInsetsMake(10, 13, 10, 13)
        print("aymen:",a)
        if Connex == "" {
            self.prenom.placeholder = Localization("prenom")
            if a["user"]["prenom"].description != "null"{
                self.prenom.text = a["user"]["prenom"].stringValue
            }
             self.nom.placeholder = Localization("nom")
            if a["user"]["nom"].description != "null"{
                self.nom.text = a["user"]["nom"].stringValue
            }
            print("saif:",a["user"]["date_naissance"].description)
            if a["user"]["date_naissance"].description != "null"{
                let q = a["user"]["date_naissance"].stringValue
                var x = q.split(separator: "-")
                var c = x[2].popFirst()
                var d = x[2].popFirst()
                
                self.data_naissance.text =  "\(c!)" + "\(d!)"  + "-" + x[1] + "-" + x[0]
            }
            if a["user"]["description"].description != "null"{
                self.desc.text = a["user"]["description"].stringValue
            }
            if a["user"]["sports"].arrayObject?.count != 0 {
                for i in 0...a["user"]["sports"].arrayObject!.count - 1 {
                    let c = SportOBJUser()
                    c.niveau = a["user"]["sports"][i]["niveau"].stringValue
                    c.pratique = a["user"]["sports"][i]["pratique"].stringValue
                    sports.append(c)
                }
                
                self.TableSport.reloadData()
            }
            //photo_profil.af_setImage(withURL: URL(string: a["user"]["photo"].stringValue)!)
        }else if Connex == "fb" || Connex == "gm" || Connex == "ln" {
            self.prenom.placeholder = Localization("prenom")
            if a["user"]["prenom"].description != "null"{
                self.prenom.text = a["user"]["prenom"].stringValue
            }
             self.nom.placeholder = Localization("nom")
            if a["user"]["nom"].description != "null"{
                self.nom.text = a["user"]["nom"].stringValue
            }
            
            print("saif:",a["user"]["date_naissance"].description)
            if a["user"]["date_naissance"].description != "null"{
                let q = a["user"]["date_naissance"].stringValue
                var x = q.split(separator: "-")
                var c = x[2].popFirst()
                var d = x[2].popFirst()
                
                self.data_naissance.text =  "\(c!)" + "\(d!)"  + "-" + x[1] + "-" + x[0]
            }
        }
        // showDatePicker()
        imagePicker.delegate = self
        
    }
    /*
     * show sport popup
     */
    @objc func goSports(_ sender: UITapGestureRecognizer){
        print("perform")
        if Connex == "" {
            performSegue(withIdentifier: "show_sport", sender: self)
        }else{
            performSegue(withIdentifier: "show_sport_once", sender: self)
        }
    }
    
    /*
     * add sport to the object
     */
    func pass(data: SportOBJUser) { //conforms to protocol
        // implement your own implementation
        print("sport ", data.pratique , "niveau" ,  data.niveau!)
        sports.append(data)
        sportsNew.append(data)
        TableSport.reloadData()
    }
    
    /*
     * pass to the popup parameters
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "show_sport_once"  || segue.identifier == "show_sport" {
            let vc = segue.destination as! AjouterSportUser
            vc.sportTable = self.sports
            vc.delegate = self
        }
    }
    
    /*
     * convert sport object to string
     */
    func getSportsString() -> String {
        var stringToReturn = "["
        var ajouterVirgule = false
        for sport in sports {
            if ajouterVirgule{
                stringToReturn += " , "
            }else{
                ajouterVirgule = true
            }
            stringToReturn += "{ \"niveau\" : \"\(sport.niveau!)\" , \"pratique\" : \"\(sport.pratique!)\" }"
        }
        stringToReturn += "]"
        print("********************")
        print(stringToReturn)
        print("********************")
        return stringToReturn
    }
    /*
     * get data from picker picture
     */
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //let user = SharedPreferences.sharedpref.prefs.string(forKey: "id_user")! as String
        let user = ""
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            DispatchQueue.main.async(execute: {
                // self.imageView?.contentMode = .redraw
                // SwiftSpinner.show("Uploading...")
                self.photo_profil?.image = pickedImage
                // self.uploadnow.setTitle("Upload Now", for: .normal)
                self.myImageUploadRequest(id_product: user)
            })
        }
        dismiss(animated: true, completion: nil)
        
    }
    /*
     * cancel image picker
     */
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        
        dismiss(animated: true, completion: nil)
    }
    /*
     * language configure
     */
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    /*
     * open date pciker and initialize
     */
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            datePicker.locale = NSLocale(localeIdentifier: "fr_FR") as Locale
            let doneButton = UIBarButtonItem(title: "Terminé", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.donedatePicker))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Annuler", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelDatePicker))
            toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        }else {
            datePicker.locale = NSLocale(localeIdentifier: "en_EN") as Locale
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.donedatePicker))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelDatePicker))
            toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        }
        if data_naissance.text != "" {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            datePicker.date = formatter.date(from: data_naissance.text!)!
            
        }
        data_naissance.inputAccessoryView = toolbar
        data_naissance.inputView = datePicker
    }
    /*
     * get data from date picker
     */
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        data_naissance.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    /*
     * back action
     */
    @IBAction func go_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadView"), object: nil)
        
    }
    /*
     * delegate of textview when begin editing
     */
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (desc.text == Localization("descTextView"))
        {
            desc.text = ""
            desc.textColor = UIColor.black
        }
        
    }
    /*
     * delegate of textview when end editing
     */
    func textViewDidChange(_ textView: UITextView) {
        if desc.text == "" {
            desc.textColor = UIColor.lightGray
            desc.text = Localization("descTextView")
            desc.resignFirstResponder()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    /*
     * open image picker
     */
    @IBAction func modif_photo(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    /*
     * validate modification action
     */
    @IBAction func valider_changement(_ sender: Any) {
        
        
        if nom.text != "" && prenom.text != "" && data_naissance.text != ""  && self.sports.count != 0 {
            self.btnValider.isEnabled = false
            LoaderAlert.shared.show(withStatus: Localization("modif2"))
            let q = getSportsString()
            let c = JSON.parse(q)
            print("aaaaaaaaa", c.rawValue)
            var params: Parameters = [ :]
            if desc.text == Localization("descTextView") {
                if self.Connex != "" {
                params = [
                    "nom": self.nom.text!,
                    "prenom" : self.prenom.text!,
                    "dateNaissance" : self.data_naissance.text!,
                    "description"  : "",
                    "sports" : c.rawValue ,
                    "firsttime" : "1"
                ]
                }else{
                    params = [
                        "nom": self.nom.text!,
                        "prenom" : self.prenom.text!,
                        "dateNaissance" : self.data_naissance.text!,
                        "description"  : "",
                        "sports" : c.rawValue ,
                        "firsttime" : "0"
                    ]
                }
            }else {
                 if self.Connex != "" {
                params = [
                    "nom": self.nom.text!,
                    "prenom" : self.prenom.text!,
                    "dateNaissance" : self.data_naissance.text!,
                    "description"  : self.desc.text,
                    "sports" : c.rawValue ,
                    "firsttime" : "1"
                ]
                 }else{
                    params = [
                        "nom": self.nom.text!,
                        "prenom" : self.prenom.text!,
                        "dateNaissance" : self.data_naissance.text!,
                        "description"  : self.desc.text,
                        "sports" : c.rawValue ,
                        "firsttime" : "0"
                    ]
                }
            }
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : a["value"].stringValue
            ]
            Alamofire.request(ScriptBase.sharedInstance.ModifierProfil , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    LoaderAlert.shared.dismiss()
                    self.btnValider.isEnabled = true
                    let b = JSON(response.data)
                    print("b:",b)
                    a["user"]["prenom"].stringValue =    self.prenom.text!
                    a["user"]["nom"].stringValue = self.nom.text!
                    a["user"]["date_naissance"].stringValue = b["data"]["date_naissance"].stringValue
                    a["user"]["sports"].arrayObject = b["data"]["sports"].arrayObject
                    a["user"]["description"].stringValue = b["data"]["description"].stringValue
                    UserDefaults.standard.setValue(a.rawString(), forKey: "User")
                    UserDefaults.standard.synchronize()
                    if b["status"].boolValue {
                        UserDefaults.standard.setValue("YES", forKey: "ModifOnce")
                        UserDefaults.standard.synchronize()
                        if self.Connex != ""{
                            _ = SweetAlert().showAlert(Localization("modifProfil"), subTitle: Localization("modif"), style: AlertStyle.success,buttonTitle: Localization("Passer"), action: { (otherButton) in
                                
                                self.performSegue(withIdentifier: "connexion_once", sender: self)
                            } )
                        }else{
                            _ = SweetAlert().showAlert(Localization("modifProfil"), subTitle: Localization("modif"), style: AlertStyle.success)
                        }
                    }else{
                        
                        
                        _ = SweetAlert().showAlert(Localization("modifProfil"), subTitle: Localization("erreur"), style: AlertStyle.error)
                        
                    }
                    
                    
            }
        }else{
            if Connex == "" {
                if nom.text == "" {
                    print("nom vide")
                }else if prenom.text == "" {
                    print("prenom vide")
                }else if data_naissance.text == ""{
                    print("date vide")
                }
            } else {
                if nom.text != "" && prenom.text != "" && data_naissance.text != "" &&  self.sports.count != 0 {
                    self.btnValider.isEnabled = false
                    LoaderAlert.shared.show(withStatus: Localization("modif2"))
                    let q = getSportsString()
                    let c = JSON.parse(q)
                    print("aaaaaaaaa", c.rawValue)
                    var params: Parameters = [:]
                    if self.Connex != "" {
                     params = [
                        "nom": self.nom.text!,
                        "prenom" : self.prenom.text!,
                        "dateNaissance" : self.data_naissance.text!,
                        "description"  : self.desc.text,
                        "sports" : c.rawValue ,
                        "firsttime" : "1"
                    ]
                    }else{
                         params = [
                            "nom": self.nom.text!,
                            "prenom" : self.prenom.text!,
                            "dateNaissance" : self.data_naissance.text!,
                            "description"  : self.desc.text,
                            "sports" : c.rawValue ,
                            "firsttime" : "0"
                        ]
                    }
                    let ab = UserDefaults.standard.value(forKey: "User") as! String
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    var a = JSON(data: dataFromString!)
                    let header: HTTPHeaders = [
                        "Content-Type" : "application/json",
                        "x-Auth-Token" : a["value"].stringValue
                    ]
                    Alamofire.request(ScriptBase.sharedInstance.ModifierProfil , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                        .responseJSON { response in
                            LoaderAlert.shared.dismiss()
                            self.btnValider.isEnabled = true
                            let b = JSON(response.data)
                            print("b:",b)
                            a["user"]["prenom"].stringValue =    self.prenom.text!
                            a["user"]["nom"].stringValue = self.nom.text!
                            a["user"]["date_naissance"].stringValue = b["data"]["date_naissance"].stringValue
                            a["user"]["sports"].arrayObject = b["data"]["sports"].arrayObject
                            a["user"]["description"].stringValue = self.desc.text
                            UserDefaults.standard.setValue(a.rawString(), forKey: "User")
                            UserDefaults.standard.synchronize()
                            if b["status"].boolValue {
                                UserDefaults.standard.setValue("YES", forKey: "ModifOnce")
                                UserDefaults.standard.synchronize()
                                if self.Connex != ""{
                                    _ = SweetAlert().showAlert(Localization("modifProfil"), subTitle: Localization("modif"), style: AlertStyle.success,buttonTitle: Localization("Passer"), action: { (otherButton) in
                                        
                                        self.performSegue(withIdentifier: "connexion_once", sender: self)
                                    } )
                                }else{
                                    _ = SweetAlert().showAlert(Localization("modifProfil"), subTitle: Localization("modif"), style: AlertStyle.success)
                                }
                            }else{
                                
                                
                                _ = SweetAlert().showAlert(Localization("modifProfil"), subTitle: Localization("erreur"), style: AlertStyle.error)
                                
                            }
                            
                    }
                    
                    
                    
                }
            }
        }
        
    }
    
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("ModifierProfilTitre")
        btnValider.setTitle(Localization("BtnValiderModif"), for: .normal)
        desc.text = Localization("descTextView")
        labelAjouter.text = Localization("labelAjouter")
        labelSportNiveau.text = Localization("labelSportNiveau")
        btnModifierPhoto.setTitle(Localization("btnModifierPhoto"), for: .normal)
        btnAjouterSport.text = Localization("btnAjouterSport")
        labelInfo.text = Localization("infoGeneral")
        data_naissance.placeholder = Localization("pickerDateAnniv")
        Localisator.sharedInstance.saveInUserDefaults = true
        showDatePicker()
        // label.text = Localization("label")
    }
    
    
    /*
     * upload picture to server
     */
    func myImageUploadRequest(id_product:String)
    {
        
        LoaderAlert.shared.show(withStatus: Localization("photoLoading"))
        let myUrl = NSURL(string: ScriptBase.sharedInstance.uploadImage)
        
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        
        let param = [
            "name"  : "Anonymous"
        ]
        var i = 0
        
        
        let boundary = self.generateBoundaryString()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        // request.setValue(a["value"].stringValue, forHTTPHeaderField: "X-Auth-token")
        //let temp = image!.resizeWith(percentage: 0.1)
        let imageData: NSData = UIImageJPEGRepresentation(photo_profil.image!, 1) as! NSData
        
        if(imageData==nil)  { return }
        
        request.httpBody = self.createBodyWithParameters(parameters: param, filePathKey: "image", imageDataKey: imageData as NSData, boundary: boundary) as Data
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(error)")
                return
            }
            
            // You can print out response object
            print("******* response = \(response)")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                
                print("JSON:",json as Any )
                let j = JSON(json)
                
                print(j["url"].stringValue)
                LoaderAlert.shared.show(withStatus: Localization("modifPhoto"))
                let params: Parameters = [
                    "photo": j["url"].stringValue
                ]
                let ab = UserDefaults.standard.value(forKey: "User") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : a["value"].stringValue
                ]
                Alamofire.request(ScriptBase.sharedInstance.ModifierProfil , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        LoaderAlert.shared.dismiss()
                        self.btnValider.isEnabled = true
                        let b = JSON(response.data)
                        print("b:",b)
                        a["user"]["photo"].stringValue = j["url"].stringValue
                        UserDefaults.standard.setValue(a.rawString(), forKey: "User")
                        UserDefaults.standard.synchronize()
                        if b["status"].boolValue {
                            
                            
                            _ = SweetAlert().showAlert(Localization("modifProfil"), subTitle: Localization("modif"), style: AlertStyle.success)
                            
                        }else{
                            
                            
                            _ = SweetAlert().showAlert(Localization("modifProfil"), subTitle: Localization("erreur"), style: AlertStyle.error)
                            
                        }
                        
                }
                
            }catch
            {
                print("errrorsaifPhoto")
                print(error)
            }
            
        }
        
        task.resume()
        
    }
    
    /*
     * convertir image to string
     */
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jvpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
}
extension ModifierProfilUser : UITableViewDataSource , UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if sports.count == 0 {
            return 0
        }
        return sports.count
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.sport_height.constant = self.TableSport.contentSize.height
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "sportsCell") as! UITableViewCell
        let pratiqueLBL = cell.viewWithTag(1) as! UILabel
        let niveauLBL = cell.viewWithTag(2) as! UILabel
        
        if sports.count == 0 {
            return cell
        }
        print ( "waaaaa " , Localisator.sharedInstance.currentLanguage )
        if Localisator.sharedInstance.currentLanguage == "Français" || Localisator.sharedInstance.currentLanguage == "French" || Localisator.sharedInstance.currentLanguage == "French_fr" {
            pratiqueLBL.text = sports[indexPath.row].pratique!
            niveauLBL.text = sports[indexPath.row].niveau!
        }else {
            switch sports[indexPath.row].pratique! {
            case SportSaifFR[0] :
                pratiqueLBL.text = SportSaifEN[0]
            case SportSaifFR[1] :
                pratiqueLBL.text = SportSaifEN[1]
            case SportSaifFR[2] :
                pratiqueLBL.text = SportSaifEN[2]
            case SportSaifFR[3] :
                pratiqueLBL.text = SportSaifEN[3]
            case SportSaifFR[4] :
                pratiqueLBL.text = SportSaifEN[4]
            case SportSaifFR[5] :
                pratiqueLBL.text = SportSaifEN[5]
            default :
                pratiqueLBL.text = ""
            }
            switch sports[indexPath.row].niveau! {
            case NiveauSaifFR[0] :
                niveauLBL.text = NiveauSaifEN[0]
            case NiveauSaifFR[1] :
                niveauLBL.text =  NiveauSaifEN[1]
            case NiveauSaifFR[2] :
                niveauLBL.text =  NiveauSaifEN[2]
            case NiveauSaifFR[3] :
                niveauLBL.text =  NiveauSaifEN[3]
            case NiveauSaifFR[4] :
                niveauLBL.text =  NiveauSaifEN[4]
            default :
                niveauLBL.text = ""
                
            }
        }
        
        return cell
    }
}

extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}


