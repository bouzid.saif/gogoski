//
//  ChoixLogementViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 18/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import DownPicker

class ChoixLogementViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{
    
    var test : Bool = true
    @IBOutlet weak var departement: UITextField!
    var departement_downPicker : DownPicker!
    @IBOutlet weak var station: UITextField!
    @IBOutlet weak var tableviewHeigh: NSLayoutConstraint!
    @IBOutlet weak var tableviewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var titre: UILabel!
    @IBOutlet weak var filterConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var tableview: UITableView!
    var station_downPicker : DownPicker!
    @IBOutlet weak var dateFin: UITextField!
    
    var massif_downPicker : DownPicker!
    @IBOutlet weak var massif: UITextField!
    var budget_downPicker : DownPicker!
    @IBOutlet weak var budget: UITextField!
    @IBOutlet weak var filtre: UIView!
    var choixPrestataire_downPicker : DownPicker!
    @IBOutlet weak var choixPrestataire: UITextField!
    @IBOutlet weak var dateDep: UITextField!
    
    
    var nbrPaticipant_downPicker : DownPicker!
    @IBOutlet weak var nbrPaticipant: UITextField!
    var typeLogement_downPicker : DownPicker!
    @IBOutlet weak var typeLogement: UITextField!
    
    
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        showDatePicker()
        showDatePicker1()
        let img2 = UIImageView()
        img2.image = UIImage(named: "calender_ahmed")
        img2.frame = CGRect(x: CGFloat(dateDep.frame.size.width - 20), y: CGFloat(20), width: CGFloat(20), height: CGFloat(20))
        dateDep.rightView = img2
        dateDep.rightViewMode = .always
        
        let img = UIImageView()
        img.image = UIImage(named: "calender_ahmed")
        img.frame = CGRect(x: CGFloat(dateFin.frame.size.width - 40), y: CGFloat(20), width: CGFloat(20), height: CGFloat(20))
        dateFin.rightView = img
        dateFin.rightViewMode = .always
        
        let bandArray : NSMutableArray = []
        bandArray.add("Budget 1")
        bandArray.add("Budget 2")
        
        //self.down = UIDownPicker(data: bandArray as! [Any])
        budget_downPicker = DownPicker(textField: budget, withData: bandArray as! [Any])
        budget_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        
        
        let bandArray1 : NSMutableArray = []
        bandArray1.add("Departement 1")
        bandArray1.add("Departement 2")
        departement_downPicker = DownPicker(textField: departement, withData: bandArray1 as! [Any])
        departement_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        
        let bandArray2 : NSMutableArray = []
        bandArray2.add("choixPrestataire 1")
        bandArray2.add("choixPrestataire 2")
        choixPrestataire_downPicker = DownPicker(textField: choixPrestataire, withData: bandArray2 as! [Any])
        choixPrestataire_downPicker.setArrowImage(UIImage(named:"downArrow"))
       
        
        
        let bandArray3 : NSMutableArray = []
        bandArray3.add("station 1")
        bandArray3.add("station 2")
        station_downPicker = DownPicker(textField: station, withData: bandArray3 as! [Any])
        station_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        
        let bandArray4 : NSMutableArray = []
        bandArray4.add("massif 1")
        bandArray4.add("massif 2")
        massif_downPicker = DownPicker(textField: massif, withData: bandArray4 as! [Any])
        massif_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        
        let bandArray5 : NSMutableArray = []
        bandArray5.add("typeLogement 1")
        bandArray5.add("typeLogement 2")
        typeLogement_downPicker = DownPicker(textField: typeLogement, withData: bandArray5 as! [Any])
        typeLogement_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        
        
        let bandArray7 : NSMutableArray = []
        bandArray7.add("nbrPaticipant 1")
        bandArray7.add("nbrPaticipant 2")
        
        //self.down = UIDownPicker(data: bandArray as! [Any])
        nbrPaticipant_downPicker = DownPicker(textField: nbrPaticipant, withData: bandArray7 as! [Any])
        nbrPaticipant_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pickDate(_ sender: Any) {
        print ("heey")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath)
        let labelSnowboard : UILabel = cell.viewWithTag(1) as! UILabel
        labelSnowboard.layer.cornerRadius = 5
        labelSnowboard.layer.masksToBounds = true
        let labelBudget : UILabel = cell.viewWithTag(3) as! UILabel
        labelBudget.layer.masksToBounds = true
        labelBudget.layer.cornerRadius = 5
        return cell
    }
    @IBAction func filtrer(_ sender: Any) {
        if (test)
        {
            
            UIView.animate(withDuration: 0.8 , delay: 0 , options: .curveEaseOut ,animations: {
                self.filtre.isHidden = true
                //   self.filterConstraint.constant = 0
                //  self.tableviewConstraint.constant = 25
                self.btnFilter.frame.origin.y -= self.filtre.frame.size.height
                self.tableview.frame.origin.y -= self.filtre.frame.size.height
                self.tableview.frame.size.height += self.filtre.frame.size.height
                
                print (self.btnFilter.frame.origin.y)
                //   self.tableview.setContentOffset(CGPoint.zero, animated: true)
                self.scroll.setContentOffset(
                    CGPoint(x: 0,y: -self.scroll.contentInset.top),
                    animated: true)
                self.btnFilter.setBackgroundImage(UIImage(named: "btndérouleur1_ahmed"), for: .normal)
                //   self.tableview.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: true)
                
            }, completion: { _ in
                //  self.filterConstraint.constant = 0
                self.tableviewConstraint.constant = 25
                self.tableviewHeigh.constant += self.filtre.frame.size.height
            })
            
            test = false
            
        }
        else {
            
            //self.tableview.beginUpdates()
            
            // self.tableview.updateConstraints()
            // self.tableview.endUpdates()
            
            UIView.animate(withDuration: 0.8 , delay: 0 , options: .curveEaseIn ,animations: {
                self.filtre.isHidden = false
                //   self.filterConstraint.constant = 100
                // self.tableviewConstraint.constant = self.filterConstraint.constant + 8
                self.tableview.frame.origin.y += self.filtre.frame.size.height
                self.btnFilter.frame.origin.y += self.filtre.frame.size.height
                print (self.btnFilter.frame.origin.y)
                
                self.btnFilter.setBackgroundImage(UIImage(named: "btndérouleur_ahmed"), for: .normal)
                
                //   self.tableviewConstraint.constant = self.filtre.frame.size.height + 8
            }, completion: { _ in
                // self.filterConstraint.constant = 546
                //self.tableviewConstraint.constant = 25
                self.tableviewConstraint.constant = self.filterConstraint.constant + 8
                self.tableviewHeigh.constant = self.tableview.contentSize.height
            })
            test = true
            
            
            
        }
        /*  if (test)
         {
         filtre.isHidden = true
         tableviewHeigh.constant = view.frame.height
         btnFilter.setBackgroundImage(UIImage(named: "btndérouleur1_ahmed"), for: .normal)
         filterConstraint.constant = 0
         tableviewConstraint.constant = 25
         test = false
         
         self.tableview.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: true)
         
         }
         else {
         test = true
         filtre.isHidden = false
         tableviewHeigh.constant = self.tableview.contentSize.height
         btnFilter.setBackgroundImage(UIImage(named: "btndérouleur_ahmed"), for: .normal)
         filterConstraint.constant = 546
         tableviewConstraint.constant = filterConstraint.constant + 8
         
         }*/
        
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(ChoixLogementViewController.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ChoixLogementViewController.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        dateDep.inputAccessoryView = toolbar
        dateDep.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        dateDep.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    func showDatePicker1(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(ChoixLogementViewController.donedatePicker1))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ChoixLogementViewController.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        dateFin.inputAccessoryView = toolbar
        dateFin.inputView = datePicker
        
    }
    
    @objc func donedatePicker1(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        dateDep.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("ChoixLogementTitre")
        departement_downPicker.setPlaceholder(Localization("Departement"))
        budget_downPicker.setPlaceholder(Localization("Budget"))
         choixPrestataire_downPicker.setPlaceholder(Localization("ChoixPrestataire"))
        station_downPicker.setPlaceholder(Localization("Station"))
        massif_downPicker.setPlaceholder(Localization("Massif"))
        typeLogement_downPicker.setPlaceholder(Localization("TypeLogement"))
        nbrPaticipant_downPicker.setPlaceholder(Localization("NbrParticipant"))
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
}
