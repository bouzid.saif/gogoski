//
//  FicheLogementViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 28/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import ImageSlideshow

class FicheLogementViewController: InternetCheckViewControllers {

     @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var labelNumber: UILabel!
    @IBOutlet weak var viewContraintHigh: NSLayoutConstraint!
     @IBOutlet weak var labelTopConstraint: NSLayoutConstraint!
      @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var labelPhotos: UILabel!
    @IBOutlet weak var btnValider: UIButton!
    @IBOutlet weak var Labelinformation: UILabel!
    @IBOutlet weak var titre: UILabel!
    @IBOutlet var slideshow: ImageSlideshow!
    var alamofireSource : [InputSource] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        
         alamofireSource = [AlamofireSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080")!, AlamofireSource(urlString: "https://images.unsplash.com/photo-1447746249824-4be4e1b76d66?w=1080")!, AlamofireSource(urlString: "https://images.unsplash.com/photo-1463595373836-6e0b0a8ee322?w=1080")!]
        self.labelNumber.text = "1/" + String(self.alamofireSource.count)
        // Do any additional setup after loading the view.
        slideshow.backgroundColor = UIColor.white
     //   slideshow.slideshowInterval = 0.0
        
        slideshow.pageControlPosition = PageControlPosition.hidden
       // slideshow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        //slideshow.pageControl.pageIndicatorTintColor = UIColor.black
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
        slideshow.isMultipleTouchEnabled = false
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.currentPageChanged = { page in
            self.labelNumber.text = String(page + 1) + "/" + String(self.alamofireSource.count)
        }
        
        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        slideshow.setImageInputs(alamofireSource)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(FicheLogementViewController.didTap))
        slideshow.addGestureRecognizer(recognizer)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func didTap() {
        let fullScreenController = slideshow.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    @IBAction func previous(_ sender: Any) {
        slideshow.setCurrentPage(slideshow.currentPage - 1, animated: true)
        
    }
    @IBAction func next(_ sender: Any) {
        slideshow.setCurrentPage(slideshow.currentPage + 1, animated: true)
    }

     @IBAction func plus(_ sender: Any) {
     if (labelDesc.numberOfLines == 2)
     {
     labelDesc.numberOfLines = 0
     btnPlus.setTitle("moin", for: .normal)
     labelDesc.sizeToFit()
     viewContraintHigh.constant = labelDesc.frame.height + 8.0 + btnPlus.frame.height
     }
     else {
     labelDesc.numberOfLines = 2
     btnPlus.setTitle("plus", for: .normal)
     labelTopConstraint.constant = 8
     labelDesc.sizeToFit()
     viewContraintHigh.constant = labelDesc.frame.height + 29.0 + btnPlus.frame.height
     }
     }
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("FicheLogementTitre")
        Labelinformation.text = Localization("information")
        labelPhotos.text = Localization("photos")
        btnValider.setTitle(Localization("BtnValider"), for: .normal)
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
}
