//
//  Mes_preferences_saif.swift
//  GogoSki
//
//  Created by Bouzid saif on 16/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import DownPicker
import Alamofire
import SwiftyJSON
import OneSignal
extension UINavigationController {
    
    func backToViewController(viewController: Swift.AnyClass) {
        
        for element in viewControllers as Array {
            if element.isKind(of: viewController) {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
}
class Mes_preferences_saif :InternetCheckViewControllers {
    /*
     * test if mail notification enabled
     */
    var mailnotif = "0"
    /*
     * array of languages
     */
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    /*
     * navigation title
     */
    @IBOutlet weak var titre: UILabel!
    /*
     * notification checkbox
     */
    @IBOutlet weak var check1: VKCheckbox!
    /*
     * mail checkbox
     */
    @IBOutlet weak var check2: VKCheckbox!
    /*
     * email textfield
     */
    @IBOutlet weak var email: PaddingTextField!
    /*
     * language textfield
     */
    @IBOutlet weak var langue: UITextField!
    /*
     * carte number textfield
     */
    @IBOutlet weak var num_carte: PaddingTextField!
    /*
     * language label
     */
    @IBOutlet weak var labelLangue: UILabel!
    /*
     * carte information label
     */
    @IBOutlet weak var labelInformationCarte: UILabel!
    /*
     * contact label
     */
    @IBOutlet weak var labelContact: UILabel!
    /*
     * bill information label
     */
    @IBOutlet weak var labelInformationFacture: UILabel!
    /*
     * notification label
     */
    @IBOutlet weak var labelNotification: UILabel!
    /*
     * prefix textfield
     */
    @IBOutlet weak var indicatif: UITextField!
    /*
     * carte type textfield
     */
    @IBOutlet weak var type_carte: UITextField!
    /*
     * modify picture button
     */
    @IBOutlet weak var btnModifier: UIButton!
    /*
     * carte title textfield
     */
    @IBOutlet weak var TitulaireCarte : UITextField!
    /*
     * street textfield
     */
    @IBOutlet weak var Rue : UITextField!
    /*
     * city textfield
     */
    @IBOutlet weak var Ville : UITextField!
    /*
     * code postal textfield
     */
    @IBOutlet weak var CodePostale : UITextField!
    /*
     * phone number textfield
     */
    @IBOutlet weak var telephone : UITextField!
    /*
     * phone number textfield
     */
    @IBOutlet weak var telephoneNotif : UITextField!
    /*
     * button delete user account
     */
    @IBOutlet weak var btnSupp: UILabel!
    /*
     * validate modification button
     */
    @IBOutlet weak var btnValider: UIButton!
    /*
     * country textfield
     */
    @IBOutlet weak var pays: UITextField!
    /*
     * language downpicker
     */
    var langue_picker = DownPicker()
    /*
     * prefix downpicker
     */
    var indicatif_picker = DownPicker()
    /*
     * carte type downpicker
     */
    var type_carte_picker = DownPicker()
    /*
     * country downpicker
     */
    var pays_picker = DownPicker()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnModifier.isEnabled = false
        self.email.isUserInteractionEnabled = false
        
        check1.line = .normal
        check1.bgColorSelected = UIColor(red: 95/255, green: 182/255, blue: 255/255, alpha: 1)
        check1.bgColor          = UIColor.white
        check1.color            = UIColor.white
        check1.borderColor      = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
        check1.borderWidth      = 2
        // check1.cornerRadius     = check1.frame.height / 2
        
        check2.line = .normal
        check2.bgColorSelected = UIColor(red: 95/255, green: 182/255, blue: 255/255, alpha: 1)
        check2.bgColor          = UIColor.white
        check2.color            = UIColor.white
        check2.borderColor      = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
        check2.borderWidth      = 2
        // check1.cornerRadius     = check1.frame.height / 2
        check2.checkboxValueChangedBlock = {
            isOn in
            print("Custom checkbox is \(isOn ? "ON" : "OFF")")
            if isOn {
                print("active")
                
                self.check2.borderWidth      = 0
                self.check2.borderColor      = UIColor.white
            } else{
                
                self.check2.borderWidth      = 2
                self.check2.borderColor      = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
            }
        }
       
        check1.checkboxValueChangedBlock = {
            isOn in
            print("Custom checkbox is \(isOn ? "ON" : "OFF")")
            if isOn {
                print("active")
                self.mailnotif = "1"
                self.check1.borderWidth      = 0
                self.check1.borderColor      = UIColor.white
            } else{
                self.mailnotif = "0"
                self.check1.borderWidth      = 2
                self.check1.borderColor      = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
            }
        }
        
        self.initPickers()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        loadData()
    }
    /*
     * initialize downpickers function
     */
    func initPickers(){
        var bandArray : NSMutableArray = []
        bandArray.add(Localization("French_fr"))
        bandArray.add(Localization("English_en"))
        self.langue_picker.updateTextFieldValueOnlyWhenDonePressed = true
        //self.down = UIDownPicker(data: bandArray as! [Any])
        self.langue_picker = DownPicker(textField: self.langue, withData: bandArray as! [Any])
        
        self.langue_picker.setArrowImage(UIImage(named:"downArrow_saif"))
        
        bandArray = [  "+1 ",
                       "+7  ",
                       "+20 ",
                       "+27 ",
                       "+30 ",
                       "+31 ",
                       "+32 ",
                       "+33 ",
                       "+34 ",
                       "+36 ",
                       "+39 ",
                       "+40 ",
                       "+41 ",
                       "+43 ",
                       "+44 ",
                       "+45 ",
                       "+46 ",
                       "+47 ",
                       "+47 ",
                       "+48 ",
                       "+49 ",
                       "+51 ",
                       "+52 ",
                       "+53 ",
                       "+54 ",
                       "+55 ",
                       "+56 ",
                       "+57 ",
                       "+58 ",
                       "+60 ",
                       "+61 ",
                       "+62 ",
                       "+63 ",
                       "+64 ",
                       "+65 ",
                       "+66 ",
                       "+77 ",
                       "+81 ",
                       "+82 ",
                       "+84 ",
                       "+86 ",
                       "+90 ",
                       "+91 ",
                       "+92 ",
                       "+93 ",
                       "+94 ",
                       "+95 ",
                       "+98 ",
                       "+211 ",
                       "+212 ",
                       "+213 ",
                       "+216 ",
                       "+218 ",
                       "+220 ",
                       "+221 ",
                       "+222 ",
                       "+223 ",
                       "+224 ",
                       "+225 ",
                       "+226 ",
                       "+227 ",
                       "+228 ",
                       "+229 ",
                       "+230 ",
                       "+231 ",
                       "+232 ",
                       "+233 ",
                       "+234 ",
                       "+235 ",
                       "+236 ",
                       "+237 ",
                       "+238 ",
                       "+239 ",
                       "+240 ",
                       "+241 ",
                       "+242 ",
                       "+243 ",
                       "+244 ",
                       "+245 ",
                       "+246 ",
                       "+248 ",
                       "+249 ",
                       "+250 ",
                       "+251 ",
                       "+252 ",
                       "+253 ",
                       "+254 ",
                       "+255 ",
                       "+256 ",
                       "+257 ",
                       "+258 ",
                       "+260 ",
                       "+261 ",
                       "+262 ",
                       "+262 ",
                       "+263 ",
                       "+264 ",
                       "+265 ",
                       "+266 ",
                       "+267 ",
                       "+268 ",
                       "+269 ",
                       "+290 ",
                       "+291 ",
                       "+297 ",
                       "+298 ",
                       "+299 ",
                       "+ 345 " ,
            "+350 ",
            "+351 ",
            "+352 ",
            "+353 ",
            "+354 ",
            "+355 ",
            "+356 ",
            "+357 ",
            "+358 ",
            "+359 ",
            "+370 ",
            "+371 ",
            "+372 ",
            "+373 ",
            "+374 ",
            "+375 ",
            "+376 ",
            "+377 ",
            "+378 ",
            "+379 ",
            "+380 ",
            "+381 ",
            "+382 ",
            "+385 ",
            "+386 ",
            "+387 ",
            "+389 ",
            "+420 ",
            "+421 ",
            "+423 ",
            "+500 ",
            "+501 ",
            "+502 ",
            "+503 ",
            "+504 ",
            "+505 ",
            "+506 ",
            "+507 ",
            "+508 ",
            "+509 ",
            "+590 ",
            "+591 ",
            "+593 ",
            "+594 ",
            "+595 ",
            "+595 ",
            "+596 ",
            "+597 ",
            "+598 ",
            "+599 ",
            "+670 ",
            "+672 ",
            "+673 ",
            "+674 ",
            "+675 ",
            "+676 ",
            "+677 ",
            "+678 ",
            "+679 ",
            "+680 ",
            "+681 ",
            "+682 ",
            "+683 ",
            "+685 ",
            "+686 ",
            "+687 ",
            "+688 ",
            "+689 ",
            "+690 ",
            "+691 ",
            "+692 ",
            "+850 ",
            "+852 ",
            "+853 ",
            "+855 ",
            "+856 ",
            "+872 ",
            "+880 ",
            "+886 ",
            "+960 ",
            "+961 ",
            "+962 ",
            "+963 ",
            "+964 ",
            "+965 ",
            "+966 ",
            "+967 ",
            "+968 ",
            "+970 ",
            "+971 ",
            "+972 ",
            "+973 ",
            "+974 ",
            "+975 ",
            "+976 ",
            "+977 ",
            "+992 ",
            "+993 ",
            "+994 ",
            "+995 ",
            "+996 ",
            "+998 ",
            "+1242 ",
            "+1246 ",
            "+1264 ",
            "+1268 ",
            "+1284 ",
            "+1340 ",
            "+1441 ",
            "+1473 ",
            "+1649 ",
            "+1664 ",
            "+1670 ",
            "+1671 ",
            "+1684 ",
            "+1758 ",
            "+1767 ",
            "+1784 ",
            "+1849 ",
            "+1868 ",
            "+1869 ",
            "+1876 ",
            "+1939 " ]
        self.indicatif_picker = DownPicker(textField: self.indicatif, withData: bandArray as! [Any])
     
        self.indicatif_picker.setArrowImage(UIImage(named:"downArrow_saif"))
           self.indicatif_picker.updateTextFieldValueOnlyWhenDonePressed = true
        bandArray = ["MasterCard","Visa","Gold","American Express","Discover"]
        
        self.type_carte_picker = DownPicker(textField: self.type_carte, withData: bandArray as! [Any])
        
        
        self.type_carte_picker.setArrowImage(UIImage(named:"downArrow_saif"))
        self.type_carte_picker.updateTextFieldValueOnlyWhenDonePressed = true
        
        bandArray = ["United States of America", "Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antigua & Deps", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bhutan", "Bolivia", "Bosnia Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Central African Rep", "Chad", "Chile", "People's Republic of China", "Republic of China", "Colombia", "Comoros", "Democratic Republic of the Congo", "Republic of the Congo", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Danzig", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland", "France", "Gabon", "Gaza Strip", "The Gambia", "Georgia", "Germany", "Ghana", "Greece", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Holy Roman Empire", "Honduras", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Republic of Ireland", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan", "Jonathanland", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "North Korea", "South Korea", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho","Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova","Monaco", "Mongolia", "Montenegro", "Morocco", "Mount Athos", "Mozambique", "Namibia, Nauru", "Nepal", "Newfoundland", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Ottoman Empire", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Prussia", "Qatar", "Romania", "Rome", "Russian", "Federation", "Rwanda", "St Kitts & Nevis", "St Lucia", "Saint Vincent & the Grenadines", "Samoa", "San Marino", "Sao Tome & Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe"]
        self.pays_picker.updateTextFieldValueOnlyWhenDonePressed = true
        self.pays_picker = DownPicker(textField: self.pays, withData: bandArray as! [Any])
        self.pays_picker.setArrowImage(UIImage(named:"downArrow_saif"))
        
        
        
        
    }
    /*
     * back button
     */
    @IBAction func go_back(_ sender: Any) {
        // self.navigationController?.backToViewController(viewController: ProfilMain.self)
        print("hiNavigation")
        
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadView"), object: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("count:",self.navigationController?.viewControllers.count)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("PreferencesTitle")
        labelLangue.text = Localization("PageLangueTitle")
        langue.placeholder = Localization("PageLangueTitle")
        labelInformationCarte.text = Localization("labelInformationCarte")
        labelContact.text = Localization("labelContact")
        labelNotification.text = Localization("labelNotification")
        labelInformationFacture.text = Localization("labelInformationFacture")
        pays_picker.setPlaceholder(Localization("payspicker"))
        langue_picker.setPlaceholder(Localization("languepicker"))
        type_carte_picker.setPlaceholder(Localization("typeCartepicker"))
        indicatif_picker.setPlaceholder(Localization("indicatifpicker"))
        btnModifier.setTitle(Localization("btnModifier"), for: .normal)
        btnSupp.text = Localization("btnSupp")
        btnValider.setTitle(Localization("BtnValiderModif"), for: .normal)
        TitulaireCarte.placeholder = Localization("TitulaireCarte")
        Rue.placeholder = Localization("Rue")
        Ville.placeholder = Localization("Ville")
        CodePostale.placeholder = Localization("CodePostale")
        telephone.placeholder = Localization("Telephone")
        telephoneNotif.placeholder = Localization("Telephone")
        telephoneNotif.isUserInteractionEnabled = false
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            langue_picker.setToolbarDoneButtonText("Terminé")
            langue_picker.setToolbarCancelButtonText("Annuler")
            
            pays_picker.setToolbarDoneButtonText("Terminé")
            pays_picker.setToolbarCancelButtonText("Annuler")
            
            
        }else {
            langue_picker.setToolbarDoneButtonText("Done")
            langue_picker.setToolbarCancelButtonText("Cancel")
            indicatif_picker.setToolbarDoneButtonText("Done")
            indicatif_picker.setToolbarCancelButtonText("Cancel")
            pays_picker.setToolbarDoneButtonText("Done")
            pays_picker.setToolbarCancelButtonText("Cancel")
            
        }
        Localisator.sharedInstance.saveInUserDefaults = true
        
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /*
     * get profile preferences
     */
    func loadData(){
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.AfficherProfil , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                
                let a = JSON(response.data)
                print("saif:",a)
                
                if a["status"].boolValue == false {
                    if a["message"].stringValue == "L utilisateur n existe pas" || a["message"].stringValue == "Mot de passe invalide" {
                        
                        _ = SweetAlert().showAlert(Localization("ConnexionChargement"), subTitle: Localization("ConnexionErreur"), style: AlertStyle.error)
                    }
                }else{
                    print(a)
                    let tap = UITapGestureRecognizer(target: self, action:  #selector(self.supprimerCompte(sender:)))
                    tap.numberOfTapsRequired = 1
                    self.btnSupp.isUserInteractionEnabled = true
                    self.btnSupp.addGestureRecognizer(tap)
                    self.btnModifier.isEnabled = true
                    self.email.text = a["data"]["email"].stringValue
                    self.telephone.text = a["data"]["telephone"].stringValue
                    self.indicatif.text = a["data"]["prefix"].stringValue
                    /* let index = self.pays.index(ofAccessibilityElement: self.indicatif.text!)
                     self.indicatif_picker.selectedIndex = index*/
                    self.num_carte.text = a["data"]["carte_bancaire"]["numero"].stringValue
                    self.type_carte.text = a["data"]["carte_bancaire"]["type"].stringValue
                    /* let index1 = self.pays.index(ofAccessibilityElement: self.type_carte.text!)
                     self.indicatif_picker.selectedIndex = index1*/
                    self.TitulaireCarte.text = a["data"]["carte_bancaire"]["titulaire"].stringValue
                    self.Rue.text = a["data"]["_address"]["rue"].stringValue
                    self.Ville.text = a["data"]["_address"]["ville"].stringValue
                    self.CodePostale.text = a["data"]["_address"]["code_postal"].stringValue
                    self.pays.text = a["data"]["_address"]["pays"].stringValue
                    /*let index2 = self.pays.index(ofAccessibilityElement: self.pays.text!)
                     self.pays_picker.selectedIndex = index2*/
                    self.langue.text = a["data"]["langue"].stringValue
                    
                    /*  if self.langue.text! == "French" || self.langue.text! == "Français" || self.langue.text! == "French_fr"
                     {
                     self.langue_picker.selectedIndex = 1
                     }
                     else {
                     self.langue_picker.selectedIndex = 2
                     }*/
                    self.mailnotif = a["data"]["mail_notif"].boolValue ? "1" : "0"
                    if ( self.mailnotif == "1")
                    {
                        self.check1.setOn(true, animated: false , user: true)
                    }
                    else
                    {
                        self.check1.setOn(false, animated: false , user: true)
                    }
                    if (a["data"]["tel_notif"].boolValue) {
                        self.check2.setOn(true, animated: false , user: true)
                    }
                    else
                    {
                        self.check2.setOn(false, animated: false , user: true)
                    }
                    
                    print (self.mailnotif , "notiiiiiif" )
                }
                
                
        }
    }
    /*
     * validate modification action
     */
    @IBAction func valider_changement(_ sender: Any) {
        
        LoaderAlert.shared.show(withStatus: Localization("modif2"))
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        self.btnValider.isEnabled = false
        LoaderAlert.shared.show(withStatus: Localization("modif2"))
        var langue = ""
        if (self.langue.text == "Anglais" || self.langue.text == "English")
        {
            langue = "English"
        }
        else {
            langue = "Français"
        }
        let params: Parameters = [
            "mailNotif" : mailnotif ,
            "telNotif" : self.check2.isOn() ? "1" : "0",
            "email": email.text! ,
            "langue" : langue ,
            "telephone" :  self.telephone.text! ,
            "prefix" : self.indicatif.text!,
            "carte_bancaire"  : [
                "numero" : num_carte.text!,
                "type" : type_carte.text!,
                "titulaire" : TitulaireCarte.text!
            ],
            "address" : [
                "rue" : Rue.text! ,
                "ville" : Ville.text! ,
                "pays" : pays.text! ,
                "code" : CodePostale.text!
            ] ,
            ]
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.ModifierPreferences , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                self.btnValider.isEnabled = true
                let b = JSON(response.data)
                print("b:",b)
                if ( b["status"].boolValue)
                {
                    if self.check2.isOn() {
                        OneSignal.setSubscription(true)
                    }else {
                        OneSignal.setSubscription(false)
                    }
                    _ = SweetAlert().showAlert(Localization("modifPreferences"), subTitle: Localization("modif"), style: AlertStyle.success,buttonTitle: "OK", action: { (otherButton) in
                        a["user"]["email"].stringValue = self.email.text!
                        if (self.langue.text == Localization("French_fr"))
                        {
                            SetLanguage(self.arrayLanguages[2])
                        }
                        else {
                            SetLanguage(self.arrayLanguages[1])
                        }
                        
                        Localisator.sharedInstance.saveInUserDefaults = true
                        UserDefaults.standard.setValue(a.rawString(), forKey: "User")
                        UserDefaults.standard.synchronize()
                        self.navigationController?.popViewController(animated: true)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadView"), object: nil)
                    } )
                    
                }
                else {
                    _ = SweetAlert().showAlert(Localization("modifPreferences"), subTitle: Localization("erreur"), style: AlertStyle.error)
                }
                
                
                
                
        }
        
        
    }
    /*
     * delete account action
     */
    @objc func supprimerCompte(sender:UITapGestureRecognizer) {
        SweetAlert().showAlert(Localization("areYouSure"), subTitle: Localization("youWillDelete"), style: AlertStyle.warning, buttonTitle:Localization("cancel"), buttonColor: UIColor.gray , otherButtonTitle:  Localization("yes"), otherButtonColor: UIColor.red) { (isOtherButton) -> Void in
            if isOtherButton == false {
                LoaderAlert.shared.show(withStatus: "Suppression...")
                let ab = UserDefaults.standard.value(forKey: "User") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : a["value"].stringValue
                ]
                Alamofire.request(ScriptBase.sharedInstance.supprimerUser , method: .get, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        LoaderAlert.shared.dismiss()
                        
                        let b = JSON(response.data!)
                        
                        if b["status"].boolValue {
                            
                            _ = SweetAlert().showAlert("Supression", subTitle: "Success", style: AlertStyle.success, buttonTitle: "OK" , action: { (otherButton) in
                                let login = self.storyboard?.instantiateViewController(withIdentifier: "PageBienvenue") as! PageBienvenue
                                // let presentaion = UIPresentationController(presentedViewController: self, presenting: login)
                                let transition = CATransition()
                                transition.duration = 0.5
                                transition.type = kCATransitionPush
                                transition.subtype = kCATransitionFromLeft
                                //login.view.window!.layer.add(transition, forKey: kCATransition)
                                let navigation = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
                                self.navigationController?.view.window?.layer.add(transition, forKey: kCATransition)
                                self.navigationController?.tabBarController?.tabBar.isHidden = true
                                self.navigationController?.pushViewController(login, animated: true)
                            } )
                            
                        }
                        else {
                            _ = SweetAlert().showAlert("Supression", subTitle: Localization("erreur"), style: AlertStyle.error)
                        }
                }
                
            }
        }
    }
}

