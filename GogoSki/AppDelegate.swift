//
//  AppDelegate.swift
//  GogoSki
//
//  Created by Bouzid saif on 30/09/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import CoreData
import FBSDKLoaginKit
import GoogleSignIn
import Google
import LinkedinSwift
import IQKeyboardManagerSwift
import Alamofire
import SwiftyJSON
import OneSignal
import UserNotifications
import Firebase
import Fabric
import Crashlytics
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate {
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    var NotifReceive = false
    var NotifAhmed = false
    var window: UIWindow?
    var ConversatioNRoom : JSON = []
    var ConnectedUser : [String] = [] {
        didSet {
            print("ConnectedUsers : ",ConnectedUser)
        }
    }
    var MessageController : UIViewController? = UIViewController()
    var navigation : UINavigationController = UINavigationController()
    var DontTest = false
    var SectionCritique = 0
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FIRApp.configure()
        Fabric.with([Crashlytics.self])
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: true , kOSSettingsKeyInAppLaunchURL: true ]
        /* if EVURLCache._cacheDirectory != "" {
         EVURLCache.LOGGING = true // We want to see all caching actions
         EVURLCache.MAX_FILE_SIZE = 26 // We want more than the default: 2^26 = 64MB
         EVURLCache.MAX_CACHE_SIZE = 30 // We want more than the default: 2^30 = 1GB
         EVURLCache.FORCE_LOWERCASE = false
         EVURLCache.activate()
         } */
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        
        UNUserNotificationCenter.current().delegate = self
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "413a8d59-63c1-4d32-886b-f0a629a4962f",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.none
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            //  _ = SweetAlert().showAlert("Push", subTitle: "Vous etes connecté", style: AlertStyle.success)
            print("Received Notification: \(notification!.payload.notificationID)")
        }
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            let payload: OSNotificationPayload = result!.notification.payload
            //_ = SweetAlert().showAlert("Push", subTitle: "Vous etes connecté", style: AlertStyle.success)
            var fullMessage = payload.body
            print("Message = \(fullMessage)")
            
            if payload.additionalData != nil {
                if payload.title != nil {
                    let messageTitle = payload.title
                    print("Message Title = \(messageTitle!)")
                }
                
                let additionalData = payload.additionalData
                if additionalData?["actionSelected"] != nil {
                    fullMessage = fullMessage! + "\nPressed ButtonID: \(additionalData!["actionSelected"])"
                }
            }
        }
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        //var configureError: NSError?
        //GGLContext.sharedInstance().configureWithError(&configureError)
        // assert(configureError == nil, "Error configuring Google services: \(configureError)")
        IQKeyboardManager.sharedManager().enable = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDisconnectedUserUpdateNotification(notification:)), name: NSNotification.Name(rawValue: "userConnection"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.connectedNow(notification:)), name: NSNotification.Name(rawValue: "userConnectNow"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserConnectNow2(notification:)), name: NSNotification.Name(rawValue: "userConnectNow2"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.DisconnectNow(notification:)), name: NSNotification.Name(rawValue: "UserDisonnectNow"), object: nil)
        
        testTocken(launchOptions: launchOptions)
        navigation = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
        MessageController = UIStoryboard(name: "Main", bundle:  nil).instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
        window?.rootViewController = navigation
        if launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] != nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.goToNotfi(launchOptions: launchOptions)
            }
        }
        if UserDefaults.standard.value(forKey: "User") != nil {
            SocketIOManager.sharedInstance.establishConnection()
            
            loadData()
        }
        if UserDefaults.standard.value(forKey: "pres") != nil {
            SocketIOManager.sharedInstance.establishConnection()
            loadData()
        }
        return true
    }
    @objc func DisconnectNow(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            print("Disconnecting")
            guard  let ab = UserDefaults.standard.value(forKey: "User") as? String else{
                
                let ab = UserDefaults.standard.value(forKey: "pres") as! String
                
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                
                var a = JSON(data: dataFromString!)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                print("Connected:",appDelegate.ConnectedUser)
                if typingUsersDictionary[0] != a["user"]["id"].stringValue {
                    var y = -1
                    var x = -1
                    print("typingUsersDictionary[0] :",typingUsersDictionary[0])
                    for b in  appDelegate.ConnectedUser{
                        y = y + 1
                        if b == typingUsersDictionary[0] {
                            x = y
                        }
                    }
                    print("x :",x)
                    if x != -1 {
                        appDelegate.ConnectedUser.remove(at: x)
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadConvKissou"), object: nil)
                }
                return
            }
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            
            var a = JSON(data: dataFromString!)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            print("Connected:",appDelegate.ConnectedUser)
            if typingUsersDictionary[0] != a["user"]["id"].stringValue {
                var y = -1
                var x = -1
                print("typingUsersDictionary[0] :",typingUsersDictionary[0])
                for b in  appDelegate.ConnectedUser{
                    y = y + 1
                    if b == typingUsersDictionary[0] {
                        x = y
                    }
                }
                print("x :",x)
                if x != -1 {
                    appDelegate.ConnectedUser.remove(at: x)
                }
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadConv"), object: nil)
            }
        }
    }
    func goToNotfi(launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        let remoteNotif = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary
        print("saisaisaissisa")
        if remoteNotif != nil {
            let aps = remoteNotif!["aps" as NSString] as? [String:AnyObject]
            NSLog("\n Custom: \(String(describing: aps))")
            
            
            
            let Notif = remoteNotif!["custom"] as? NSDictionary
            let Message = Notif!["a"] as? NSDictionary
            let convId = Message!["convid"] as! String
            let type  = Message!["type"] as! String
            //print(convId)
           // NotifReceive = true
            if convId != "0" {
                if type == "ROLE_USER" {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushMessage"), object: convId as String)
                }else if UserDefaults.standard.value(forKey: "pres") != nil {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushMessagePres"), object: convId as String)
                    
                }
            }else {
                if type == "ROLE_USER" {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushNotif"), object: convId as String)
                }else if UserDefaults.standard.value(forKey: "pres") != nil {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushNotifPres"), object: convId as String)
                    
                }
            }
        }
        else {
            NSLog("//////////////////////////Normal launch")
        }
    }
   
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("pushNotification")
        print("Notif:",userInfo)
        guard let info = userInfo as? [String : AnyObject] else {
            return
        }
        
        guard let aps = info["aps"] as? NSDictionary else {
            return
        }
        guard let Notif = info ["custom"] as? NSDictionary else {
            return
        }
        
        //let Message = Notif!["a"] as? NSDictionary
        guard let NotifFinal = Notif ["a"] as? NSDictionary else {
            return
        }
        guard let convId = NotifFinal["convid"] as? String else {
            return
        }
        guard let type  = NotifFinal["type"] as? String else {
            return
        }
        print("Notif:",Notif)
        if type == "ROLE_USER" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddBadge"), object: nil)
            
        }else{
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddBadg2"), object: nil)
        }
        //presentNotif(data:info)
        switch application.applicationState {
        case .active:
            
            break
        case .inactive:
            print("foreground:",userInfo)
        /*    if convId != "0" && convId != "" {
                if type == "ROLE_USER" {
                    //NotifReceive = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushMessage"), object: convId as String)
                }else {
                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushMessagePres"), object: convId as String)
                }
            }else {
                if type == "ROLE_USER" {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushNotif"), object: convId as String)
                }else {
                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushNotifPres"), object: convId as String)
                }
            } */
            break
        case .background:
            print("background:",userInfo)
           if convId != "0" && convId != "" {
                if type == "ROLE_USER" {
                    //NotifReceive = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushMessage"), object: convId as String)
                }else {
                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushMessagePres"), object: convId as String)
                }
            }else {
                if type == "ROLE_USER" {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushNotif"), object: convId as String)
                }else {
                   NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushNotifPres"), object: convId as String)
                }
            } 
            break
        default:
            break
        }
        //NotifReceive = true
        //if convId != "0" {
        //    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushMessage"), object: convId as String)
        // }else {
        // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushNotif"), object: convId as String)
        //  }
        /* print(convId)
         print(info)
         NotifReceive = true
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushMessage"), object: convId as String) */
        //_ = SweetAlert().showAlert("PushApple", subTitle: "Vous etes connecté", style: AlertStyle.success)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    @objc func handleUserConnectNow2(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            let typing = JSON.parse(typingUsersDictionary[0])
            
            
            
            
            guard  let ab = UserDefaults.standard.value(forKey: "User") as? String else{
                
                
                print("kissou got company ")
                
                
                let ab = UserDefaults.standard.value(forKey: "pres") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                print("Connected:",appDelegate.ConnectedUser)
                if typing["userid"].stringValue != a["user"]["id"].stringValue {
                    var y = -1
                    var  x = "non exist"
                    print("typingUsersDictionary[0] :",typing["userid"].stringValue)
                    for b in  appDelegate.ConnectedUser{
                        y = y + 1
                        if b == typing["userid"].stringValue {
                            x = ""
                        }else {
                            x = "non exist"
                        }
                    }
                    if x == "non exist"{
                        print("x :",x)
                        x = typing["userid"].stringValue
                        appDelegate.ConnectedUser.append(x)
                        print("kissou got company ", x)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadConvKissou"), object: nil)
                        print("Connected:",appDelegate.ConnectedUser)
                    }
                }
                return
            }
            
            
            
            print("setOnline2:",typingUsersDictionary)
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            print("Connected:",appDelegate.ConnectedUser)
            if typing["userid"].stringValue != a["user"]["id"].stringValue {
                var y = -1
                var  x = "non exist"
                print("typingUsersDictionary[0] :",typing["userid"].stringValue)
                for b in  appDelegate.ConnectedUser{
                    y = y + 1
                    if b == typing["userid"].stringValue {
                        x = ""
                    }else {
                        x = "non exist"
                    }
                    
                }
                if x == "non exist"{
                    print("x :",x)
                    x = typing["userid"].stringValue
                    appDelegate.ConnectedUser.append(x)
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadConv"), object: nil)
                    print("Connected:",appDelegate.ConnectedUser)
                }
                
                
            }
        }
        
        
    }
    @objc func connectedNow(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            
            let q = JSON.parse(typingUsersDictionary[0])
            //print(q)
            
            
            
            guard  let ab = UserDefaults.standard.value(forKey: "User") as? String else{
                
                print("kissou got pres")
                
                print("userConnectNow:",typingUsersDictionary)
                let ab = UserDefaults.standard.value(forKey: "pres")  as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                
                print("userConnectNow:",typingUsersDictionary)
                
                SocketIOManager.sharedInstance.SetOnline(room: q["room"].stringValue, UserId: a["user"]["id"].stringValue)
                
                return
            }
            
            
            
            print("room:",q["room"].stringValue)
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            
            
            print("userConnectNow:",typingUsersDictionary)
            
            SocketIOManager.sharedInstance.SetOnline(room: q["room"].stringValue, UserId: a["user"]["id"].stringValue)
        }
    }
    @objc func handleDisconnectedUserUpdateNotification(notification: NSNotification){
        SocketIOManager.sharedInstance.establishConnection()
        loadData()
    }
    func testTocken (launchOptions: [UIApplicationLaunchOptionsKey: Any]?)
    {
        //  LoaderAlert.shared.show(withStatus: "Connecting ...")
        
        if (UserDefaults.standard.value(forKey: "User") != nil && UserDefaults.standard.value(forKey: "ModifOnce") != nil )
        {
            if UserDefaults.standard.value(forKey: "ModifOnce") as! String == "YES" {
                let ab = UserDefaults.standard.value(forKey: "User") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let params : Parameters = [
                    "token" : a["value"].stringValue ,
                    ]
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json"
                    ,"x-Auth-Token" : a["value"].stringValue
                ]
                Alamofire.request(ScriptBase.sharedInstance.testToken , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        //      LoaderAlert.shared.dismiss()
                        
                        let b = JSON(response.data!)
                        print("testToken:",b)
                        if b["status"].boolValue {
                            UserDefaults.standard.setValue(b["Blocked"].rawString(), forKey: "UserBlocked")
                            UserDefaults.standard.synchronize()
                            if b["data"]["user"]["langue"].stringValue == "English" || b["data"]["user"]["langue"].stringValue == "Anglais" || b["data"]["user"]["langue"].stringValue == "English_en" {
                                
                                SetLanguage(self.arrayLanguages[1])
                            }else if b["data"]["user"]["langue"].stringValue == "French" || b["data"]["user"]["langue"].stringValue == "Français" || b["data"]["user"]["langue"].stringValue == "French_fr" {
                                SetLanguage(self.arrayLanguages[2])
                            }
                            Localisator.sharedInstance.saveInUserDefaults = true
                            print("launchOptions: ", launchOptions)
                            if launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] != nil {
                                print("**********push****************")
                                self.goToNotfi(launchOptions: launchOptions)
                                
                            }
                            let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController")
                            DispatchQueue.main.async(execute: {
                                // self.navigation.pushViewController(a, animated: true)
                                self.window?.rootViewController = view
                                
                            })
                            
                            
                        }
                        //  else {
                        //      _ = SweetAlert().showAlert("Conexion", subTitle: "Veuillez vous connecter", style: AlertStyle.error)
                        //}
                }
            }
        }
        else if (UserDefaults.standard.value(forKey: "pres") != nil )
        {
            let ab = UserDefaults.standard.value(forKey: "pres") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let params : Parameters = [
                "token" : a["value"].stringValue ,
                ]
            let header: HTTPHeaders = [
                "Content-Type" : "application/json"
                ,"x-Auth-Token" : a["value"].stringValue
            ]
            Alamofire.request(ScriptBase.sharedInstance.testToken , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    //      LoaderAlert.shared.dismiss()
                    
                    let b = JSON(response.data!)
                    
                    if b["status"].boolValue {
                        
                        if b["data"]["user"]["langue"].stringValue == "English" || b["data"]["user"]["langue"].stringValue == "Anglais" || b["data"]["user"]["langue"].stringValue == "English_en" {
                            
                            SetLanguage(self.arrayLanguages[1])
                        }else if b["data"]["user"]["langue"].stringValue == "French" || b["data"]["user"]["langue"].stringValue == "Français" || b["data"]["user"]["langue"].stringValue == "French_fr" {
                            SetLanguage(self.arrayLanguages[2])
                        }
                        Localisator.sharedInstance.saveInUserDefaults = true
                        let view = UIStoryboard(name: "Prestataire", bundle: nil).instantiateViewController(withIdentifier: "HomePrest")
                        DispatchQueue.main.async(execute: {
                            // self.navigation.pushViewController(a, animated: true)
                            self.window?.rootViewController = view
                            if launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] != nil {
                                print("**********push****************")
                                self.goToNotfi(launchOptions: launchOptions)
                                
                            }
                            //_ = SweetAlert().showAlert("Connexion", subTitle: "Vous etes connecté", style: AlertStyle.success)
                        })
                        
                        
                        
                    }
                    //  else {
                    //      _ = SweetAlert().showAlert("Conexion", subTitle: "Veuillez vous connecter", style: AlertStyle.error)
                    //}
            }
        }
    }
    func quitMessanger(){
        SectionCritique = 0
        if (UserDefaults.standard.value(forKey: "User") != nil ) {
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : a["value"].stringValue
            ]
            Alamofire.request(ScriptBase.sharedInstance.afficherConversation , method: .get, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    
                    let b = JSON(response.data)
                    
                    let c = JSON(b["data"].arrayObject)
                    if c.count != 0 {
                        
                        
                        print("count:",c.count)
                        
                        
                        for  i in 0...c.count - 1 {
                            SocketIOManager.sharedInstance.unscubscribe(Conv: c[i]["id"].stringValue, nickname: a["user"]["id"].stringValue)
                        }
                        
                        
                        
                    }
                    
                    
            }
        }
        else if ( UserDefaults.standard.value(forKey: "pres") != nil)
        {
            let ab = UserDefaults.standard.value(forKey: "pres") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : a["value"].stringValue
            ]
            Alamofire.request(ScriptBase.sharedInstance.afficherConversation , method: .get, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    
                    let b = JSON(response.data)
                    print("b:",b)
                    let c = JSON(b["data"].arrayObject)
                    if c.count != 0 {
                        
                        
                        print("count:",c.count)
                        
                        
                        for  i in 0...c.count - 1 {
                            SocketIOManager.sharedInstance.unscubscribe(Conv: c[i]["id"].stringValue, nickname: a["user"]["id"].stringValue)
                        }
                        
                        
                        
                    }
                    
                    
            }
        }
        
    }
    func loadData(){
        print("SectionCritique: ",SectionCritique)
        if SectionCritique == 0 {
            if (UserDefaults.standard.value(forKey: "User") != nil ) || (UserDefaults.standard.value(forKey: "pres") != nil) {
                SectionCritique = 1
            }
            if (UserDefaults.standard.value(forKey: "User") != nil ) {
                let ab = UserDefaults.standard.value(forKey: "User") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : a["value"].stringValue
                ]
                Alamofire.request(ScriptBase.sharedInstance.afficherConversation , method: .get, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        
                        let b = JSON(response.data)
                        print("b:",b)
                        let c = JSON(b["data"].arrayObject)
                        if c.count != 0 {
                            self.ConversatioNRoom = c
                            
                            print("count:",c.count)
                            SocketIOManager.sharedInstance.connectUsername(id: a["user"]["id"].stringValue, completionHandler: { (x) in
                                var first = true
                                for  i in 0...c.count - 1 {
                                    if i == 0 {
                                        first = true
                                    } else {
                                        first = false
                                    }
                                    SocketIOManager.sharedInstance.connectToServerWithNickname(nickname: c[i]["id"].stringValue, userid: a["user"]["id"].stringValue, first: first)
                                }
                            })
                            
                            
                        }
                        
                        
                }
            }
            else if (UserDefaults.standard.value(forKey: "pres") != nil )
            {
                
                print("kissou subscribing.....")
                let ab = UserDefaults.standard.value(forKey: "pres") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : a["value"].stringValue
                ]
                Alamofire.request(ScriptBase.sharedInstance.afficherConversation , method: .get, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        
                        let b = JSON(response.data)
                        print("b:",b)
                        let c = JSON(b["data"].arrayObject)
                        if c.count != 0 {
                            print("count:",c.count)
                            SocketIOManager.sharedInstance.connectUsername(id: a["user"]["id"].stringValue, completionHandler: { (x) in
                                var first = true
                                for  i in 0...c.count - 1 {
                                    if i == 0 {
                                        first = true
                                    } else {
                                        first = false
                                    }
                                    print("kissou subscribing.....",c[i]["id"].stringValue)
                                    SocketIOManager.sharedInstance.connectToServerWithNickname(nickname: c[i]["id"].stringValue, userid: a["user"]["id"].stringValue, first: first)
                                }
                            })
                        }
                        
                }
            }
        }
    }
    func testTocken2 ()
    {
        //  LoaderAlert.shared.show(withStatus: "Connecting ...")
        navigation = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
        let navx = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PageBienvenue") as! PageBienvenue
        if window?.rootViewController != navx {
            if (UserDefaults.standard.value(forKey: "User") != nil )
            {
                let ab = UserDefaults.standard.value(forKey: "User") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let params : Parameters = [
                    "token" : a["value"].stringValue ,
                    ]
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json"
                    ,"x-Auth-Token" : a["value"].stringValue
                ]
                Alamofire.request(ScriptBase.sharedInstance.testToken , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        //      LoaderAlert.shared.dismiss()
                        
                        let b = JSON(response.data!)
                        print(b)
                        if b["status"].boolValue == false {
                            
                            _ = SweetAlert().showAlert("GogoSki", subTitle: Localization("reconnect"), style: AlertStyle.error,buttonTitle: "OK", action: { (otherButton) in
                                StaticPositionController.shared.StopLocation()
                                SocketIOManager.sharedInstance.closeConnection()
                                StaticPositionController.shared.UsersPos.removeAll()
                                UserDefaults.standard.removeObject(forKey: "User")
                                UserDefaults.standard.synchronize()
                                self.navigation = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
                                self.window?.rootViewController = self.navigation
                            })
                            
                            
                            
                            //  _ = SweetAlert().showAlert("Connexion", subTitle: "Vous etes connecté", style: AlertStyle.success)
                            //  let a = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController")
                            
                            //  self.navigation.pushViewController(a, animated: true)
                            
                        }
                        //  else {
                        //      _ = SweetAlert().showAlert("Conexion", subTitle: "Veuillez vous connecter", style: AlertStyle.error)
                        //}
                }
            }
            else if (UserDefaults.standard.value(forKey: "pres") != nil )
            {
                let ab = UserDefaults.standard.value(forKey: "pres") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let params : Parameters = [
                    "token" : a["value"].stringValue ,
                    ]
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json"
                    ,"x-Auth-Token" : a["value"].stringValue
                ]
                Alamofire.request(ScriptBase.sharedInstance.testToken , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        //      LoaderAlert.shared.dismiss()
                        
                        let b = JSON(response.data!)
                        print(b)
                        if b["status"].boolValue == false {
                            _ = SweetAlert().showAlert("GogoSki", subTitle: "Veuillez vous Reconnecter", style: AlertStyle.error,buttonTitle: "OK", action: { (otherButton) in
                                UserDefaults.standard.removeObject(forKey: "pres")
                                UserDefaults.standard.synchronize()
                                self.navigation = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
                                self.window?.rootViewController = self.navigation
                            })
                            
                            
                            
                            //  _ = SweetAlert().showAlert("Connexion", subTitle: "Vous etes connecté", style: AlertStyle.success)
                            //  let a = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController")
                            
                            //  self.navigation.pushViewController(a, animated: true)
                            
                        }
                        //  else {
                        //      _ = SweetAlert().showAlert("Conexion", subTitle: "Veuillez vous connecter", style: AlertStyle.error)
                        //}
                }
            }
        }
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if LinkedinSwiftHelper.shouldHandle(url) {
            print("yes LinkedIn")
            DontTest = true
            return LinkedinSwiftHelper.application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        } else if FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)  {
            return true
        }else if GIDSignIn.sharedInstance().handle(url,sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,annotation: options[UIApplicationOpenURLOptionsKey.annotation]){
            return true
        }else{
            return false
        }
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        //SocketIOManager.sharedInstance.establishConnection()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        // quitMessanger()
        //  SocketIOManager.sharedInstance.closeConnection()
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadScrollProfilMain"), object: nil)
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("DontTest Foreground:",DontTest)
        if UserDefaults.standard.value(forKey: "User") != nil ||  UserDefaults.standard.value(forKey: "pres") != nil {
            testTocken2()
        }
        // loadData()
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //SocketIOManager.sharedInstance.establishConnection()
        
        //loadData()
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "GogoSki")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

