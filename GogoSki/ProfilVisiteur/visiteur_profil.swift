//
//  visiteur_profil.swift
//  GogoSki
//
//  Created by Bouzid saif on 19/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class visiteur_profil : InternetCheckViewControllers , UITableViewDelegate,UITableViewDataSource{
    
    /*
     *  about hight view
     */
    var fromWhereYouCome = ""
    /*
     *  about hight view
     */
    @IBOutlet weak var height_apropos: NSLayoutConstraint!
    /*
     * friends button
     */
    @IBOutlet weak var btn_amisss: UIButton!
    /*
     * icon button friends
     */
    @IBOutlet weak var btn_amis: UIButton!
    /*
     * icon button family
     */
    @IBOutlet weak var btn_tribuuu: UIButton!
    /*
     * button family
     */
    @IBOutlet weak var btn_tribu: UIButton!
    /*
     * button blacklist
     */
    @IBOutlet weak var btn_blacklist: UIButton!
    /*
     * hight constraint top of about description
     */
    @IBOutlet weak var height_top_textview: NSLayoutConstraint!
    /*
     * hight constraint bottom of about description
     */
    @IBOutlet weak var height_bot_textview: NSLayoutConstraint!
    /*
     * about description view
     */
    @IBOutlet weak var height_view_apropos: UIView!
    /*
     * about label
     */
    @IBOutlet weak var labelPropos: UILabel!
    /*
     * sport label
     */
    @IBOutlet weak var sportUser: UIButton!
    /*
     * button message
     */
    @IBOutlet weak var btnMsg: UIButton!
    /*
     * popup view button (plus)
     */
    @IBOutlet weak var btnPopup: UIButton!
    /*
     * button appointement (invite the user )
     */
    @IBOutlet weak var btnRdv: UIButton!
    /*
     * sport icon user
     */
    @IBOutlet weak var sportPicture: UIImageView!
    /*
     * user name
     */
    @IBOutlet weak var name: UILabel!
    /*
     * profil picture
     */
    @IBOutlet weak var profil_picture: RoundedUIImageView!
    /*
     * about discription
     */
    @IBOutlet weak var disc: UILabel!
    /*
     * user sport label
     */
    @IBOutlet weak var labelSports: UILabel!
    /*
     * popup view
     */
    @IBOutlet weak var popup: UIView!
    /*
     * Sport label width
     */
    @IBOutlet weak var SportUserContWidth: NSLayoutConstraint!
    /*
     * array for levels in french
     */
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     * array for levels in English
     */
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     * array for sports in French
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     * array for sports in English
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     * id user
     */
    var id : String = ""
    /*
     * variable of user's sports
     */
    var sports : JSON = []
    /*
     * variable of all station
     */
    var stations : JSON = []
    /*
     * navigation controller of this controller
     */
    var parentNavigationController : UINavigationController?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (sports.arrayObject?.count)!
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            (cell.viewWithTag(1) as! UILabel).text = self.sports[indexPath.row]["pratique"].stringValue
            (cell.viewWithTag(2) as! UILabel).text = "Niveau " + self.sports[indexPath.row]["niveau"].stringValue
        }else {
            switch self.sports[indexPath.row]["pratique"].stringValue {
            case SportSaifFR[0] :
                
                (cell.viewWithTag(1) as! UILabel).text = SportSaifEN[0]
            case SportSaifFR[1] :
                (cell.viewWithTag(1) as! UILabel).text = SportSaifEN[1]
                
            case SportSaifFR[2] :
                (cell.viewWithTag(1) as! UILabel).text = SportSaifEN[2]
                
            case SportSaifFR[3] :
                (cell.viewWithTag(1) as! UILabel).text = SportSaifEN[3]
                
            case SportSaifFR[4] :
                (cell.viewWithTag(1) as! UILabel).text = SportSaifEN[4]
            case SportSaifFR[5] :
                (cell.viewWithTag(1) as! UILabel).text = SportSaifEN[5]
                
            default :
                
                (cell.viewWithTag(1) as! UILabel).text = ""
                
            }
            switch self.sports[indexPath.row]["niveau"].stringValue {
            case NiveauSaifFR[0] :
                
                (cell.viewWithTag(2) as! UILabel).text = "Level " + NiveauSaifEN[0]
            case NiveauSaifFR[1] :
                (cell.viewWithTag(2) as! UILabel).text = "Level " + NiveauSaifEN[1]
                
            case NiveauSaifFR[2] :
                (cell.viewWithTag(2) as! UILabel).text = "Level " + NiveauSaifEN[2]
                
            case NiveauSaifFR[3] :
                (cell.viewWithTag(2) as! UILabel).text = "Level " + NiveauSaifEN[3]
            case NiveauSaifFR[4] :
                (cell.viewWithTag(2) as! UILabel).text = "Level " + NiveauSaifEN[4]
                
                
            default :
                
                (cell.viewWithTag(2) as! UILabel).text = ""
                
            }
            
        }
        
        
        switch self.sports[indexPath.row]["pratique"].stringValue {
        case "Snowboard":
            (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "Snowboard-kissou")
            
        case "Ski Alpin":
            (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "ski-alpin-kissou")
            
        case "Ski de fond" :
            (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "Ski-fond-kissou")
            
        case "Handiski" :
            (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "Handi-ski-kissou")
            
        case "Raquettes" :
            (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "Raquettes-kissou")
            
        case "Ski de randonnée" :
            (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "Randonnée-ski-kissou")
            
        default:
            (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "Snow débutant_saif")
        }
        
        if self.sports[indexPath.row]["niveau"].stringValue == "Débutant"
        {
            (cell.viewWithTag(2) as! UILabel).textColor = UIColor("#D3D3D3")
            (cell.viewWithTag(4) as! UIImageView).tintColor = UIColor("#D3D3D3")
        }
        else if self.sports[indexPath.row]["niveau"].stringValue == "Intermédiaire"
        {
            (cell.viewWithTag(2) as! UILabel).textColor = UIColor("#75A7FF")
            (cell.viewWithTag(4) as! UIImageView).tintColor = UIColor("#75A7FF")
        }
        else if self.sports[indexPath.row]["niveau"].stringValue == "Confirmé"
        {
            (cell.viewWithTag(2) as! UILabel).textColor = UIColor("#FF3E53")
            (cell.viewWithTag(4) as! UIImageView).tintColor = UIColor("#FF3E53")
        }
        else if self.sports[indexPath.row]["niveau"].stringValue == "Expert"
        {
            (cell.viewWithTag(2) as! UILabel).textColor = UIColor("#4D4D4D")
            (cell.viewWithTag(4) as! UIImageView).tintColor = UIColor("#4D4D4D")
        }
        else
        {
            (cell.viewWithTag(2) as! UILabel).textColor = UIColor("#76EC9E")
            (cell.viewWithTag(4) as! UIImageView).tintColor = UIColor("#76EC9E")
        }
        //  (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "Snow débutant_saif")
        
        
        
        return cell
    }
    
    /*
     * user's sports tableview
     */
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.disc.text = ""
        // self.view.frame.width
        /*  if #available(iOS 11.0, *) {
         myScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
         } */
        btnMsg.isEnabled = false
        btnPopup.isEnabled = false
        btnRdv.isEnabled = false
        LoadAllStation()
        print("hi3")
        popup.layer.cornerRadius = 10
        
        tableview.delegate = self
        tableview.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        labelPropos.text = Localization("labelPropos")
        labelSports.text = Localization("labelSport")
        btn_blacklist.setTitle(Localization("btn_black"), for: .normal)
        //Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getProfil()
    }
    /*
     * add user to blacklist action
     */
    @IBAction func addToBlackList(_ sender: Any) {
        let params: Parameters = [
            "idUserNoire": self.id
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.addListNoire , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                
                let b = JSON(response.data)
                print ("add blacklist "  , b)
                if b["status"].boolValue {
                    
                    _ = SweetAlert().showAlert(Localization("addBlackList"), subTitle: Localization("addBlackList2"), style: AlertStyle.success)
                    let tabBarViewControllers = self.tabBarController?.viewControllers
                    if self.fromWhereYouCome == "" {
                    let adController = tabBarViewControllers![0] as! UINavigationController
                    
                    let adminTVC = adController.viewControllers[0] as! SocialViewController
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddToBlackList"), object:
                        ["\(self.id)"])
                    
                    _ = adminTVC.navigationController?.popToRootViewController(animated: false)
                    adminTVC.selectPage()
                    }else{
                        let adController = tabBarViewControllers![3] as! UINavigationController
                        
                        let adminTVC = adController.viewControllers[0] as! MessageNotificationSection
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddToBlackList"), object:
                            ["\(self.id)"])
                        
                        _ = adminTVC.navigationController?.popToRootViewController(animated: false)
                        //adminTVC.selectPage()
                    }
                    
                    
                }else{
                    
                    _ = SweetAlert().showAlert(Localization("addBlackList"), subTitle: Localization("erreur"), style: AlertStyle.error)
                    
                }
                
                
        }
    }
    /*
     * back action
     *
     */
      @IBAction func back(_ sender: Any) {
    self.parentNavigationController?.popToRootViewController(animated: true)
    }
    /*
     * add user to favorite action
     */
    @IBAction func addToFavorites(_ sender: Any) {
        var supp = ""
        var c = ""
        self.popup.isHidden = true
        if (sender as! UIButton).tag == 50 {
            c = "a"
            supp = Localization("suppAmis")
        }else {
            c = "f"
            supp = Localization("suppTribu")
        }
        if ( supp == (sender as! UIButton).title(for: .normal) )
        {
            let params: Parameters = [
                "idUserFav": self.id
            ]
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : a["value"].stringValue
            ]
            Alamofire.request(ScriptBase.sharedInstance.removeFavoris , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    
                    let b = JSON(response.data)
                    print ("supp favoris : " , b)
                    if b["status"].boolValue {
                        
                        _ = SweetAlert().showAlert(Localization("removeFavoris"), subTitle: Localization("removeFavoris2"), style: AlertStyle.success)
                        self.btn_tribuuu.isEnabled = true
                        self.btn_tribu.isEnabled = true
                        self.btn_amisss.isEnabled = true
                        self.btn_amis.isEnabled = true
                        self.btn_amisss.setTitle(Localization("btn_amiss"), for: .normal)
                        self.btn_tribu.setTitle(Localization("btn_tribu"), for: .normal)
                        self.btnPopup.setBackgroundImage(UIImage(named : "btn_rate"), for: .normal)
                        
                    }else{
                        
                        _ = SweetAlert().showAlert(Localization("removeFavoris"), subTitle: Localization("erreur"), style: AlertStyle.error)
                        
                    }
                    
                    
            }
        }
        else {
            
            let params: Parameters = [
                "idUserFav": self.id ,
                "type" : c
            ]
            print("hedhi el amis/famille:",params.description)
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : a["value"].stringValue
            ]
            Alamofire.request(ScriptBase.sharedInstance.addFavoris , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    
                    let b = JSON(response.data)
                    print ("add Favoris "  , b)
                    if b["status"].boolValue {
                        if c == "a"
                        {
                            self.btnPopup.setBackgroundImage(UIImage(named : "favoris_saifsaif"), for: .normal)
                            self.btn_tribu.isEnabled = false
                            self.btn_tribuuu.isEnabled = false
                            self.btn_amisss.setTitle(Localization("suppAmis"), for: .normal)
                        }
                        else {
                            self.btnPopup.setBackgroundImage(UIImage(named : "coeur_saif"), for: .normal)
                            self.btn_amisss.isEnabled = false
                            self.btn_amis.isEnabled = false
                            self.btn_tribu.setTitle(Localization("suppTribu"), for: .normal)
                        }
                        if ( b["message"].stringValue == "Already friend")
                        {
                            _ = SweetAlert().showAlert(Localization("addFavorite"), subTitle: Localization("alreadyExist"), style: AlertStyle.error)
                        }
                        else {
                            _ = SweetAlert().showAlert(Localization("addFavorite"), subTitle: Localization("addFavorite2"), style: AlertStyle.success)
                            let ab = UserDefaults.standard.value(forKey: "User") as! String
                            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                            var a = JSON(data: dataFromString!)
                            let header: HTTPHeaders = [
                                "Content-Type" : "application/json",
                                "x-Auth-Token" : a["value"].stringValue
                            ]
                            
                            let params: Parameters = [
                                "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " vous a ajouté a ses favoris" ,
                                "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " added you to his favorites" ,
                                "userids" : ["\(self.id)"] ,
                                "type" : "ajout" ,
                                "titre" : "Invitation" ,
                                "photo" : a["user"]["photo"].stringValue,
                                "activite" : "" ,
                                "rdv" : "" ,
                                "sender" : a["user"]["id"].stringValue
                            ]
                            Alamofire.request(ScriptBase.sharedInstance.creeNotif , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                                .responseJSON { response in
                                    print (response)
                            }
                        }
                        
                    }else{
                        
                        _ = SweetAlert().showAlert(Localization("addFavorite"), subTitle: Localization("erreur"), style: AlertStyle.error)
                        
                    }
                    
                    
            }
        }
    }
    
    /*
     * hide and show popup
     */
    @IBAction func hide_show(_ sender: Any) {
        if self.popup.isHidden == true {
            self.popup.isHidden = false
        }else{
            self.popup.isHidden = true
            
        }
    }
    ///***
    /*
     * get profil user
     */
    func getProfil ()
    {
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        
        print (self.id)
        Alamofire.request(ScriptBase.sharedInstance.afficherUtilisateur + self.id , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                
                
                let a = JSON(response.data)
                print ("user : " ,a)
                
                if a["status"].boolValue == false {
                    
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        _ = SweetAlert().showAlert("Profile visiteur", subTitle: "Une erreur est survenue", style: AlertStyle.error)
                    }else {
                        _ = SweetAlert().showAlert("Profil users", subTitle: "An error has occured", style: AlertStyle.error)
                    }
                    
                    
                }else{
                    print(a)
                    self.btnMsg.isEnabled = true
                    self.btnPopup.isEnabled = true
                    self.btn_amisss.setTitle(Localization("btn_amiss"), for: .normal)
                    self.btn_tribu.setTitle(Localization("btn_tribu"), for: .normal)
                    self.btn_blacklist.setTitle(Localization("btn_black"), for: .normal)
                    if (a["message"].stringValue == "User BlackListed" )
                    {
                        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                            _ = SweetAlert().showAlert("Profile visiteur", subTitle: "Ce profile est bloqué", style: AlertStyle.error)
                            self.navigationController?.popViewController(animated: true)
                            
                        }else {
                            _ = SweetAlert().showAlert("Profil users", subTitle: "This profile is blocked", style: AlertStyle.error)
                        }
                        self.navigationController?.popViewController(animated: true)
                    }
                    else {
                        
                        
                        self.name.text = a["data"]["user"]["prenom"].stringValue + " " + a["data"]["user"]["nom"].stringValue
                        if (a["data"]["user"]["sports"].arrayObject?.count != 0)
                        {
                            for i in 0 ... ((a["data"]["user"]["sports"].arrayObject?.count)! - 1)
                            {
                                if (a["data"]["user"]["sports"][i]["is_default"].boolValue)
                                {
                                    self.sportUser.setTitle(a["data"]["user"]["sports"][i]["pratique"].stringValue, for: .normal)
                                    switch a["data"]["user"]["sports"][i]["pratique"].stringValue {
                                    case "Snowboard":
                                        //   self.sportPicture.image = UIImage(named: "Snowboard-kissou")
                                        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                                            self.sportUser.setTitle("Snowboard", for: .normal)
                                        }else {
                                            self.sportUser.setTitle("Snowboard", for: .normal)
                                        }
                                    case "Ski Alpin":
                                        // self.sportPicture.image = UIImage(named: "ski-alpin-kissou")
                                        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                                            self.sportUser.setTitle("Ski Alpin", for: .normal)
                                        }else {
                                            self.sportUser.setTitle("Alpine skiing", for: .normal)
                                        }
                                    case "Ski de fond" :
                                        // self.sportPicture.image = UIImage(named: "Ski-fond-kissou")
                                        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                                            self.sportUser.setTitle("Ski de fond", for: .normal)
                                        }else {
                                            self.sportUser.setTitle("Cross-country skiing", for: .normal)
                                        }
                                    case "Handiski" :
                                        //    self.sportPicture.image = UIImage(named: "Handi-ski-kissou")
                                        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                                            self.sportUser.setTitle("Handiski", for: .normal)
                                        }else {
                                            self.sportUser.setTitle("Handiski", for: .normal)
                                        }
                                    case "Raquette" :
                                        //  self.sportPicture.image = UIImage(named: "Raquettes-kissou")
                                        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                                            self.sportUser.setTitle("Raquette", for: .normal)
                                        }else{
                                            self.sportUser.setTitle("Racket", for: .normal)
                                        }
                                    case "Ski de randonnée" :
                                        //  self.sportPicture.image = UIImage(named: "Randonnée-ski-kissou")
                                        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                                            self.sportUser.setTitle("Ski de randonnée", for: .normal)
                                        } else {
                                            self.sportUser.setTitle("Nordic skiing", for: .normal)
                                        }
                                    default:
                                        //     self.sportPicture.image = UIImage(named: "Snow débutant_saif")
                                        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                                            self.sportUser.setTitle("Snowboard", for: .normal)
                                        }else {
                                            self.sportUser.setTitle("Snowboard", for: .normal)
                                        }
                                    }
                                    // self.sportUser.titleLabel?.minimumScaleFactor = 0.5
                                    // self.sportUser.titleLabel?.numberOfLines = 1
                                    
                                    if ((self.sportUser.title(for: .normal)?.count)! > 19)
                                    {
                                        self.SportUserContWidth.constant = 200
                                    }else if ((self.sportUser.title(for: .normal)?.count)! > 15)
                                    {
                                        self.SportUserContWidth.constant = 150
                                    }else if ((self.sportUser.title(for: .normal)?.count)! > 10) {
                                        self.SportUserContWidth.constant = 120
                                    }
                                    else {
                                        self.SportUserContWidth.constant = 100
                                    }
                                    //SportUserContWidth.constant = evaluateStringWidth(textToEvaluate: (self.sportUser.titleLabel?.text)!)
                                    // self.sportUser.updateConstraintsIfNeeded()
                                    //let frame = CGSize(width: evaluateStringWidth(textToEvaluate: (self.sportUser.titleLabel?.text)!), height: self.sportUser.frame.size.height)
                                    //print("******Frame****** ",frame.width)
                                    //  self.sportUser.frame.size = frame
                                    //  self.sportUser.updateConstraintsIfNeeded()
                                    
                                    
                                    if a["data"]["user"]["sports"][i]["niveau"].stringValue == "Débutant" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Snowboard"
                                    {
                                        self.sportPicture.image = UIImage(named: "Snowboard Débutant")
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Intermédiaire" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Snowboard"
                                    {
                                        self.sportPicture.image = UIImage(named: "Snowboard Intermédiaire")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Confirmé" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Snowboard"
                                    {
                                        self.sportPicture.image = UIImage(named: "Snowboard Confirmé")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Expert" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Snowboard"
                                    {
                                        self.sportPicture.image = UIImage(named: "Snowboard Expert")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Débrouillard" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Snowboard"
                                    {
                                        self.sportPicture.image = UIImage(named: "Snowboard Débrouillard")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Débutant" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski Alpin"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski Alpin Débutant")
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Intermédiaire" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski Alpin"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski Alpin Intermédiaire")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Confirmé" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski Alpin"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski Alpin Confirmé")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Expert" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski Alpin"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski Alpin Expert")
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Débrouillard" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski Alpin"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski Alpin Débrouillard")
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Débutant" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski de fond"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski de fond Débutant")
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Intermédiaire" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski de fond"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski de fond Intermédiaire")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Confirmé" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski de fond"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski de fond Confirmé")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Expert" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski de fond"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski de fond Expert")
                                        
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Débrouillard" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski de fond"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski de fond Débrouillard")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Débutant" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Handiski"
                                    {
                                        self.sportPicture.image = UIImage(named: "Handiski Débutant")
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Intermédiaire" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Handiski"
                                    {
                                        self.sportPicture.image = UIImage(named: "Handiski intermédiaire")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Confirmé" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Handiski"
                                    {
                                        self.sportPicture.image = UIImage(named: "Handiski Confirmé")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Expert" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Handiski"
                                    {
                                        self.sportPicture.image = UIImage(named: "Handiski Expert")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Débrouillard" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Handiski"
                                    {
                                        self.sportPicture.image = UIImage(named: "Handiski Débrouillard")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Débutant" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Raquette"
                                    {
                                        self.sportPicture.image = UIImage(named: "Raquettes Débutant")
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Intermédiaire" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Raquette"
                                    {
                                        self.sportPicture.image = UIImage(named: "Raquettes Intermédiaire")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Confirmé" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Raquette"
                                    {
                                        self.sportPicture.image = UIImage(named: "Raquettes Confirmé")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Expert" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Raquette"
                                    {
                                        self.sportPicture.image = UIImage(named: "Raquettes Expert")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Débrouillard" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Raquette"
                                    {
                                        self.sportPicture.image = UIImage(named: "Raquettes  Débrouillard")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Débutant" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski de randonnée"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski de randonnée Débutant")
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Intermédiaire" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski de randonnée"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski de randonnée Intermédiaire")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Confirmé" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski de randonnée"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski de randonnée Confirmé")
                                        
                                    }
                                    else if  a["data"]["user"]["sports"][i]["niveau"].stringValue == "Expert" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski de randonnée"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski de randonnée Expert")
                                        
                                    }
                                    else if a["data"]["user"]["sports"][i]["niveau"].stringValue == "Débrouillard" && a["data"]["user"]["sports"][i]["pratique"].stringValue == "Ski de randonnée"
                                    {
                                        self.sportPicture.image = UIImage(named: "Ski de randonnée Débrouillard")
                                        
                                    }
                                }
                            }
                        }
                        
                        if let urlImgUser = URL(string : a["data"]["user"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
                            self.profil_picture.setImage(withUrl: urlImgUser , placeholder: placeholder)
                        }
                        print("ss:",a["data"]["user"]["description"].stringValue,"saif")
                        if a["data"]["user"]["description"].stringValue !=  "" && a["data"]["user"]["description"].stringValue != "Décrivez-vous en quelques lignes." && a["data"]["user"]["description"].stringValue != "Describe yourself in a few lines."  {
                            self.disc.text = a["data"]["user"]["description"].stringValue
                            self.height_top_textview.constant = 19.0
                            self.height_bot_textview.constant = 35.0
                            self.height_apropos.constant = 63.0
                        }else {
                            self.height_top_textview.constant = -1
                            self.height_bot_textview.constant = -1
                            self.disc.text = ""
                            self.height_apropos.constant = -1
                            self.height_view_apropos.isHidden = true
                        }
                        
                        self.sports = a["data"]["user"]["sports"]
                        self.tableview.reloadData()
                        if (a["data"]["rel"].stringValue == "Nada" )
                        {
                            self.btnPopup.setBackgroundImage(UIImage(named : "btn_rate"), for: .normal)
                            self.btn_amisss.setTitle(Localization("btn_amiss"), for: .normal)
                            self.btn_tribu.setTitle(Localization("btn_tribu"), for: .normal)
                        }
                        else if (a["data"]["rel"].stringValue == "Amis" ) {
                            self.btnPopup.setBackgroundImage(UIImage(named : "favoris_saifsaif"), for: .normal)
                            self.btn_amisss.setTitle(Localization("suppAmis"), for: .normal)
                            self.btn_tribu.isEnabled = false
                            self.btn_tribuuu.isEnabled = false
                        }
                        else {
                            self.btnPopup.setBackgroundImage(UIImage(named : "coeur_saif"), for: .normal)
                            self.btn_tribu.setTitle(Localization("suppTribu"), for: .normal)
                            self.btn_amisss.isEnabled = false
                            self.btn_amis.isEnabled = false
                        }
                    }
                }
                
                
        }
    }
    /*
     * go to user conversation action
     */
    @IBAction func go_toMessage(_ sender: Any) {
        LoaderAlert.show()
        let params: Parameters = [
            "amiId": self.id
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.addConvPriv , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                print ("add conv "  , b)
                if b["status"].boolValue {
                    
                    let ConvId = b["data"]["id"].stringValue
                    SocketIOManager.sharedInstance.connectToServerWithNickname(nickname: ConvId, userid: a["user"]["id"].stringValue, first: false)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToMessageForomNoWhere2"), object: ConvId )
                }
                
                
        }
        
    }
    /*
     * invite user to appointement
     */
    @IBAction func invitRdv(_ sender: Any) {
        let view = self.storyboard?.instantiateViewController(withIdentifier: "InviterRdvViewController") as! InviterRdvViewController
        view.idUser = self.id
        view.stations = stations
        self.navigationController?.pushViewController(view, animated: true)
    }
    /*
     * get all station
     */
    func LoadAllStation ()
    {
        LoaderAlert.show()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.Stations , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                let b = JSON(response.data)
                
                if (b["status"].boolValue)
                {
                    
                    self.btnRdv.isEnabled = true
                    self.stations = b["data"]
                }
                
                
        }
    }
}

