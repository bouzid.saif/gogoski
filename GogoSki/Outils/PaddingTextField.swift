//
//  PaddingTextField.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 22/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//
import Foundation
import UIKit

class PaddingTextField: UITextField {
    @IBInspectable
    public var naissance : Bool = false
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        if naissance {
       return CGRect(x: 15 , y: 15, width: 20, height: 20)
        }
        else {
            return CGRect(x: 0 , y: 0, width: 0, height: 0)
        }
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        if naissance == false {
        return UIEdgeInsetsInsetRect(bounds,
                                     UIEdgeInsetsMake(0, 15, 0, 15))
        }else{
            return UIEdgeInsetsInsetRect(bounds,
                                         UIEdgeInsetsMake(0, 45, 0, 45))
        }
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        if naissance == false {
        return UIEdgeInsetsInsetRect(bounds,
                                     UIEdgeInsetsMake(0, 15, 0, 15))
        }else{
            return UIEdgeInsetsInsetRect(bounds,
                                         UIEdgeInsetsMake(0, 45, 0, 45))
        }
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        if naissance == false {
            return UIEdgeInsetsInsetRect(bounds,
                                         UIEdgeInsetsMake(0, 15, 0, 15))
        }else{
            return UIEdgeInsetsInsetRect(bounds,
                                         UIEdgeInsetsMake(0, 45, 0, 45))
        }
    }
}
