//
//  ScriptBase.swift
//  RoadTwo7
//
//  Created by seifeddine bouzid on 11/27/16.
//  Copyright © 2016 EspritMobile. All rights reserved.
//

import Foundation
class ScriptBase {
    static var sharedInstance = ScriptBase()
    private init(){}
    //Serveur
   static let  server = "http://www.gogoski.fr"
    
    /**
     * Méthode:POST
     * request:{email,password,langue}
     */
   let InscriptionUser = server + "/api/user/inscription"
    
    /**
     * Méthode : Post
     * Request: {email,password}
     */
   let ConnexionUser = server + "/api/user/connexion"
    
    /**
     * Méthode : Post
     * request :{photo,nom,prenom,description,dateNaissance,Sport[{niveau,pratique}...]
     */
   let ModifierProfil = server + "/api/user/ModifierProfil"
    
    /**
     * Methode:Post
     * request:{langue,email,telephone,carte_bancaire{numero,type,titulaire},address{rue, ville,pays,code}}
     */
   let ModifierPreferences = server + "/api/user/ModifierPreferences"
    
    /**
     * url:/api/user/monProfil
     * methode:Get
     */
   let AfficherProfil = server + "/api/user/monProfil"
    
    /**
     * Methode:Post
     * request{motPassActuel,nouveauMotPass}
     */
   let ModiferMP = server + "/api/user/modifierPassword"
    
    /**
     * url:/api/user/profil/id
     * Methode:Get
     */
   let afficherUtilisateur =  server + "/api/user/profil/"
    
    /**
     * Url :/api/user/listFavoris
     * Methode :Get
     */
   let afficherFavoris = server + "/api/user/listFavoris"
    
    /**
     * Url:/api/user/listNoire
     * Methode:Get
     */
   let afficherListeNoir = server + "/api/user/getListNoire"
    
    /**
     * Url:/api/user/mesNotification
     * Methode:Get
     */
   let afficherNotifs = server + "/api/user/mesNotification"
    
    /**
     * Url:/api/user/mesActivite
     * Methode:Get
     */
   let mesActivites = server + "/api/user/mesActivite"
    
    /**
     * url:/api/user/mesConversations
     * methode:get
     */
   let afficherConversation = server + "/api/user/mesConversations"
    
    /**
     * Url:/api/user/messageConversation/{id}
     * Methode:Get
     */
   let afficherMessageConversation = server + "/api/user/messageConversation/"
    
    /**
     * Url:/api/user/envoieMessage
     * Methode:Post
     * request:{converationId,message}
     */
    let Send_Message = server + "/api/user/envoieMessage"
    
    /**
     * Url : /api/user/testToken
     * Methode: Get
     */
    let testToken = server + "/api/testToken"
    
    /**
     * Url : /api/upload_pic
     * Methode: Post
     */
    let uploadImage = server + "/uploadimage.php"
    
    /**
     * Url : /api/deconnxion
     * Methode: GET
     */
    let Logout = server + "/api/deconnxion/ios"
    
    /**
     * Méthode : Post
     * Request: {facebookId}
     */
    let ConnexionUserFb = server + "/api/user/connInscrFacb"
    
    /**
     * Méthode : Post
     * Request: {facebookId , nom , prenom , email , photo}
     */
    let InscriUserFb = server + "/api/user/connInscrFacb"
    
    /**
     * Méthode : Post
     * Request: {LinkedInId}
     */
    let ConnexionUserLinkedin = server + "/api/user/connInscrLink"
    
    /**
     * Méthode : Post
     * Request: {LinkedInId , nom , prenom , email , photo}
     */
    let InscriUserLinkedin = server + "/api/user/connInscrLink"
    
    /**
     * Méthode : Post
     * Request: {googleId}
     */
    let ConnexionUserGoogle = server + "/api/user/connInscrGoG"
    
    /**
     * Méthode : Post
     * Request: {googleId , nom , prenom , email , photo}
     */
    let InscriUserGoogle = server + "/api/user/connInscrGoG"
    
    /**
     * Méthode : Get
     * URL: /api/user/search/{query}
     */
    let searchUser = server + "/api/user/search/"
    
  
    
    /**
     * url:/api/user/creeLecon
     * methode:post
     * request:{sport : ["pratique","niveau"],date,massif,departement,station,titre,detail,prestation,duree,nombre de participants,langue}
     */
    let creeLecon = server + "/api/user/creeLecon"
    
    /**
     * url:/api/user/listPrestation/{id}
     * methode:get
     */
    let listePrest = server + "/api/user/listPrestation/"
    
    /**
     * Méthode : Get
     * URL:/api/user/sorties
     */
    let allSorties = server + "/api/user/sorties"
    
    /**
     * Méthode : Get
     * URL:/api/sortie/search
     * Request: {pratique,niveau,massif,date}
     */
    let searchSortie = server + "/api/sortie/search"
    
    /**
     * Méthode : Get
     * URL:/api/sortie/search
     *
     */
    let supprimerUser = server + "/api/user/suppCompte"
    
    /**
     * Méthode : Get
     * URL:/api/sortie/{id}
     *
     */
    let getSortieParId = server + "/api/sortie/"
    
    /**
     * Méthode : Post
     * URL:/api/user/addFavoris
     * Request : idUserFav
     *
     */
    let addFavoris = server + "/api/user/AddFavoris"
    
    /**
     * Méthode : Post
     * URL:/api/user/addListNoire
     * Request : idUserNoire
     *
     */
    let addListNoire = server + "/api/user/addListNoire"
    
    /**
     * Méthode : Post
     * URL:/api/user/removeFavoris
     * Request : idUserFav
     *
     */
    let removeFavoris = server + "/api/user/removeFavoris"
    
    /**
     * Méthode : Post
     * URL:/api/user/RemoveListNoire
     * Request : idUserNoire
     *
     */
    let removeListNoire = server + "/api/user/RemoveListNoire"
    
    /**
     * Méthode : Post
     * URL:/api/user/addToActivite
     * Request : idAct
     *
     */
    let joinSortie = server + "/api/user/addToActivite"
    
    /**
     * Méthode : Post
     * URL:/api/user/selectSportDefault
     * Request : pratique
     *
     */
    let changeSportDefault = server + "/api/user/selectSportDefault"
    
    /**
     * Méthode : POST
     * URL:/api/user/sortie/{id}/desabonne
     *
     */
    let suppActivity = server + "/api/user/sortie/"
    
    /******************** Prestataire **************/
    
    /**
     * Méthode:POST
     * request:{email,password,langue}
     */
    let inscriptionPrestataire = server + "/api/Pres/inscription"
    
    /**
     Methode: Post
     request:{email,password}
     */
    let connexionPrestataire = server + "/api/Pres/connexion"
    
    /**
     * Url:/api/Pres/monProfil
     * Methode:Get
     */
    let afficherProfilePrestataire = server + "/api/Pres/monProfil"
    
    /**
     * Url :/api/Pres/modifierProfil
     * Methode :Post
     * Request :{photo,langue,nom Etablissement ,description,address{pays,code,rue,ville}}
     */
    let modifierProfilePrestataire = server + "/api/Pres/modifierProfil"
    
    /**
     *Url:/api/Pres/creePrestation
     *methode:Post
     *Request:{"titre":"nombre de participant Maximum":"stock de la prestation":"duree":"sport":{"niveau":"pratique":"},"tarifs":[ {"prixMin","prixMax":,"dateDebut": "dateFin":}]}
     */
    let creerPrestation = server + "/api/Pres/creePrestation"
    
    /**
     *Url:/api/Pres/creePrestation
     *methode:Post
     *Request:{"titre":"nombre de participant Maximum":"stock de la prestation":"duree":"sport":{"niveau":"pratique":"},"tarifs":[ {"prixMin","prixMax":,"dateDebut": "dateFin":}]}
     */
    let modifierPreferencesPres = server + "/api/Pres/modifierPreferences"
    
    /**
     * Méthode : Get
     * Url:/api/deconnxion
     */
    let disconnectPres = server + "/api/deconnxion/ios"
    
    
    /**
     * url:/api/Pres/ajoutMoniteur
     * methode:post
     * request:{photo,nom,prenom,sport[{niveau,pratique}...]
     */
    let ajouterMoniteurPres = server + "/api/Pres/ajoutMoniteur"
    
    
    /**
     *url:/api/Pres/monProfil
     *Methode:Get
     */
    let monProfilPres = server + "/api/Pres/monProfil"
    
    /**
     * url:/api/sport/create
     * methode:post
     * request:{photo,niveau,pratique}
     */
    let ajouterSport = server + "/api/sport/create"
    
    /**
     * url:/api/Pres/supprimerMoniteur/
     * methode:get
     */
    let supprimerMoniteurPres = server + "/api/Pres/supprimerMoniteur/"
    
    /**
     * methode : POST
     * prototype : {
     * "id": 12,
     * "photo": "photo ",
     * "nom": "name text",
     * "prenom": "prenom text",
     * }
     * url : /api/Pres/modifierMoniteur
     */
    let modifierMoniteurPres = server + "/api/Pres/modifierMoniteur"
    
    /**
     * methode : POST
     *
     * url : /api/pres/add_indispo
     * request: date_debut, date_fin
     */
    let creerIndis = server + "/api/pres/add_indispo"
    
    /**
     * methode : POST
     *
     * url : /api/user/subscribeToPush
     * request: os, pid
     */
    let subscribePush = server + "/api/user/subscribeToPush"
    
    /**
     * Méthode : Post
     * URL:/api/sortie/creer
     * Request {datedebut,detail,titre,prestation,sport,massif,departement,lieurdv,lieudepart,duree,nbparticipant}
     *
     */
    let addSortie = server + "/api/sortie/creer"
    
    /**
     * methode : POST
     *
     * url : /api/pres/add_indispo
     * request: date_debut, date_fin
     */
    let supprimerIndis = server + "/api/pres/delete_indispo/"
    
    /**
     * URL : "/api/sortie/participant"
     * methode post
     * request: idsortie , idparticipant
     *
     */
    let invitParticipant = server + "/api/sortie/participant"
    
    let creeNotif = server + "/api/user/creeNotif"
    
    /**
     * URL : "/api/conversation/leave"
     * methode post
     * request: idConv
     *
     */
   let suppMessages = server + "/api/conversation/leave"
    
    /**
     * URL : "/api/user/creeConvPriv"
     * methode post
     * request: amiId
     *
     */
    let addConvPriv = server + "/api/user/creeConvPriv"
    
    /**
     * URL : "/api/conversation/join"
     * methode post
     * request: idConv
     *
     */
    let joinConvActivite = server + "/api/conversation/join"
    
    /**
     *url: /api/station/getall
     *Methode:Get
     */
    let Stations = server + "/api/station/getall"
    
    /**
     * URL : "/api/activite/respond"
     * methode post
     * request: id , respond
     *
     */
    let acceptRefuseInvit = server + "/api/activite/respond"
    
  
    let getAllStation = server + "/api/station/getall"
    
    /**
     *url: /api/user/deletenotifs
     *Methode:Get
     */
    let suppAllNotif = server + "/api/user/deletenotifs"
    
    let motDePasseOublié = server + "/api/user/pass_Oub"
    
    /**
     * URL : "/api/user/sendrendezvous"
     * methode post
     * request: sender , receiver , point , daterdv
     *
     */
    let sendRdv = server + "/api/user/sendrendezvous"
    
    
    /**
     * URL : "/api/user/respondrendezvous"
     * methode post
     * request: id , respond
     *
     */
    let repondreRdv = server + "/api/user/respondrendezvous"
    
    let supprimerSport = server + "/api/user/sport/remove"
    
    /**
     *url: /api/lecon/getall
     *Methode:Get
     */
    let getAllLessons = server + "/api/lecon/getall"
    
    /**
     * URL : /api/user/searchPres
     * methode post
     * request: pratique , niveau , massif , date , nbPlaceDispo , station , departement , duree , prestataire
     *
     */
    let searchPrestation = server + "/api/user/searchPres"
    
    /**
     * Url :/api/lecon/Cree
     * Methode: Post
     * request : prestation , Moniteur , detail , date , nombreMin
     *
     */
    let createLesson = server + "/api/lecon/Cree"
    
    /**
     * Url: /api/leconById
     * Methode: Post
     * request: id
     *
     */
    let getLesson = server + "/api/leconById"
    
    /**
     * Url: /api/getAllPrestataire
     * Methode: GET
     *
     */
    let getAllPrestataire = server + "/api/getAllPrestataire"
    
    /**
     * URL : /api/Lecon/search
     * methode post
     * request: pratique , niveau , massif , date , nbPlaceDispo , station , departement , duree
     *
     */
    let searchLesson = server + "/api/Lecon/search"
    let invitLesson = server + "/api/Lecon/participant"
    
    
    
    //kissou
    
    ////////prestation kissou
    /**ajoutPrestation
     * URL : "/api/Pres/ajoutPrestation"
     * methode post
     * request: id , respond
     *
     */
    let ajouterPrestation = server + "/api/Pres/ajoutPrestation"
    
    /** Url:/api/removePres
     Methode:Post
     request:{
     id
     }
     */
    let supprimerPrestation = server + "/api/removePres"
    
    
    /**  Url:/api/PresUpdate
     Methode:Post
     Request:{
     id
     titre
     niveau
     pratique
     nbParticipant
     nbParticipantmin
     stock
     duree
     }
     */
    let modifierPrestation = server + "/api/PresUpdate"
    
    /**  Url:/api/PresUpdate
     Methode:Post
     Request:{
     mois
     annee
     }
     */
    let presCalendarLecon = server + "/api/prestationByDate"
    
    /**    Url :/api/pres/leconByID
     Methode:Post
     Request:
     id
     */
    let presLeconById = server + "/api/pres/leconByID"
    
    /**    Url :/api/pres/leconByID
     Methode:Post
     Request:
     id
     */
    let leconMoniteur = server + "/api/Moniteur_Lecon"
    
    /**    Url :/api/pres/leconByID
     Methode:Post
     Request:
     id
     */
    let annulerLecon = server + "/api/Lecon/annuler"
    
    /**    Url :/api/pres/leconByID
     Methode:Post
     Request:
     id
     */
    let modifierLecon = server + "/api/pres/modfiLec"
    
    /**    Url :/api/user/AddPayement
     Methode:Post
     Request: presation , cvv , expiry
     */
    let payer = server + "/api/user/AddPayement"
    /**
     *url: /api/station/getall
     *Methode:Get
     */
    let StationsPres = server + "/api/station/getAll"
    
}
