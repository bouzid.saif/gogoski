//
//  TabBarControllViewController.swift
//  GogoSki
//
//  Created by Bouzid saif on 06/12/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit

class TabBarControllViewController: UITabBarController,UITabBarControllerDelegate {
    var ConvId = ""
    var first = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        print("loadTabar")
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotif(notification:)), name: NSNotification.Name(rawValue: "PushMessage"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleMessageActive(notification:)), name: NSNotification.Name(rawValue: "PushNotif"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleMessageActive(notification:)), name: NSNotification.Name(rawValue: "MessageViewActive"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handle_go_to_message(notification:)), name: NSNotification.Name(rawValue: "GoToMessageForomNoWhere"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handle_go_to_message2(notification:)), name: NSNotification.Name(rawValue: "GoToMessageForomNoWhere2"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.addBadge(notification:)), name: NSNotification.Name(rawValue: "AddBadge"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.DeleteBadge(notification:)), name: NSNotification.Name(rawValue: "DeleteBadge"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.ResetPost(notification:)), name: NSNotification.Name(rawValue: "ResetPost"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handle_go_to_sortie(notification:)), name: NSNotification.Name(rawValue: "GotoCreateSortie"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handle_go_to_leçon(notification:)), name: NSNotification.Name(rawValue: "GotoCreateLesson"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handle_popup(notification:)), name: NSNotification.Name(rawValue: "POPUP"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        // Do any additional setup after loading the view.
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    
    @objc func handle_popup (notification: NSNotification)
    {
        print("popup")
        popAll()
        
    }
    @objc func handle_go_to_sortie (notification: NSNotification)
    {
        popAll()
        let q = notification.object
        //ConvId = q
        self.selectedIndex = 1
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GotoCreate"), object: q )
        })
    }
    @objc func handle_go_to_leçon (notification: NSNotification)
    {
        popAll()
        let q = notification.object
        //ConvId = q
        self.selectedIndex = 2
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GotoCreate"), object: q )
        })
    }
    
    @objc func handle_go_to_message (notification: NSNotification)
    {
        popAll()
        let q = notification.object as! String
        ConvId = q
        self.selectedIndex = 3
        let tabBarController = self
        tabBarController.delegate = self
        
        if tabBarController.viewControllers![3].tabBarItem.badgeValue != nil {
            tabBarController.viewControllers![3].tabBarItem.badgeValue = nil
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            
            MessagesViewController.shared.handleNotifStatic(q:q)
            
        })
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("TabBarGone")
    }
    @objc func ResetPost (notification: NSNotification)
    {
        self.first = false
        
    }
    @objc func handle_go_to_message2 (notification: NSNotification)
    {
        popAll()
        let q = notification.object as! String
        // ConvId = q
        self.selectedIndex = 3
        print("notttttififfff:",notification.name)
        if ConvId != "0" {
            print("s77777777777")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                //if self.first == false {
                //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MessageIsActiveAhmed"), object: q as! String)
                //self.first = true
                MessagesViewController.shared.handleNotifStatic(q:q)
                // }
            })
            
        }
        
    }
    @objc func addBadge (notification: NSNotification)
    {
        
        let tabBarController = self
        tabBarController.delegate = self
        if !(tabBarController.selectedIndex == 3) {
            if tabBarController.viewControllers![3].tabBarItem.badgeValue == nil {
                tabBarController.viewControllers![3].tabBarItem.badgeValue = "1"
            }else {
                tabBarController.viewControllers![3].tabBarItem.badgeValue = String(Int(tabBarController.viewControllers![3].tabBarItem.badgeValue!)! + 1 )
            }
        }
    }
    @objc func DeleteBadge (notification: NSNotification)
    {
        
        let tabBarController = self
        tabBarController.delegate = self
        
        if tabBarController.viewControllers![3].tabBarItem.badgeValue != nil {
            tabBarController.viewControllers![3].tabBarItem.badgeValue = nil
        }
        
    }
    @objc func handleNotif(notification: NSNotification) {
        popAll()
        let q = notification.object as! String
        ConvId = q
        
        self.selectedIndex = 3
        let tabBarController = self
        tabBarController.delegate = self
        
        if tabBarController.viewControllers![3].tabBarItem.badgeValue != nil {
            tabBarController.viewControllers![3].tabBarItem.badgeValue = nil
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            //if self.first == false {
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MessageIsActiveAhmed"), object: q as! String)
            //self.first = true
            MessagesViewController.shared.handleNotifStatic(q:q)
            // }
        })
    }
    @objc func handleMessageActive(notification: NSNotification) {
        print("ConvId:", ConvId)
        let q = notification.object as! String
        self.selectedIndex = 3
        ConvId = q
        if ConvId != "0" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MessageIsActive"), object: ConvId as! String)
        }else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotifIsActive"), object: self.ConvId as! String)
            })
            
            
        }
        ConvId = ""
        
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        popAll()
        item.badgeValue = nil
    }
    func configureViewFromLocalisation() {
        
        
        let tabBarController = self
        tabBarController.delegate = self
        if let tabBarViewControllers = tabBarController.viewControllers {
            let campusController = tabBarViewControllers[0] as! UINavigationController
            campusController.title = Localization("Social")
            let adController = tabBarViewControllers[1] as! UINavigationController
            adController.title = Localization("Sortie")
            let LeconVC = tabBarViewControllers[2] as! UINavigationController
            LeconVC.title = Localization("Leçons")
            let MessagesVC = tabBarViewControllers[3] as! UINavigationController
            MessagesVC.title = Localization("Messages")
        }
        
        //Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    func popAll(){
        
        let tabBarController = self
        tabBarController.delegate = self
        
        if let tabBarViewControllers = tabBarController.viewControllers {
            
            
            let campusController = tabBarViewControllers[0] as! UINavigationController
            
            let campusTVC = campusController.viewControllers[0] as! SocialViewController
            
            _ = campusTVC.navigationController?.popToRootViewController(animated: false)
            campusTVC.selectPage()
            
            let adController = tabBarViewControllers[1] as! UINavigationController
            
            let adminTVC = adController.viewControllers[0] as! SortiesViewController
            
            _ = adminTVC.navigationController?.popToRootViewController(animated: false)
            
            let searchController = tabBarViewControllers[2] as! UINavigationController
            
            let searchTVC = searchController.viewControllers[0] as! Lec_onViewController
            _ = searchTVC.navigationController?.popToRootViewController(animated: false)
            
            /*   let SejourVC = tabBarViewControllers[3] as! UINavigationController
             
             let Sejour = SejourVC.viewControllers[0] as! Sejour
             
             _ = Sejour.navigationController?.popToRootViewController(animated: false) */
            let MessagesVC = tabBarViewControllers[3] as! UINavigationController
            
            let message = MessagesVC.viewControllers[0] as! MessageNotificationSection
            
            _ = message.navigationController?.popToRootViewController(animated: false)
            // message.LoadMessage()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

