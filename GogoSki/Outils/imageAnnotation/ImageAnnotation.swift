//
//  ImageAnnotation.swift
//  testIMG
//
//  Created by AYMEN on 9/30/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import MapKit


open class ImageAnnotation: NSObject , MKAnnotation{
    
    public  var coordinate: CLLocationCoordinate2D
    public   var image1: String?
    public  var image2: String?
    public var image4 : String?
    public   var colour: UIColor?
    public var title : String?
    public var subtitle : String?
    public var id_point : String?
    public var nom_etab : String?
    override init() {
        self.coordinate = CLLocationCoordinate2D()
        self.image1 = nil
        self.image2 = nil
        self.image4 = nil
        self.colour = UIColor.white
        self.title = nil
        self.subtitle = nil
    }
}

