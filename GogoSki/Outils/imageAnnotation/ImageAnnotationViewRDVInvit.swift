//
//  ImageAnnotationView.swift
//  testIMG
//
//  Created by AYMEN on 9/30/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import MapKit
import MapleBacon
class ImageAnnotationViewRDVInvit: MKAnnotationView {
    
    private var imageView: UIImageView! = {
        let image = UIImageView()
        image.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return image
    }()
    private var im : String = {
        let im = ""
        return im
    }()
    var viewHeight = 70
    var viewWidth = 70
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        self.frame = CGRect(x: 0, y: 0, width: viewHeight, height: viewWidth)
        self.imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        self.imageView.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.pinView))
        gesture.numberOfTapsRequired = 1
        self.imageView.addGestureRecognizer(gesture)
        self.addSubview(self.imageView)
        self.addGestureRecognizer(gesture)
        
        ////border (width / color )
        self.imageView.layer.borderWidth = 6
        self.imageView.layer.borderColor = UIColor.white.cgColor
        
        //corner radius
        self.imageView.layer.cornerRadius = imageView.frame.height / 2
        self.imageView.layer.masksToBounds = true
        
        
        
        
        
        
        self.imageView.contentMode = .scaleAspectFit
        
    }
    @objc func pinView() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PinView"), object: nil)
        self.imageView.isUserInteractionEnabled = false
    }
    func setimageview(){
        if self.im == "default" {
            self.imageView.image = UIImage(named: "user_placeholder.png")
        }else  if self.im == "default2"{
            self.imageView.image = UIImage(named: "station")
        }else {
            let URL = NSURL(string: (self.im).addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
            self.imageView.setImage(withUrl: URL as! URL)
        }
        //// Houni t seti les logo mta3 e ski
        
        
        
    }
    override var image: UIImage? {
        get {
            return self.imageView.image
        }
        
        set {
            self.imageView.image = newValue
            
        }
    }
    
    var image_name : String? {
        get {
            return self.image_name
        }
        set {
            
            self.image_name = newValue
        }
    }
    
    /*
     var heightAnnotationView : Int? {
     get {
     return self.viewHeight
     }
     
     set {
     self.viewHeight = newValue
     }
     }
     
     var widthAnnotationView : Int? {
     get {
     return self.viewWidth
     }
     
     set {
     self.viewWidth = newValue
     }
     }
     */
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var image1: String? {
        get {
            return self.im
        }
        set {
            self.im = newValue as! String!
        }
    }
    
    
}



