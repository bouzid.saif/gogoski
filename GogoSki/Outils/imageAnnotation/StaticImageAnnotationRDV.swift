//
//  StaticImageAnnotation.swift
//  TestFireBaseDataBase
//
//  Created by Bouzid saif on 01/10/2017.
//  Copyright © 2017 Bouzid saif. All rights reserved.
//

import Foundation
import UIKit
class StaticImageAnnotationRDV {
    static var sharedInstance = StaticImageAnnotationRDV()
    private init(){}
    var AImages : [ImageAnnotation]! = []
    func add(im : ImageAnnotation){
        AImages.append(im)
    }
    func remove(){
        AImages.removeAll()
    }
}

