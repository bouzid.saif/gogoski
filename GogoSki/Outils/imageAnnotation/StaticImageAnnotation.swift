//
//  StaticImageAnnotation.swift
//  TestFireBaseDataBase
//
//  Created by Bouzid saif on 01/10/2017.
//  Copyright © 2017 Bouzid saif. All rights reserved.
//

import Foundation
import UIKit
class StaticImageAnnotation {
    static var sharedInstance = StaticImageAnnotation()
    private init(){}
    var AImages : [ImageAnnotation]! = []
    var size  = 90.0
    var scale : CGFloat! = -1
    func add(im : ImageAnnotation){
        AImages.append(im)
    }
    func resize(zoomlevel : Double) {
        switch zoomlevel {
        case 0...2.99 :
            size = 20
        case 3...5.99 :
            size = 30
        case 6...8.99 :
            size = 45
        case 9...11.99 :
            size = 55
        case 12...14.99 :
            size = 70
        case 15...17.99 :
            size = 80
        case 18...19.99 :
            size = 90
        default :
            size = 90
        }
    }
    func remove(){
        AImages.removeAll()
    }
}

