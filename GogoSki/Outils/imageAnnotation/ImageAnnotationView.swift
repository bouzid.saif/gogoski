//
//  ImageAnnotationView.swift
//  testIMG
//
//  Created by AYMEN on 9/30/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import MapKit
import MapleBacon
class ImageAnnotationView: MKAnnotationView {
    
    public var imageView: UIImageView! = {
        let image = UIImageView()
        ///image.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return image
    }()
    public var imageView2: UIImageView!  = {
        let image = UIImageView()
        // image.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return image
    }()
    private var im : String = {
        let im = ""
        return im
    }()
    private var im2 : String = {
        let im2 = ""
        return im2
    }()
    private var im3 : String = {
        let im3 = ""
        return im3
    }()
    var viewHeight = 90
    var viewWidth = 90
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
       
        viewHeight = Int(StaticImageAnnotation.sharedInstance.size)
        viewWidth = Int(StaticImageAnnotation.sharedInstance.size)
        self.frame = CGRect(x: 0, y: 0, width: viewHeight, height: viewWidth)
        self.imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        self.imageView2 = UIImageView(frame: CGRect(x: self.frame.maxX - self.frame.height / 2.5 , y: self.frame.maxY - self.frame.height / 2.5 , width: self.frame.width / 2, height: self.frame.height / 2 ))
       
        
        //self.imageView.addGestureRecognizer(gesture)
        
        
        self.addSubview(self.imageView)
        self.addSubview(self.imageView2)
        
        ////border (width / color )
        self.imageView.layer.borderWidth = 6
        self.imageView.layer.borderColor = UIColor.white.cgColor
        
        //corner radius
        self.imageView.layer.cornerRadius = imageView.frame.height / 2
        self.imageView.layer.masksToBounds = true
        
        self.imageView2.layer.cornerRadius = imageView2.frame.height / 2
        self.imageView2.layer.masksToBounds = true
        
        
        
        
        self.imageView.contentMode = .scaleAspectFit
        
    }
    
    func setimageview(scale: CGFloat){
        if self.im == "default" {
            self.imageView.image = UIImage(named: "user_placeholder.png")
            //self.imageView.transform = CGAffineTransform(scaleX: scale, y: scale)
        }else  if self.im == "default2"{
            self.imageView.image = UIImage(named: "user_placeholder.png")
            //self.imageView.transform = CGAffineTransform(scaleX: scale, y: scale)
        }else {
            let URL = NSURL(string: (self.im).addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
            self.imageView.setImage(withUrl: URL! as URL, placeholder: UIImage(named: "user_placeholder.png"), crossFadePlaceholder: false, cacheScaled: true, completion: nil)
            //self.imageView.transform = CGAffineTransform(scaleX: scale, y: scale)
        }
        //// Houni t seti les logo mta3 e ski
        
        if self.im2 == "Ski Alpin"
        {
            self.imageView2.image = UIImage(named: "ski-alpin-kissou")
            // self.imageView2.transform = CGAffineTransform(scaleX: scale, y: scale)
            
        }
        else if self.im2 == "Handiski"
        {
            self.imageView2.image = UIImage(named: "Handi-ski-kissou")
            //self.imageView2.transform = CGAffineTransform(scaleX: scale, y: scale)
        }
        else if self.im2 == "Raquettes"
        {
            self.imageView2.image = UIImage(named: "Raquettes-kissou")
            // self.imageView2.transform = CGAffineTransform(scaleX: scale, y: scale)
        }
        else if self.im2 == "Ski de fond"
        {
            self.imageView2.image = UIImage(named: "Ski-fond-kissou")
            //self.imageView2.transform = CGAffineTransform(scaleX: scale, y: scale)
        }
        else if self.im2 == "Snowboard"
        {
            self.imageView2.image = UIImage(named: "Snowboard-kissou")
            // self.imageView2.transform = CGAffineTransform(scaleX: scale, y: scale)
        }
        else
        {
            self.imageView2.image = UIImage(named: "Randonnée-ski-kissou")
            // self.imageView2.transform = CGAffineTransform(scaleX: scale, y: scale)
        }
        //print("Niveau2: ", self.im3)
        if self.im3 == "Débutant"
        {
            
            self.imageView2.tintColor = UIColor("#D3D3D3")
        }
        else if self.im3 == "Débrouillard"
        {
            
            self.imageView2.tintColor = UIColor("#76EC9E")
        }
        else if self.im3 == "Intermédiaire"
        {
            //print ("8alet")
            self.imageView2.tintColor = UIColor("#75A7FF")
        }
        else if self.im3 == "Confirmé"
        {
            // print ("8alet")
            self.imageView2.tintColor = UIColor("#FF3E53")
        }
        else if self.im3 == "Expert"
        {
            //  print ("8alet")
            self.imageView2.tintColor = UIColor("#4D4D4D")
        }
        
    }
    override var image: UIImage? {
        get {
            return self.imageView.image
        }
        
        set {
            self.imageView.image = newValue
            
        }
    }
    var image3: UIImage? {
        get {
            return self.imageView2.image
        }
        
        set {
            self.imageView2.image = newValue
        }
    }
    var image_name : String? {
        get {
            return self.image_name
        }
        set {
            
            self.image_name = newValue
        }
    }
    
    /*
     var heightAnnotationView : Int? {
     get {
     return self.viewHeight
     }
     
     set {
     self.viewHeight = newValue
     }
     }
     
     var widthAnnotationView : Int? {
     get {
     return self.viewWidth
     }
     
     set {
     self.viewWidth = newValue
     }
     }
     */
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var image1: String? {
        get {
            return self.im
        }
        set {
            self.im = newValue as! String!
        }
    }
    var image2: String? {
        get {
            return self.im2
        }
        set {
            self.im2 = newValue as! String
        }
    }
    var image4: String? {
        get {
            return self.im3
        }
        set {
            self.im3 = newValue as! String!
        }
    }
    
}

