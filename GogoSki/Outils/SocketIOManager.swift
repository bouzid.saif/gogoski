//
//  SocketIOManager.swift
//  SocketChat
//
//  Created by Ahmed Haddar on 03/10/2017.
//  Copyright © 2017 AppCoda. All rights reserved.
//

import UIKit
import SocketIO
import SwiftyJSON

class SocketIOManager: NSObject {
    static let sharedInstance = SocketIOManager()
    //51.254.37.192
    //, config: [.log(true)]
    var socket: SocketIOClient = SocketIOClient(socketURL: URL(string: "http://51.254.37.192:3000")! as URL)
    override init() {
        super.init()
        
    }
    
    func establishConnection() {
        print("connecting..")
        
        socket.connect()
    }
    
    
    func closeConnection() {
        socket.disconnect()
    }
    
    func connectUsername(id:String,completionHandler : @escaping ((Bool) -> Void)){
        socket.emit("connectUser", id)
        completionHandler(true)
    }
    func connectToMapServer(id_user:String,nom:String,prenom:String,niveau:String,pratique:String,photo:String,latitude:String,longitude:String,completionHandler : @escaping ((Bool) -> Void)) {
        
        socket.emit("MyConnect" ,["user_id" : id_user , "nom" : nom , "prenom" : prenom,"niveau" : niveau , "pratique"
            : pratique , "latitude" : latitude , "longitude" : longitude ,"photo" : photo])
        
        completionHandler(true)
    }
    func updatePosition (id_user:String,nom:String,prenom:String,niveau:String,pratique:String,latitude:String,longitude:String,photo:String) {
        
        socket.emit("updateUser",["user_id" : id_user , "nom" : nom , "prenom" : prenom,"niveau" : niveau , "pratique"
            : pratique , "latitude" : latitude , "longitude" : longitude , "photo" : photo ])
    }
    func listenRemoveUser(){
        socket.on("userRemoved") { (dataArray, socketAck) -> Void in
            //print("userDeleted",dataArray)
            let id_user = dataArray[0] as! Int
            StaticPositionController.shared.UsersPos.removeValue(forKey: "\(String(id_user))")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MapRemove"), object: "\(String(id_user))")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserDeleted"), object: "\(String(id_user))")
        }
    }
    func listenNewUser() {
        socket.on("newUserJoined") { (dataArray, socketAck) -> Void in
            // print("newUserJoined",dataArray)
            //StaticPositionController.shared.UsersPos["5"]?.appendIfArray(json: JSON())
            //StaticPositionController.shared.UsersPos["\(dataArray[1])"] = JSON(dataArray[0])
            //StaticPositionController.shared.UsersPos.ind
            //StaticPositionController.shared.UsersPos.updateValue(JSON(dataArray[0]), forKey: "\(dataArray[1])")
            // StaticPositionController.shared.UsersPos.setValue(JSON(dataArray[0], forKey: "\(dataArray[1])")
            StaticPositionController.shared.UsersPos["\(dataArray[1])"] = JSON(dataArray[0])
            StaticPositionController.shared.TablrePos["\(dataArray[1])"] = IndexPath(row:StaticPositionController.shared.UsersPos.count - 1 , section: 0)
            //StaticPositionController.shared.UsersPos.setValue(JSON(dataArray[0]), forKey: "\(dataArray[1])")
            
            //StaticPositionController.shared.UsersPos.updateValue(JSON(dataArray[0]), forKey: "\(dataArray[1])")
            //print("NewUser:",StaticPositionController.shared.UsersPos)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MapUpdateLocation"), object: String(describing: dataArray[1]))
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NewUserJoined"), object: String(describing: dataArray[1]))
            
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: dataArray[0] as! [String: AnyObject])
        }
    }
    func listenAllUser() {
        socket.on("allUsersList") { (dataArray, socketAck) -> Void in
            //print("allUsersList",dataArray)
            //print("json:",JSON(dataArray))
            //var results = [String:[AnyObject]]()
            /* do {
             let jsonResult = try JSONSerialization.JSONObjectWithData(dataArray, options:JSONSerialization.ReadingOptions.mutableContainers)
             }catch {
             print("err:",error)
             } */
            var i = 0
            StaticPositionController.shared.UsersPos.removeAll()
            let results = JSON(dataArray)
            for (key, value) in results {
                //print("key \(key) value2 \(value)")
                for (key2, value2) in value {
                    //  print("key2 \(key2) value3 \(value2)")
                    //StaticPositionController.shared.UsersPos.updateValue(value2, forKey: key2)
                    //StaticPositionController.shared.UsersPos.setValue(value2, forKey: "\(key2)")
                    StaticPositionController.shared.UsersPos[key2] = value2
                    // StaticPositionController.shared.UsersPos.add
                    StaticPositionController.shared.TablrePos[key2] = IndexPath(row: i, section: 0)
                }
                i = i + 1
            }
            
            StaticPositionController.shared.count = i
            
            
            //let c = JSON(dataArray)
            // let t = c.
            //  StaticPositionController.shared.UsersPos = StaticPositionController.shared.UsersPos.reversed()
            //print("lol:",StaticPositionController.shared.UsersPos)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MapGetDone"), object: nil)
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: dataArray[0] as! [String: AnyObject])
        }
    }
    func listenUserUpdatePosition() {
        socket.on("updatePositionBroadcast") { (dataArray, socketAck) -> Void in
             print("updatePositionBroadcast",dataArray)
            
            StaticPositionController.shared.UsersPos["\(dataArray[1])"] = JSON(dataArray[0])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MapUpdateLocation"), object: String(describing: dataArray[1]))
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserUpdateLocation"), object: String(describing: dataArray[1]))
            
            // print("Update:",StaticPositionController.shared.UsersPos)
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: dataArray[0] as! [String: AnyObject])
        }
    }
    
    func connectToServerWithNickname(nickname: String,userid: String,first:Bool) {
        
        
        self.socket.emit("subscribe",["room" :  nickname,"userid" : userid ])
        //self.socket.joinNamespace(nickname)
        
        //JoinRooms ()
        
        //les conversations (conversation) eli 3and user
        //subscribe tous les converastions
        //Lors du click get all messages mel BD
        //send_msg ChatMessage
        if first {
            listenForOtherMessages()
        }
    }
    func SetOnline(room:String,UserId:String){
        socket.emit("userAlreadyOnline",["room" : room , "userid" : UserId])
    }
    func exitChatWithNickname(nickname: String, completionHandler: () -> Void) {
        socket.emit("exitUser", nickname)
        completionHandler()
    }
    
    func stopmessage(Conv:String,nickname: String) {
        socket.emit("stopTyping", Conv,nickname)
    }
    func sendMessage(idNickname: String, nickname: String, msg : String, conv : String,photo: String) {
        socket.emit("chatMessage",["clientid" : idNickname , "senderusername" : nickname, "message" : msg, "conv" : conv, "senderphoto" : photo ])
    }
    func unscubscribe(Conv:String,nickname: String) {
        socket.emit("unsubscribe",["room" :  nickname,"userid" : nickname ])
    }
    
    func sendStartTypingMessage(Conv:String,nickname: String) {
        print("yekteb")
        socket.emit("startTyping", Conv,nickname)
    }
    func getChatMessage(completionHandler: @escaping (_ messageInfo: JSON) -> Void) {
        socket.on("newChatMessage") { (dataArray, socketAck) -> Void in
            
            print("allo")
            // print(dataArray)
            var messageDictionary = JSON.parse(dataArray.description)
            //messageDictionary["conv"] = dataArray[5] as! Int as AnyObject?
            completionHandler(messageDictionary )
        }
    }
    private func listenForOtherMessages() {
        socket.on("userConnectUpdate") { (dataArray, socketAck) -> Void in
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: dataArray[0] as! [String: AnyObject])
        }
        
        socket.on("userExitUpdate") { (dataArray, socketAck) -> Void in
            print("Exit:",dataArray)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserDisonnectNow"), object: dataArray as? [String])
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasDisconnectedNotification"), object: dataArray[0] as! String)
        }
        
        socket.on("newUserIsTyping") { (dataArray, socketAck) -> Void in
            //print(dataArray[0] as! String)
            //print(dataArray[0] as? [String: AnyObject])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userTypingNotification"), object: dataArray as? [String])
        }
        socket.on("newUserStoppedTyping") { (dataArray, socketAck) -> Void in
            //print(dataArray[0] as! String)
            //print(dataArray[0] as? [String: AnyObject])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userStopTypingNotification"), object: dataArray as? [String])
        }
        socket.on("userOnline") { (dataArray, socketAck) -> Void in
            print("userOnline:",dataArray)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userConnectNow"), object: dataArray as? [String])
        
        }
        socket.on("setOnline") { (dataArray, socketAck) -> Void in
            print("SetOnline")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userConnectNow2"), object: dataArray as? [String])
            
        }
        socket.on("userOffline") { (dataArray, socketAck) -> Void in
            print("userOffline:",dataArray)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserDisonnectNow"), object: dataArray as? [String])
        }
    }
    
    
}

