//
//  FBAnnotation.swift
//  FBAnnotationClusteringSwift
//
//  Created by Robert Chen on 4/2/15.
//  Copyright (c) 2015 Robert Chen. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

open class FBAnnotation: NSObject {
    
    open var coordinate = CLLocationCoordinate2D()
    open var title: String?
    open  var subtitle: String?
    open   var image1: String?
    open  var image2: String?
    open   var colour: UIColor? = UIColor.white
}

extension FBAnnotation: MKAnnotation { }
