//
//  RoundedUIImageView.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 13/07/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import  UIKit
@IBDesignable
class RoundedUIImageView: UIImageView {
    @IBInspectable var round: Bool = true {
        didSet { self.setNeedsLayout() }
    }
    
    @IBInspectable var width: CGFloat = 2.5 {
        didSet { self.setNeedsLayout() }
    }
    
    @IBInspectable var color: UIColor = UIColor(red: 208, green: 208, blue: 208, alpha: 1){
        didSet { self.setNeedsLayout() }
    }
    @IBInspectable var profil : Bool = true {
        didSet {
            let gesture = UITapGestureRecognizer(target: self,  action: #selector(self.callProfil))
            self.isUserInteractionEnabled  = true
            gesture.numberOfTapsRequired = 1
            addGestureRecognizer(gesture)
        }
    }
    @objc func callProfil (){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.navigation.pushViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfilMain") as! ProfilMain, animated: true)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.clipsToBounds = true
        
        if round {
            self.layer.cornerRadius = self.frame.width / 2
        } else {
            self.layer.cornerRadius = 0
        }
        
        self.layer.borderWidth = self.width
        self.layer.borderColor = self.color.cgColor
    }
}
