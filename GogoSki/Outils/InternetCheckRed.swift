//
//  InternetCheckRed.swift
//  GogoSki
//
//  Created by AYMEN on 1/6/18.
//  Copyright © 2018 Velox-IT. All rights reserved.
//


import Foundation
import UIKit
import EFInternetIndicator

class InternetCheckRed: UIViewController,InternetStatusIndicable2 {
    var internetConnectionIndicator: InternetViewIndicator2?
    static var shared = InternetCheckRed()
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification3(notification:)), name: kNotificationLanguageChanged, object: nil)
        
        configureViewFromLocalisation3()
    }
    
    func configureViewFromLocalisation3() {
        self.startMonitoringInternet(backgroundColor: UIColor.red , style:  .statusLine, textColor: UIColor.white, message: Localization("connexionperdu"), remoteHostName: "apple.com")
    }
    
    @objc func receiveLanguageChangedNotification3(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation3()
        }
    }
    
}


