//
//  viewTableCell.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 26/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import Foundation
import  UIKit
@IBDesignable
class viewTableCell: UIView {
    @IBInspectable
    var cornerRadius: CGFloat = 10 {
        didSet { self.setNeedsLayout() }
    }
    @IBInspectable
    var shadowColor: UIColor = .black {
        didSet { self.setNeedsLayout() }
    }
    @IBInspectable
    var shadowOpacity: Float = 0.5 {
        didSet { self.setNeedsLayout() }
    }
    @IBInspectable
    var shadowRadius: CGFloat = 1 {
        didSet { self.setNeedsLayout() }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.clipsToBounds = true
        self.layer.cornerRadius = self.cornerRadius
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = self.shadowColor.cgColor
        self.layer.shadowOpacity = self.shadowOpacity
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = self.shadowRadius
      
    }
    
}
