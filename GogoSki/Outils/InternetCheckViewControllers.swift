//
//  InternetCheckViewControllers.swift
//  GogoSki
//
//  Created by Bouzid saif on 01/11/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import EFInternetIndicator
class InternetCheckViewControllers: UIViewController,InternetStatusIndicable {
    var internetConnectionIndicator: InternetViewIndicator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification2(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation2()
        
    }
     func configureViewFromLocalisation2() {
    self.startMonitoringInternet(backgroundColor: UIColor.blue, style:  .statusLine, textColor: UIColor.white, message: Localization("connexionperdu"), remoteHostName: "apple.com")
    
    }
    @objc func receiveLanguageChangedNotification2(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation2()
        }
    }
   
}

