//
//  LangueChoixUser.swift
//  GogoSki
//
//  Created by Bouzid saif on 21/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
class LangueChoixUser : InternetCheckViewControllers,UITableViewDelegate,UITableViewDataSource {
    /*
     * array of all languages available
     */
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    /*
     * array of langauges without device current language
     */
    var languages : [String] = []
    /*
     * selected language
     */
    var selectedIndex : Int = -1
    /*
     * indexpath of selected language
     */
    var indexLangue : IndexPath = []
    /*
     * title of this page
     */
    @IBOutlet weak var titre: UILabel!
    /*
     * validate button
     */
    @IBOutlet weak var validerBtn: UIButton!
    /*
     * configue language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("PageLangueTitle")
        Localisator.sharedInstance.saveInUserDefaults = true
        validerBtn.setTitle(Localization("BtnValider"), for: .normal)
        table.reloadData()
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        //  let img : UIImageView = cell.viewWithTag(2) as! UIImageView
        let label : UILabel = cell.viewWithTag(1) as! UILabel
        print(NSLocale.current.languageCode == "en")
        if Localisator.sharedInstance.currentLanguage == arrayLanguages[0] {
            if NSLocale.current.languageCode == "en" && indexPath.row == 0 {
                
                indexLangue = indexPath
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }else if NSLocale.current.languageCode == "fr" && indexPath.row == 1 {
                
                indexLangue = indexPath
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            } else if (NSLocale.current.languageCode != "fr" && NSLocale.current.languageCode != "en") && indexPath.row == 1 {
                
                indexLangue = indexPath
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }
        }
        else if (Localisator.sharedInstance.currentLanguage != languages[indexPath.row])
        {
            
            cell.accessoryType = UITableViewCellAccessoryType.none
            // img.isHidden = false
        }
        else {
            
            indexLangue = indexPath
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        }
        
        label.text       = Localization(languages[indexPath.row])
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
        // let img : UIImageView = cell!.viewWithTag(2) as! UIImageView
        //  img.isHidden = false
        if (cell?.accessoryType != UITableViewCellAccessoryType.checkmark) {
            
            cell?.accessoryType = UITableViewCellAccessoryType.checkmark
            let cell1 = tableView.cellForRow(at: indexLangue)
            cell1?.accessoryType = UITableViewCellAccessoryType.none
            tableView.deselectRow(at: indexLangue, animated: true)
            selectedIndex = indexPath.row
            indexLangue = indexPath
            
        }
        /*  else {
         cell?.accessoryType = UITableViewCellAccessoryType.none
         tableView.deselectRow(at: indexPath, animated: true)
         }*/
        
        
    }
    /*
     * language tableview
     */
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        languages.append(arrayLanguages[1])
        languages.append(arrayLanguages[2])
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*
     * back action
     */
    @IBAction func  go_back(_ sender :Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     * validate action after language selected
     */
    @IBAction func  valider(_ sender :Any) {
        
        let alert = UIAlertController(title: nil, message: Localization("languageChangedWarningMessage"), preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            SetLanguage(self.languages[self.indexLangue.row])
            
            Localisator.sharedInstance.saveInUserDefaults = true
            self.navigationController?.popViewController(animated: true)
        }
        let cancelAction = UIAlertAction(title: "Cancel" , style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
        
    }
}

