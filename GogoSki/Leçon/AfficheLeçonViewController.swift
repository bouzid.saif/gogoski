//
//  AfficheLeçonViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 20/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import MapKit
import DownPicker
import SwiftyJSON
import Alamofire

class AfficheLec_onViewController: InternetCheckViewControllers , MKMapViewDelegate , UITextViewDelegate{
    /*
     *Level in French
     *
     */
    let NiveauSaifFR = ["Débutant","Débrouillard" ,"Intermédiaire","Confirmé","Expert"]
    /*
     *Level in English
     *
     */
    let NiveauSaifEN = ["Beginner","Resourceful" ,"Intermediate","Confirmed","Expert"]
    /*
     *Practice in French
     *
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     *Practice in English
     *
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     *Participants table View
     *
     */
    @IBOutlet weak var participantTableView: UITableView!
    /*
     *duration label
     *
     */
    @IBOutlet weak var duree: UILabel!
    /*
     *remaining number of participants
     *
     */
    @IBOutlet weak var nbrPlaceRest: UILabel!
    /*
     *from hour
     *
     */
    @IBOutlet weak var heureDep: UILabel!
    /*
     *maximum of participants
     *
     */
    @IBOutlet weak var nbrPlaceDispo: UILabel!
    /*
     *participants data
     *
     */
    var participants : JSON = []
    /*
     *test if this activity was created by the user or by the renew button
     *
     */
    var newLesson = true
    /*
     *from hour received from the preview view
     *
     */
    var heureDepart = ""
    /*
     *from date received from the preview view
     *
     */
    var dateDepart = ""
    /*
     *station name received from the preview view
     *
     */
    var nomStation = ""
    /*
     *min price titile
     *
     */
    @IBOutlet weak var btnPrixMin: UIButton!
    /*
     *max price title
     *
     */
    @IBOutlet weak var btnPrixMax: UIButton!
    /*
     *from date text
     *
     */
    @IBOutlet weak var dateDep: UILabel!
    /*
     *benefit data
     *
     */
    var prestation : JSON = []
    // @IBOutlet weak var btnPlus: UIButton!
    //  @IBOutlet weak var viewContraintHigh: NSLayoutConstraint!
    // @IBOutlet weak var labelTopConstraint: NSLayoutConstraint!
    /*
     *station title
     *
     */
    @IBOutlet weak var stationTitle: UILabel!
    /*
     *sport button height
     *
     */
    @IBOutlet weak var btnSportHeight: NSLayoutConstraint!
    /*
     *level button title
     *
     */
    @IBOutlet weak var btnNiveau: UIButton!
    /*
     *lesson name
     *
     */
    @IBOutlet weak var lessonName: UILabel!
    /*
     *sports button
     *
     */
    @IBOutlet weak var btnSport: UIButton!
    /*
     *detail text
     *
     */
    @IBOutlet weak var details: UITextView!
    /*
     *title of the view
     *
     */
    @IBOutlet weak var titre: UILabel!
    /*
     *number of participants
     *
     */
    @IBOutlet weak var labelNbrParticipant: UILabel!
    /*
     *other details
     *
     */
    @IBOutlet weak var labelInfoComplementaire: UILabel!
    /*
     *lesson details
     *
     */
    @IBOutlet weak var labelDetailsLeçon: UILabel!
    /*
     *create lesson button
     *
     */
    @IBOutlet weak var btnCreerLeçon: UIButton!
    /*
     *min number of participants
     *
     */
    @IBOutlet weak var labelnbrPArticipMin: UILabel!
    /*
     *price label
     *
     */
    @IBOutlet weak var labelPrix: UILabel!
    /*
     *participants label
     *
     */
    @IBOutlet weak var labelparticipant: UILabel!
    /*
     *flat rate label
     *
     */
    @IBOutlet weak var labelForfait: UILabel!
    /*
     *information label
     *
     */
    @IBOutlet weak var labelInformation: UILabel!
    @IBOutlet weak var map: MKMapView!
    /*
     *flat rate checkbox
     *
     */
    @IBOutlet weak var forfait: VKCheckbox!
    /*
     *massif choice picker
     *
     */
    var massif_downPicker : DownPicker!
    /*
     *massif name
     *
     */
    @IBOutlet weak var massif: PaddingTextField!
    /*
     *image of the actual user
     *
     */
    @IBOutlet weak var imageToProfileNavBar: RoundedUIImageView!
 
    //  @IBOutlet weak var labelDesc: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if let urlImgUser = URL(string : a["user"]["photo"].stringValue) , let placeholder =  UIImage(named: "ahmed.jpg") {
            imageToProfileNavBar.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        map.layer.cornerRadius = 20
        details.layer.cornerRadius = 5
        details.delegate = self
        let bandArray7 : NSMutableArray = []
        for i in 2...self.prestation["nombre_participant"].intValue  {
            bandArray7.add(String(i))
        }
        massif_downPicker = DownPicker(textField: massif, withData: bandArray7 as! [Any])
        massif_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        forfait.line = .normal
        forfait.bgColorSelected = UIColor(red: 95/255, green: 182/255, blue: 255/255, alpha: 1)
        forfait.bgColor          = UIColor.white
        forfait.color            = UIColor.white
        forfait.borderColor      = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
        forfait.borderWidth      = 2
        // check1.cornerRadius     = check1.frame.height / 2
        forfait.checkboxValueChangedBlock = {
            isOn in
            print("Custom checkbox is \(isOn ? "ON" : "OFF")")
            if isOn {
                self.forfait.borderWidth      = 0
                self.forfait.borderColor      = UIColor.white
            } else{
                self.forfait.borderWidth      = 2
                self.forfait.borderColor      = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
            }
        }
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            self.dateDep.text = "Départ le : " + dateDepart
        }else {
            self.dateDep.text = "Departure on : " + dateDepart
        }
        var s = heureDepart
        let toArray = s.components(separatedBy: "/")
        let backToString = toArray.joined(separator: ":")
        let sr = dateDepart
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            heureDep.text = " à : " + backToString.prefix(5)
        }
        else {
            heureDep.text = " at : " + backToString.prefix(5)
        }
        
        let heure = String(self.prestation["duree"].intValue / 60)
        var minute = String (self.prestation["duree"].intValue - ( (self.prestation["duree"].intValue / 60) * 60 ))
        if (minute == "0")
        {
            minute = ""
        }
        print ("dureeee",self.prestation["duree"].intValue)
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            duree.text = "Durée : " + heure + "h" + minute
        }else {
            duree.text = "Duration : " + heure + "h" + minute
        }
        
        
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            nbrPlaceDispo.text = self.prestation["nombre_participant"].stringValue + " pers. / "
            nbrPlaceRest.text = " " + String(self.prestation["nombre_participant"].intValue - 1) + " restante(s)"
            
            
        }else {
            
            nbrPlaceDispo.text = self.prestation["nombre_participant"].stringValue + " pers. / "
            nbrPlaceRest.text = " " + String(self.prestation["nombre_participant"].intValue - 1) + " remaining"
            
            
            
        }
        
        stationTitle.text = self.nomStation
        /*   if let urlImgUser = URL(string : self.prestation[0]["station"]["photo"].stringValue) , let placeholder =  UIImage(named: "station") {
         // imageStation.setImage(withUrl: urlImgUser , placeholder: placeholder)
         }*/
        lessonName.text = self.prestation["titre"].stringValue
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            btnSport.setTitle(self.prestation["sport"]["pratique"].stringValue, for: .normal)
            
        }else {
            switch self.prestation["sport"]["pratique"].stringValue {
            case SportSaifFR[0] :
                btnSport.setTitle(SportSaifEN[0], for: .normal)
            case SportSaifFR[1] :
                
                btnSport.setTitle(SportSaifEN[1], for: .normal)
            case SportSaifFR[2] :
                
                btnSport.setTitle(SportSaifEN[2], for: .normal)
            case SportSaifFR[3] :
                
                btnSport.setTitle(SportSaifEN[3], for: .normal)
            case SportSaifFR[4] :
                
                btnSport.setTitle(SportSaifEN[4], for: .normal)
            case SportSaifFR[5] :
                
                btnSport.setTitle(SportSaifEN[5], for: .normal)
            default :
                
                btnSport.setTitle("", for: .normal)
            }
        }
        if ((self.btnSport.title(for: .normal)?.count)! > 19)
        {
            btnSportHeight.constant = 200
        }else if ((self.btnSport.title(for: .normal)?.count)! > 15)
        {
            btnSportHeight.constant = 150
        }else if ((self.btnSport.title(for: .normal)?.count)! > 10) {
            btnSportHeight.constant = 120
        }
        else {
            btnSportHeight.constant = 100
        }
        if ( self.prestation["sport"]["niveau"].stringValue == "Confirmé" )
        {
            btnNiveau.setTitleColor(UIColor("#FF3E53"), for: .normal)
        }
        else if (self.prestation["sport"]["niveau"].stringValue == "Débutant")
        {
            btnNiveau.setTitleColor(UIColor("#D3D3D3"), for: .normal)
        }
        else if (self.prestation["sport"]["niveau"].stringValue == "Intermédiaire")
        {
            btnNiveau.setTitleColor(UIColor("#75A7FF"), for: .normal)
        }
        else if (self.prestation["sport"]["niveau"].stringValue == "Débrouillard" ){
            btnNiveau.setTitleColor(UIColor("#76EC9E"), for: .normal)
        }
        else {
            btnNiveau.setTitleColor(UIColor("#4D4D4D"), for: .normal)
        }
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            btnNiveau.setTitle(self.prestation["sport"]["niveau"].stringValue, for: .normal)
        }else {
            switch self.prestation["sport"]["niveau"].stringValue {
            case NiveauSaifFR[0] :
                btnNiveau.setTitle(NiveauSaifEN[0], for: .normal)
            case NiveauSaifFR[1] :
                
                btnNiveau.setTitle(NiveauSaifEN[1], for: .normal)
            case NiveauSaifFR[2] :
                
                btnNiveau.setTitle(NiveauSaifEN[2], for: .normal)
            case NiveauSaifFR[3] :
                
                btnNiveau.setTitle(NiveauSaifEN[3], for: .normal)
            case NiveauSaifFR[4] :
                
                btnNiveau.setTitle(NiveauSaifEN[4], for: .normal)
                
            default :
                
                btnNiveau.setTitle("", for: .normal)
            }
            
            
        }
        var long = ""
        var lat = ""
        if (self.prestation["prestataire"]["point"]["longitude"].stringValue.prefix(1) == " ")
        {
            
            long = String(self.prestation["prestataire"]["point"]["longitude"].stringValue.dropFirst())
            
        }
        if ( self.prestation["prestataire"]["point"]["latitude"].stringValue.suffix(1) == " ")
        {
            lat = String (self.prestation["prestataire"]["point"]["latitude"].stringValue.dropLast())
        }
        
        let point = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!)
        let annotation = ColorPointAnnotation2(pinColor: UIColor.blue)
        annotation.coordinate = point
        self.map.delegate = self
        self.map.addAnnotation(annotation)
        self.map.setCenter(point, animated: true)
        self.btnPrixMin.setTitle(self.prestation["tarifs"][0]["prix_min"].stringValue + " € TTC min" , for: .normal)
        self.btnPrixMax.setTitle(self.prestation["tarifs"][0]["prix_max"].stringValue + " € TTC max", for: .normal)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        configureTileOverlay()
        // Do any additional setup after loading the view.
    }
    private func configureTileOverlay() {
        // We first need to have the path of the overlay configuration JSON
        guard let overlayFileURLString = Bundle.main.path(forResource: "Overlay", ofType: "json") else {
            return
        }
        let overlayFileURL = URL(fileURLWithPath: overlayFileURLString)
        
        // After that, you can create the tile overlay using MapKitGoogleStyler
        guard let tileOverlay = try? MapKitGoogleStyler.buildOverlay(with: overlayFileURL) else {
            return
        }
        
        // And finally add it to your MKMapView
        map.add(tileOverlay)
    }
    /*
     *custom image of pinView
     *
     */
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let saifPin =  MKAnnotationView(annotation: annotation, reuseIdentifier: "PINA")
        saifPin.isEnabled = true
        saifPin.canShowCallout = true
        
        saifPin.image = UIImage(named: "Pin_saif")
        
        // saifPin.tintColor = MKAnnotationView.blueColor()
        // saifPin.tintAdjustmentMode = .normal
        return saifPin
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     *back action
     *
     */
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
     *go to next step
     *
     */
    @IBAction func valider(_ sender: Any) {
        if ( self.massif.text! != "" && details.text != "" && details.text != Localization("DetailsLeçon"))
        {
            let view = self.storyboard?.instantiateViewController(withIdentifier: "Panier") as! PanierKissouViewController
            view.activity = self.prestation
            view.heureDep = self.heureDepart
            view.dateDep = self.dateDepart
            view.nombreMinParticipant = Int(self.massif.text!)!
            view.detailsActivity = self.details.text
            view.newLesson = newLesson
            view.participants = participants
            view.titleLec = self.prestation["titre"].stringValue
            self.navigationController?.pushViewController(view, animated: true)
        }
        else {
            
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    /*
     *configure texts
     *
     */
    func configureViewFromLocalisation() {
        titre.text = Localization("CréerLeçon2Titre")
        labelInformation.text = Localization("information")
        massif_downPicker.setPlaceholder(Localization("NbrParticipant"))
        labelPrix.text = Localization("Prix")
        labelInfoComplementaire.text = Localization("InfoComplementaire")
        // labelparticipant.text = Localization("LabelParticipant")
        //labelNbrParticipant.text = Localization("placeRestant")
        labelnbrPArticipMin.text = Localization("NbrParticipantMin")
        labelDetailsLeçon.text = Localization("DetailsLeçon")
        details.text = Localization("DetailsTextView")
        labelForfait.text = Localization("forfait")
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            massif_downPicker.setToolbarDoneButtonText("Terminé")
            massif_downPicker.setToolbarCancelButtonText("Annuler")
            
            
        }else {
            massif_downPicker.setToolbarDoneButtonText("Done")
            massif_downPicker.setToolbarCancelButtonText("Cancel")
            
        }
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        print(Localization("DetailsLeçon"))
        if  details.text == Localization("DetailsTextView")
        {
            details.text = ""
            details.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        if details.text == "" {
            details.textColor = UIColor.lightGray
            details.text = Localization("DetailsTextView")
            details.resignFirstResponder()
        }
    }
}
class ColorPointAnnotation2: MKPointAnnotation {
    var pinColor: UIColor
    
    init(pinColor: UIColor) {
        self.pinColor = pinColor
        super.init()
    }
}

