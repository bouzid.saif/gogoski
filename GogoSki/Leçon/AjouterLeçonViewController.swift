//
//  AjouterLeçonViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 18/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import DownPicker
import SwiftyJSON
import Alamofire
class AjouterLec_onViewController: InternetCheckViewControllers , UITableViewDelegate , UITableViewDataSource{
    @IBOutlet weak var imageToProfileNavBar: RoundedUIImageView!
    /*
     *Level in French
     *
     */
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     *Level in English
     *
     */
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     *Practice in French
     *
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     *Practice in English
     *
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     *Level in French
     *
     */
    let NiveauSaifFR2 = ["","Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     *Level in English
     *
     */
    let NiveauSaifEN2 = ["","Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     *Practice in French
     *
     */
    let SportSaifFR2 = ["","Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     *Practice in English
     *
     */
    let SportSaifEN2 = ["","Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     *lesson data
     *
     */
    var leçon : JSON = []
    /*
     *Praticiapants data
     *
     */
    var participants : JSON = []
    /*
     *test if this activity was created byt the user or by the renew button
     *
     */
    var newLesson : Bool = true
    /*
     *Practice type TF
     *
     */
    @IBOutlet weak var typePratique: PaddingTextField!
    /*
     *Practice type picker
     *
     */
    var typePratique_downPicker : DownPicker!
    /*
     *department TF
     *
     */
    @IBOutlet weak var departement: PaddingTextField!
    /*
     *department picker
     *
     */
    var departement_downPicker : DownPicker!
    /*
     *level TF
     *
     */
    @IBOutlet weak var niveau: PaddingTextField!
    /*
     *Level picker
     *
     */
    var niveau_downPicker : DownPicker!
    /*
     *Station TF
     *
     */
    @IBOutlet weak var station: PaddingTextField!
    /*
     *Station picker
     *
     */
    var station_downPicker : DownPicker!
    /*
     *from hour TF
     *
     */
    @IBOutlet weak var heureDep: PaddingTextField!
    /*
     *title label
     *
     */
    @IBOutlet weak var titre: UILabel!
    /*
     *providers data
     *
     */
    var prestataires : JSON = []
    /*
     *go to the next step
     *
     */
    @IBOutlet weak var btnCreer: UIButton!
    /*
     *massif picker
     *
     */
    var massif_downPicker : DownPicker!
    /*
     *massif TF
     *
     */
    @IBOutlet weak var massif: PaddingTextField!
    /*
     *activity duration picker
     *
     */
    var dureeActivite_downPicker : DownPicker!
    /*
     *activity duration TF
     *
     */
    @IBOutlet weak var dureeActivite: PaddingTextField!
    /*
     *the view where it combine the place and the duration
     *
     */
    @IBOutlet weak var placeDuree: UIView!
    /*
     *provider choice picker
     *
     */
    var choixPrestataire_downPicker : DownPicker!
    /*
     *provider choice TF
     *
     */
    @IBOutlet weak var choixPrestataire: PaddingTextField!
    /*
     *from date TF
     *
     */
    @IBOutlet weak var dateDep: PaddingTextField!
    /*
     *the view wher it combine the practice and his level
     *
     */
    @IBOutlet weak var typePratiqueNiveau: UIView!
    /*
     *participants number picker
     *
     */
    var nbrPaticipant_downPicker : DownPicker!
    /*
     *Participatns number TF
     *
     */
    @IBOutlet weak var nbrPaticipant: PaddingTextField!
    /*
     *benefits data
     *
     */
    var prestations : JSON = []
    /*
     *language picker
     *
     */
    var langue_downPicker : DownPicker!
    /*
     *language TF
     *
     */
    @IBOutlet weak var langue: PaddingTextField!
    /*
     *stations data
     *
     */
    var StationObject : JSON = []
    /*
     *duration activity picker
     *
     */
    var bandArray5 : NSMutableArray = []
    /*
     *massif array
     *
     */
    var massifArray : [String] = []
    /*
     *date picker
     *
     */
    let datePicker = UIDatePicker()
    /*
     *hours picker
     *
     */
    let datePicker1 = UIDatePicker()
    /*
     *latitude received from the preview view
     *
     */
    var latitude = ""
    /*
     *longitude received from the preview view
     *
     */
    var longitude = ""
    /*
     *station title received from the preview view
     *
     */
    var TitreStations = ""
    /*
     *massif title received from the preview view
     *
     */
    var TitreMassif = ""
    /*
     *department  received from the preview view
     *
     */
    var TitreDepart = ""
    /*
     *id of the appointment    received from the preview view
     *
     */
    var id = ""
    var dateSelected : Date = Date()
    @IBOutlet weak var scroll: UIScrollView!
    var test = true
    @IBOutlet weak var tableviewHeigh: NSLayoutConstraint!
    @IBOutlet weak var tableviewConstraint: NSLayoutConstraint!
    /*
     *the filtre view
     *
     */
    @IBOutlet weak var filtre: UIView!
    @IBOutlet weak var filterConstraint: NSLayoutConstraint!
    /*
     *the filter button
     *
     */
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if let urlImgUser = URL(string : a["user"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imageToProfileNavBar.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        loadData()
        LoadPrestataires()
        showDatePicker()
        //showTimePicker()
        heureDep.isUserInteractionEnabled = false
        btnCreer.isEnabled = false
        typePratiqueNiveau.layer.cornerRadius = 10
        placeDuree.layer.cornerRadius = 10
        
        
        let img2 = UIImageView()
        img2.image = UIImage(named: "calender_ahmed")
        img2.frame = CGRect(x: CGFloat(dateDep.frame.size.width - 20), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        dateDep.rightView = img2
        dateDep.rightViewMode = .always
        
        let img3 = UIImageView()
        img3.image = UIImage(named: "time_ahmed")
        img3.frame = CGRect(x: CGFloat(heureDep.frame.size.width - 20), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        heureDep.rightView = img3
        heureDep.rightViewMode = .always
        
        let bandArray : NSMutableArray = []
        bandArray.add("Ski Alpin")
        bandArray.add("Snowboard")
        bandArray.add("Ski de fond")
        bandArray.add("Handiski")
        bandArray.add("Raquette")
        bandArray.add("Ski de randonnée")
        //self.down = UIDownPicker(data: bandArray as! [Any])
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            typePratique_downPicker = DownPicker(textField: typePratique, withData: SportSaifFR2)
        }else {
            typePratique_downPicker = DownPicker(textField: typePratique, withData: SportSaifEN2)
        }
        typePratique_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        
        
        let bandArray1 : NSMutableArray = []
        bandArray1.add("Departement 1")
        bandArray1.add("Departement 2")
        departement_downPicker = DownPicker(textField: departement, withData: bandArray1 as! [Any])
        departement_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        
        let bandArray2 : NSMutableArray = []
        bandArray2.add("Débutant")
        bandArray2.add("Intermédiaire")
        bandArray2.add("Confirmé")
        bandArray2.add("Expert")
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            niveau_downPicker = DownPicker(textField: niveau, withData: NiveauSaifFR2)
        } else {
            niveau_downPicker = DownPicker(textField: niveau, withData: NiveauSaifEN2)
        }
        niveau_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        
        let bandArray3 : NSMutableArray = []
        bandArray3.add("station 1")
        bandArray3.add("station 2")
        station_downPicker = DownPicker(textField: station, withData: bandArray3 as! [Any])
        station_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        
        let bandArray4 : NSMutableArray = []
        bandArray4.add("massif 1")
        bandArray4.add("massif 2")
        massif_downPicker = DownPicker(textField: massif, withData: bandArray4 as! [Any])
        massif_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        
        
        dureeActivite_downPicker = DownPicker(textField: dureeActivite, withData: bandArray5 as! [Any])
        dureeActivite_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        /*  let ab = UserDefaults.standard.value(forKey: "User") as! String
         let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
         var a = JSON(data: dataFromString!)
         let header: HTTPHeaders = [
         "Content-Type" : "application/json",
         "x-Auth-Token" : a["value"].stringValue
         ]
         Alamofire.request(ScriptBase.sharedInstance.listePrest , method: .get, encoding: JSONEncoding.default,headers : header)
         .responseJSON { response in
         } */
        let bandArray6 : NSMutableArray = []
        bandArray6.add("choixPrestataire 1")
        bandArray6.add("choixPrestataire 2")
        choixPrestataire_downPicker = DownPicker(textField: choixPrestataire, withData: bandArray6 as! [Any])
        choixPrestataire_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        let bandArray7 : NSMutableArray = []
        for i in 2...20 {
            bandArray7.add(String(i))
        }
        
        nbrPaticipant_downPicker = DownPicker(textField: nbrPaticipant, withData: bandArray7 as! [Any])
        nbrPaticipant_downPicker.setArrowImage(UIImage(named:"downArrow"))
        nbrPaticipant_downPicker.updateTextFieldValueOnlyWhenDonePressed = true
        
        let bandArray9 : NSMutableArray = []
        bandArray9.add(Localization("French_fr"))
        bandArray9.add(Localization("English_en"))
        
        //self.down = UIDownPicker(data: bandArray as! [Any])
        langue_downPicker = DownPicker(textField: langue, withData: bandArray9 as! [Any])
        langue_downPicker.setArrowImage(UIImage(named:"downArrow"))
        langue_downPicker.updateTextFieldValueOnlyWhenDonePressed = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        
        massif_downPicker.addTarget(self, action: #selector(self.massifDidChange), for : .editingDidEnd )
        departement_downPicker.addTarget(self, action: #selector(self.departementDidChange), for : .editingDidEnd )
        station_downPicker.addTarget(self, action: #selector(self.stationDidChange), for : .editingDidEnd )
        niveau_downPicker.addTarget(self, action: #selector(self.TextfiedDidChange), for : .editingDidEnd )
        dureeActivite_downPicker.addTarget(self, action: #selector(self.TextfiedDidChange), for : .editingDidEnd )
        langue.addTarget(self, action: #selector(self.TextfiedDidChange), for : .valueChanged )
        typePratique_downPicker.addTarget(self, action: #selector(self.TextfiedDidChange), for : .editingDidEnd )
        choixPrestataire_downPicker.addTarget(self, action: #selector(self.TextfiedDidChange), for : .editingDidEnd )
        nbrPaticipant_downPicker.addTarget(self, action: #selector(self.TextfiedDidChange), for : .editingDidEnd)
        
        niveau_downPicker.updateTextFieldValueOnlyWhenDonePressed = true
        station_downPicker.updateTextFieldValueOnlyWhenDonePressed = true
        typePratique_downPicker.updateTextFieldValueOnlyWhenDonePressed = true
        nbrPaticipant_downPicker.updateTextFieldValueOnlyWhenDonePressed = true
        langue_downPicker.updateTextFieldValueOnlyWhenDonePressed = true
        if ( leçon.arrayObject?.count != 0)
        {
            print ( "waaaa : " , leçon["prestation"]["prestataire"]["point"]["station"]["massif"].stringValue)
            massif.text = leçon["prestation"]["prestataire"]["point"]["station"]["massif"].stringValue
            departement.text = leçon["prestation"]["prestataire"]["point"]["station"]["dept"].stringValue
            station.text = leçon["prestation"]["prestataire"]["point"]["station"]["nom"].stringValue
            nbrPaticipant.text = leçon["nb_place_maximum"].stringValue
            langue.text = leçon["langue"].stringValue
            let heure = String(leçon["duree"].intValue / 60)
            var minute = String (leçon["duree"].intValue - ( (leçon["duree"].intValue / 60) * 60 ))
            if (minute == "0")
            {
                minute = "00"
            }
            if ( minute.characters.count == 1 )
            {
                minute = minute + "0"
            }
            if (heure.characters.count == 2)
            {
                dureeActivite.text = heure + ":" + minute
            }
            else {
                dureeActivite.text = "0" + heure + ":" + minute
            }
            choixPrestataire.text = leçon["prestation"]["prestataire"]["nom_etablissement"].stringValue
            massif.isUserInteractionEnabled = false
            departement.isUserInteractionEnabled = false
            station.isUserInteractionEnabled = false
            typePratique.isUserInteractionEnabled = false
            niveau.isUserInteractionEnabled = false
            nbrPaticipant.isUserInteractionEnabled = false
            //langue.isUserInteractionEnabled = false
            dureeActivite.isUserInteractionEnabled = false
            choixPrestataire.isUserInteractionEnabled = false
            participants = leçon["list_participant"]
            if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr" {
                switch leçon["sport"]["pratique"].stringValue {
                case SportSaifFR[0] :
                    
                    typePratique.text = SportSaifEN[0]
                case SportSaifFR[1] :
                    typePratique.text = SportSaifEN[1]
                    
                case SportSaifFR[2] :
                    typePratique.text = SportSaifEN[2]
                    
                case SportSaifFR[3] :
                    typePratique.text  = SportSaifEN[3]
                    
                case SportSaifFR[4] :
                    typePratique.text = SportSaifEN[4]
                case SportSaifFR[5] :
                    typePratique.text = SportSaifEN[5]
                    
                default :
                    
                    typePratique.text  = ""
                    
                }
                switch leçon["sport"]["niveau"].stringValue {
                case NiveauSaifFR[0] :
                    
                    niveau.text = NiveauSaifEN[0]
                case NiveauSaifFR[1] :
                    niveau.text = NiveauSaifEN[1]
                    
                case NiveauSaifFR[2] :
                    niveau.text = NiveauSaifEN[2]
                    
                case NiveauSaifFR[3] :
                    niveau.text = NiveauSaifEN[3]
                case NiveauSaifFR[4] :
                    niveau.text = NiveauSaifEN[4]
                    
                    
                default :
                    
                    niveau.text = ""
                    
                }
            }else {
                typePratique.text = leçon["sport"]["pratique"].stringValue
                niveau.text = leçon["sport"]["niveau"].stringValue
            }
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        btnCreer.setBackgroundImage(UIImage(named : "Rectangle1_ahmed"), for: .normal)
        btnCreer.isEnabled = true
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        btnCreer.setBackgroundImage(UIImage(named :"RectangleGris_ahmed"), for: .normal)
        btnCreer.isEnabled = false
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        LoaderAlert.dismiss()
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        let dateLioum = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        let result = formatter.string(from: dateLioum)
        let calendar = Calendar.current
        let hour = String(calendar.component(.hour, from: dateLioum))
        let minutes = String(calendar.component(.minute, from: dateLioum))
        if hour == "19" && Int(minutes)! > 0
        {
            let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: formatter.date(from: result)!)
            datePicker.minimumDate = tomorrow
        }else {
            datePicker.minimumDate = formatter.date(from: result)
        }
        //datePicker.minimumDate = formatter.date(from: result)
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            datePicker.locale = NSLocale(localeIdentifier: "fr_FR") as Locale
            let doneButton = UIBarButtonItem(title: "Terminé", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.donedatePicker))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Annuler", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelDatePicker))
            toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        }else {
            datePicker.locale = NSLocale(localeIdentifier: "en_EN") as Locale
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.donedatePicker))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelDatePicker))
            toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        }
        
        dateDep.inputAccessoryView = toolbar
        dateDep.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        dateDep.text = formatter.string(from: datePicker.date)
        dateSelected = datePicker.date
        heureDep.isUserInteractionEnabled = true
        showTimePicker()
        self.view.endEditing(true)
        searchPrestation()
        // dateDep.addTarget(self, action: #selector(self.TextfiedDidChange), for : .valueChanged )
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func showTimePicker(){
        let date = Date()
        //Formate Date
        datePicker1.datePickerMode = .time
        datePicker1.minuteInterval = 30
        let formatter = DateFormatter()
        //   formatter.timeStyle = .medium
        formatter.dateFormat = "HH:mm"
        formatter.timeZone = TimeZone(identifier: "GMT+1")
        //var s = formatter.date(from: "08:00")
        //datePicker1.minimumDate = s
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "dd/MM/yyyy"
        var dateSelectedString = formatter1.string(from: dateSelected)
        var dateLioum = formatter1.string(from: date)
        var s = formatter.date(from: "08:00")
        if dateSelectedString == dateLioum
        {
            let calendar = Calendar.current
            var hour = String(calendar.component(.hour, from: date))
            let minutes = String(calendar.component(.minute, from: date))
            if ( hour == "19" && minutes == "0"){
                s = formatter.date(from: "19:00")!
            }else if Int(hour)! < 19 && Int(hour)! > 8 {
                if ( hour == "8" || hour == "9"){
                    hour = "0" + hour
                }
                if ( minutes == "0") {
                    s = formatter.date(from: hour + ":00")!
                }
            }
            else if Int(hour)! > 19 {
                heureDep.isUserInteractionEnabled = false
            }
            
        }
        datePicker1.minimumDate = s
        var s2 = formatter.date(from: "19:00")
        datePicker1.maximumDate = s2
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            datePicker1.locale = NSLocale(localeIdentifier: "fr_FR") as Locale
            let doneButton = UIBarButtonItem(title: "Terminé", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneTimePicker))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Annuler", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelTimePicker))
            toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        }else {
            datePicker1.locale = NSLocale(localeIdentifier: "en_EN") as Locale
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneTimePicker))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelTimePicker))
            toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        }
        
        heureDep.inputAccessoryView = toolbar
        heureDep.inputView = datePicker1
        
    }
    
    @objc func doneTimePicker(){
        
        let formatter = DateFormatter()
        //   formatter.timeStyle = .medium
        formatter.dateFormat = "HH:mm"
        formatter.timeZone = TimeZone(identifier: "GMT+1")
        heureDep.text = formatter.string(from: datePicker1.date)
        var characters = Array(heureDep.text!)
        characters[3] = "0"
        heureDep.text = String(characters)
        
        self.view.endEditing(true)
        
        
        
        bandArray5 = []
        switch heureDep.text {
        case "19:00"? :
            bandArray5.add("01:00")
        case "18:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
        case "17:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
        case "16:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
        case "15:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
        case "14:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
            bandArray5.add("05:30")
            bandArray5.add("06:00")
        case "13:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
            bandArray5.add("05:30")
            bandArray5.add("06:00")
            bandArray5.add("06:30")
            bandArray5.add("07:00")
        case "12:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
            bandArray5.add("05:30")
            bandArray5.add("06:00")
            bandArray5.add("06:30")
            bandArray5.add("07:00")
            bandArray5.add("07:30")
            bandArray5.add("08:00")
        case "11:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
            bandArray5.add("05:30")
            bandArray5.add("06:00")
            bandArray5.add("06:30")
            bandArray5.add("07:00")
            bandArray5.add("07:30")
            bandArray5.add("08:00")
            bandArray5.add("08:30")
            bandArray5.add("09:00")
        case "10:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
            bandArray5.add("05:30")
            bandArray5.add("06:00")
            bandArray5.add("06:30")
            bandArray5.add("07:00")
            bandArray5.add("07:30")
            bandArray5.add("08:00")
            bandArray5.add("08:30")
            bandArray5.add("09:00")
            bandArray5.add("09:30")
            bandArray5.add("10:00")
        case "09:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
            bandArray5.add("05:30")
            bandArray5.add("06:00")
            bandArray5.add("06:30")
            bandArray5.add("07:00")
            bandArray5.add("07:30")
            bandArray5.add("08:00")
            bandArray5.add("08:30")
            bandArray5.add("09:00")
            bandArray5.add("09:30")
            bandArray5.add("10:00")
            bandArray5.add("10:30")
            bandArray5.add("11:00")
        case "08:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
            bandArray5.add("05:30")
            bandArray5.add("06:00")
            bandArray5.add("06:30")
            bandArray5.add("07:00")
            bandArray5.add("07:30")
            bandArray5.add("08:00")
            bandArray5.add("08:30")
            bandArray5.add("09:00")
            bandArray5.add("09:30")
            bandArray5.add("10:00")
            bandArray5.add("10:30")
            bandArray5.add("11:00")
            bandArray5.add("11:30")
            bandArray5.add("12:00")
        default : print ("hata chay")
        }
        dureeActivite_downPicker = DownPicker(textField: dureeActivite, withData: bandArray5 as! [Any])
        
        dureeActivite_downPicker.setPlaceholder(Localization("dureeActivite"))
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            dureeActivite_downPicker.setToolbarDoneButtonText("Terminé")
            dureeActivite_downPicker.setToolbarCancelButtonText("Annuler")
        }
        else {
            dureeActivite_downPicker.setToolbarDoneButtonText("Done")
            dureeActivite_downPicker.setToolbarCancelButtonText("Cancel")
        }
        dureeActivite.addTarget(self, action: #selector(self.TextfiedDidChange), for : .valueChanged )
        searchPrestation()
        
    }
    
    @objc func cancelTimePicker(){
        self.view.endEditing(true)
    }
    /*
     *to show or not the whole filter
     *
     */
    @IBAction func filtrer(_ sender: Any) {
        if (test)
        {
            
            UIView.animate(withDuration: 0.8 , delay: 0 , options: .curveEaseOut ,animations: {
                self.btnFilter.isEnabled = false
                //   self.filterConstraint.constant = 0
                //  self.tableviewConstraint.constant = 25
                self.btnFilter.frame.origin.y -= self.filtre.frame.size.height
                self.tableview.frame.origin.y -= self.filtre.frame.size.height
                //  self.tableview.frame.size.height += self.filtre.frame.size.height
                
                // print (self.btnFilter.frame.origin.y)
                //   self.tableview.setContentOffset(CGPoint.zero, animated: true)
                self.scroll.setContentOffset(
                    CGPoint(x: 0,y: -self.scroll.contentInset.top),
                    animated: true)
                self.btnFilter.setBackgroundImage(UIImage(named: "btndérouleur1_ahmed"), for: .normal)
                //   self.tableview.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: true)
                self.filtre.isHidden = true
            }, completion: { _ in
                
                //  self.filterConstraint.constant = 0
                self.btnFilter.isEnabled = true
                self.tableviewConstraint.constant = 160
                self.tableviewHeigh.constant = 200
            })
            
            test = false
            
        }
        else {
            
            //self.tableview.beginUpdates()
            
            // self.tableview.updateConstraints()
            // self.tableview.endUpdates()
            
            UIView.animate(withDuration: 0.8 , delay: 0 , options: .curveEaseIn ,animations: {
                self.btnFilter.isEnabled = false
                //   self.filterConstraint.constant = 100
                // self.tableviewConstraint.constant = self.filterConstraint.constant + 8
                self.tableview.frame.origin.y += self.filtre.frame.size.height
                self.btnFilter.frame.origin.y += self.filtre.frame.size.height
                //  print (self.btnFilter.frame.origin.y)
                
                self.btnFilter.setBackgroundImage(UIImage(named: "btndérouleur_ahmed"), for: .normal)
                
                //   self.tableviewConstraint.constant = self.filtre.frame.size.height + 8
            }, completion: { _ in
                // self.filterConstraint.constant = 546
                //self.tableviewConstraint.constant = 25
                self.filtre.isHidden = false
                self.btnFilter.isEnabled = true
                self.tableviewConstraint.constant = self.filterConstraint.constant + 24
                self.tableviewHeigh.constant = 200
            })
            test = true
            
            
            
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (prestations.arrayObject?.count == 0)
        {
            return 0
        }else {
            return 1
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath)
        let labelSnowboard : UILabel = cell.viewWithTag(1) as! UILabel
        labelSnowboard.layer.cornerRadius = 5
        labelSnowboard.layer.masksToBounds = true
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            labelSnowboard.text = self.prestations[indexPath.row]["sport"]["pratique"].stringValue
            
        }else {
            switch self.prestations[indexPath.row]["sport"]["pratique"].stringValue {
            case SportSaifFR[0] :
                
                labelSnowboard.text = SportSaifEN[0]
            case SportSaifFR[1] :
                labelSnowboard.text = SportSaifEN[1]
                
            case SportSaifFR[2] :
                labelSnowboard.text = SportSaifEN[2]
                
            case SportSaifFR[3] :
                labelSnowboard.text = SportSaifEN[3]
                
            case SportSaifFR[4] :
                labelSnowboard.text = SportSaifEN[4]
            case SportSaifFR[5] :
                labelSnowboard.text = SportSaifEN[5]
                
            default :
                
                labelSnowboard.text = ""
                
            }
        }
        if ( self.prestations[indexPath.row]["sport"]["niveau"].stringValue == "Confirmé" )
        {
            labelSnowboard.backgroundColor = UIColor("#FF3E53")
        }
        else if (self.prestations[indexPath.row]["sport"]["niveau"].stringValue == "Débutant")
        {
            labelSnowboard.backgroundColor = UIColor("#D3D3D3")
        }
        else if (self.prestations[indexPath.row]["sport"]["niveau"].stringValue == "Intermédiaire")
        {
            labelSnowboard.backgroundColor = UIColor("#75A7FF")
        }
        else if (self.prestations[indexPath.row]["sport"]["niveau"].stringValue == "Débrouillard"){
            labelSnowboard.backgroundColor = UIColor("#76EC9E")
        }
        else {
            labelSnowboard.backgroundColor = UIColor("#4D4D4D")
        }
        let dateHeure : UILabel = cell.viewWithTag(2) as! UILabel
        let duree : UILabel = cell.viewWithTag(3) as! UILabel
        let place : UILabel = cell.viewWithTag(4) as! UILabel
        //  let placeRestantes : UILabel = cell.viewWithTag(5) as! UILabel
        let labelmin : UILabel = cell.viewWithTag(6) as! UILabel
        let labelmax : UILabel = cell.viewWithTag(7) as! UILabel
        let labelStation : UILabel = cell.viewWithTag(8) as! UILabel
        let imgUser : UIImageView = cell.viewWithTag(10) as! UIImageView
        var s = heureDep.text!
        let toArray = s.components(separatedBy: "/")
        let backToString = toArray.joined(separator: ":")
        let sr = dateDep.text!
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            dateHeure.text = sr + " à " + backToString
            dateHeure.adjustsFontSizeToFitWidth = true
        }
        else {
            dateHeure.text = sr + " at " + backToString
            dateHeure.adjustsFontSizeToFitWidth = true
        }
        
        let heure = String(self.prestations[indexPath.row]["duree"].intValue / 60)
        var minute = String (self.prestations[indexPath.row]["duree"].intValue - ( (self.prestations[indexPath.row]["duree"].intValue / 60) * 60 ))
        if (minute == "0")
        {
            minute = ""
        }
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            duree.text = "Durée : " + heure + "h" + minute
        }else {
            duree.text = "Duration : " + heure + "h" + minute
        }
        
        
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            //    if (self.prestations[indexPath.row]["nb_place_maximum"].intValue - self.prestations[indexPath.row]["nb_participant"].intValue) > 0 {
            place.text = self.prestations[indexPath.row]["nombre_participant"].stringValue + " personnes "
            //      placeRestantes.text = " " + String(self.prestations[indexPath.row]["nb_place_maximum"].intValue - self.prestations[indexPath.row]["nb_participant"].intValue) + " restante(s)"
            //   } else {
            //     place.text = self.prestations[indexPath.row]["nb_place_maximum"].stringValue + " pers. / "
            //   placeRestantes.text = " " + "0" + " restante(s)"
            //   }
            
        }else {
            //  if (self.prestations[indexPath.row]["nb_place_maximum"].intValue - self.prestations[indexPath.row]["participants"].count) > 0 {
            place.text = self.prestations[indexPath.row]["nombre_participant"].stringValue + " persons "
            //    placeRestantes.text = " " + String(self.prestations[indexPath.row]["nb_place_maximum"].intValue - self.prestations[indexPath.row]["nb_participant"].intValue) + " remaining"
            // } else {
            //   place.text = self.prestations[indexPath.row]["nb_place_maximum"].stringValue + " pers. / "
            //  placeRestantes.text = " " + "0" + " remaining"
            // }
            
            
        }
        
        labelStation.text = self.station.text
        labelStation.numberOfLines = 2
        labelStation.lineBreakMode = .byWordWrapping
        /* if let urlImgUser = URL(string : self.prestations[indexPath.row]["station"]["photo"].stringValue) , let placeholder =  UIImage(named: "station") {
         imgUser.setImage(withUrl: urlImgUser , placeholder: placeholder)
         }*/
        labelmin.text = self.prestations[indexPath.row]["tarifs"][0]["prix_min"].stringValue + " € TTC"
        labelmax.text = self.prestations[indexPath.row]["tarifs"][0]["prix_max"].stringValue + " € TTC"
        labelmax.adjustsFontSizeToFitWidth = true
        labelmin.adjustsFontSizeToFitWidth = true
        labelmin.layer.cornerRadius = 5
        labelmin.layer.masksToBounds = true
        labelmax.layer.cornerRadius = 5
        labelmax.layer.masksToBounds = true
        
        return cell
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("CréerLeçon1Titre")
        departement_downPicker.setPlaceholder(Localization("Departement"))
        typePratique_downPicker.setPlaceholder(Localization("Pratique"))
        choixPrestataire_downPicker.setPlaceholder(Localization("ChoixPrestataire"))
        station_downPicker.setPlaceholder(Localization("Station"))
        massif_downPicker.setPlaceholder(Localization("Massif"))
        niveau_downPicker.setPlaceholder(Localization("Niveau"))
        nbrPaticipant_downPicker.setPlaceholder(Localization("NbrParticipant"))
        langue_downPicker.setPlaceholder(Localization("Langue"))
        dureeActivite_downPicker.setPlaceholder(Localization("dureeActivite"))
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            massif_downPicker.setToolbarDoneButtonText("Terminé")
            massif_downPicker.setToolbarCancelButtonText("Annuler")
            
            niveau_downPicker.setToolbarDoneButtonText("Terminé")
            niveau_downPicker.setToolbarCancelButtonText("Annuler")
            
            typePratique_downPicker.setToolbarDoneButtonText("Terminé")
            typePratique_downPicker.setToolbarCancelButtonText("Annuler")
            
            departement_downPicker.setToolbarDoneButtonText("Terminé")
            departement_downPicker.setToolbarCancelButtonText("Annuler")
            
            station_downPicker.setToolbarDoneButtonText("Terminé")
            station_downPicker.setToolbarCancelButtonText("Annuler")
            
            dureeActivite_downPicker.setToolbarDoneButtonText("Terminé")
            dureeActivite_downPicker.setToolbarCancelButtonText("Annuler")
            
            nbrPaticipant_downPicker.setToolbarDoneButtonText("Terminé")
            nbrPaticipant_downPicker.setToolbarCancelButtonText("Annuler")
        }else {
            massif_downPicker.setToolbarDoneButtonText("Done")
            massif_downPicker.setToolbarCancelButtonText("Cancel")
            niveau_downPicker.setToolbarDoneButtonText("Done")
            niveau_downPicker.setToolbarCancelButtonText("Cancel")
            typePratique_downPicker.setToolbarDoneButtonText("Done")
            typePratique_downPicker.setToolbarCancelButtonText("Cancel")
            departement_downPicker.setToolbarDoneButtonText("Done")
            departement_downPicker.setToolbarCancelButtonText("Cancel")
            station_downPicker.setToolbarDoneButtonText("Done")
            station_downPicker.setToolbarCancelButtonText("Cancel")
            dureeActivite_downPicker.setToolbarDoneButtonText("Done")
            dureeActivite_downPicker.setToolbarCancelButtonText("Cancel")
            nbrPaticipant_downPicker.setToolbarDoneButtonText("Done")
            nbrPaticipant_downPicker.setToolbarCancelButtonText("Cancel")
        }
        showDatePicker()
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /*
     *go to the newt step
     *
     */
    @IBAction func goCreateLesson2(_ sender: Any) {
        let view = self.storyboard?.instantiateViewController(withIdentifier: "CreerLeçon2") as! AfficheLec_onViewController
        view.prestation = self.prestations[0]
        view.nomStation = self.station.text!
        view.heureDepart = self.heureDep.text!
        view.dateDepart = self.dateDep.text!
        view.newLesson = newLesson
        view.participants = participants
        
        self.navigationController?.pushViewController(view, animated: true)
        
    }
    
    
    @objc func massifDidChange() {
        print("massif changed")
        var departementx : [String] = []
        if self.massif.text! != "" {
            for i in 0...StationObject["data"].arrayObject!.count - 1 {
                if StationObject["data"][i]["massif"].stringValue == self.massif.text! {
                    departementx.append(StationObject["data"][i]["dept"].stringValue)
                }
            }
            departementx = departementx.removeDuplicates()
            let bandArray4 : NSMutableArray = []
            bandArray4.addObjects(from: departementx)
            self.departement_downPicker = DownPicker(textField: self.departement, withData: bandArray4 as! [Any])
            departement_downPicker.addTarget(self, action: #selector(self.departementDidChange), for : .valueChanged )
            self.departement_downPicker.setPlaceholder(Localization("Departement"))
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                
                departement_downPicker.setToolbarDoneButtonText("Terminé")
                departement_downPicker.setToolbarCancelButtonText("Annuler")
            }
            else
            {
                departement_downPicker.setToolbarDoneButtonText("Done")
                departement_downPicker.setToolbarCancelButtonText("Cancel")
            }
            self.departement_downPicker.updateTextFieldValueOnlyWhenDonePressed = true
        }
    }
    @objc func departementDidChange() {
        print("massif changed")
        var stationsx : [String] = []
        print("true")
        if self.massif.text! != "" && self.departement.text! != "" {
            for i in 0...StationObject["data"].arrayObject!.count - 1 {
                print("massif:",StationObject["data"][i]["massif"].stringValue)
                print("dep:",StationObject["data"][i]["dept"].stringValue)
                if StationObject["data"][i]["massif"].stringValue == self.massif.text! && StationObject["data"][i]["dept"].stringValue == self.departement.text! {
                    print("false")
                    stationsx.append(StationObject["data"][i]["nom"].stringValue)
                }
            }
            //stationsx = stationsx.removeDuplicates()
            let bandArray4 : NSMutableArray = []
            bandArray4.addObjects(from: stationsx)
            self.station_downPicker = DownPicker(textField: self.station, withData: bandArray4 as! [Any])
            station_downPicker.addTarget(self, action: #selector(self.stationDidChange), for : .valueChanged )
            self.station_downPicker.setPlaceholder(Localization("Station"))
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                
                station_downPicker.setToolbarDoneButtonText("Terminé")
                station_downPicker.setToolbarCancelButtonText("Annuler")
            }
            else
            {
                station_downPicker.setToolbarDoneButtonText("Done")
                station_downPicker.setToolbarCancelButtonText("Cancel")
            }
            self.station_downPicker.updateTextFieldValueOnlyWhenDonePressed = true
        }
    }
    @objc func stationDidChange() {
        print("station changed")
        if self.massif.text! != "" && self.departement.text! != "" && self.station.text != "" {
            for i in 0...StationObject["data"].arrayObject!.count - 1 {
                if StationObject["data"][i]["massif"].stringValue == self.massif.text! && StationObject["data"][i]["dept"].stringValue == self.departement.text! && StationObject["data"][i]["nom"].stringValue == self.station.text{
                    print("false")
                    print("lat:",StationObject["data"][i])
                    if StationObject["data"][i]["listpointsrdv"].arrayObject?.count != 0 {
                        self.latitude = StationObject["data"][i]["listpointsrdv"][0]["latitude"].stringValue
                    }else{
                        self.latitude = "48.956467"
                    }
                    print("lat:",self.latitude)
                    if StationObject["data"][i]["listpointsrdv"].arrayObject?.count != 0 {
                        self.longitude = StationObject["data"][i]["listpointsrdv"][0]["longitude"].stringValue
                    }else {
                        self.longitude = "4.358637"
                    }
                    self.TitreStations = self.station.text!
                    self.TitreMassif = self.massif.text!
                    self.TitreDepart = self.departement.text!
                    if StationObject["data"][i]["listpointsrdv"].arrayObject?.count != 0 {
                        self.id = StationObject["data"][i]["listpointsrdv"][0]["id"].stringValue
                    }else {
                        self.id = "default"
                    }
                    
                }
            }
            station_downPicker.addTarget(self, action: #selector(self.TextfiedDidChange), for : .valueChanged )
            //stationsx = stationsx.removeDuplicates()
        }
    }
    /*
     *get all the stations,department,massif from DataBase
     *
     */
    func loadData(){
        LoaderAlert.show()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.Stations , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                self.massifArray = []
                let a = JSON(response.data)
                self.StationObject = a
                if a["status"].boolValue {
                    for i in 0...a["data"].arrayObject!.count - 1 {
                        self.massifArray.append(a["data"][i]["massif"].stringValue)
                    }
                    self.massifArray = self.massifArray.removeDuplicates()
                    print("Stations: ",a)
                    let bandArray4 : NSMutableArray = []
                    bandArray4.addObjects(from: self.massifArray)
                    self.massif_downPicker = DownPicker(textField: self.massif, withData: bandArray4 as! [Any])
                    self.massif_downPicker.addTarget(self, action: #selector(self.massifDidChange), for : .valueChanged )
                    self.massif_downPicker.setPlaceholder(Localization("Massif"))
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        
                        self.massif_downPicker.setToolbarDoneButtonText("Terminé")
                        self.massif_downPicker.setToolbarCancelButtonText("Annuler")
                    }
                    else
                    {
                        self.massif_downPicker.setToolbarDoneButtonText("Done")
                        self.massif_downPicker.setToolbarCancelButtonText("Cancel")
                    }
                    
                }
                
                
        }
    }
    /*
     *search of the benefits
     *
     */
    func searchPrestation() {
        prestations = []
        self.tableview.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            if self.massif.text != "" && self.station.text != "" && self.departement.text != "" && self.dureeActivite.text != "" && self.heureDep.text != "" && self.dateDep.text != "" && self.nbrPaticipant.text != "" &&  self.typePratique.text != "" && self.niveau.text != ""
            {
                self.massif.isEnabled = false
                self.station.isEnabled = false
                self.departement.isEnabled = false
                self.dureeActivite.isEnabled = false
                self.heureDep.isEnabled = false
                self.dateDep.isEnabled = false
                self.nbrPaticipant.isEnabled = false
                self.typePratique.isEnabled = false
                self.niveau.isEnabled = false
                self.langue.isEnabled = false
                self.choixPrestataire.isEnabled = false
                if ( self.prestations.arrayObject?.count != 0)
                {
                    self.prestations.arrayObject?.removeAll()
                }
                LoaderAlert.show()
                var str = ""
                if self.dureeActivite.text! != ""
                {
                    var minute = ""
                    var heure = ""
                    
                    
                    heure = self.dureeActivite.text!.prefix(2) + ""
                    minute = self.dureeActivite.text!.suffix(2) + ""
                    
                    str = String ( (Int (heure)! * 60) + Int (minute)!)
                }
                
                let ab = UserDefaults.standard.value(forKey: "User") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : a["value"].stringValue
                ]
                var params: Parameters = [:]
                print (self.typePratique_downPicker.selectedIndex , " " ,  self.niveau_downPicker.selectedIndex , " " , self.choixPrestataire_downPicker.selectedIndex)
                if self.typePratique_downPicker.selectedIndex != -1 && self.niveau_downPicker.selectedIndex != -1 {
                    
                    if self.choixPrestataire_downPicker.selectedIndex == -1
                    {
                        params = [
                            "pratique": self.SportSaifFR2[self.typePratique_downPicker.selectedIndex] ,
                            "niveau" : self.NiveauSaifFR2[self.niveau_downPicker.selectedIndex] ,
                            "date" : self.dateDep.text! + " " + self.heureDep.text!,
                            "massif"  : self.massif.text!,
                            "nbPlaceDispo" : self.nbrPaticipant.text! ,
                            "station" : self.station.text! ,
                            "Departement" : self.departement.text! ,
                            "duree" : str ,
                            "prestataire" : ""
                            
                        ]
                    }
                    else {
                        params = [
                            "pratique": self.SportSaifFR2[self.typePratique_downPicker.selectedIndex] ,
                            "niveau" : self.NiveauSaifFR2[self.niveau_downPicker.selectedIndex] ,
                            "date" : self.dateDep.text! + " " + self.heureDep.text! ,
                            "massif"  : self.massif.text!,
                            "nbPlaceDispo" : self.nbrPaticipant.text! ,
                            "station" : self.station.text! ,
                            "Departement" : self.departement.text! ,
                            "duree" : str ,
                            "prestataire" : self.prestataires[self.choixPrestataire_downPicker.selectedIndex-1]["id"].stringValue
                            
                        ]
                    }
                }else if  self.typePratique_downPicker.selectedIndex != -1 && self.niveau_downPicker.selectedIndex == -1 {
                    if self.choixPrestataire_downPicker.selectedIndex == -1
                    {
                        params = [
                            "pratique": self.SportSaifFR2[self.typePratique_downPicker.selectedIndex] ,
                            "niveau" : "" ,
                            "date" : self.dateDep.text! + " " + self.heureDep.text!,
                            "massif"  : self.massif.text!,
                            "nbPlaceDispo" : self.nbrPaticipant.text! ,
                            "station" : self.station.text! ,
                            "Departement" : self.departement.text! ,
                            "duree" : str ,
                            "prestataire" : ""
                            
                        ]
                    }
                    else {
                        params = [
                            "pratique": self.SportSaifFR2[self.typePratique_downPicker.selectedIndex] ,
                            "niveau" : "" ,
                            "date" : self.dateDep.text! + " " + self.heureDep.text!,
                            "massif"  : self.massif.text!,
                            "nbPlaceDispo" : self.nbrPaticipant.text! ,
                            "station" : self.station.text! ,
                            "Departement" : self.departement.text! ,
                            "duree" : str ,
                            "prestataire" : self.prestataires[self.choixPrestataire_downPicker.selectedIndex-1]["id"].stringValue
                            
                        ]
                    }
                }else if self.typePratique_downPicker.selectedIndex == -1 && self.niveau_downPicker.selectedIndex != -1 {
                    if self.choixPrestataire_downPicker.selectedIndex != -1
                    {
                        params = [
                            "pratique": "",
                            "niveau" : self.NiveauSaifFR2[self.niveau_downPicker.selectedIndex] ,
                            "date" : self.dateDep.text! + " " + self.heureDep.text!,
                            "massif"  : self.massif.text!,
                            "nbPlaceDispo" : self.nbrPaticipant.text! ,
                            "station" : self.station.text! ,
                            "Departement" : self.departement.text! ,
                            "duree" : str ,
                            "prestataire" : self.prestataires[self.choixPrestataire_downPicker.selectedIndex-1]["id"].stringValue
                            
                        ]
                    }
                    else {
                        params = [
                            "pratique": "",
                            "niveau" : self.NiveauSaifFR2[self.niveau_downPicker.selectedIndex] ,
                            "date" : self.dateDep.text! + " " + self.heureDep.text!,
                            "massif"  : self.massif.text!,
                            "nbPlaceDispo" : self.nbrPaticipant.text! ,
                            "station" : self.station.text! ,
                            "Departement" : self.departement.text! ,
                            "duree" : str ,
                            "prestataire" : ""
                            
                        ]
                    }
                } else if self.choixPrestataire_downPicker.selectedIndex == -1 {
                    params = [
                        "pratique": "",
                        "niveau" : "" ,
                        "date" : self.dateDep.text! + " " + self.heureDep.text!,
                        "massif"  : self.massif.text!,
                        "nbPlaceDispo" : self.nbrPaticipant.text! ,
                        "station" : self.station.text! ,
                        "Departement" : self.departement.text! ,
                        "duree" : str ,
                        "prestataire" : ""
                        
                    ]
                }
                else {
                    params = [
                        "pratique": "",
                        "niveau" : "" ,
                        "date" : self.dateDep.text! + " " + self.heureDep.text!,
                        "massif"  : self.massif.text!,
                        "nbPlaceDispo" : self.nbrPaticipant.text! ,
                        "station" : self.station.text! ,
                        "Departement" : self.departement.text! ,
                        "duree" : str ,
                        "prestataire" : self.prestataires[self.choixPrestataire_downPicker.selectedIndex-1]["id"].stringValue
                        
                    ]
                }
                print ("paraaaaams : " , params)
                Alamofire.request(ScriptBase.sharedInstance.searchPrestation  , method: .post, parameters: params , encoding: JSONEncoding.default, headers : header)
                    .responseJSON { response in
                        
                        let b = JSON(response.data)
                        print(response)
                        LoaderAlert.dismiss()
                        self.massif.isEnabled = true
                        self.station.isEnabled = true
                        self.departement.isEnabled = true
                        self.dureeActivite.isEnabled = true
                        self.heureDep.isEnabled = true
                        self.dateDep.isEnabled = true
                        self.nbrPaticipant.isEnabled = true
                        self.typePratique.isEnabled = true
                        self.niveau.isEnabled = true
                        self.langue.isEnabled = true
                        self.choixPrestataire.isEnabled = true
                        if b["status"].boolValue {
                            
                            
                            if ( b["data"] != "")
                            {
                                self.prestations.appendIfArray(json: b["data"])
                                self.tableview.reloadData()
                                print (" aaab ::" , self.prestations)
                            }
                            
                            
                        }else {
                            
                        }
                        
                        
                }
            }
        }
        
        /*  else {
         if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
         _ = SweetAlert().showAlert(Localization("LeçonTitre"), subTitle: "Veuillez Remplir Tous les champs" ,style: AlertStyle.error)
         }else{
         _ = SweetAlert().showAlert(Localization("LeçonTitre"), subTitle: "Please fill all in the blanks" ,style: AlertStyle.error)
         }
         }*/
    }
    /*
     *here the action of the search
     *
     */
    @objc func TextfiedDidChange()
    {
        print("changed")
        searchPrestation()
        
        
    }
    /*
     *Load all the providers
     *
     */
    func LoadPrestataires()
    {
        LoaderAlert.show()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.getAllPrestataire  , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                print("prestataires:",b)
                if b["status"].boolValue {
                    
                    self.prestataires = b["data"]
                    let bandArray6 : NSMutableArray = []
                    bandArray6.add("")
                    for i in 0 ... ((self.prestataires.arrayObject?.count)! - 1)
                    {
                        
                        bandArray6.add(self.prestataires[i]["nom_etablissement"].stringValue)
                        
                        
                    }
                    self.choixPrestataire_downPicker = DownPicker(textField: self.choixPrestataire, withData: bandArray6 as! [Any])
                    self.choixPrestataire_downPicker.setArrowImage(UIImage(named:"downArrow"))
                    self.choixPrestataire_downPicker.setPlaceholder(Localization("ChoixPrestataire"))
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        
                        self.choixPrestataire_downPicker.setToolbarDoneButtonText("Terminé")
                        self.choixPrestataire_downPicker.setToolbarCancelButtonText("Annuler")
                    }
                    else
                    {
                        self.choixPrestataire_downPicker.setToolbarDoneButtonText("Done")
                        self.choixPrestataire_downPicker.setToolbarCancelButtonText("Cancel")
                    }
                    self.choixPrestataire_downPicker.updateTextFieldValueOnlyWhenDonePressed = true
                }
                
                
        }
    }
}

