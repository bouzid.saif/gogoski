//
//  LeçonViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 26/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import DownPicker
import SwiftyJSON
import Alamofire

class Lec_onViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{
    /*
     *Levels in French
     *
     */
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     *Levels in English
     *
     */
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     *Practice in French
     *
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     *Practice in English
     *
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     *Levels in French
     *
     */
    let NiveauSaifFR2 = ["","Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     *Levels in English
     *
     */
    let NiveauSaifEN2 = ["","Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     *Practice in French
     *
     */
    let SportSaifFR2 = ["","Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     *Practice in English
     *
     */
    let SportSaifEN2 = ["","Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     *Date Picker
     *
     */
    let datePicker = UIDatePicker()
    var test : Bool = false
    var first = true
    /*
     *Practice TF
     *
     */
    @IBOutlet weak var sport: PaddingTextField!
    /*
     *Practice picker
     *
     */
    var sport_downPicker : DownPicker!
    /*
     *Level TF
     *
     */
    @IBOutlet weak var niveau: PaddingTextField!
    @IBOutlet weak var tableviewConstraint: NSLayoutConstraint!
    /*
     *image of the actual user
     *
     */
    @IBOutlet weak var imageToProfileNavBar: RoundedUIImageView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var filterConstraint: NSLayoutConstraint!
    /*
     *filtre button
     *
     */
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var tableviewHeigh: NSLayoutConstraint!
    /*
     *massif array
     *
     */
    var massifArray : [String] = []
    /*
     *department array
     *
     */
    var departArray : [String] = []
    /*
     *stations array
     *
     */
    var stationArray : [String] = []
    /*
     *level picker
     *
     */
    var niveau_downPicker : DownPicker!
    /*
     *Station data
     *
     */
    var StationObject : JSON = []
    @IBOutlet weak var scroll: UIScrollView!
    /*
     *Station TF
     *
     */
    @IBOutlet weak var stations: PaddingTextField!
    /*
     *Station picker
     *
     */
    var stations_downPicker : DownPicker!
    /*
     *number of available places
     *
     */
    @IBOutlet weak var nbrPlaceDisponible: PaddingTextField!
    /*
     *number of available places picker
     *
     */
    var nbrPlaceDisponible_downPicker : DownPicker!
    /*
     *Department TF
     *
     */
    @IBOutlet weak var département: PaddingTextField!
    /*
     *department picker
     *
     */
    var département_downPicker : DownPicker!
    /*
     *Duration TF
     *
     */
    @IBOutlet weak var duree: PaddingTextField!
    /*
     *Duration picker
     *
     */
    var duree_downPicker : DownPicker!
    /*
     *massif picker
     *
     */
    var massif_downPicker : DownPicker!
    /*
     *massif TF
     *
     */
    @IBOutlet weak var massif: PaddingTextField!
    // var date_downPicker : DownPicker!
    /*
     *Date TF
     *
     */
    @IBOutlet weak var date: PaddingTextField!
    /*
     *filtre view
     *
     */
    @IBOutlet weak var filtre: UIView!
    /*
     *add button
     *
     */
    @IBOutlet weak var btnAjouter: UIButton!
    /*
     *title label
     *
     */
    @IBOutlet weak var titre: UILabel!
    /*
     *lesson data
     *
     */
    var leçon : JSON = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if first {
            NotificationCenter.default.addObserver(self, selector: #selector(self.handle_go_to_leçon(notification:)), name: NSNotification.Name(rawValue: "GotoCreate"), object: nil)
        }
        loadData()
        
        //  NotificationCenter.default.addObserver(self, selector: #selector(self.handle_go_to_sortie(notification:)), name: NSNotification.Name(rawValue: "GotoCreate"), object: nil)
        self.tableviewConstraint.constant = 120
        self.tableviewHeigh.constant += self.filtre.frame.size.height
        self.filtre.isHidden = true
        self.btnFilter.setBackgroundImage(UIImage(named: "btndérouleur1_ahmed"), for: .normal)
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if let urlImgUser = URL(string : a["user"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imageToProfileNavBar.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        showDatePicker()
        
        let bandArray : NSMutableArray = []
        bandArray.add("")
        bandArray.add("Ski alpin")
        bandArray.add("Snowboard")
        bandArray.add("Ski de randonnée")
        bandArray.add("Handiski")
        bandArray.add("Raquette")
        bandArray.add("Ski de fond")
        
        tableview.allowsMultipleSelection = false
        
        //self.down = UIDownPicker(data: bandArray as! [Any])
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            sport_downPicker = DownPicker(textField: sport, withData: SportSaifFR2)
        }else {
            sport_downPicker = DownPicker(textField: sport, withData: SportSaifEN2)
        }
        sport_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        self.sport.text = ""
        
        
        /* let bandArray1 : NSMutableArray = []
         bandArray1.add("date 1")
         bandArray1.add("date 2")
         date_downPicker = DownPicker(textField: date, withData: bandArray1 as! [Any])
         date_downPicker.setArrowImage(UIImage(named:"downArrow"))*/
        
        let img2 = UIImageView()
        img2.image = UIImage(named: "downArrow")
        img2.frame = CGRect(x: CGFloat(date.frame.size.width - 30), y: CGFloat(20), width: CGFloat(32), height: CGFloat(23))
        date.rightView = img2
        date.rightViewMode = .always
        
        let bandArray4 : NSMutableArray = []
        massif_downPicker = DownPicker(textField: massif, withData: bandArray4 as! [Any])
        massif_downPicker.setArrowImage(UIImage(named:"downArrow"))
            self.massif.text = ""
        let bandArray8 : NSMutableArray = []
        département_downPicker = DownPicker(textField: département, withData: bandArray8 as! [Any])
        département_downPicker.setArrowImage(UIImage(named:"downArrow"))
            self.département.text = ""
        let bandArray9 : NSMutableArray = []
        stations_downPicker = DownPicker(textField: stations, withData: bandArray9 as! [Any])
        stations_downPicker.setArrowImage(UIImage(named:"downArrow"))
            self.stations.text = ""
        
        
        
        let bandArray2 : NSMutableArray = []
        bandArray2.add("")
        bandArray2.add("Débutant")
        bandArray2.add("Intermédiaire")
        bandArray2.add("Confirmé")
        bandArray2.add("Expert")
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            niveau_downPicker = DownPicker(textField: niveau, withData: NiveauSaifFR2)
        }else {
            niveau_downPicker = DownPicker(textField: niveau, withData: NiveauSaifEN2)
        }
        niveau_downPicker.setArrowImage(UIImage(named:"downArrow"))
            self.niveau.text = ""
        let bandArray5 : NSMutableArray = []
        bandArray5.add("")
        bandArray5.add("01:00")
        bandArray5.add("01:30")
        bandArray5.add("02:00")
        bandArray5.add("02:30")
        bandArray5.add("03:00")
        bandArray5.add("03:30")
        bandArray5.add("04:00")
        bandArray5.add("04:30")
        bandArray5.add("05:00")
        bandArray5.add("05:30")
        bandArray5.add("06:00")
        bandArray5.add("06:30")
        bandArray5.add("07:00")
        bandArray5.add("07:30")
        bandArray5.add("08:00")
        bandArray5.add("08:30")
        bandArray5.add("09:00")
        bandArray5.add("09:30")
        bandArray5.add("10:00")
        bandArray5.add("10:30")
        bandArray5.add("11:00")
        bandArray5.add("11:30")
        bandArray5.add("12:00")
        //self.down = UIDownPicker(data: bandArray as! [Any])
        duree_downPicker = DownPicker(textField: duree, withData: bandArray5 as! [Any])
        duree_downPicker.setArrowImage(UIImage(named:"downArrow"))
        duree_downPicker.addTarget(self, action: #selector(self.TextfiedDidChange), for : .valueChanged )
            self.duree.text = ""
        let bandArray6 : NSMutableArray = []
        bandArray6.add("")
        for i in 0...20 {
            bandArray6.add(String(i))
        }
        bandArray6.add("illimité")
        
        //self.down = UIDownPicker(data: bandArray as! [Any])
        nbrPlaceDisponible_downPicker = DownPicker(textField: nbrPlaceDisponible, withData: bandArray6 as! [Any])
        nbrPlaceDisponible_downPicker.setArrowImage(UIImage(named:"downArrow"))
        nbrPlaceDisponible_downPicker.addTarget(self, action: #selector(self.TextfiedDidChange), for : .valueChanged )
            self.nbrPlaceDisponible.text = ""
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        
        
        niveau_downPicker.addTarget(self, action: #selector(self.TextfiedDidChange), for : .valueChanged )
        
        
        massif_downPicker.addTarget(self, action: #selector(self.TextfiedDidChange), for : .valueChanged )
        
        
        sport_downPicker.addTarget(self, action: #selector(self.TextfiedDidChange), for : .valueChanged )
        niveau_downPicker.updateTextFieldValueOnlyWhenDonePressed = true
        massif_downPicker.updateTextFieldValueOnlyWhenDonePressed = true
        sport_downPicker.updateTextFieldValueOnlyWhenDonePressed = true
        
    }
    /*
     *go to lesson detail
     *
     */
      @objc func handle_go_to_leçon (notification: NSNotification)
     {
     
     let q = notification.object
     //ConvId = q
     let view =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AjouterLec_onViewController") as! AjouterLec_onViewController
     view.leçon = q as! JSON
        view.newLesson = false
     self.navigationController?.pushViewController(view, animated: true)
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tableviewHeigh.constant = self.tableview.contentSize.height
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ( leçon.arrayObject?.count == 0)
        {
            return 0
        }
        else{
            return (leçon.arrayObject?.count)!
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let view = self.storyboard?.instantiateViewController(withIdentifier: "FicheLeçon") as! FicheLec_onViewController
        view.id = self.leçon[indexPath.row]["id"].stringValue
        self.navigationController?.pushViewController(view, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath)
        let labelSnowboard : UILabel = cell.viewWithTag(1) as! UILabel
        labelSnowboard.layer.cornerRadius = 5
        labelSnowboard.layer.masksToBounds = true
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            labelSnowboard.text = self.leçon[indexPath.row]["sport"]["pratique"].stringValue
            
        }else {
            switch self.leçon[indexPath.row]["sport"]["pratique"].stringValue {
            case SportSaifFR[0] :
                
                labelSnowboard.text = SportSaifEN[0]
            case SportSaifFR[1] :
                labelSnowboard.text = SportSaifEN[1]
                
            case SportSaifFR[2] :
                labelSnowboard.text = SportSaifEN[2]
                
            case SportSaifFR[3] :
                labelSnowboard.text = SportSaifEN[3]
                
            case SportSaifFR[4] :
                labelSnowboard.text = SportSaifEN[4]
            case SportSaifFR[5] :
                labelSnowboard.text = SportSaifEN[5]
                
            default :
                
                labelSnowboard.text = ""
                
            }
        }
        if ( self.leçon[indexPath.row]["sport"]["niveau"].stringValue == "Confirmé" )
        {
            labelSnowboard.backgroundColor = UIColor("#FF3E53")
        }
        else if (self.leçon[indexPath.row]["sport"]["niveau"].stringValue == "Débutant")
        {
            labelSnowboard.backgroundColor = UIColor("#D3D3D3")
        }
        else if (self.leçon[indexPath.row]["sport"]["niveau"].stringValue == "Intermédiaire")
        {
            labelSnowboard.backgroundColor = UIColor("#75A7FF")
        }
        else if (self.leçon[indexPath.row]["sport"]["niveau"].stringValue == "Débrouillard"){
            labelSnowboard.backgroundColor = UIColor("#76EC9E")
        }
        else {
            labelSnowboard.backgroundColor = UIColor("#4D4D4D")
        }
        let dateHeure : UILabel = cell.viewWithTag(2) as! UILabel
        let duree : UILabel = cell.viewWithTag(3) as! UILabel
        let place : UILabel = cell.viewWithTag(4) as! UILabel
        let placeRestantes : UILabel = cell.viewWithTag(5) as! UILabel
        let labelmin : UILabel = cell.viewWithTag(6) as! UILabel
        let labelmax : UILabel = cell.viewWithTag(7) as! UILabel
        let labelStation : UILabel = cell.viewWithTag(8) as! UILabel
        
        let imgUser : UIImageView = cell.viewWithTag(10) as! UIImageView
        var s = self.leçon[indexPath.row]["date_debut"].stringValue.suffix(8)
        let toArray = s.components(separatedBy: "/")
        let backToString = toArray.joined(separator: ":")
        let sr = self.leçon[indexPath.row]["date_debut"].stringValue.prefix(10)
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            dateHeure.text = sr + " à " + backToString.prefix(5)
        }
        else {
            dateHeure.text = sr + " at " + backToString.prefix(5)
        }
        
        let heure = String(self.leçon[indexPath.row]["duree"].intValue / 60)
        var minute = String (self.leçon[indexPath.row]["duree"].intValue - ( (self.leçon[indexPath.row]["duree"].intValue / 60) * 60 ))
        if (minute == "0")
        {
            minute = ""
        }
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            duree.text = "Durée : " + heure + "h" + minute
        }else {
            duree.text = "Duration : " + heure + "h" + minute
        }
        
        
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            if (self.leçon[indexPath.row]["nb_place_maximum"].intValue - self.leçon[indexPath.row]["nb_participant"].intValue) > 0 {
                place.text = self.leçon[indexPath.row]["nb_place_maximum"].stringValue + " pers. / "
                placeRestantes.text = " " + String(self.leçon[indexPath.row]["nb_place_maximum"].intValue - self.leçon[indexPath.row]["nb_participant"].intValue) + " restante(s)"
            } else {
                place.text = self.leçon[indexPath.row]["nb_place_maximum"].stringValue + " pers. / "
                placeRestantes.text = " " + "0" + " restante(s)"
            }
            
        }else {
            if (self.leçon[indexPath.row]["nb_place_maximum"].intValue - self.leçon[indexPath.row]["participants"].count) > 0 {
                place.text = self.leçon[indexPath.row]["nb_place_maximum"].stringValue + " pers. / "
                placeRestantes.text = " " + String(self.leçon[indexPath.row]["nb_place_maximum"].intValue - self.leçon[indexPath.row]["nb_participant"].intValue) + " remaining"
            } else {
                place.text = self.leçon[indexPath.row]["nb_place_maximum"].stringValue + " pers. / "
                placeRestantes.text = " " + "0" + " remaining"
            }
            
            
        }
        labelStation.numberOfLines = 2
        labelStation.lineBreakMode = .byWordWrapping
        labelStation.text = self.leçon[indexPath.row]["prestation"]["prestataire"]["point"]["station"]["nom"].stringValue
        imgUser.image = UIImage(named : "station")
         if let urlImgUser = URL(string : self.leçon[indexPath.row]["prestation"]["prestataire"]["photo"].stringValue) , let placeholder =  UIImage(named: "station") {
         imgUser.setImage(withUrl: urlImgUser , placeholder: placeholder)
         }
        
        labelmin.text = self.leçon[indexPath.row]["prestation"]["tarifs"][0]["prix_min"].stringValue + " € TTC"
        labelmax.text = self.leçon[indexPath.row]["prestation"]["tarifs"][0]["prix_max"].stringValue + " € TTC"
        labelmin.layer.cornerRadius = 5
        labelmin.layer.masksToBounds = true
        labelmax.layer.cornerRadius = 5
        labelmax.layer.masksToBounds = true
        
        
        return cell
    }
    /*
     *filtre action
     *
     */
    @IBAction func filtrer(_ sender: Any) {
        if (test)
        {
            
            UIView.animate(withDuration: 0.8 , delay: 0 , options: .curveEaseOut ,animations: {
                
                //   self.filterConstraint.constant = 0
                //  self.tableviewConstraint.constant = 25
                self.btnFilter.frame.origin.y -= self.filtre.frame.size.height
                self.tableview.frame.origin.y -= self.filtre.frame.size.height
                self.tableview.frame.size.height += self.filtre.frame.size.height
                
                print (self.btnFilter.frame.origin.y)
                //   self.tableview.setContentOffset(CGPoint.zero, animated: true)
                
                self.btnFilter.setBackgroundImage(UIImage(named: "btndérouleur1_ahmed"), for: .normal)
                //   self.tableview.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: true)
                
            }, completion: { _ in
                //  self.filterConstraint.constant = 0
                self.filtre.isHidden = true
                self.tableviewConstraint.constant = 120
                self.tableviewHeigh.constant += self.filtre.frame.size.height
            })
            
            test = false
            
        }
        else {
            
            //self.tableview.beginUpdates()
            
            // self.tableview.updateConstraints()
            // self.tableview.endUpdates()
            
            UIView.animate(withDuration: 0.8 , delay: 0 , options: .curveEaseIn ,animations: {
                
                //   self.filterConstraint.constant = 100
                // self.tableviewConstraint.constant = self.filterConstraint.constant + 8
                self.tableview.frame.origin.y += self.filtre.frame.size.height
                self.btnFilter.frame.origin.y += self.filtre.frame.size.height
                print (self.btnFilter.frame.origin.y)
                
                self.btnFilter.setBackgroundImage(UIImage(named: "btndérouleur_ahmed"), for: .normal)
                
                //   self.tableviewConstraint.constant = self.filtre.frame.size.height + 8
            }, completion: { _ in
                // self.filterConstraint.constant = 546
                //self.tableviewConstraint.constant = 25
                self.tableviewConstraint.constant = self.filterConstraint.constant + 120
                self.tableviewHeigh.constant = self.tableview.contentSize.height
                self.filtre.isHidden = false
            })
            test = true
            
            
            
        }
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("LeçonTitre")
        massif_downPicker.setPlaceholder(Localization("Massif"))
        niveau_downPicker.setPlaceholder(Localization("Niveau"))
        sport_downPicker.setPlaceholder(Localization("Sport"))
        département_downPicker.setPlaceholder(Localization("Departement"))
        stations_downPicker.setPlaceholder(Localization("Station"))
        duree_downPicker.setPlaceholder(Localization("Duree"))
        nbrPlaceDisponible_downPicker.setPlaceholder(Localization("Place"))
        //  date_downPicker.setPlaceholder(Localization("Date"))
        date.placeholder = Localization("Date")
        btnAjouter.setTitle(Localization("BtnCréer"), for: .normal)
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            massif_downPicker.setToolbarDoneButtonText("Terminé")
            massif_downPicker.setToolbarCancelButtonText("Annuler")
            
            niveau_downPicker.setToolbarDoneButtonText("Terminé")
            niveau_downPicker.setToolbarCancelButtonText("Annuler")
            
            sport_downPicker.setToolbarDoneButtonText("Terminé")
            sport_downPicker.setToolbarCancelButtonText("Annuler")
            
            département_downPicker.setToolbarDoneButtonText("Terminé")
            département_downPicker.setToolbarCancelButtonText("Annuler")
            
            stations_downPicker.setToolbarDoneButtonText("Terminé")
            stations_downPicker.setToolbarCancelButtonText("Annuler")
            
            duree_downPicker.setToolbarDoneButtonText("Terminé")
            duree_downPicker.setToolbarCancelButtonText("Annuler")
            
            nbrPlaceDisponible_downPicker.setToolbarDoneButtonText("Terminé")
            nbrPlaceDisponible_downPicker.setToolbarCancelButtonText("Annuler")
        }else {
            massif_downPicker.setToolbarDoneButtonText("Done")
            massif_downPicker.setToolbarCancelButtonText("Cancel")
            niveau_downPicker.setToolbarDoneButtonText("Done")
            niveau_downPicker.setToolbarCancelButtonText("Cancel")
            sport_downPicker.setToolbarDoneButtonText("Done")
            sport_downPicker.setToolbarCancelButtonText("Cancel")
            département_downPicker.setToolbarDoneButtonText("Done")
            département_downPicker.setToolbarCancelButtonText("Cancel")
            stations_downPicker.setToolbarDoneButtonText("Done")
            stations_downPicker.setToolbarCancelButtonText("Cancel")
            duree_downPicker.setToolbarDoneButtonText("Done")
            duree_downPicker.setToolbarCancelButtonText("Cancel")
            nbrPlaceDisponible_downPicker.setToolbarDoneButtonText("Done")
            nbrPlaceDisponible_downPicker.setToolbarCancelButtonText("Cancel")
        }
        showDatePicker()
        //Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        if first == false {
            self.viewDidLoad()
        }
          getAllLessons()
        first = false
    }
    /*
     *action the search
     *
     */
    @objc func TextfiedDidChange()
    {
        
        searchLessons()
        
        
    }
    /*
     *get all lessons from data base
     *
     */
    func getAllLessons () {
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.getAllLessons  , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                
                let b = JSON(response.data)
                print("Lessons : ",b)
                if b["status"].boolValue {
                    
                    self.leçon = b["data"]
                    self.tableview.reloadData()
                    
                    
                }
                else {
                    _ = SweetAlert().showAlert(Localization("LeçonTitre"), subTitle: Localization("erreur"), style: AlertStyle.error)
                }
        }
    }
    /*
     *search lessons
     *
     */
    func searchLessons ()
    {
        var str = ""
        if self.duree.text! != ""
        {
            var minute = ""
            var heure = ""
            
            
            heure = self.duree.text!.prefix(2) + ""
            minute = self.duree.text!.suffix(2) + ""
            
            str = String ( (Int (heure)! * 60) + Int (minute)!)
        }
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        var params: Parameters = [:]
        if self.sport_downPicker.selectedIndex != -1 && self.niveau_downPicker.selectedIndex != -1 {
            params = [
                "pratique": SportSaifFR2[self.sport_downPicker.selectedIndex ],
                "niveau" : NiveauSaifFR2[self.niveau_downPicker.selectedIndex] ,
                "date" : self.date.text!,
                "massif"  : self.massif.text!,
                "nbPlaceDispo" : self.nbrPlaceDisponible.text! ,
                "station" : self.stations.text! ,
                "Departement" : self.département.text! ,
                "duree" : str
                
            ]
        }else if  self.sport_downPicker.selectedIndex != -1 && self.niveau_downPicker.selectedIndex == -1 {
            params = [
                "pratique": SportSaifFR2[self.sport_downPicker.selectedIndex],
                "niveau" : "" ,
                "date" : self.date.text!,
                "massif"  : self.massif.text!,
                "nbPlaceDispo" : self.nbrPlaceDisponible.text! ,
                "station" : self.stations.text! ,
                "Departement" : self.département.text! ,
                "duree" : str
                
            ]
        }else if  self.sport_downPicker.selectedIndex == -1 && self.niveau_downPicker.selectedIndex != -1 {
            params = [
                "pratique": "",
                "niveau" : NiveauSaifFR2[self.niveau_downPicker.selectedIndex] ,
                "date" : self.date.text!,
                "massif"  : self.massif.text!,
                "nbPlaceDispo" : self.nbrPlaceDisponible.text! ,
                "station" : self.stations.text! ,
                "Departement" : self.département.text! ,
                "duree" : str
                
            ]
        } else {
            params = [
                "pratique": "",
                "niveau" : "" ,
                "date" : self.date.text!,
                "massif"  : self.massif.text!,
                "nbPlaceDispo" : self.nbrPlaceDisponible.text! ,
                "station" : self.stations.text! ,
                "Departement" : self.département.text! ,
                "duree" : str
                
            ]
        }
        
        Alamofire.request(ScriptBase.sharedInstance.searchLesson  , method: .post, parameters: params , encoding: JSONEncoding.default, headers : header)
            .responseJSON { response in
                
                let b = JSON(response.data)
                print(response)
                if b["status"].boolValue {
                    
                    self.leçon = b["data"]
                    self.tableview.reloadData()
                    
                    
                    
                }
                
        }
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker2(){
        self.date.text = ""
        searchLessons()
        self.view.endEditing(true)
        
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
         let dateLioum = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        let result = formatter.string(from: dateLioum)
        
        datePicker.minimumDate = formatter.date(from: result)
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            datePicker.locale = NSLocale(localeIdentifier: "fr_FR") as Locale
            let doneButton = UIBarButtonItem(title: "Terminé", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.donedatePicker))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Annuler", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelDatePicker2))
            toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        }else {
            datePicker.locale = NSLocale(localeIdentifier: "en_EN") as Locale
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.donedatePicker))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelDatePicker2))
            toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        }
        
        date.inputAccessoryView = toolbar
        date.inputView = datePicker
        
    }
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        date.text = formatter.string(from: datePicker.date)
        searchLessons()
        self.view.endEditing(true)
        
    }
    /*
     *load all stations
     *
     */
    func loadData(){
        LoaderAlert.show()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.Stations , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                self.massifArray = []
                let a = JSON(response.data)
                self.StationObject = a
                if (a["status"].boolValue)
                {
                    for i in 0 ... (a["data"].arrayObject!.count - 1) {
                        self.massifArray.append(a["data"][i]["massif"].stringValue)
                        self.departArray.append(a["data"][i]["dept"].stringValue)
                        self.stationArray.append(a["data"][i]["nom"].stringValue)
                    }
                    self.massifArray = self.massifArray.removeDuplicates()
                    self.departArray = self.departArray.removeDuplicates()
                    self.stationArray = self.stationArray.removeDuplicates()
                    
                    let bandArray4 : NSMutableArray = []
                    bandArray4.add("")
                    bandArray4.addObjects(from: self.massifArray)
                    self.massif_downPicker = DownPicker(textField: self.massif, withData: bandArray4 as! [Any])
                    self.massif_downPicker.setPlaceholder(Localization("Massif"))
                    self.massif_downPicker.addTarget(self, action: #selector(self.TextfiedDidChange), for : .valueChanged )
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        
                        self.massif_downPicker.setToolbarDoneButtonText("Terminé")
                        self.massif_downPicker.setToolbarCancelButtonText("Annuler")

                    }
                    else {
                        self.massif_downPicker.setToolbarDoneButtonText("Done")
                        self.massif_downPicker.setToolbarCancelButtonText("Cancel")
                    }
                    let bandArray7 : NSMutableArray = []
                    bandArray7.add("")
                    bandArray7.addObjects(from: self.stationArray)
                    
                    //self.down = UIDownPicker(data: bandArray as! [Any])
                    self.stations_downPicker = DownPicker(textField: self.stations, withData: bandArray7 as! [Any])
                    self.stations_downPicker.setArrowImage(UIImage(named:"downArrow"))
                    self.stations_downPicker.addTarget(self, action: #selector(self.TextfiedDidChange), for : .valueChanged )
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        
                        self.stations_downPicker.setToolbarDoneButtonText("Terminé")
                        self.stations_downPicker.setToolbarCancelButtonText("Annuler")
                        
                    }
                    else {
                        self.stations_downPicker.setToolbarDoneButtonText("Done")
                        self.stations_downPicker.setToolbarCancelButtonText("Cancel")
                    }
                    let bandArray8 : NSMutableArray = []
                    bandArray8.add("")
                    bandArray8.addObjects(from: self.departArray)
                    
                    //self.down = UIDownPicker(data: bandArray as! [Any])
                    self.département_downPicker = DownPicker(textField: self.département, withData: bandArray8 as! [Any])
                    self.département_downPicker.setArrowImage(UIImage(named:"downArrow"))
                    self.département_downPicker.addTarget(self, action: #selector(self.TextfiedDidChange), for : .valueChanged )
                    self.département_downPicker.setPlaceholder(Localization("Departement"))
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        
                        self.département_downPicker.setToolbarDoneButtonText("Terminé")
                        self.département_downPicker.setToolbarCancelButtonText("Annuler")
                        
                    }
                    else {
                        self.département_downPicker.setToolbarDoneButtonText("Done")
                        self.département_downPicker.setToolbarCancelButtonText("Cancel")
                    }
                    self.stations_downPicker.setPlaceholder(Localization("Station"))
                }
        }
    }
}

