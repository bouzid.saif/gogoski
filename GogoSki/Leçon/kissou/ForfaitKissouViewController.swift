//
//  ForfaitKissouViewController.swift
//  GogoSki
//
//  Created by AYMEN on 11/16/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit

class ForfaitKissouViewController: UIViewController {
    
// nav bar
    @IBOutlet weak var titreNavBar: UILabel!
    @IBOutlet weak var imageToProfile: UIImageView!
    
    @IBOutlet weak var dateDebutLBL: UILabel!
    @IBOutlet weak var forfaitPeriodeDescLBL: UILabel!
    @IBOutlet weak var selectionHeaderLBL: UILabel!
    
    @IBOutlet weak var forfaitTV: UITableView!
    @IBOutlet weak var totalTitleLBL: UILabel!
    
    @IBOutlet weak var totaleMontantLBL: UILabel!
    
    @IBOutlet weak var ajouterAutreForfaitText: UILabel!
    @IBOutlet weak var ajouterUnAutreForfaitBTN: UIButton!
    
    @IBOutlet weak var passerCetteEtapeBTN: UILabel!
    @IBOutlet weak var validerForfaitBTN: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        
    }

///actions
   
    @IBAction func ajouterForfaitAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func validerSortie(_ sender: Any) {
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /////////********langue"**********///////
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {

     
        titreNavBar.text = Localization("ForfaitTitre")
        totalTitleLBL.text = Localization("ForfaitMontantTotal")
        selectionHeaderLBL.text = Localization("ForfaitVotreSelec")
        
        ajouterAutreForfaitText.text = Localization("ForfaitAjoutK")
        validerForfaitBTN.setTitle(Localization("BtnValider"), for: .normal)
        passerCetteEtapeBTN.text = Localization("ForfaitPasserCetteEtape")

        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////

    
}


extension ForfaitKissouViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "forfaitCell") as! UITableViewCell
        return cell
    }
    
    
}
