//
//  FicheLeçonViewController.swift
//  GogoSki
//
//  Created by Ahmed Haddar on 15/11/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyJSON

class FicheLec_onViewController: InternetCheckViewControllers ,MKMapViewDelegate {
    /*
     *Levels in French
     *
     */
    let NiveauSaifFR = ["Débutant","Débrouillard" ,"Intermédiaire","Confirmé","Expert"]
    /*
     *Levels in English
     *
     */
    let NiveauSaifEN = ["Beginner","Resourceful" ,"Intermediate","Confirmed","Expert"]
    /*
     *Practice in French
     *
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     *Practice in English
     *
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     *Participants data
     *
     */
    var participants : JSON = []
    /*
     *Lesson id
     *
     */
    var id : String = ""
    /*
     *Lesson data
     *
     */
    var leçon : JSON = []
    /*
     *Conversation Id
     *
     */
    var idConv = ""
    /*
     *imageview of user from the top right bar
     *
     */
    @IBOutlet weak var barProfilePic: UIImageView!
    /*
     *bar title label
     *
     */
    @IBOutlet weak var titleNavBar: UILabel!
    /*
     *Practice label width
     *
     */
    @IBOutlet weak var SportUserContWidth: NSLayoutConstraint!
    /*
     *Practice type text
     *
     */
    @IBOutlet weak var sportTypeBTN: UIButton!
    /*
     *Level type text
     *
     */
    @IBOutlet weak var sportNiveauBTN: UIButton!
    /*
     *Lesson imageview
     *
     */
    @IBOutlet weak var leconImage: UIImageView!
    /*
     *Station title
     *
     */
    @IBOutlet weak var stationTitre: UILabel!
    /*
     *Share button
     *
     */
    @IBOutlet weak var partageBtn: UIButton!
    /*
     *lesson title
     *
     */
    @IBOutlet weak var titreLecon: UILabel!
    /*
     *Lesson type
     *
     */
    @IBOutlet weak var typeLecon: UIButton!
    /*
     *Lesson level
     *
     */
    @IBOutlet weak var niveauLecon: UIButton!
    //informations
    /*
     *from date lesson
     *
     */
    @IBOutlet weak var departDateLecon: UILabel!
    /*
     *from hour lesson
     *
     */
    @IBOutlet weak var departHeureLecon: UILabel!
    /*
     *duration lesson
     *
     */
    @IBOutlet weak var DureeLecon: UILabel!
    /*
     *how many persons to this lesson
     *
     */
    @IBOutlet weak var nombreDePersonne: UILabel!
    /*
     *number of remaining  place in this lesson
     *
     */
    @IBOutlet weak var placeRestante: UILabel!
    
    @IBOutlet weak var map: MKMapView!
    ///participant header
    /*
     *number of remaining place in the participant header
     *
     */
    @IBOutlet weak var placeDisponiblePH: UILabel!
    /*
     *groupe message button
     *
     */
    @IBOutlet weak var buttonMSG: UIButton!
    /*
     *add user to this lesson button
     *
     */
    @IBOutlet weak var AjouterParticipantBTN: UIButton!
    /*
     *participants tableView
     *
     */
    @IBOutlet weak var participantsTableView: UITableView!
    // prix header
    /*
     *price of this activity
     *
     */
    @IBOutlet weak var prixLBL: UILabel!
    /*
     *minimum price of this activity
     *
     */
    @IBOutlet weak var minPrixBTN: UIButton!
    /*
     *maximum price of this activity
     *
     */
    @IBOutlet weak var maxPrixBTN: UIButton!
    /*
     *join this activity
     *
     */
    @IBOutlet weak var validerBTN: UIButton!
    ///descriptionLabel
    /*
     *description height
     *
     */
    @IBOutlet weak var descriptionCaontainerViewHeight: NSLayoutConstraint!
    /*
     *description label
     *
     */
    @IBOutlet weak var descriptionLBL: UILabel!
    /*
     *description view more/less
     *
     */
    @IBOutlet weak var plusBTN: UIButton!
    /*
     *description + map container height
     *
     */
    @IBOutlet weak var theWholeContainerViewHeight: NSLayoutConstraint!
    var prestations : JSON = []
    var heureDepart : String = ""
    var dateDepart : String = ""
    /*
     *configure the controller
     *
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        getLesson()
        validerBTN.isEnabled = false
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if let urlImgUser = URL(string : a["user"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            barProfilePic.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        self.leconImage.image = UIImage(named: "station")
        self.map.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        /*
         *a revoir
         *
         */
        NotificationCenter.default.addObserver(self, selector: #selector(self.DeleteNotifVar(notification:)), name: NSNotification.Name(rawValue: "SkaerNotif"), object: nil)
        configureViewFromLocalisation()
        // Do any additional setup after loading the view.
    }
    @objc func DeleteNotifVar(notification:NSNotification) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.NotifReceive =  false
    }
    /*
     *share action
     *
     */
    @IBAction func partageAction(_ sender: Any) {
        
        /*self.viewLfouk.isHidden = true
         UIGraphicsBeginImageContext(self.shoot.frame.size)
         shoot.layer.render(in: UIGraphicsGetCurrentContext()!)
         let image = UIGraphicsGetImageFromCurrentImageContext()
         UIGraphicsEndImageContext()
         self.viewLfouk.isHidden = false */
        let URL = "http://www.gogoski.fr/user/lecon/" + self.id
        let vc = UIActivityViewController(activityItems: [URL], applicationActivities: [])
        present(vc, animated: true)
        
        
        
    }
    /*
     * return UILabel height
     *
     */
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    /*
     *more /less action
     *
     */
    @IBAction func plusAction(_ sender: Any) {
        
        
        if plusBTN.title(for: .normal) == Localization("plusP") {
            descriptionCaontainerViewHeight.constant -= self.descriptionLBL.frame.height
            theWholeContainerViewHeight.constant -= self.descriptionLBL.frame.height
            descriptionCaontainerViewHeight.constant += heightForView(text: self.descriptionLBL.text!, font: self.descriptionLBL.font, width: self.descriptionLBL.frame.width)
            descriptionLBL.numberOfLines = 0
            
            
            theWholeContainerViewHeight.constant =  descriptionCaontainerViewHeight.constant + 48.5
            
            plusBTN.setTitle(Localization("moinsP"), for: .normal)
        }else {
            descriptionCaontainerViewHeight.constant = 433.0
            theWholeContainerViewHeight.constant = 433.0
            descriptionLBL.numberOfLines = 3
            plusBTN.setTitle(Localization("plusP"), for: .normal)
        }
        
    }
    /*
     *add participant action => go to invite person controller
     *
     */
    @IBAction func ajouterParticipantAction(_ sender: Any) {
        let view = self.storyboard?.instantiateViewController(withIdentifier: "InviterParticipant") as! InviterParticipantViewController
        view.sortie = self.leçon
        view.chkoun = "lesson"
        self.navigationController?.present(view, animated: true, completion: nil)
    }
    /*
     *group message button action
     *
     */
    @IBAction func messageParticipantAction(_ sender: Any) {
        LoaderAlert.show()
        let params: Parameters = [
            "idConv": self.idConv
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.joinConvActivite , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                print ("add conv "  , b)
                if b["status"].boolValue {
                    let ConvId = self.idConv
                    print("ConvID",ConvId)
                    SocketIOManager.sharedInstance.connectToServerWithNickname(nickname: ConvId, userid: a["user"]["id"].stringValue, first: false)
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.NotifAhmed =  true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToMessageForomNoWhere2"), object: ConvId )
                }
                
                
        }
    }
    /*
     *back action
     *
     */
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
     *set the custom image of place
     *
     */
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let saifPin =  MKAnnotationView(annotation: annotation, reuseIdentifier: "PINA")
        saifPin.isEnabled = true
        saifPin.canShowCallout = true
        
        saifPin.image = UIImage(named: "Pin_saif")
        
        // saifPin.tintColor = MKAnnotationView.blueColor()
        // saifPin.tintAdjustmentMode = .normal
        return saifPin
    }
    
    /////////********langue"**********///////
    /*
     *remove the language observer
     *
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    /*
     *configure the texts
     *
     */
    func configureViewFromLocalisation() {
        //   titleNavBar.text = Localization("FicheLeconTitre")
        prixLBL.text = Localization("Prix")
        validerBTN.setTitle(Localization("btnRejoindreLeçon"), for: .normal)
        
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    /*
     *share the activity
     *
     */
    @IBAction func shareAction(_ sender: Any) {
        
        
        /*self.viewLfouk.isHidden = true
         UIGraphicsBeginImageContext(self.shoot.frame.size)
         shoot.layer.render(in: UIGraphicsGetCurrentContext()!)
         let image = UIGraphicsGetImageFromCurrentImageContext()
         UIGraphicsEndImageContext()
         self.viewLfouk.isHidden = false */
        let URL = "http://www.gogoski.fr/user/lecon/" + self.id
        let vc = UIActivityViewController(activityItems: [URL], applicationActivities: [])
        present(vc, animated: true)
        
        
    }
    /*
     *language notification received
     *
     */
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
    /*
     *get Lesson data from DataBase
     *
     */
    func getLesson () {
        LoaderAlert.show()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        let params : Parameters = [
            "id" : self.id
        ]
        Alamofire.request(ScriptBase.sharedInstance.getLesson  , method: .post ,parameters : params , encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                print ("******* Leçon :::" , response)
                if b["status"].boolValue {
                    self.validerBTN.isEnabled = true
                    self.partageBtn.isEnabled = true
                    self.minPrixBTN.setTitle(b["data"]["prestation"]["tarifs"][0]["prix_min"].stringValue + " € TTC min" , for: .normal)
                    self.maxPrixBTN.setTitle(b["data"]["prestation"]["tarifs"][0]["prix_max"].stringValue + " € TTC max", for: .normal)
                    self.leçon = b["data"]
                    self.prestations = b["data"]["prestation"]
                    self.idConv = b["data"]["conversation"]["id"].stringValue
                    print ( "station : : :" , b["data"]["prestataire"]["point"]["station"]["nom"].stringValue)
                    self.stationTitre.text = b["data"]["prestation"]["prestataire"]["point"]["station"]["nom"].stringValue
                    self.titreLecon.text = b["data"]["titre"].stringValue
                    var mySubstring = b["data"]["date_debut"].stringValue.prefix(10)
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        self.departDateLecon.text = "Départ le : " + mySubstring
                    }else {
                        self.departDateLecon.text = "Departure on : " + mySubstring
                    }
                    if b["data"]["prestation"]["prestataire"]["photo"].stringValue.first == "h" {
                        self.leconImage.setImage(withUrl: URL(string:b["data"]["prestation"]["prestataire"]["photo"].stringValue)!, placeholder: UIImage(named:"station"), crossFadePlaceholder: false, cacheScaled: true, completion: nil)
                    }
                    self.dateDepart = String(mySubstring)
                    var mySubstring1 = b["data"]["date_debut"].stringValue.replacingOccurrences(of: "/", with: ":").suffix(8)
                    self.heureDepart = String(mySubstring1)
                    //   mySubstring1 = mySubstring1.replacingOccurrences(of: "/", with: ":")
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        self.departHeureLecon.text = "à : " + mySubstring1.prefix(5)
                    }else {
                        self.departHeureLecon.text = "at : " + mySubstring1.prefix(5)
                    }
                    self.descriptionLBL.text = b["data"]["detail"].stringValue
                    if self.descriptionLBL.text!.count < 98 {
                        self.plusBTN.isHidden = true
                    }else{
                        
                        self.plusBTN.setTitle(Localization("plusP"), for: .normal)
                        self.plusBTN.isHidden = false
                    }
                    let heure =  String(b["data"]["duree"].intValue / 60)
                    var minute = String (b["data"]["duree"].intValue - ( (b["data"]["duree"].intValue / 60) * 60 ))
                    if (minute == "0")
                    {
                        minute = ""
                    }
                    self.participants = b["data"]["list_participants"]
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        self.DureeLecon.text = "Durée : " + heure + "h" + minute
                    }else {
                        self.DureeLecon.text = "Duration : " + heure + "h" + minute
                    }
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        if b["data"]["nb_place_maximum"].intValue != 0 {
                            self.placeRestante.text =  String(b["data"]["nb_place_maximum"].intValue - (self.participants.arrayObject?.count)!) + " restante(s)"
                        }else {
                            self.placeRestante.text =  "0" + " restante(s)"
                        }
                    }else {
                        if (b["data"]["nb_place_maximum"].intValue - (self.participants.arrayObject?.count)!) > 0 {
                            self.placeRestante.text =  String(b["data"]["nb_place_maximum"].intValue - (self.participants.arrayObject?.count)!) + " remaining"
                        }else {
                            self.placeRestante.text =  "0" + " remaining"
                        }
                    }
                    self.nombreDePersonne.text = b["data"]["nb_place_maximum"].stringValue + " pers. / "
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        if(b["data"]["nb_place_maximum"].intValue - (self.participants.arrayObject?.count)!) > 0 {
                            self.placeDisponiblePH.text = " " + String(b["data"]["nb_place_maximum"].intValue - (self.participants.arrayObject?.count)!) + " restante(s)"
                        }else {
                            self.placeDisponiblePH.text = " " + "0" + " restante(s)"
                        }
                    }else {
                        if b["data"]["nb_place_maximum"].stringValue != "0" {
                            self.placeDisponiblePH.text = " " + String(b["data"]["nb_place_maximum"].intValue - (self.participants.arrayObject?.count)!) + " remaining"
                        }else {
                            self.placeDisponiblePH.text = " " + "0" + " remaining"
                        }
                    }
                    ///*//****** Sport
                    if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr"{
                        switch b["data"]["sport"]["pratique"].stringValue {
                        case self.SportSaifFR[0] :
                            
                            self.sportTypeBTN.setTitle(self.SportSaifEN[0], for: .normal)
                        case self.SportSaifFR[1] :
                            
                            
                            self.sportTypeBTN.setTitle(self.SportSaifEN[1], for: .normal)
                        case self.SportSaifFR[2] :
                            
                            
                            self.sportTypeBTN.setTitle(self.SportSaifEN[2], for: .normal)
                        case self.SportSaifFR[3] :
                            
                            self.sportTypeBTN.setTitle(self.SportSaifEN[3], for: .normal)
                        case self.SportSaifFR[4] :
                            
                            
                            self.sportTypeBTN.setTitle(self.SportSaifEN[4], for: .normal)
                        case self.SportSaifFR[5] :
                            
                            
                            self.sportTypeBTN.setTitle(self.SportSaifEN[5], for: .normal)
                        default :
                            self.sportTypeBTN.setTitle("", for: .normal)
                        }
                    } else {
                        self.sportTypeBTN.setTitle(b["data"]["sport"]["pratique"].stringValue, for: .normal)
                    }
                    if ((self.sportTypeBTN.title(for: .normal)?.count)! > 19)
                    {
                        self.SportUserContWidth.constant = 200
                    }else if ((self.sportTypeBTN.title(for: .normal)?.count)! > 15)
                    {
                        self.SportUserContWidth.constant = 150
                    }else if ((self.sportTypeBTN.title(for: .normal)?.count)! > 10) {
                        self.SportUserContWidth.constant = 120
                    }
                    else {
                        self.SportUserContWidth.constant = 100
                    }
                    ///lognLat
                    var lat = ""
                    var long = ""
                    
                    lat = b["data"]["prestation"]["prestataire"]["point"]["latitude"].stringValue
                    long = b["data"]["prestation"]["prestataire"]["point"]["longitude"].stringValue
                    print (b["data"]["prestation"]["prestataire"]["point"]["latitude"].stringValue , " azerty " , b["data"]["prestation"]["prestataire"]["point"]["longitude"].stringValue )
                    if long.prefix(1) == " "
                    {
                        long = String (long.dropFirst())
                    }
                    
                    if ( lat.suffix(1) == " ")
                    {
                        lat = String ( lat.dropLast())
                    }
                    if ( lat != "" && long != "" )
                    {
                        let point = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!)
                        let annotation = ColorPointAnnotation(pinColor: UIColor.blue)
                        annotation.coordinate = point
                        
                        self.map.addAnnotation(annotation)
                        self.map.setCenter(point, animated: true)
                        
                    }
                    ////
                    
                    
                    if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr"{
                        switch b["data"]["sport"]["niveau"].stringValue {
                        case self.NiveauSaifFR[0] :
                            
                            self.sportNiveauBTN.setTitle(self.NiveauSaifEN[0], for: .normal)
                        case self.NiveauSaifFR[1] :
                            self.sportNiveauBTN.setTitle(self.NiveauSaifEN[1], for: .normal)
                            
                        case self.NiveauSaifFR[2] :
                            self.sportNiveauBTN.setTitle(self.NiveauSaifEN[2], for: .normal)
                            
                        case self.NiveauSaifFR[3] :
                            
                            self.sportNiveauBTN.setTitle(self.NiveauSaifEN[3], for: .normal)
                        case self.NiveauSaifFR[4] :
                            self.sportNiveauBTN.setTitle(self.NiveauSaifEN[4], for: .normal)
                        default :
                            self.sportNiveauBTN.setTitle("", for: .normal)
                        }
                    } else {
                        self.sportNiveauBTN.setTitle(b["data"]["sport"]["niveau"].stringValue, for: .normal)
                    }
                    
                    //////
                    
                    
                    ////////********Niveau
                    
                    
                    /////
                    
                    
                    if ( b["data"]["sport"]["niveau"].stringValue == "Confirmé" )
                    {
                        self.sportNiveauBTN.setTitleColor(UIColor("#FF3E53"), for: .normal)
                    }
                    else if (b["data"]["sport"]["niveau"].stringValue == "Débutant")
                    {
                        self.sportNiveauBTN.setTitleColor(UIColor("#D3D3D3"), for: .normal)
                    }
                    else if (b["data"]["sport"]["niveau"].stringValue == "Intermédiaire")
                    {
                        self.sportNiveauBTN.setTitleColor(UIColor("#75A7FF"), for: .normal)
                    }
                    else if (b["data"]["sport"]["niveau"].stringValue == "Expert") {
                        self.sportNiveauBTN.setTitleColor(UIColor("#4D4D4D"), for: .normal)
                    }
                    else {
                        self.sportNiveauBTN.setTitleColor(UIColor("#76EC9E"), for: .normal)
                    }
                    
                    
                    self.participantsTableView.reloadData()
                    if self.participants.arrayObject?.count != 0
                    {
                        for i in 0 ... ((self.participants.arrayObject?.count)! - 1)
                        {
                            if ( self.participants[i]["id"].stringValue == a["user"]["id"].stringValue)
                            {
                                self.validerBTN.isHidden = true
                                self.partageBtn.isHidden = false
                                self.buttonMSG.isHidden = false
                                if (b["data"]["nb_place_maximum"].intValue == self.participants.arrayObject?.count)
                                {
                                    self.AjouterParticipantBTN.isHidden = true
                                }
                                else {
                                    
                                    self.AjouterParticipantBTN.isHidden = false
                                }
                            }
                            
                        }
                    }
                    if b["data"]["nb_place_maximum"].intValue == self.participants.arrayObject?.count
                    {
                        
                        self.validerBTN.isEnabled = false
                        _ = SweetAlert().showAlert(Localization("LeçonTitre"), subTitle: Localization("full2"), style: AlertStyle.error)
                        
                    }
                    
                }
                else {
                    _ = SweetAlert().showAlert(Localization("LeçonTitre"), subTitle: Localization("erreur"), style: AlertStyle.error)
                }
        }
    }
    /*
     *Join a lesson action
     *
     */
    @IBAction func JoinLesson(_ sender: Any) {
        /* self.validerBTN.isEnabled = false
         let params: Parameters = [
         "idAct": self.id
         ]
         let ab = UserDefaults.standard.value(forKey: "User") as! String
         let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
         var a = JSON(data: dataFromString!)
         let header: HTTPHeaders = [
         "Content-Type" : "application/json",
         "x-Auth-Token" : a["value"].stringValue
         ]
         Alamofire.request(ScriptBase.sharedInstance.joinSortie , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
         .responseJSON { response in
         self.validerBTN.isEnabled = true
         let b = JSON(response.data)
         print ("join : " , b)
         if b["status"].boolValue {
         
         _ = SweetAlert().showAlert(Localization("btnRejoindreLeçon"), subTitle: Localization("cetLeçon"), style: AlertStyle.success)
         self.validerBTN.isHidden = true
         self.partageBtn.isHidden = false
         self.buttonMSG.isHidden = false
         self.AjouterParticipantBTN.isHidden = false
         self.participants = b["data"]
         self.participantsTableView.reloadData()
         }else{
         
         _ = SweetAlert().showAlert(Localization("btnRejoindreLeçon"), subTitle: Localization("erreur"), style: AlertStyle.error)
         
         }
         
         
         } */
        let view = self.storyboard?.instantiateViewController(withIdentifier: "Panier") as! PanierKissouViewController
        view.activity = self.prestations
        view.heureDep = self.heureDepart
        view.dateDep = self.dateDepart
        view.id = self.id
        self.navigationController?.pushViewController(view, animated: true)
        
    }
}

extension FicheLec_onViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ( self.participants.arrayObject?.count == 0)
        {
            return 0
        }
        else {
            return (self.participants.arrayObject?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "participantsCell") as! UITableViewCell
        let imgUser : UIImageView = cell.viewWithTag(10) as! UIImageView
        let labelName : UILabel = cell.viewWithTag(1) as! UILabel
        let labelSport : UILabel = cell.viewWithTag(2) as! UILabel
        let labelNiveau : UILabel = cell.viewWithTag(3) as! UILabel
        if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr"{
            print("pratique:",self.participants[indexPath.row]["sports"][0]["pratique"].stringValue)
            switch self.participants[indexPath.row]["sports"][0]["pratique"].stringValue {
            case SportSaifFR[0] :
                labelSport.text = SportSaifEN[0]
            case SportSaifFR[1] :
                
                labelSport.text = SportSaifEN[1]
            case SportSaifFR[2] :
                
                labelSport.text = SportSaifEN[2]
            case SportSaifFR[3] :
                labelSport.text = SportSaifEN[3]
            case SportSaifFR[4] :
                
                labelSport.text = SportSaifEN[4]
            case SportSaifFR[5] :
                
                labelSport.text = SportSaifEN[5]
            default :
                labelSport.text = ""
            }
        } else {
            labelSport.text = self.participants[indexPath.row]["sports"][0]["pratique"].stringValue
        }
        
        if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr" {
            switch self.participants[indexPath.row]["sports"][0]["niveau"].stringValue {
            case NiveauSaifFR[0] :
                labelNiveau.text = "- "  + NiveauSaifEN[0]
            case NiveauSaifFR[1] :
                
                labelNiveau.text = "- "  + NiveauSaifEN[1]
            case NiveauSaifFR[2] :
                
                labelNiveau.text = "- "  + NiveauSaifEN[2]
            case NiveauSaifFR[3] :
                labelNiveau.text = "- "  + NiveauSaifEN[3]
            case NiveauSaifFR[4] :
                labelNiveau.text = "- "  + NiveauSaifEN[4]
            default :
                labelNiveau.text = ""
            }
        } else {
            labelNiveau.text = "- " + self.participants[indexPath.row]["sports"][0]["niveau"].stringValue
        }
        
        
        
        if ( self.participants[indexPath.row]["sports"][0]["niveau"].stringValue == "Confirmé" )
        {
            labelNiveau.textColor = UIColor("#FF3E53")
        }
        else if (self.participants[indexPath.row]["sports"][0]["niveau"].stringValue == "Débutant")
        {
            labelNiveau.textColor = UIColor("#D3D3D3")
        }
        else if (self.participants[indexPath.row]["sports"][0]["niveau"].stringValue == "Intermédiaire")
        {
            labelNiveau.textColor = UIColor("#75A7FF")
        }
        else if (self.participants[indexPath.row]["sports"][0]["niveau"].stringValue == "Expert")
        {
            labelNiveau.textColor = UIColor("#4D4D4D")
        }
        else {
            labelNiveau.textColor = UIColor("#76EC9E")
        }
        labelName.text = self.participants[indexPath.row]["prenom"].stringValue + " " + self.participants[indexPath.row]["nom"].stringValue
        if let urlImgUser = URL(string : self.participants[indexPath.row]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder") {
            imgUser.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        
        let btnMessages : CustomButton = cell.viewWithTag(40) as! CustomButton
        btnMessages.index = indexPath.row
        btnMessages.indexPath = indexPath
        btnMessages.isUserInteractionEnabled = true
        btnMessages.addTarget(self, action: #selector(self.messageAction), for: .touchUpInside)
        if a["user"]["id"].stringValue != self.participants[indexPath.row]["id"].stringValue {
            btnMessages.isHidden = false
        }else {
            btnMessages.isHidden = true
        }
        return cell
    }
    /*
     *go to private conversation user
     *
     */
    @objc func messageAction(sender : CustomButton) {
        
        LoaderAlert.show()
        let params: Parameters = [
            "amiId": self.participants[sender.index!]["id"].stringValue
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.addConvPriv , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                print ("add conv "  , b)
                if b["status"].boolValue {
                    
                    let ConvId = b["data"]["convId"].stringValue
                    SocketIOManager.sharedInstance.connectToServerWithNickname(nickname: ConvId, userid: a["user"]["id"].stringValue, first: false)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToMessageForomNoWhere"), object: ConvId as! String)
                }
                
                
        }
    }
    
}


