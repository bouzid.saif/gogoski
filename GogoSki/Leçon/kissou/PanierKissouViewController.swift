//
//  PanierKissouViewController.swift
//  GogoSki
//
//  Created by AYMEN on 11/16/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import DownPicker
import SwiftyJSON
import Alamofire

class PanierKissouViewController: InternetCheckViewControllers {
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    //nav bar
    /*
     *Level in French
     *
     */
    let NiveauSaifFR = ["Débutant","Débrouillard" ,"Intermédiaire","Confirmé","Expert"]
    /*
     *Level in English
     *
     */
    let NiveauSaifEN = ["Beginner","Resourceful" ,"Intermediate","Confirmed","Expert"]
    /*
     *Practice in French
     *
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     *Practice in English
     *
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     *activity data
     *
     */
    var activity : JSON = []
    /*
     *participants data
     *
     */
    var participants : JSON = []
    /*
     *number of participants
     *
     */
    var nombreMinParticipant = 0
    /*
     *activity details received from the preview view
     *
     */
    var detailsActivity = ""
    /*
     *From hours received from the preview view
     *
     */
    var heureDep = ""
    /*
     *From date received from the preview view
     *
     */
    var dateDep = ""
    /*
     *longitude received from the preview view
     *
     */
    var long = ""
    /*
     *latitude received from the preview view
     *
     */
    var lat = ""
    /*
     *nav bar title
     *
     */
    @IBOutlet weak var paniertitleNavBarLBL: UILabel!
    /*
     *test if this activity was created by the user or from the renew activity button
     *
     */
    var newLesson = true
    /*
     *image of the actual user
     *
     */
    @IBOutlet weak var imageToProfileNB: UIImageView!
    /*
     *basket tableview
     *
     */
    @IBOutlet weak var panierTV: UITableView!
    /*
     *min amount title
     *
     */
    @IBOutlet weak var montantMinTitre: UILabel!
    /*
     *min amount
     *
     */
    @IBOutlet weak var montantMin: UILabel!
    /*
     *min price  title
     *
     */
    @IBOutlet weak var ttcLBLmin: UILabel!
    /*
     *max amount title
     *
     */
    @IBOutlet weak var montantMAxTitre: UILabel!
    /*
     *max amount
     *
     */
    @IBOutlet weak var montantMax: UILabel!
    /*
     *max price title
     *
     */
    @IBOutlet weak var ttcLBLmax: UILabel!
    //information bancaire
    /*
     *info banking title
     *
     */
    @IBOutlet weak var infoBancaireTitreLBL: UILabel!
    /*
     *card number
     *
     */
    @IBOutlet weak var ribLBL: UITextField!
    /*
     *type of card
     *
     */
    @IBOutlet weak var typeDeCarte: UITextField!
    /*
     *card holder
     *
     */
    @IBOutlet weak var titulaireDeCarte: UITextField!
    /*
     *expiration date title
     *
     */
    @IBOutlet weak var dateExpirationTitle: UILabel!
    /*
     *expiration date text field
     *
     */
    @IBOutlet weak var dateExpirationTF: UITextField!
    /*
     *cryptogramme title
     *
     */
    @IBOutlet weak var cryptogrammeTitre: UILabel!
    /*
     *cryptogramme text field
     *
     */
    @IBOutlet weak var cryptogrammeTF: UITextField!
    /*
     *pay this activity
     *
     */
    @IBOutlet weak var validerPAnierBTN: UIButton!
    /*
     *id of the lesson
     *
     */
    var id : String = ""
    /*
     *title of the lesson
     *
     */
    var titleLec : String = ""
    var inviter = false
    var idNotif = ""
      var typeCarteDownPicker: DownPicker!
     let typeCarteArray = ["MasterCard","Visa","Gold","American Express","Discover"]
    /*
     * get profile preferences
     */
    func loadData(){
        self.validerPAnierBTN.isEnabled = false
        LoaderAlert.show()
        cryptogrammeTF.keyboardType = .numberPad
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.AfficherProfil , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                 self.validerPAnierBTN.isEnabled = true
                let a = JSON(response.data)
                print("saif:",a)
                
                if a["status"].boolValue {
                    print(a)
                   
                  
                    self.ribLBL.text = a["data"]["carte_bancaire"]["numero"].stringValue
                    self.typeDeCarte.text = a["data"]["carte_bancaire"]["type"].stringValue
                   
                    self.titulaireDeCarte.text = a["data"]["carte_bancaire"]["titulaire"].stringValue
                   
                  
                    
                }
                
                
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        long = self.activity["prestataire"]["point"]["longitude"].stringValue
        lat = self.activity["prestataire"]["point"]["latitude"].stringValue
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if let urlImgUser = URL(string : a["user"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imageToProfileNB.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
         typeCarteDownPicker = DownPicker(textField: typeDeCarte, withData: typeCarteArray as! [Any])
         typeCarteDownPicker.setPlaceholder("type de carte")
         typeCarteDownPicker.setArrowImage(nil)
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            typeCarteDownPicker.setToolbarDoneButtonText("Terminé")
            typeCarteDownPicker.setToolbarCancelButtonText("Annuler")
            
            
        }else {
            typeCarteDownPicker.setToolbarDoneButtonText("Done")
            typeCarteDownPicker.setToolbarCancelButtonText("Cancel")
            
        }
        montantMax.text = self.activity["tarifs"][0]["prix_max"].stringValue + " € "
        montantMin.text = self.activity["tarifs"][0]["prix_min"].stringValue + " € "
        
        if typeDeCarte.text != "" && titulaireDeCarte.text != "" && ribLBL.text != "" {
            typeDeCarte.isEnabled = false
            titulaireDeCarte.isEnabled = false
            ribLBL.isEnabled = false
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    }
    
    ///actions
    /*
     *back action
     *
     */
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /////////********langue"**********///////
    /*
     *remove observer
     *
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    /*
     *configure texts
     *
     */
    func configureViewFromLocalisation() {
        
        paniertitleNavBarLBL.text = Localization("Panier")
        montantMinTitre.text = Localization("PanierMontantTotalMin")
        montantMAxTitre.text = Localization("PanierMontantTotalMAx")
          infoBancaireTitreLBL.text = Localization("PanierInformationBancaire")
        ribLBL.text = Localization("PanierTitulaireCarte")
         typeDeCarte.placeholder = Localization("PanierTypeCarte")
         titulaireDeCarte.placeholder = Localization("PanierTitulaireCarte")
        dateExpirationTitle.text = Localization("PanierDateExp")
         cryptogrammeTitre.text = Localization("PanierCryptogramme")
        // validerPAnierBTN.setTitle(Localization("BtnValider"), for: .normal)
        
        
        Localisator.sharedInstance.saveInUserDefaults = true
        
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
    /*
     *create this activity
     *
     */
    @IBAction func createActivity(_ sender: Any) {
        self.validerPAnierBTN.isEnabled = false
        if typeDeCarte.text != "" && titulaireDeCarte.text != "" && ribLBL.text != "" && cryptogrammeTF.text != "" && dateExpirationTF.text != "" {
        LoaderAlert.show()
            let path = Bundle.main.path(forResource: "gogoski", ofType: "txt")
            print(path)
            //let x = NSKeyedUnarchiver.unarchiveObject(withFile: path!) as? String
            var key = ""
            var text = ""
            do {
                key = try  String(contentsOfFile: path!)
                print (key.bytes)
                //let iv = AES.randomIV(AES.blockSize)
                let textiv = "gogoskiiksogogsa"
                let iv = textiv.data(using: .utf8)!
                //let encryptedData = Data(iv)
                //let decrypteddata = String(data: encryptedData, encoding: .utf8)
                print("iv:",iv.count)
                 text = try aesEncrypt(key: key, text: self.cryptogrammeTF.text!, iv: iv.bytes)
                print ( text)
               // let decrypt = try aesDecrypt(key: key, text: text, iv: iv.bytes)
               // print(decrypt)
            }catch {
                
            }
        let params: Parameters = [
            "prestation" : self.activity["id"].stringValue,
            "cvv"  :    text ,
            "expiry" : self.dateExpirationTF.text! ,
            "typeCarte" : self.typeDeCarte.text! ,
            "titulaireCarte" : self.titulaireDeCarte.text! ,
            "numCarte" : self.ribLBL.text!
        ]
        //lieuRdv
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.payer , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseString { response in
                print ("paymennnnt : ", response)
                let act = JSON(response.data)
                LoaderAlert.dismiss()
                LoaderAlert.show()
                if act["status"].boolValue {
                    
                    if self.inviter {
                        let params: Parameters = [
                            "id": self.idNotif ,
                            "respond" : true ,
                            "actid" : self.id
                        ]
                        print ("***********" , params)
                        Alamofire.request(ScriptBase.sharedInstance.acceptRefuseInvit , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                            .responseJSON { response in
                                let b = JSON(response.data)
                                print ("accept : " , b)
                                self.validerPAnierBTN.isEnabled = true
                                if b["status"].boolValue {
                                    self.createNotifPrest(typeNotif: "JoinLesson", presId: self.activity["prestataire"]["id"].stringValue, titleLesson: self.titleLec, LessonId: self.id)
                                    _ = SweetAlert().showAlert(Localization("BtnAccepter"), subTitle: Localization("invitAccept"),  style: AlertStyle.success , buttonTitle: "OK" , action: { (otherButton) in
                                        let tabBarViewControllers = self.tabBarController?.viewControllers
                                        LoaderAlert.dismiss()
                                        let adController = tabBarViewControllers![3] as! UINavigationController
                                        
                                        let adminTVC = adController.viewControllers[0] as! MessageNotificationSection
                                        
                                        _ = adminTVC.navigationController?.popToRootViewController(animated: false)
                                    })
                                }else{
                                    
                                    _ = SweetAlert().showAlert(Localization("BtnAccepter"), subTitle: Localization("erreur"), style: AlertStyle.error)
                                    
                                }
                                
                        }
                    }
                    else if self.id != ""
                    {
                        
                        let params: Parameters = [
                            "idAct": self.id
                        ]
                        let ab = UserDefaults.standard.value(forKey: "User") as! String
                        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                        var a = JSON(data: dataFromString!)
                        let header: HTTPHeaders = [
                            "Content-Type" : "application/json",
                            "x-Auth-Token" : a["value"].stringValue
                        ]
                        Alamofire.request(ScriptBase.sharedInstance.joinSortie , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                            .responseJSON { response in
                                
                                let b = JSON(response.data)
                                print ("join : " , b)
                                if b["status"].boolValue {
                                    self.createNotifPrest(typeNotif: "JoinLesson", presId: self.activity["prestataire"]["id"].stringValue, titleLesson: self.titleLec, LessonId: self.id)
                                    _ = SweetAlert().showAlert(Localization("btnRejoindreLeçon"), subTitle: Localization("cetLeçon"),  style: AlertStyle.success , buttonTitle: "OK" , action: { (otherButton) in
                                        let tabBarViewControllers = self.tabBarController?.viewControllers
                                        LoaderAlert.dismiss()
                                        let adController = tabBarViewControllers![2] as! UINavigationController
                                        
                                        let adminTVC = adController.viewControllers[0] as! Lec_onViewController
                                        
                                        _ = adminTVC.navigationController?.popToRootViewController(animated: false)
                                    })
                                }else{
                                    
                                    _ = SweetAlert().showAlert(Localization("btnRejoindreLeçon"), subTitle: Localization("erreur"), style: AlertStyle.error)
                                    
                                }
                                
                                
                        }
                    }else {
                        let params: Parameters = [
                            "prestation" : self.activity["id"].stringValue,
                            "moniteur"  : self.activity["moniteur"]["id"].stringValue ,
                            "detail" : self.detailsActivity ,
                            "date" : self.dateDep + " " + self.heureDep ,
                            "NombreMinim" : self.nombreMinParticipant ,
                            "payement" : act["data"]["id"].stringValue
                        ]
                        //lieuRdv
                        let ab = UserDefaults.standard.value(forKey: "User") as! String
                        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                        var a = JSON(data: dataFromString!)
                        let header: HTTPHeaders = [
                            "Content-Type" : "application/json",
                            "x-Auth-Token" : a["value"].stringValue
                        ]
                        Alamofire.request(ScriptBase.sharedInstance.createLesson , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                            .responseJSON { response in
                                print ( "addLesson :  " , response)
                                LoaderAlert.dismiss()
                                
                                self.validerPAnierBTN.isEnabled = true
                                let act = JSON(response.data)
                                print ("*******")
                                if act["status"].boolValue {
                                    
                                     self.createNotifPrest(typeNotif: "CreateLesson", presId: self.activity["prestataire"]["id"].stringValue, titleLesson: self.titleLec, LessonId: act["data"]["id"].stringValue)
                                    if ( self.newLesson)
                                    {
                                        
                                        _ = SweetAlert().showAlert(Localization("addLecon"), subTitle: Localization("descAddLecon"), style: AlertStyle.success)
                                        let tabBarViewControllers = self.tabBarController?.viewControllers
                                        let adController = tabBarViewControllers![2] as! UINavigationController
                                        
                                        let adminTVC = adController.viewControllers[0] as! Lec_onViewController
                                        
                                        _ = adminTVC.navigationController?.popToRootViewController(animated: false)
                                    }
                                    else {
                                        if self.participants.count != 0
                                        {
                                            LoaderAlert.show(withStatus: "theltha")
                                            var point = ""
                                            if self.long == "48.956467" && self.lat == "4.358637" {
                                                point = "default"
                                            }else {
                                                point = self.activity["prestataire"]["point"]["id"].stringValue
                                            }
                                            let group = DispatchGroup()
                                            
                                            for i in 0 ... (self.participants.count - 1)
                                            {
                                                
                                                if (self.participants[i]["id"].stringValue != a["user"]["id"].stringValue )
                                                {
                                                    var params: Parameters = [:]
                                                    
                                                    params = [
                                                        "idLecon": act["data"].stringValue ,
                                                        "idParticipant" : self.participants[i]["id"].stringValue
                                                    ]
                                                    
                                                    
                                                    print ("***********" , params)
                                                    
                                                    Alamofire.request(ScriptBase.sharedInstance.invitLesson , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                                                        .responseJSON { response in
                                                            
                                                            let b = JSON(response.data)
                                                            print ("invite friends : " , response)
                                                            LoaderAlert.dismiss()
                                                            if b["status"].boolValue {
                                                                if ( b["message"].stringValue == "user already participating")
                                                                {
                                                                    _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("alreadyUser"), style: AlertStyle.error)
                                                                    return
                                                                }
                                                                else {
                                                                    
                                                                    let ab = UserDefaults.standard.value(forKey: "User") as! String
                                                                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                                                                    var a = JSON(data: dataFromString!)
                                                                    let header: HTTPHeaders = [
                                                                        "Content-Type" : "application/json",
                                                                        "x-Auth-Token" : a["value"].stringValue
                                                                    ]
                                                                    
                                                                    let params: Parameters = [
                                                                        "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " vous a invité a une leçon" ,
                                                                        "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " invited you to join lesson" ,
                                                                        "userids" : ["\(self.participants[i]["id"].stringValue)"] ,
                                                                        "type" : "rdv" ,
                                                                        "titre" : "Invitation" ,
                                                                        "photo" : "default" ,
                                                                        "activite" : act["data"]["id"].stringValue ,
                                                                        "sender" : a["user"]["id"].stringValue ,
                                                                        "rdv" : point
                                                                    ]
                                                                    group.enter()
                                                                    Alamofire.request(ScriptBase.sharedInstance.creeNotif , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                                                                        .responseJSON { response in
                                                                            
                                                                            if i == (self.participants.count - 1)
                                                                            {
                                                                                print ("haddar:",response)
                                                                                
                                                                                
                                                                            }
                                                                            
                                                                            group.leave()
                                                                    }
                                                                    
                                                                }
                                                                
                                                            }else{
                                                                
                                                                _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("erreur"), style: AlertStyle.error)
                                                                
                                                            }
                                                            
                                                            
                                                            
                                                    }
                                                    
                                                    // group.wait()
                                                }
                                                group.notify(queue: DispatchQueue.main, execute: {
                                                    print("jajajaja")
                                                    DispatchQueue.main.async(execute: {
                                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "POPUP"), object: nil )
                                                    })
                                                })
                                                
                                            }
                                            
                                        }
                                    }
                                }else {
                                    _ = SweetAlert().showAlert(Localization("addLecon"), subTitle: Localization("erreur"), style: AlertStyle.error)
                                }
                                
                                
                        }
                    }
                    
                }
                else {
                    LoaderAlert.dismiss()
                    self.validerPAnierBTN.isEnabled = true
                    print ("chaay mat3adech")
                }
            }
            
        }
        else {
            self.validerPAnierBTN.isEnabled = true
            _ = SweetAlert().showAlert(Localization("payer"), subTitle: Localization("entrerInfos"), style: AlertStyle.error)
        }
        
    }
    func createNotifPrest(typeNotif:String,presId:String,titleLesson:String,LessonId:String){
        if typeNotif == "CreateLesson" {
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        var params: Parameters = [:]
        
        params = [
            "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " a creé une leçon" ,
            "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " created a lesson" ,
            "userids" : ["\(presId)"] ,
            "type" : "create" ,
            "titre" :  titleLesson  ,
            "photo" : a["user"]["photo"].stringValue ,
            "activite" : LessonId ,
            "sender" : a["user"]["id"].stringValue,
            "rdv" : ""
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.creeNotif , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print ("NotifLesson:",response)
        }
        }else if typeNotif == "JoinLesson"{
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : a["value"].stringValue
            ]
            var params: Parameters = [:]
            
            params = [
                "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " s'est inscrit." ,
                "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " subscribed" ,
                "userids" : ["\(presId)"] ,
                "type" : "join" ,
                "titre" :  titleLesson  ,
                "photo" : a["user"]["photo"].stringValue ,
                "activite" : LessonId ,
                "sender" : a["user"]["id"].stringValue,
                "rdv" : ""
            ]
            
            Alamofire.request(ScriptBase.sharedInstance.creeNotif , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    print ("NotifLesson:",response)
            }
        }
    }
        
}


extension PanierKissouViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tableHeight.constant = tableView.contentSize.height
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "panierCell") as! UITableViewCell
        let labelSnowboard : UILabel = cell.viewWithTag(1) as! UILabel
        let viewSnowboard : UIView = cell.viewWithTag(1000) as! UIView
        viewSnowboard.layer.cornerRadius = 5
        viewSnowboard.layer.masksToBounds = true
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            //labelSnowboard.text = self.activity[indexPath.row]["sport"]["pratique"].stringValue
            labelSnowboard.text = "Leçon"
        }else {
            /*   switch self.activity["sport"]["pratique"].stringValue {
             case SportSaifFR[0] :
             
             labelSnowboard.text = SportSaifEN[0]
             case SportSaifFR[1] :
             labelSnowboard.text = SportSaifEN[1]
             
             case SportSaifFR[2] :
             labelSnowboard.text = SportSaifEN[2]
             
             case SportSaifFR[3] :
             labelSnowboard.text = SportSaifEN[3]
             
             case SportSaifFR[4] :
             labelSnowboard.text = SportSaifEN[4]
             case SportSaifFR[5] :
             labelSnowboard.text = SportSaifEN[5]
             
             default :
             
             labelSnowboard.text = ""
             
             } */
            labelSnowboard.text = "Lesson"
        }
        if ( self.activity["sport"]["niveau"].stringValue == "Confirmé" )
        {
            viewSnowboard.backgroundColor = UIColor("#FF3E53")
        }
        else if (self.activity["sport"]["niveau"].stringValue == "Débutant")
        {
            viewSnowboard.backgroundColor = UIColor("#D3D3D3")
        }
        else if (self.activity["sport"]["niveau"].stringValue == "Intermédiaire")
        {
            viewSnowboard.backgroundColor = UIColor("#75A7FF")
        }
        else if (self.activity["sport"]["niveau"].stringValue == "Débrouillard"){
            viewSnowboard.backgroundColor = UIColor("#76EC9E")
        }
        else {
            viewSnowboard.backgroundColor = UIColor("#4D4D4D")
        }
        let dateHeure : UILabel = cell.viewWithTag(2) as! UILabel
        let duree : UILabel = cell.viewWithTag(3) as! UILabel
        let place : UILabel = cell.viewWithTag(4) as! UILabel
        // let placeRestantes : UILabel = cell.viewWithTag(5) as! UILabel
        // let labelmin : UILabel = cell.viewWithTag(6) as! UILabel
        //let labelmax : UILabel = cell.viewWithTag(7) as! UILabel
        let labelStation : UILabel = cell.viewWithTag(5) as! UILabel
        let imgUser : UIImageView = cell.viewWithTag(10) as! UIImageView
        var s = heureDep
        let toArray = s.components(separatedBy: "/")
        let backToString = toArray.joined(separator: ":")
        let sr = dateDep
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            dateHeure.text = sr + " à " + backToString.prefix(5)
        }
        else {
            dateHeure.text = sr + " at " + backToString.prefix(5)
        }
        
        let heure = String(self.activity["duree"].intValue / 60)
        var minute = String (self.activity["duree"].intValue - ( (self.activity["duree"].intValue / 60) * 60 ))
        if (minute == "0")
        {
            minute = ""
        }
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            duree.text = "Durée : " + heure + "h" + minute
        }else {
            duree.text = "Duration : " + heure + "h" + minute
        }
        
        
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            //  if (self.activity[indexPath.row]["nb_place_maximum"].intValue - self.activity[indexPath.row]["nb_participant"].intValue) > 0 {
            place.text = self.activity["nombre_participant"].stringValue + " personnes "
            //  placeRestantes.text = " " + String(self.activity[indexPath.row]["nb_place_maximum"].intValue - self.activity[indexPath.row]["nb_participant"].intValue) + " restante(s)"
            //  } else {
            //    place.text = self.activity[indexPath.row]["nb_place_maximum"].stringValue + " pers. / "
            //  placeRestantes.text = " " + "0" + " restante(s)"
            //  }
            
        }else {
            //  if (self.activity[indexPath.row]["nb_place_maximum"].intValue - self.activity[indexPath.row]["participants"].count) > 0 {
            place.text = self.activity["nombre_participant"].stringValue + " persons "
            //  placeRestantes.text = " " + String(self.activity[indexPath.row]["nb_place_maximum"].intValue - self.activity[indexPath.row]["nb_participant"].intValue) + " remaining"
            //  } else {
            //    place.text = self.activity[indexPath.row]["nb_place_maximum"].stringValue + " pers. / "
            //  placeRestantes.text = " " + "0" + " remaining"
            // }
            
            
        }
        
        // labelStation.text = self.activity[indexPath.row]["station"].stringValue
        if let urlImgUser = URL(string : self.activity[indexPath.row]["station"]["photo"].stringValue) , let placeholder =  UIImage(named: "station") {
            imgUser.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        let labelmin : UILabel = cell.viewWithTag(6) as! UILabel
        let labelmax : UILabel = cell.viewWithTag(7) as! UILabel
        labelStation.text = self.activity["prestataire"]["point"]["station"]["nom"].stringValue
        labelStation.numberOfLines = 2
        labelStation.lineBreakMode = .byWordWrapping
        labelmin.text = self.activity["tarifs"][0]["prix_min"].stringValue + " € TTC "
        labelmax.text = self.activity["tarifs"][0]["prix_max"].stringValue  + " € TTC"
        labelmin.layer.cornerRadius = 5
        labelmin.layer.masksToBounds = true
        labelmax.layer.cornerRadius = 5
        labelmax.layer.masksToBounds = true
        return cell
    }
    func aesEncrypt(key:String, text: String,iv:[UInt8]) throws -> String {
        //let iv = AES.randomIV(AES.blockSize)
        let data = text.data(using: .utf8)!
        let encrypted = try AES(key: key.bytes, blockMode: .CBC(iv: iv), padding: .pkcs7).encrypt([UInt8](data))
        print(encrypted)
        let encryptedData = Data(encrypted)
        return encryptedData.base64EncodedString()
    }
    func aesDecrypt(key:String, text: String,iv:[UInt8]) throws -> String {
        //let iv = AES.randomIV(AES.blockSize)
        // let data = text.data(using: .utf8)!
        let data = Data(base64Encoded: text, options: NSData.Base64DecodingOptions(rawValue: 0))
        let encrypted = try AES(key: key.bytes, blockMode: .CBC(iv: iv), padding: .pkcs7).decrypt([UInt8](data!.bytes  ))
        print(encrypted)
        let encryptedData = Data(encrypted)
        let decrypteddata = String(data: encryptedData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        return String(decrypteddata!)
    }
    
    
}
