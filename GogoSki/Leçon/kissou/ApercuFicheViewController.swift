//
//  ApercuFicheViewController.swift
//  GogoSki
//
//  Created by AYMEN on 11/16/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyJSON
extension MKPinAnnotationView {
    class func bluePinColor() -> UIColor {
        return UIColor.blue
    }
}
extension MKAnnotationView {
    class func blueColor() -> UIColor {
        return UIColor.blue
    }
}
class ApercuFicheViewController: InternetCheckViewControllers , MKMapViewDelegate  {
    /*
     *Levels in French
     *
     */
    let NiveauSaifFR = ["Débutant","Débrouillard" ,"Intermédiaire","Confirmé","Expert"]
    /*
     *Levels in English
     *
     */
    let NiveauSaifEN = ["Beginner","Resourceful" ,"Intermediate","Confirmed","Expert"]
    /*
     *Practice in French
     *
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     *Practice in English
     *
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     *participants data
     *
     */
    var participants : JSON = []
    /*
     * hangout id
     *
     */
    var id : String = ""
    /*
     *hangouts data
     *
     */
    var sortie : JSON = []
    /*
     *conversation id from this activity
     *
     */
    var idConv = ""
    //bav bar
    /*
     *bar title label
     *
     */
    @IBOutlet weak var titreNavBarLBL: UILabel!
    /*
     *imageview of user from the top right bar
     *
     */
    @IBOutlet weak var imageToProfileNavBar: UIImageView!
    ///header
    /*
     *label practice width
     *
     */
    @IBOutlet weak var SportUserContWidth: NSLayoutConstraint!
    /*
     *view of the navigation bar
     *
     */
    @IBOutlet weak var viewLfouk: UIView!
    /*
     *the whole view without the nav bar view (to take a screenshoot fro share)
     *
     */
    @IBOutlet weak var shoot: UIView!
    /*
     *Station Title
     *
     */
    @IBOutlet weak var stationTitleLBL: UILabel!
    /*
     *Share button
     *
     */
    @IBOutlet weak var partageBTN: UIButton!
    /*
     *hangout imageView
     *
     */
    @IBOutlet weak var imageSortie: UIImageView!
    /*
     *hangout title label
     *
     */
    @IBOutlet weak var sortieTitreLBL: UILabel!
    /*
     *Practice type title
     *
     */
    @IBOutlet weak var sportTypeBTN: UIButton!
    /*
     *level sport type title
     *
     */
    @IBOutlet weak var sportNiveauBTN: UIButton!
    //informations
    /*
     *From Date label
     *
     */
    @IBOutlet weak var departDateLBL: UILabel!
    /*
     *From hour label
     *
     */
    @IBOutlet weak var departHeureLBL: UILabel!
    /*
     *duration label
     *
     */
    @IBOutlet weak var periodeHeuresLBL: UILabel!
    /*
     *how many particiapnts in this activity
     *
     */
    @IBOutlet weak var nombreParticipantLBL: UILabel!
    /*
     *how many remaining participants
     *
     */
    @IBOutlet weak var nombrePlacesRestantesLBL: UILabel!
    @IBOutlet weak var map: MKMapView!
    //description
    /*
     *description + map container height
     *
     */
    @IBOutlet weak var theWholeContainerHeight: NSLayoutConstraint!
    /*
     *description height
     *
     */
    @IBOutlet weak var descriptionContainerHeight: NSLayoutConstraint!
    /*
     *description label
     *
     */
    @IBOutlet weak var descriptionLBL: UILabel!
    /*
     * more/less button description
     *
     */
    @IBOutlet weak var plusBtn: UIButton!
    //participants
    /*
     *number of remaining place in the participant header
     *
     */
    @IBOutlet weak var placeRestantePHLBL: UILabel!
    /*
     *Conversation-user button
     *
     */
    @IBOutlet weak var messagePHBTN: UIButton!
    /*
     *add new user to this activity
     *
     */
    @IBOutlet weak var ajouterPHBTN: UIButton!
    /*
     *participants table view
     *
     */
    @IBOutlet weak var participantsTV: UITableView!
    /*
     *join this activity
     *
     */
    @IBOutlet weak var validerBTN: UIButton!
    
    /*
     *get the houngout data from Data Base
     *
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getSortie()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if let urlImgUser = URL(string : a["user"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imageToProfileNavBar.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        self.map.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        
        configureViewFromLocalisation()
        configureTileOverlay()
    }
    @objc func DeleteNotifVar(notification:NSNotification) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.NotifReceive =  false
    }
    private func configureTileOverlay() {
        // We first need to have the path of the overlay configuration JSON
        guard let overlayFileURLString = Bundle.main.path(forResource: "Overlay", ofType: "json") else {
            return
        }
        let overlayFileURL = URL(fileURLWithPath: overlayFileURLString)
        
        // After that, you can create the tile overlay using MapKitGoogleStyler
        guard let tileOverlay = try? MapKitGoogleStyler.buildOverlay(with: overlayFileURL) else {
            return
        }
        
        // And finally add it to your MKMapView
        map.add(tileOverlay)
    }
    // actions
    /*
     *join this activity action
     *
     */
    @IBAction func validerAction(_ sender: Any) {
        let params: Parameters = [
            "idAct": self.id
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.joinSortie , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                
                let b = JSON(response.data)
                print ("join : " , b)
                if b["status"].boolValue {
                    
                    _ = SweetAlert().showAlert(Localization("BtnRejoindreSortie"), subTitle: Localization("cetSortie"), style: AlertStyle.success)
                    self.validerBTN.isHidden = true
                    self.partageBTN.isHidden = false
                    self.messagePHBTN.isHidden = false
                    self.ajouterPHBTN.isHidden = false
                    self.participants = b["data"]
                    self.participantsTV.reloadData()
                }else{
                    
                    _ = SweetAlert().showAlert(Localization("BtnRejoindreSortie"), subTitle: Localization("erreur"), style: AlertStyle.error)
                    
                }
                
                
        }
    }
    /*
     *message action
     *
     */
    @IBAction func messgeAction(_ sender: Any) {
        LoaderAlert.show()
        self.validerBTN.isEnabled = false
        let params: Parameters = [
            "idConv": self.idConv
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.joinConvActivite , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                self.validerBTN.isEnabled = true
                print ("add conv "  , b)
                if b["status"].boolValue {
                    let ConvId = self.idConv
                    print("ConvID",ConvId)
                    SocketIOManager.sharedInstance.connectToServerWithNickname(nickname: ConvId, userid: a["user"]["id"].stringValue, first: false)
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.NotifAhmed =  true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToMessageForomNoWhere2"), object: ConvId )
                }
                
                
        }
        
        
    }
    /*
     *invite user action
     *
     */
    @IBAction func inviteAction(_ sender: Any) {
        
        let view = self.storyboard?.instantiateViewController(withIdentifier: "InviterParticipant") as! InviterParticipantViewController
        view.sortie = self.sortie
        self.navigationController?.present(view, animated: true, completion: nil)
    }
    /*
     *share action
     *
     */
    @IBAction func shareAction(_ sender: Any) {
        
        
        /*self.viewLfouk.isHidden = true
         UIGraphicsBeginImageContext(self.shoot.frame.size)
         shoot.layer.render(in: UIGraphicsGetCurrentContext()!)
         let image = UIGraphicsGetImageFromCurrentImageContext()
         UIGraphicsEndImageContext()
         self.viewLfouk.isHidden = false */
        let URL = "http://www.gogoski.fr/user/sortie/" + self.id
        let vc = UIActivityViewController(activityItems: [URL], applicationActivities: [])
        present(vc, animated: true)
        
        
    }
    /*
     * return UILabel height
     *
     */
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    /*
     * more/less action description
     *
     */
    @IBAction func expandCollapseDescription(_ sender: Any) {
        if plusBtn.title(for: .normal) == Localization("plusP") {
            descriptionContainerHeight.constant -= self.descriptionLBL.frame.height
            theWholeContainerHeight.constant -= self.descriptionLBL.frame.height
            descriptionContainerHeight.constant += heightForView(text: self.descriptionLBL.text!, font: self.descriptionLBL.font, width: self.descriptionLBL.frame.width)
            descriptionLBL.numberOfLines = 0
            print(theWholeContainerHeight.constant)
            
            theWholeContainerHeight.constant =  descriptionContainerHeight.constant + 48.5
            
            plusBtn.setTitle(Localization("moinsP"), for: .normal)
        }else {
            descriptionContainerHeight.constant = 433.0
            theWholeContainerHeight.constant = 433.0
            descriptionLBL.numberOfLines = 3
            plusBtn.setTitle(Localization("plusP"), for: .normal)
        }
        
    }
    /*
     *back action
     *
     */
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /////////********langue"**********///////
    /*
     *remove language observer
     *
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    /*
     *configure texts
     *
     */
    func configureViewFromLocalisation() {
        //   titleNavBar.text = Localization("FicheLeconTitre")
        titreNavBarLBL.text = Localization("FicheLeconTitre")
        
        validerBTN.setTitle(Localization("BtnRejoindreSortie"), for: .normal)
        
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
    /*
     *custom image for annotation
     *
     */
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let saifPin =  MKAnnotationView(annotation: annotation, reuseIdentifier: "PINA")
        saifPin.isEnabled = true
        saifPin.canShowCallout = true
        
        saifPin.image = UIImage(named: "Pin_saif")
        
        // saifPin.tintColor = MKAnnotationView.blueColor()
        // saifPin.tintAdjustmentMode = .normal
        return saifPin
    }
    /*
     *get hangout data from database
     *
     */
    func getSortie () {
        LoaderAlert.show()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.getSortieParId + self.id  , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                print ("********act" , b)
                if b["status"].boolValue {
                    self.validerBTN.isEnabled = true
                    self.partageBTN.isEnabled = true
                    self.sortie = b["data"]
                    self.idConv = b["data"]["sortie"]["conversation"]["id"].stringValue
                    self.stationTitleLBL.text = b["data"]["sortie"]["station"].stringValue
                    self.sortieTitreLBL.text = b["data"]["sortie"]["titre"].stringValue
                    var mySubstring = b["data"]["sortie"]["date_debut"].stringValue.prefix(10)
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        self.departDateLBL.text = "Départ le : " + mySubstring
                    }else {
                        self.departDateLBL.text = "Departure on : " + mySubstring
                    }
                    var mySubstring1 = b["data"]["sortie"]["date_debut"].stringValue.replacingOccurrences(of: "/", with: ":").suffix(8)
                    //   mySubstring1 = mySubstring1.replacingOccurrences(of: "/", with: ":")
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        self.departHeureLBL.text = "à : " + mySubstring1
                    }else {
                        self.departHeureLBL.text = "at : " + mySubstring1
                    }
                    self.descriptionLBL.text = b["data"]["sortie"]["detail"].stringValue
                    //self.descriptionLBL.text = "" + String(self.descriptionLBL.text!.prefix(98))
                    if self.descriptionLBL.text!.count < 98 {
                        self.plusBtn.setTitle(Localization("plusP"), for: .normal)
                        self.plusBtn.isHidden = true
                    }else{
                        self.plusBtn.setTitle(Localization("plusP"), for: .normal)
                        self.plusBtn.isHidden = false
                    }
                    print("*******count*****:",self.descriptionLBL.text?.count)
                    //if self.descriptionLBL.text.count >=
                    let heure =  String(b["data"]["sortie"]["duree"].intValue / 60)
                    var minute = String (b["data"]["sortie"]["duree"].intValue - ( (b["data"]["sortie"]["duree"].intValue / 60) * 60 ))
                    if (minute == "0")
                    {
                        minute = ""
                    }
                    
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        self.periodeHeuresLBL.text = "Durée : " + heure + "h" + minute
                    }else {
                        self.periodeHeuresLBL.text = "Duration : " + heure + "h" + minute
                    }
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                        if b["data"]["sortie"]["nb_place_maximum"].intValue == 100{
                            self.placeRestantePHLBL.text = ""
                            
                        }else if b["data"]["sortie"]["nb_place_maximum"].intValue != 0 {
                            self.placeRestantePHLBL.text =  String(b["data"]["sortie"]["nb_place_maximum"].intValue - b["data"]["participants"].count) + " restante(s)"
                        }
                        else {
                            self.placeRestantePHLBL.text =  "0" + " restante(s)"
                        }
                    }else {
                        if b["data"]["sortie"]["nb_place_maximum"].intValue == 100{
                            self.placeRestantePHLBL.text = ""
                            
                        }else if (b["data"]["sortie"]["nb_place_maximum"].intValue - b["data"]["participants"].count) > 0 {
                            self.placeRestantePHLBL.text =  String(b["data"]["sortie"]["nb_place_maximum"].intValue - b["data"]["participants"].count) + " remaining"
                        }
                        else {
                            self.placeRestantePHLBL.text =  "0" + " remaining"
                        }
                    }
                    
                    if (b["data"]["sortie"]["nb_place_maximum"].intValue != 0 && b["data"]["sortie"]["nb_place_maximum"].intValue != 100)
                    {
                        self.nombreParticipantLBL.text = b["data"]["sortie"]["nb_place_maximum"].stringValue + " pers. / "
                        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                            
                            if(b["data"]["sortie"]["nb_place_maximum"].intValue - b["data"]["participants"].count) > 0 {
                                self.nombrePlacesRestantesLBL.text = " " + String(b["data"]["sortie"]["nb_place_maximum"].intValue - b["data"]["participants"].count) + " restante(s)"
                            }else {
                                self.nombrePlacesRestantesLBL.text = " " + "0" + " restante(s)"
                            }
                        }else {
                            if b["data"]["sortie"]["nb_place_maximum"].stringValue != "0" {
                                self.nombrePlacesRestantesLBL.text = " " + String(b["data"]["sortie"]["nb_place_maximum"].intValue - b["data"]["participants"].count) + " remaining"
                            }else {
                                self.nombrePlacesRestantesLBL.text = " " + "0" + " remaining"
                            }
                        }
                    }
                    else {
                        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                            self.nombreParticipantLBL.text = "Illimité"
                            self.nombrePlacesRestantesLBL.text = ""
                        }
                        else {
                            self.nombreParticipantLBL.text = "Limitless"
                            self.nombrePlacesRestantesLBL.text = ""
                        }
                    }
                    ///*//****** Sport
                    if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr"{
                        switch b["data"]["sortie"]["sport"]["pratique"].stringValue {
                        case self.SportSaifFR[0] :
                            
                            self.sportTypeBTN.setTitle(self.SportSaifEN[0], for: .normal)
                        case self.SportSaifFR[1] :
                            
                            
                            self.sportTypeBTN.setTitle(self.SportSaifEN[1], for: .normal)
                        case self.SportSaifFR[2] :
                            
                            
                            self.sportTypeBTN.setTitle(self.SportSaifEN[2], for: .normal)
                        case self.SportSaifFR[3] :
                            
                            self.sportTypeBTN.setTitle(self.SportSaifEN[3], for: .normal)
                        case self.SportSaifFR[4] :
                            
                            
                            self.sportTypeBTN.setTitle(self.SportSaifEN[4], for: .normal)
                        case self.SportSaifFR[5] :
                            
                            
                            self.sportTypeBTN.setTitle(self.SportSaifEN[5], for: .normal)
                        default :
                            self.sportTypeBTN.setTitle("", for: .normal)
                        }
                    } else {
                        self.sportTypeBTN.setTitle(b["data"]["sortie"]["sport"]["pratique"].stringValue, for: .normal)
                    }
                    if ((self.sportTypeBTN.title(for: .normal)?.count)! > 19)
                    {
                        self.SportUserContWidth.constant = 200
                    }else if ((self.sportTypeBTN.title(for: .normal)?.count)! > 15)
                    {
                        self.SportUserContWidth.constant = 150
                    }else if ((self.sportTypeBTN.title(for: .normal)?.count)! > 10) {
                        self.SportUserContWidth.constant = 120
                    }
                    else {
                        self.SportUserContWidth.constant = 100
                    }
                    ///lognLat
                    var lat = ""
                    var long = ""
                    
                    lat = b["data"]["sortie"]["point"]["latitude"].stringValue
                    long = b["data"]["sortie"]["point"]["longitude"].stringValue
                    if long.prefix(1) == " "
                    {
                        long = String (long.dropFirst())
                    }
                    if ( lat.suffix(1) == " " )
                    {
                        lat = String (lat.dropLast())
                    }
                    
                    if lat != "" && long != "" {
                        let point = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!)
                        let annotation = ColorPointAnnotation(pinColor: UIColor.blue)
                        annotation.coordinate = point
                        
                        self.map.addAnnotation(annotation)
                        self.map.setCenter(point, animated: true)
                        
                    }
                    ////
                    
                    
                    if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr"{
                        switch b["data"]["sortie"]["sport"]["niveau"].stringValue {
                        case self.NiveauSaifFR[0] :
                            
                            self.sportNiveauBTN.setTitle(self.NiveauSaifEN[0], for: .normal)
                        case self.NiveauSaifFR[1] :
                            self.sportNiveauBTN.setTitle(self.NiveauSaifEN[1], for: .normal)
                            
                        case self.NiveauSaifFR[2] :
                            self.sportNiveauBTN.setTitle(self.NiveauSaifEN[2], for: .normal)
                            
                        case self.NiveauSaifFR[3] :
                            
                            self.sportNiveauBTN.setTitle(self.NiveauSaifEN[3], for: .normal)
                        case self.NiveauSaifFR[4] :
                            self.sportNiveauBTN.setTitle(self.NiveauSaifEN[4], for: .normal)
                        default :
                            self.sportNiveauBTN.setTitle("", for: .normal)
                        }
                    } else {
                        self.sportNiveauBTN.setTitle(b["data"]["sortie"]["sport"]["niveau"].stringValue, for: .normal)
                    }
                    
                    //////
                    
                    
                    ////////********Niveau
                    
                    
                    /////
                    
                    
                    if ( b["data"]["sortie"]["sport"]["niveau"].stringValue == "Confirmé" )
                    {
                        self.sportNiveauBTN.setTitleColor(UIColor("#FF3E53"), for: .normal)
                    }
                    else if (b["data"]["sortie"]["sport"]["niveau"].stringValue == "Débutant")
                    {
                        self.sportNiveauBTN.setTitleColor(UIColor("#D3D3D3"), for: .normal)
                    }
                    else if (b["data"]["sortie"]["sport"]["niveau"].stringValue == "Intermédiaire")
                    {
                        self.sportNiveauBTN.setTitleColor(UIColor("#75A7FF"), for: .normal)
                    }
                    else if (b["data"]["sortie"]["sport"]["niveau"].stringValue == "Expert") {
                        self.sportNiveauBTN.setTitleColor(UIColor("#4D4D4D"), for: .normal)
                    }
                    else {
                        self.sportNiveauBTN.setTitleColor(UIColor("#76EC9E"), for: .normal)
                    }
                    
                    self.participants = b["data"]["participants"]
                    self.participantsTV.reloadData()
                    if ( b["data"]["participant"].boolValue)
                    {
                        self.validerBTN.isHidden = true
                        self.partageBTN.isHidden = false
                        self.messagePHBTN.isHidden = false
                        if (b["data"]["sortie"]["nb_place_maximum"].intValue == b["data"]["sortie"]["nb_participant"].intValue)
                        {
                            self.ajouterPHBTN.isHidden = true
                        }
                        else {
                            
                            self.ajouterPHBTN.isHidden = false
                        }
                    }
                    else if b["data"]["sortie"]["nb_place_maximum"].intValue == b["data"]["sortie"]["nb_participant"].intValue
                    {
                        
                        self.validerBTN.isEnabled = false
                        _ = SweetAlert().showAlert(Localization("SoritesTitre"), subTitle: Localization("full"), style: AlertStyle.error)
                        
                    }
                    
                    
                }
                else {
                    _ = SweetAlert().showAlert("Sortie", subTitle: Localization("erreur"), style: AlertStyle.error)
                }
        }
    }
    
    
}


extension ApercuFicheViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (participants.arrayObject?.count == 0)
        {
            return 0
        }
        else {
            return (participants.arrayObject?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "participantsCell" , for: indexPath)
        let imgUser : UIImageView = cell.viewWithTag(10) as! UIImageView
        let labelName : UILabel = cell.viewWithTag(1) as! UILabel
        let labelSport : UILabel = cell.viewWithTag(2) as! UILabel
        let labelNiveau : UILabel = cell.viewWithTag(3) as! UILabel
        if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr"{
            print("pratique:",self.participants[indexPath.row]["sports"][0]["pratique"].stringValue)
            switch self.participants[indexPath.row]["sports"][0]["pratique"].stringValue {
            case SportSaifFR[0] :
                labelSport.text = SportSaifEN[0]
            case SportSaifFR[1] :
                
                labelSport.text = SportSaifEN[1]
            case SportSaifFR[2] :
                
                labelSport.text = SportSaifEN[2]
            case SportSaifFR[3] :
                labelSport.text = SportSaifEN[3]
            case SportSaifFR[4] :
                
                labelSport.text = SportSaifEN[4]
            case SportSaifFR[5] :
                
                labelSport.text = SportSaifEN[5]
            default :
                labelSport.text = ""
            }
        } else {
            labelSport.text = self.participants[indexPath.row]["sports"][0]["pratique"].stringValue
        }
        
        if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr" {
            switch self.participants[indexPath.row]["sports"][0]["niveau"].stringValue {
            case NiveauSaifFR[0] :
                labelNiveau.text = NiveauSaifEN[0]
            case NiveauSaifFR[1] :
                
                labelNiveau.text = NiveauSaifEN[1]
            case NiveauSaifFR[2] :
                
                labelNiveau.text = NiveauSaifEN[2]
            case NiveauSaifFR[3] :
                labelNiveau.text = NiveauSaifEN[3]
            case NiveauSaifFR[4] :
                labelNiveau.text = NiveauSaifEN[4]
            default :
                labelNiveau.text = ""
            }
        } else {
            labelNiveau.text = self.participants[indexPath.row]["sports"][0]["niveau"].stringValue
        }
        
        
        
        if ( self.participants[indexPath.row]["sports"][0]["niveau"].stringValue == "Confirmé" )
        {
            labelNiveau.textColor = UIColor("#FF3E53")
        }
        else if (self.participants[indexPath.row]["sports"][0]["niveau"].stringValue == "Débutant")
        {
            labelNiveau.textColor = UIColor("#D3D3D3")
        }
        else if (self.participants[indexPath.row]["sports"][0]["niveau"].stringValue == "Intermédiaire")
        {
            labelNiveau.textColor = UIColor("#75A7FF")
        }
        else if (self.participants[indexPath.row]["sports"][0]["niveau"].stringValue == "Expert")
        {
            labelNiveau.textColor = UIColor("#4D4D4D")
        }
        else {
            labelNiveau.textColor = UIColor("#76EC9E")
        }
        labelName.text = self.participants[indexPath.row]["prenom"].stringValue + " " + self.participants[indexPath.row]["nom"].stringValue
        if let urlImgUser = URL(string : self.participants[indexPath.row]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder") {
            imgUser.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        let btnMessages : CustomButton = cell.viewWithTag(100) as! CustomButton
        btnMessages.index = indexPath.row
        btnMessages.indexPath = indexPath
        btnMessages.isUserInteractionEnabled = true
        btnMessages.addTarget(self, action: #selector(self.messageAction), for: .touchUpInside)
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if a["user"]["id"].stringValue != self.participants[indexPath.row]["id"].stringValue {
            btnMessages.isHidden = false
        }else {
            btnMessages.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if (self.participants[indexPath.row]["id"].stringValue != a["users"]["id"].stringValue )
        {
            let view = self.storyboard?.instantiateViewController(withIdentifier: "visiteur_profil") as! visiteur_profil
            view.id = self.participants[indexPath.row]["id"].stringValue
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
    /*
     *conversation user action
     *
     */
    @objc func messageAction(sender : CustomButton) {
        LoaderAlert.show()
        let params: Parameters = [
            "amiId": self.participants[sender.index!]["id"].stringValue
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.addConvPriv , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                print ("add conv "  , b)
                if b["status"].boolValue {
                    var ConvId = "-1"
                    if b["message"].stringValue == "Conversation existe" {
                        ConvId =  b["data"]["id"].stringValue
                    }else {
                        ConvId = b["data"]["convId"].stringValue
                        SocketIOManager.sharedInstance.connectToServerWithNickname(nickname: ConvId, userid: a["user"]["id"].stringValue, first: false)
                    }
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToMessageForomNoWhere"), object: ConvId )
                }
                
                
        }
    }
    
}

class ColorPointAnnotation: MKPointAnnotation {
    var pinColor: UIColor
    
    init(pinColor: UIColor) {
        self.pinColor = pinColor
        super.init()
    }
}

