//
//  InscriptionUser.swift
//  GogoSki
//
//  Created by Bouzid saif on 21/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import FBSDKLoginKit
import Google
import GoogleSignIn
import LinkedinSwift
import WebKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
class InscriptionUser: InternetCheckViewControllers, GIDSignInDelegate,GIDSignInUIDelegate,UIWebViewDelegate , UITextViewDelegate{
    /*
     * array of all languages available
     */
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    /*
     * mail label
     */
    @IBOutlet weak var labelMail: UILabel!
    /*
     * or label
     */
    @IBOutlet weak var ou: UILabel!
    /*
     * description under the logo
     */
    @IBOutlet weak var desc: UITextView!
    /*
     * title of this page
     */
    @IBOutlet weak var titre: UILabel!
    /*
     * email pattern
     */
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    /*
     * sign in google
     */
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if user != nil {
            print(user.profile.email)
            LoaderAlert.shared.dismiss()
            GIDSignIn.sharedInstance().signOut()
            let params: Parameters = [
                "googleId": user.userID, //id
                "nom" : user.profile.familyName ,
                "prenom" : user.profile.givenName ,
                "email" : user.profile.email ,
                "photo" : user.profile.imageURL(withDimension: 400).absoluteString
            ]
            let header: HTTPHeaders = [
                "Content-Type" : "application/json"
            ]
            
            Alamofire.request(ScriptBase.sharedInstance.ConnexionUserGoogle , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    LoaderAlert.shared.dismiss()
                    //   self.btnConnexion.isEnabled = true
                    let a = JSON(response.data)
                    print("saif:",a)
                    
                    if a["status"].boolValue == false {
                        if a["message"].stringValue == "Email existe deja "{
                            let err = a["data"].stringValue
                            if err == "sql" {
                                _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionAutreMail") + "GogoSki", style: AlertStyle.error)
                            }else if err == "facebook" {
                                _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                            }else if err == "google" {
                                _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                            }else if err == "linkedin" {
                                _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                            }
                        } else {
                            _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionExist"), style: AlertStyle.error)
                        }
                        
                    }else if a["message"].stringValue != "Authentification successful" {
                        _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionErreur"), style: AlertStyle.success)
                    }else {
                        _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionExist"), style: AlertStyle.error)
                    }
                    
            }
        }
    }
    /*
     * configure language
     */
    func configureViewFromLocalisation() {
        titre.text = Localization("labelInscriptionUser")
        ou.text = Localization("ou")
        desc.text = Localization("politique")
        labelMail.text = Localization("labelMail")
        inscription.setTitle(Localization("btnInscritUser"), for: .normal)
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    
    /*
     * array of all languages available
     */
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        LoaderAlert.shared.show(withStatus: Localization("recup"))
        
    }
    /*
     * open webview of linkedin if there is no linkedin application
     */
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url = request.url!
        print(url)
        
        if url.host == "com.appcoda.linkedin.oauth" {
            
            if url.absoluteString.range(of: "code") != nil {
                // Extract the authorization code.
                let urlParts = url.absoluteString.components(separatedBy: "?")
                let code = urlParts[1].components(separatedBy: "=")[1]
                
                requestForAccessToken(authorizationCode: code)
            }
        }
        
        return true
    }
    /*
     * get access tocken from linkedin
     */
    func requestForAccessToken(authorizationCode: String) {
        let grantType = "authorization_code"
        let redirectURL = "https://com.appcoda.linkedin.oauth/oauth".addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        let linkedInKey = "86em0va3cx3dpw"
        let linkedInSecret = "ZEyvrZJPq61Rt2WX"
        let accessTokenEndPoint = "https://www.linkedin.com/uas/oauth2/accessToken"
        var postParams = "grant_type=\(grantType)&"
        postParams += "code=\(authorizationCode)&"
        postParams += "redirect_uri=\(redirectURL!)&"
        postParams += "client_id=\(linkedInKey)&"
        postParams += "client_secret=\(linkedInSecret)"
        let postData = postParams.data(using: String.Encoding.utf8)
        let request = NSMutableURLRequest(url: NSURL(string: accessTokenEndPoint)! as URL)
        // Indicate that we're about to make a POST request.
        request.httpMethod = "POST"
        
        // Set the HTTP body using the postData object created above.
        request.httpBody = postData
        request.addValue("application/x-www-form-urlencoded;", forHTTPHeaderField: "Content-Type")
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        // Make the request.
        let task: URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            let statusCode = (response as! HTTPURLResponse).statusCode
            
            if statusCode == 200 {
                // Convert the received JSON data into a dictionary.
                do {
                    let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String : Any]
                    
                    /* let accessToken = dataDictionary["access_token"] as! String
                     UserDefaults.standard.set(accessToken, forKey: "LIAccessToken")
                     UserDefaults.standard.synchronize() */
                    self.getProfile()
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.webView.removeFromSuperview()
                        //self.dismiss(animated: true, completion: nil)
                    })
                }
                catch {
                    print("Could not convert JSON data into a dictionary.")
                }
            }
        }
        
        task.resume()
    }
    
    /*
     * get information of linkedin profile
     */
    func getProfile(){
        if let accessToken = UserDefaults.standard.object(forKey: "LIAccessToken") {
            // Specify the URL string that we'll get the profile info from.
            let targetURLString = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,email-address)?format=json"
            let request = NSMutableURLRequest(url: NSURL(string: targetURLString)! as URL)
            // Indicate that we're about to make a POST request.
            request.httpMethod = "GET"
            
            // Set the HTTP body using the postData object created above.
            
            request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
            let session = URLSession(configuration: URLSessionConfiguration.default)
            let task: URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
                let statusCode = (response as! HTTPURLResponse).statusCode
                
                if statusCode == 200 {
                    // Convert the received JSON data into a dictionary.
                    do {
                        let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)  as! [String : Any]
                        print(dataDictionary)
                        
                        //let profileURLString = dataDictionary["publicProfileUrl"] as! String
                        //print(profileURLString)
                    }
                    catch {
                        print("Could not convert JSON data into a dictionary.")
                    }
                }
                
            }
            
            task.resume()
            
            
        }
    }
    /*
     * check if there is tocken of linkedin
     */
    func checkForExistingAccessToken() {
        if UserDefaults.standard.object(forKey: "LIAccessToken") != nil {
            print("we got this")
        }
    }
    
    /*
     * linkedin access information
     */
    var linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "86em0va3cx3dpw", clientSecret: "ZEyvrZJPq61Rt2WX", state: "\(arc4random())", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://com.appcoda.linkedin.oauth/oauth"))
    
    /*
     * email textfield
     */
    @IBOutlet weak var email: PaddingTextField!
    /*
     * password textfield
     */
    @IBOutlet weak var password: PaddingTextField!
    /*
     * facebook button
     */
    @IBOutlet weak var facebook_inscrit: UIButton!
    /*
     * gmail button
     */
    @IBOutlet weak var gmail_inscrit: UIButton!
    /*
     * linkedin button
     */
    @IBOutlet weak var linkedin_inscrit: UIButton!
    /*
     * incription button
     */
    @IBOutlet weak var inscription: UIButton!
    /*
     * textview of condition and terms
     */
    @IBOutlet weak var condition_view: UITextView!
    /*
     * initialize webview of linkedin
     */
    var webView = UIWebView()
    /*
     * variable contains facebook information
     */
    var dict : [String : AnyObject]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().clientID = "876794620181-b7fs583arl17d0nf6gaju0fg0blhaj6t.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
        // reach.addAction(UIAlertAction(title: "d'accord", style: .default, handler: nil))
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        desc.isUserInteractionEnabled = true
        desc.delegate = selfs
        desc.dataDetectorTypes = [.link]
        
        print(Localisator.sharedInstance.currentLanguage)
        if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr" {
            let privacy = desc.text!
            let attributedString : NSMutableAttributedString = NSMutableAttributedString(string: privacy)
            
            attributedString.addAttribute(NSAttributedStringKey.link, value: NSURL(string: "https://www.google.com")!, range: NSRange(location: 54, length: 20))
            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: NSMakeRange(54, 20))
            attributedString.addAttribute(NSAttributedStringKey.link, value: NSURL(string: "https://www.google.com")!, range: NSRange(location: 83, length: 14))
            
            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: NSMakeRange(83, 14))
            attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 54, length: 20))
            attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 83, length: 14))
            desc.attributedText = attributedString
        }
        else {
            let privacy = desc.text!
            let attributedString : NSMutableAttributedString = NSMutableAttributedString(string: privacy)
            //attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont.boldSystemFont(ofSize: 17), range: NSMakeRange(0, privacy.characters.count))
            attributedString.addAttribute(NSAttributedStringKey.link, value: NSURL(string: "https://www.google.com")!, range: NSRange(location: 60, length: 20))
            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: NSMakeRange(60, 20))
            attributedString.addAttribute(NSAttributedStringKey.link, value: NSURL(string: "https://www.google.com")!, range: NSRange(location: 87, length: 28))
            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: NSMakeRange(87, 28))
            attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 60, length: 20))
            attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 87, length: 28))
            desc.attributedText = attributedString
            
        }
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        return false
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if characterRange == NSRange(location: 54, length: 20) {
            let view =  self.storyboard?.instantiateViewController(withIdentifier: "TermsEn") as! PrivacyViewController
            self.present(view, animated: true, completion: nil)
        } else  if  characterRange == NSRange(location: 83, length: 14){
            
            let view =  self.storyboard?.instantiateViewController(withIdentifier: "MentionEn") as! PrivacyViewController
            self.present(view, animated: true, completion: nil)
        }else if characterRange == NSRange(location: 60, length: 20) {
            
            let view =  self.storyboard?.instantiateViewController(withIdentifier: "TermsFr") as! PrivacyViewController
            self.present(view, animated: true, completion: nil)
        }else {
            let view =  self.storyboard?.instantiateViewController(withIdentifier: "MentionFr") as! PrivacyViewController
            self.present(view, animated: true, completion: nil)
        }
        return false
    }
    
    /*
     * language configure
     */
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*
     * start autorization of api linkedin
     */
    func startAuthorization() {
        let linkedInKey = "86em0va3cx3dpw"
        let linkedInSecret = "ZEyvrZJPq61Rt2WX"
        let authorizationEndPoint = "https://www.linkedin.com/uas/oauth2/authorization"
        let accessTokenEndPoint = "https://www.linkedin.com/uas/oauth2/accessToken"
        let responseType = "code"
        
        // Set the redirect URL. Adding the percent escape characthers is necessary.
        let redirectURL = "https://com.appcoda.linkedin.oauth/oauth".addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        // Create a random string based on the time interval (it will be in the form linkedin12345679).
        let state = "linkedin\(UUID().uuidString)"
        
        // Set preferred scope.
        let scope = "r_basicprofile,r_emailaddress"
        
        // Create the authorization URL string.
        var authorizationURL = "\(authorizationEndPoint)?"
        authorizationURL += "response_type=\(responseType)&"
        authorizationURL += "client_id=\(linkedInKey)&"
        authorizationURL += "redirect_uri=\(redirectURL!)&"
        authorizationURL += "state=\(state)&"
        authorizationURL += "scope=\(scope)"
        
        print(authorizationURL)
        let request = NSURLRequest(url: NSURL(string: authorizationURL)! as URL )
        // let webView = WebViewController(url: NSURL(string: authorizationURL)! as URL)
        //  self.definesPresentationContext = true
        //self.present( UINavigationController(rootViewController: webView), animated: true, completion: nil)
        webView = UIWebView(frame: self.view.bounds)
        webView.delegate = self
        self.view.addSubview(webView)
        webView.loadRequest(request as URLRequest)
        
        
    }
    /*
     * back action
     */
    @IBAction func  go_back(_ sender :Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     * connect to facebook
     */
    @IBAction func connect_facebook(_ sender: Any) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        if FBSDKAccessToken.current() == nil {
            fbLoginManager.logIn(withReadPermissions: ["public_profile","email"], from: self) { (result, error) in
                if (error == nil){
                    let fbloginresult : FBSDKLoginManagerLoginResult = result!
                    if fbloginresult.grantedPermissions != nil {
                        if(fbloginresult.grantedPermissions.contains("email"))
                        {
                            self.getFBUserData()
                            
                        } else {
                            
                        }
                    }
                }
            }
            
        }
    }
    
    /*
     * connect to gmail
     */
    @IBAction func connect_gmail(_ sender: Any) {
        
        GIDSignIn.sharedInstance().signIn()
    }
    
    /*
     * connect to linkedin
     */
    @IBAction func connect_linkedin(_ sender: Any) {
        //SweetAlert
        //SVProgressHUD
        LoaderAlert.shared.show(withStatus: Localization("Chargement"))
        let appName = "Linkedin"
        let appScheme = "\(appName)://"
        let appUrl = URL(string: appScheme)
        if UIApplication.shared.canOpenURL(appUrl! as URL){
            if LinkedinNativeClient.validSession() {
                print("clearing")
                LinkedinNativeClient.clearSession()
                linkedinHelper =  LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "86em0va3cx3dpw", clientSecret: "ZEyvrZJPq61Rt2WX", state: "\(arc4random())", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://com.appcoda.linkedin.oauth/oauth"))
            }
            LoaderAlert.shared.dismiss()
            linkedinHelper.authorizeSuccess({ (token) in
                print("saif2")
                print(token)
                LoaderAlert.shared.show(withStatus: Localization("recup"))
                self.linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),positions,date-of-birth,phone-numbers,location)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
                    print("saif3")
                    let q = JSON(response.jsonObject)
                    print("q:",q)
                    LoaderAlert.shared.dismiss()
                    //let q = JSON (response)
                    let params: Parameters = [
                        "LinkedInId": q["id"].stringValue, //id
                        "nom" :  q["lastName"].stringValue ,
                        "prenom" : q["firstName"].stringValue ,
                        "email" : q["emailAddress"].stringValue ,
                        "photo" : q["pictureUrl"].stringValue ,
                        ]
                    let header: HTTPHeaders = [
                        "Content-Type" : "application/json"
                    ]
                    print(params.description)
                    Alamofire.request(ScriptBase.sharedInstance.ConnexionUserLinkedin , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                        .responseJSON { response in
                            LoaderAlert.shared.dismiss()
                            //   self.btnConnexion.isEnabled = true
                            let a = JSON(response.data)
                            print("saif:",a)
                            
                            if a["status"].boolValue == false {
                                
                                if a["message"].stringValue == "Email existe deja "{
                                    let err = a["data"].stringValue
                                    if err == "sql" {
                                        _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionAutreMail") + "GogoSki", style: AlertStyle.error)
                                    }else if err == "facebook" {
                                        _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                                    }else if err == "google" {
                                        _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                                    }else if err == "linkedin" {
                                        _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                                    }
                                } else {
                                    _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionExist"), style: AlertStyle.error)
                                }
                                
                            }else if a["message"].stringValue != "Authentification successful" {
                                _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionErreur"), style: AlertStyle.success)
                            }else {
                                _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionExist"), style: AlertStyle.error)
                            }
                    }
                    // self.dismiss(animated: true, completion: nil)
                    //parse this response which is in the JSON format
                }) {(error) -> Void in
                    print("looooooooooooooooooooool")
                    print(error.localizedDescription)
                    //handle the error
                }
                //This token is useful for fetching profile info from LinkedIn server
                
            }, error: { (error) in
                print("saif")
                print(error.localizedDescription)
                //show respective error
            }, cancel:  {
                //show sign in cancelled event
                print("cancled")
            })
            
        }else{
            startAuthorization()
        }
        
        
        
    }
    
    /*
     * get facebook information
     */
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email , gender , birthday" ]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let q = JSON(result as Any)
                    self.dict = result as! [String : AnyObject]
                    print(result!)
                    print(self.dict)
                    let params: Parameters = [
                        "facebookId": q["id"].stringValue,
                        "nom" : q["last_name"].stringValue ,
                        "prenom" : q["first_name"].stringValue ,
                        "email" : q["email"].stringValue ,
                        "photo" : q["picture"]["data"]["url"].stringValue
                    ]
                    let header: HTTPHeaders = [
                        "Content-Type" : "application/json"
                    ]
                    print(params.description)
                    Alamofire.request(ScriptBase.sharedInstance.ConnexionUserFb , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                        .responseString { response in
                            LoaderAlert.shared.dismiss()
                            //   self.btnConnexion.isEnabled = true
                            print(response)
                            let a = JSON(response.data)
                            print("saif:",a)
                            if a["status"].boolValue == false {
                                
                                // _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionExist"), style: AlertStyle.error)
                                if a["message"].stringValue == "Email existe deja "{
                                    let err = a["data"].stringValue
                                    if err == "sql" {
                                        _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionAutreMail") + "GogoSki", style: AlertStyle.error)
                                    }else if err == "facebook" {
                                        _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                                    }else if err == "google" {
                                        _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                                    }else if err == "linkedin" {
                                        _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                                    }
                                } else {
                                    _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionExist"), style: AlertStyle.error)
                                }
                                
                            }else if a["message"].stringValue != "Authentification successful" {
                                _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionErreur"), style: AlertStyle.success)
                            }else {
                                _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionExist"), style: AlertStyle.error)
                            }
                            
                            
                    }
                }
            })
        }
    }
    
    /*
     * incription action
     */
    @IBAction func inscription_action(_ sender: Any) {
        var langue : String = ""
        if Localisator.sharedInstance.currentLanguage == arrayLanguages[0] {
            if NSLocale.current.languageCode == "en" {
                
                langue = Localization(arrayLanguages[1])
            }else if NSLocale.current.languageCode == "fr"  {
                langue = Localization(arrayLanguages[2])
            }
        }
        else if Localisator.sharedInstance.currentLanguage == arrayLanguages[1]
        {
            langue = Localization(arrayLanguages[1])
        }
        else {
            langue = Localization(arrayLanguages[2])
        }
        if self.email.text != "" && self.password.text != ""{
            if isValidEmailAddress(emailAddressString: self.email.text!) {
                LoaderAlert.shared.show(withStatus: Localization("ChargementInscription"))
                self.inscription.isEnabled = false
                let params: Parameters = [
                    "email": self.email.text!,
                    "password" : self.password.text!,
                    "langue" : langue
                ]
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json"
                ]
                print(ScriptBase.sharedInstance.InscriptionUser)
                Alamofire.request(ScriptBase.sharedInstance.InscriptionUser , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        LoaderAlert.shared.dismiss()
                        self.inscription.isEnabled = true
                        let a = JSON(response.data)
                        print("saif:",a)
                        
                        if a["status"].boolValue {
                            self.email.text = ""
                            self.password.text = ""
                            _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionErreur"), style: AlertStyle.success)
                        }else{
                            if a["message"].stringValue == "email existe déja"{
                                self.email.text = ""
                                self.password.text = ""
                                _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("InscriptionExist"), style: AlertStyle.error)
                            }
                        }
                        
                        
                }
            }else {
                _ = SweetAlert().showAlert(Localization("InscriptionChargement"), subTitle: Localization("notemail"), style: AlertStyle.error)
            }
        }
    }
}



