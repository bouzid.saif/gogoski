//
//  AjouterSéjourViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 17/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import DownPicker

class AjouterSe_jourViewController: InternetCheckViewControllers {
    
    @IBOutlet weak var typePratique: UITextField!
    var typePratique_downPicker : DownPicker!
    @IBOutlet weak var departement: UITextField!
    var departement_downPicker : DownPicker!
    @IBOutlet weak var niveau: UITextField!
    var niveau_downPicker : DownPicker!
    @IBOutlet weak var station: UITextField!
    @IBOutlet weak var titre: UILabel!
    var station_downPicker : DownPicker!
    @IBOutlet weak var heureDep: UITextField!
    @IBOutlet weak var dateFin: UITextField!
    @IBOutlet weak var labelForfait: UILabel!
    @IBOutlet weak var heureFin: UITextField!
    var massif_downPicker : DownPicker!
    @IBOutlet weak var massif: UITextField!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var btnCreerSejour: UIButton!
    @IBOutlet weak var labelLogement: UILabel!
    @IBOutlet weak var lieuRdv: UITextField!
    @IBOutlet weak var placeDuree: UIView!
    @IBOutlet weak var lieuDepartOrganisateur: UITextField!
    @IBOutlet weak var dateDep: UITextField!
    @IBOutlet weak var typePratiqueNiveau: UIView!
    @IBOutlet weak var detailsSortie: UITextView!
    @IBOutlet weak var forfait: VKCheckbox!
    @IBOutlet weak var logement: VKCheckbox!
    @IBOutlet weak var cout: UITextField!
    
    let datePicker = UIDatePicker()
    let datePicker1 = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        showDatePicker()
        showTimePicker()
        showDatePicker1()
        showTimePicker1()
        typePratiqueNiveau.layer.cornerRadius = 10
        placeDuree.layer.cornerRadius = 10
        detailsSortie.layer.cornerRadius = 10
        
        let img = UIImageView()
        img.image = UIImage(named: "calender_ahmed")
        img.frame = CGRect(x: CGFloat(dateFin.frame.size.width - 20), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        
        dateFin.rightView = img
        dateFin.rightViewMode = .always
        
        let img1 = UIImageView()
        img1.image = UIImage(named: "time_ahmed")
        img1.frame = CGRect(x: CGFloat(heureFin.frame.size.width - 20), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        heureFin.rightView = img1
        heureFin.rightViewMode = .always
        
        let img2 = UIImageView()
        img2.image = UIImage(named: "calender_ahmed")
        img2.frame = CGRect(x: CGFloat(dateDep.frame.size.width - 20), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        dateDep.rightView = img2
        dateDep.rightViewMode = .always
        
        let img3 = UIImageView()
        img3.image = UIImage(named: "time_ahmed")
        img3.frame = CGRect(x: CGFloat(heureDep.frame.size.width - 20), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        heureDep.rightView = img3
        heureDep.rightViewMode = .always
        
        let button2 = UIButton(type: .custom)
        button2.setImage(UIImage(named: "location_ahmed"), for: .normal)
        button2.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button2.frame = CGRect(x: CGFloat(lieuRdv.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        button2.addTarget(self, action: #selector(self.pickLocation), for: .touchUpInside)
        lieuRdv.rightView = button2
        lieuRdv.rightViewMode = .always
        
        let img4 = UIImageView()
        img4.image = UIImage(named: "euro_ahmed")
        img4.frame = CGRect(x: CGFloat(cout.frame.size.width - 40), y: CGFloat(5), width: CGFloat(15), height: CGFloat(15))
        cout.rightView = img4
        cout.rightViewMode = .always
        
        let bandArray : NSMutableArray = []
        bandArray.add("Pratique 1")
        bandArray.add("Pratique 2")
        
        //self.down = UIDownPicker(data: bandArray as! [Any])
        typePratique_downPicker = DownPicker(textField: typePratique, withData: bandArray as! [Any])
        typePratique_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        
        let bandArray1 : NSMutableArray = []
        bandArray1.add("Departement 1")
        bandArray1.add("Departement 2")
        departement_downPicker = DownPicker(textField: departement, withData: bandArray1 as! [Any])
        departement_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        let bandArray2 : NSMutableArray = []
        bandArray2.add("niveau 1")
        bandArray2.add("niveau 2")
        niveau_downPicker = DownPicker(textField: niveau, withData: bandArray2 as! [Any])
        niveau_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        let bandArray3 : NSMutableArray = []
        bandArray3.add("station 1")
        bandArray3.add("station 2")
        station_downPicker = DownPicker(textField: station, withData: bandArray3 as! [Any])
        station_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        let bandArray4 : NSMutableArray = []
        bandArray4.add("massif 1")
        bandArray4.add("massif 2")
        massif_downPicker = DownPicker(textField: massif, withData: bandArray4 as! [Any])
        massif_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        forfait.borderWidth = 1
        forfait.bgColorSelected = UIColor("#5FB6FF")
        forfait.borderColor = UIColor.gray
        forfait.bgColor = UIColor.white
        
        logement.borderWidth = 1
        logement.bgColorSelected = UIColor("#5FB6FF")
        logement.borderColor = UIColor.gray
        logement.bgColor = UIColor.white
      
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showDatePicker(){
        
        datePicker.datePickerMode = .date
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(AjouterSe_jourViewController.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        dateDep.inputAccessoryView = toolbar
        dateDep.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        dateDep.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    func showDatePicker1(){
        
        datePicker.datePickerMode = .date
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(AjouterSe_jourViewController.donedatePicker1))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        dateFin.inputAccessoryView = toolbar
        dateFin.inputView = datePicker
        
    }
    
    @objc func donedatePicker1(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        dateDep.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func showTimePicker(){
        
        datePicker1.datePickerMode = .time
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(AjouterSe_jourViewController.doneTimePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelTimePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        heureDep.inputAccessoryView = toolbar
        heureDep.inputView = datePicker1
        
    }
    
    @objc func doneTimePicker(){
        
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        heureDep.text = formatter.string(from: datePicker1.date)
        self.view.endEditing(true)
    }
    func showTimePicker1(){
        
        datePicker1.datePickerMode = .time
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(AjouterSe_jourViewController.doneTimePicker1))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelTimePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        heureFin.inputAccessoryView = toolbar
        heureFin.inputView = datePicker1
        
    }
    
    @objc func doneTimePicker1(){
        
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        heureFin.text = formatter.string(from: datePicker1.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelTimePicker(){
        self.view.endEditing(true)
    }
    @objc func pickLocation ()
    {
        let view = self.storyboard?.instantiateViewController(withIdentifier: "choisirLieu") as! ChoisirLieuViewController
        self.navigationController?.present(view, animated: true, completion: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("CréerSéjourTitre")
        massif_downPicker.setPlaceholder(Localization("Massif"))
        niveau_downPicker.setPlaceholder(Localization("Niveau"))
        departement_downPicker.setPlaceholder(Localization("Departement"))
        station_downPicker.setPlaceholder(Localization("Station"))
        typePratique_downPicker.setPlaceholder(Localization("TypePratique"))
        lieuRdv.placeholder = Localization("LieuRdv")
        lieuDepartOrganisateur.placeholder = Localization("lieuDepartOrganisateur")
        detailsSortie.text = Localization("DetailsSortie")
        heureFin.placeholder = Localization("HeureFin")
        heureDep.placeholder = Localization("HeureDep")
        dateDep.placeholder = Localization("DateDep")
        dateFin.placeholder = Localization("DateFin")
        cout.placeholder = Localization("Cout")
        labelForfait.text = Localization("forfait")
        labelLogement.text = Localization("logement")
        labelDesc.text = Localization("DescSéjour")
        btnCreerSejour.setTitle(Localization("BtnCréerSéjour"), for: .normal)
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
}

