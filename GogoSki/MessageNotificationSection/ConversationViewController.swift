//
//  ConversationViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 15/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapleBacon
/*
 *this is actually the messageControlelr
 *
 */
class ConversationViewController: InternetCheckViewControllers , UITableViewDataSource , UITableViewDelegate,UITextFieldDelegate{
    /*
     * navigation controller of this controller
     */
    var parentNavigationController : UINavigationController?
    /*
     *the default message when to message between the user and you
     *
     */
    @IBOutlet weak var default_message: UILabel!
    /*
     *the typing user name
     *
     */
    @IBOutlet weak var TypingName: UILabel!
    /*
     *the image of the user who talking to you
     *
     */
    @IBOutlet weak var ImageName: UIImageView!
    @IBOutlet weak var bottomtable: NSLayoutConstraint!
    /*
     *to know this is the first user of the view
     *
     */
    var first = true

    @IBOutlet weak var tableview: UITableView!
   
    /*
     *conversation data
     *
     */
    var Conversation = JSON()
    /*
     *messages data
     *
     */
    var Messages = JSON()
    /*
     *the send message button
     *
     */
    @IBOutlet weak var button : UIButton!
    /*
     *the name of the view
     *
     */
    @IBOutlet weak var labelName: UILabel!
    /*
     *the nameof the talking user
     *
     */
    var name = ""
    /*
     *the user id to show his profile
     *
     */
    var id_user_profile = ""
    /*
     *the send messages TF
     *
     */
    @IBOutlet weak var conversationTextfield: UITextField!
    @IBOutlet weak var titre: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.tabBarController?.tabBar.isHidden = false
        //self.navigationController?.tabBarController?.tabBar.selectedItem = nil
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DeleteBadge"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDisconnectedUserUpdateNotification(notification:)), name: NSNotification.Name(rawValue: "userWasDisconnectedNotification"), object: nil)
        configureViewFromLocalisation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserConnectNow(notification:)), name: NSNotification.Name(rawValue: "userConnectNow"), object: nil)
        let abc = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = abc.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var accc = JSON(data: dataFromString!)
        if Conversation["titre"].stringValue == "" {
          if Conversation["users"][0]["id"].stringValue == accc["user"]["id"].stringValue {
        
            let placeholder =  UIImage(named: "user_placeholder.png")
           if let urlImgUser = URL(string : Conversation["users"][1]["photo"].stringValue)
           {
            self.id_user_profile =  Conversation["users"][1]["id"].stringValue
            ImageName.setImage(withUrl: urlImgUser , placeholder: placeholder)
            let gesture = UITapGestureRecognizer(target: self, action: #selector(self.goVisteur))
            gesture.numberOfTapsRequired = 1
            ImageName.addGestureRecognizer(gesture)
            ImageName.isUserInteractionEnabled = true
            }
          }
            
          else {
            
            let placeholder1 =  UIImage(named: "user_placeholder.png")
          if  let urlImgUser1 = URL(string : Conversation["users"][0]["photo"].stringValue)
          {
            self.id_user_profile =  Conversation["users"][0]["id"].stringValue
            ImageName.setImage(withUrl: urlImgUser1 , placeholder: placeholder1)
            let gesture = UITapGestureRecognizer(target: self, action: #selector(self.goVisteur))
            gesture.numberOfTapsRequired = 1
            ImageName.addGestureRecognizer(gesture)
            ImageName.isUserInteractionEnabled = true
            }
        }
        }else {
             self.id_user_profile =  ""
             ImageName.image = UIImage(named:"station")
        }
      
    
        
        labelName.text = name
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.goVisteur))
        tapgesture.numberOfTapsRequired = 1
        labelName.isUserInteractionEnabled = true
        labelName.addGestureRecognizer(tapgesture)
        ImageName.isUserInteractionEnabled = true
        ImageName.addGestureRecognizer(tapgesture)
        TypingName.text = name + " est entrain d'écrire..."
        TypingName.textColor = UIColor.gray
        
        //button.setImage(UIImage(named: "Shape_ahmed"), for: .normal)
        //button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        //button.frame = CGRect(x: CGFloat(conversationTextfield.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        //button.addTarget(self, action: #selector(self.sendMessages), for: .touchUpInside)
        //conversationTextfield.rightView = button
        //conversationTextfield.rightViewMode = .always
        loadData(animated: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleConnectedUserUpdateNotification(notification:)), name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserTypingNotification(notification:)), name: NSNotification.Name(rawValue: "userTypingNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserStopTypingNotification(notification:)), name: NSNotification.Name(rawValue: "userStopTypingNotification"), object: nil)
        self.conversationTextfield.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        // Do any additional setup after loading the view.
    }
    @objc func goVisteur () {
        if id_user_profile != "" {
        let view = self.storyboard?.instantiateViewController(withIdentifier: "visiteur_profil") as! visiteur_profil
        view.id = id_user_profile
            view.parentNavigationController = self.parentNavigationController
            view.fromWhereYouCome = "Conversation"
        parentNavigationController?.pushViewController(view, animated: true)
        }
    }
    /*
     *notification received when new user is connected
     *
     */
    @objc func handleUserConnectNow(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            print("userConnectNow:",typingUsersDictionary)
            SocketIOManager.sharedInstance.SetOnline(room: Conversation["id"].stringValue, UserId: a["user"]["id"].stringValue)
        }
        
    }
    /*
     *not in use
     *
     */
    @objc func handleUserConnectNow2(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            print("setOnline:",typingUsersDictionary)
        }
        
        
    }
    /*
     *the user stopped typing
     *
     */
    @objc func handleUserStopTypingNotification(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            var names = ""
            var totalTypingUsers = 0
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let userId = typingUsersDictionary[1] as! String
            print("user:",typingUsersDictionary)
            if userId != a["user"]["id"].stringValue {
                bottomtable.constant = 0
                self.TypingName.isHidden = true
            }
            /* for (typingUser, _) in typingUsersDictionary {
             if typingUser != nickname {
             names = (names == "") ? typingUser : "\(names), \(typingUser)"
             totalTypingUsers += 1
             }
             }
             
             if totalTypingUsers > 0 {
             let verb = (totalTypingUsers == 1) ? "is" : "are"
             
             lblOtherUserActivityStatus.text = "\(names) \(verb) now typing a message..."
             lblOtherUserActivityStatus.isHidden = false
             }
             else {
             lblOtherUserActivityStatus.isHidden = true
             } */
        }
    }
    /*
     *the user start typing
     *
     */
    @objc func handleUserTypingNotification(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            var names = ""
            var totalTypingUsers = 0
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let userId = typingUsersDictionary[1] as! String
            print("user:",typingUsersDictionary)
            
            if !(userId.elementsEqual(a["user"]["id"].stringValue)) {
                bottomtable.constant = 20.0
                self.TypingName.isHidden = false
            }
            /* for (typingUser, _) in typingUsersDictionary {
             if typingUser != nickname {
             names = (names == "") ? typingUser : "\(names), \(typingUser)"
             totalTypingUsers += 1
             }
             }
             
             if totalTypingUsers > 0 {
             let verb = (totalTypingUsers == 1) ? "is" : "are"
             
             lblOtherUserActivityStatus.text = "\(names) \(verb) now typing a message..."
             lblOtherUserActivityStatus.isHidden = false
             }
             else {
             lblOtherUserActivityStatus.isHidden = true
             } */
        }
    }
    /*
     *not in use
     *
     */
    @objc func handleDisconnectedUserUpdateNotification(notification: NSNotification) {
        let disconnectedUserNickname = notification.object as! String
        
        // lblNewsBanner.text = "User \(disconnectedUserNickname.uppercased()) has left."
        //showBannerLabelAnimated()
    }
    /*
     *not in user
     *
     */
    @objc func handleConnectedUserUpdateNotification(notification: NSNotification) {
        let connectedUserInfo = notification.object as! [String: AnyObject]
        let connectedUserNickname = connectedUserInfo["nickname"] as? String
        //print(connectedUserNickname)
        //  lblNewsBanner.text = "User \(connectedUserNickname!.uppercased()) was just connected."
        //showBannerLabelAnimated()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     *back action
     *
     */
    @IBAction func back(_ sender: Any) {
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadView"), object: nil)
        
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DeleteBadge"), object: nil)
         self.navigationController?.tabBarController?.tabBar.isHidden = false
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Messages.count != 0 {
           
                return Messages.count
            
            
        }else{
            return 0
        }
    }
    /*
     *prepare the node js
     *
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         loadData(animated: false)
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DeleteBadge"), object: nil)
        //currentDateTime
        SocketIOManager.sharedInstance.getChatMessage { (messageInfo) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                print("*******************************")
                //print(messageInfo as JSON)
                let s = messageInfo as JSON
                print("MessageSocket: ",s)
                var x = " { \"id\" : 400, "
                x = x + " \"etat\" : \"non lu\" ,"
                x = x + " \"message\" : \"\(messageInfo[0]["message"].stringValue)\","
                x = x + "\"conversation\" : {} ,"
                x = x + "\"expediteur\" : { "
                x = x + "\"id\" : \"\(messageInfo[0]["clientid"].stringValue)\"  }, \"date\" : \"\(messageInfo[0]["currentDateTime"].stringValue)\" } "
                print("9bal : ", x)
                let p = JSON.parse(x)
                
                // print("saifMessage ",p.description)
                //self.loadData(animated: false)
                let taille = self.Messages.count
                var c = self.Messages.description
                c.characters.popFirst()
                c.characters.popLast()
                print("*******************************")
                print("self.messages ", self.Messages.description)
                if taille != 0 {
                self.Messages = JSON.parse("[ " + c + "," + p.description + "]")
                }else {
                    self.Messages = JSON.parse("[ " + p.description + "]")
                }
                print("*******************************")
                //print(self.Messages.description)
                print("*******************************")
                self.tableview.beginUpdates()
                let indexPath = IndexPath(row: (self.Messages.count)  - 1, section: 0)
                self.tableview.insertRows(at: [indexPath], with: .automatic)
                self.tableview.endUpdates()
                
                self.tableview.reloadData()
                self.tableview.scrollToRow(at: IndexPath(row: self.Messages.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: false)
                print("*******************************")
                // let q = messageInfo as [String]
                //print("message:",q[2])
            })
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        
        if Messages[indexPath.row]["expediteur"]["id"].stringValue != a["user"]["id"].stringValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sender", for: indexPath)
            let labelContent : PaddingLabel = cell.viewWithTag(1) as! PaddingLabel
            labelContent.text = Messages[indexPath.row]["message"].stringValue
            labelContent.setNeedsLayout()
            let labelTime : UILabel = cell.viewWithTag(2) as! UILabel
            let Time = Messages[indexPath.row]["date"].stringValue
            if (Time != "")
            {
                let x = Time.split(separator: "T")
                let c = x[1].split(separator: ":")
                labelTime.text = c[0] + ":" + c[1]
            }
            if #available(iOS 11.0, *) {
                labelContent.clipsToBounds = true
                labelContent.layer.cornerRadius = 10
                labelContent.layer.maskedCorners = [ .layerMinXMinYCorner , .layerMaxXMinYCorner , .layerMaxXMaxYCorner]
            }
            else {
                let path = UIBezierPath(roundedRect:labelContent.bounds,
                                        byRoundingCorners:[.topRight, .bottomRight , .topLeft],
                                        cornerRadii: CGSize(width: 10, height:  10))
                let maskLayer = CAShapeLayer()
                maskLayer.path = path.cgPath
                labelContent.layer.mask = maskLayer
            }
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "receiver", for: indexPath)
            let labelContent : PaddingLabel = cell.viewWithTag(1) as! PaddingLabel
            labelContent.text = Messages[indexPath.row]["message"].stringValue
            let labelTime : UILabel = cell.viewWithTag(2) as! UILabel
            let Time = Messages[indexPath.row]["date"].stringValue
            if (Time != "")
            {
                let x = Time.split(separator: "T")
                let c = x[1].split(separator: ":")
                labelTime.text = c[0] + ":" + c[1]
            }
            labelContent.setNeedsLayout()
            if #available(iOS 11.0, *) {
                labelContent.clipsToBounds = true
                labelContent.layer.cornerRadius = 10
                labelContent.layer.maskedCorners = [ .layerMinXMinYCorner , .layerMaxXMinYCorner , .layerMinXMaxYCorner]
            }
            else {
                let path = UIBezierPath(roundedRect:labelContent.bounds,
                                        byRoundingCorners:[.topRight, .bottomLeft , .topLeft],
                                        cornerRadii: CGSize(width: 10, height:  10))
                let maskLayer = CAShapeLayer()
                maskLayer.path = path.cgPath
                labelContent.layer.mask = maskLayer
            }
            return cell
        }
        
    }
    /*
     *send message action
     *
     */
    @IBAction func sendMessages(_ sender: Any) {
        print ("heey")
        if conversationTextfield.text != "" {
            self.button.isEnabled = false
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let params: Parameters = [
                "converationId": Conversation["id"].stringValue,
                "message" : self.conversationTextfield.text!
            ]
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : a["value"].stringValue
            ]
            Alamofire.request(ScriptBase.sharedInstance.Send_Message , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    self.button.isEnabled = true
                    
                    let b = JSON(response.data)
                    print("message:",response)
                    SocketIOManager.sharedInstance.sendMessage(idNickname: a["user"]["id"].stringValue, nickname: a["user"]["email"].stringValue, msg: self.conversationTextfield.text!, conv: self.Conversation["id"].stringValue,photo:a["user"]["photo"].stringValue)
                    self.conversationTextfield.text = ""
                    self.default_message.isHidden = true
                    self.tableview.isHidden = false
                    self.loadData(animated: true)
                    
                    
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    /*
     *configure texts
     *
     */
    func configureViewFromLocalisation() {
        titre.text = Localization("ConversationTitre")
          default_message.text = Localization("default_message")
        conversationTextfield.placeholder = Localization("ConversationTextField")
        
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    /*
     *load messages from database
     *
     */
    func loadData(animated:Bool){
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.afficherMessageConversation + Conversation["id"].stringValue , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                let b = JSON(response.data)
                print("Message:",b)
                let c = JSON(b["data"].arrayObject)
                
                if c.count != 0 {
                    self.default_message.isHidden = true
                    self.tableview.isHidden = false
                    self.Messages = c
                    //print("count:",self.Messages.count)
                    self.tableview.reloadData()
                    if animated {
                        self.tableview.scrollToRow(at: IndexPath(row: self.Messages.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                    }
                    else{
                        self.tableview.scrollToRow(at: IndexPath(row: self.Messages.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: false)
                    }
                }
                else {
                    self.default_message.isHidden = false
                    self.tableview.isHidden = true
                }
                
                
        }
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        SocketIOManager.sharedInstance.sendStartTypingMessage(Conv: Conversation["id"].stringValue , nickname: a["user"]["id"].stringValue)
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        SocketIOManager.sharedInstance.stopmessage(Conv: Conversation["id"].stringValue , nickname: a["user"]["id"].stringValue)
        return true
    }
    
    
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
   
}

