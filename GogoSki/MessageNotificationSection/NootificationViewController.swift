//
//  NootificationViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 15/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class NootificationViewController: InternetCheckViewControllers , UITableViewDelegate , UITableViewDataSource {
    var parentNavigationController : UINavigationController?
    
    /*
     * delete button
     */
    @IBOutlet weak var btnSupp: UIButton!
    /*
     * text appears when there is no notifications
     */
    @IBOutlet weak var noNotification: UILabel!
    /*
     * notification tableview
     */
    @IBOutlet weak var tableview: UITableView!
    /*
     * variable contains user notifications
     */
    var notifications : JSON = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.allowsMultipleSelection = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        // Do any additional setup after loading the view.
    }
    
    /*
     * configure language
     */
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    func configureViewFromLocalisation() {
        noNotification.text = Localization("noNotification")
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (notifications.arrayObject?.count)!
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notif", for: indexPath)
        let labelTitle : UILabel = cell.viewWithTag(1) as! UILabel
        let labelContent : UILabel = cell.viewWithTag(2) as! UILabel
        let imgNotif : UIImageView = cell.viewWithTag(4) as! UIImageView
        let dateNotif : UILabel = cell.viewWithTag(3) as! UILabel
        let img : UIImageView = cell.viewWithTag(1000) as! UIImageView
        labelTitle.text = self.notifications[indexPath.row]["titre"].stringValue
        var s = self.notifications[indexPath.row]["date"].stringValue.suffix(8)
        let toArray = s.components(separatedBy: "/")
        let backToString = toArray.joined(separator: ":")
        dateNotif.text = backToString
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if (a["user"]["langue"].stringValue == "Français" || a["user"]["langue"].stringValue == "Frensh")
        {
            labelContent.text = self.notifications[indexPath.row]["content_fr"].stringValue
        }
        else {
            labelContent.text = self.notifications[indexPath.row]["content_en"].stringValue
        }
        if ( self.notifications[indexPath.row]["type"].stringValue == "message" )
        {
            imgNotif.image = UIImage(named : "notif_nouveauMsg_ahmed")
        }
        else if ( self.notifications[indexPath.row]["type"].stringValue == "rdv" ) || self.notifications[indexPath.row]["type"].stringValue == "AcceptRdv" || self.notifications[indexPath.row]["type"].stringValue == "RejectRdv" || self.notifications[indexPath.row]["type"].stringValue == "AcceptSortie" || self.notifications[indexPath.row]["type"].stringValue == "RejectSortie" || self.notifications[indexPath.row]["type"].stringValue == "supp" {
            imgNotif.image = UIImage(named : "notif_proposeRdv_ahmed")
            img.image = UIImage(named : "station")
        }
        else if (self.notifications[indexPath.row]["type"].stringValue == "ajout")
        {
            imgNotif.image = UIImage(named : "notif_ajoutFavoris_ahmed")
            if let urlImgUser = URL(string : self.notifications[indexPath.row]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
                img.setImage(withUrl: urlImgUser , placeholder: placeholder)
            }
        }
        else if (self.notifications[indexPath.row]["type"].stringValue == "activité")
        {
            imgNotif.image = UIImage(named : "notif_changeActivite_ahmed")
        }
        else {
            imgNotif.image = UIImage(named : "notif_bienvenue_ahmed")
        }
        if (self.notifications[indexPath.row]["etat"].stringValue != "seen")
        {
            cell.backgroundColor = UIColor("#F7F7F7")
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print (self.notifications[indexPath.row]["etat"].stringValue , " etaaaat")
        if ( self.notifications[indexPath.row]["etat"].stringValue == "unseen")
        {
            if (self.notifications[indexPath.row]["type"].stringValue == "rdv")
            {
                if (self.notifications[indexPath.row]["activite"]["id"].stringValue != "")
                {
                    let view = self.storyboard?.instantiateViewController(withIdentifier: "InvitationRec_uViewController") as! InvitationRec_uViewController
                    
                    view.nom = self.notifications[indexPath.row]["activite"]["titre"].stringValue
                    view.participants = self.notifications[indexPath.row]["activite"]["list_participants"]
                    view.idSortie = self.notifications[indexPath.row]["activite"]["id"].stringValue
                    view.user = self.notifications[indexPath.row]["sender"]
                    view.id = self.notifications[indexPath.row]["id"].stringValue
                    view.longitiude = self.notifications[indexPath.row]["rendezvous"]["point"]["longitude"].stringValue
                    view.latitude = self.notifications[indexPath.row]["rendezvous"]["point"]["latitude"].stringValue
                    view.massif = self.notifications[indexPath.row]["rendezvous"]["point"]["station"]["massif"].stringValue
                    view.departement = self.notifications[indexPath.row]["rendezvous"]["point"]["station"]["dept"].stringValue
                    view.station = self.notifications[indexPath.row]["rendezvous"]["point"]["station"]["nom"].stringValue
                    if ( self.notifications[indexPath.row]["activite"]["discr"].stringValue == "Lecon")
                    {
                        view.prestations = self.notifications[indexPath.row]["activite"]["prestation"]
                        view.dateDepart = String(self.notifications[indexPath.row]["activite"]["date_debut"].stringValue.prefix(10))
                        view.heureDepart = String(self.notifications[indexPath.row]["activite"].stringValue.replacingOccurrences(of: "/", with: ":").suffix(8))
                    }
                    parentNavigationController?.pushViewController(view, animated: true)
                }
                else if (self.notifications[indexPath.row]["rendezvous"]["id"].stringValue != "" ){
                    let view = self.storyboard?.instantiateViewController(withIdentifier: "InvitationRdvRec_uViewController") as! InvitationRdvRec_uViewController
                    view.photo = self.notifications[indexPath.row]["rendezvous"]["point"]["photo"].stringValue
                    view.user = self.notifications[indexPath.row]["sender"]
                    view.idRdv = self.notifications[indexPath.row]["rendezvous"]["id"].stringValue
                    view.longitiude = self.notifications[indexPath.row]["rendezvous"]["point"]["longitude"].stringValue
                    view.latitude = self.notifications[indexPath.row]["rendezvous"]["point"]["latitude"].stringValue
                    view.massif = self.notifications[indexPath.row]["rendezvous"]["point"]["station"]["massif"].stringValue
                    view.departement = self.notifications[indexPath.row]["rendezvous"]["point"]["station"]["dept"].stringValue
                    view.station = self.notifications[indexPath.row]["rendezvous"]["point"]["station"]["nom"].stringValue
                    view.nom_etab = self.notifications[indexPath.row]["rendezvous"]["point"]["prestataire"]["nom_etablissement"].stringValue
                    parentNavigationController?.pushViewController(view, animated: true)
                }
            }
            else if ( self.notifications[indexPath.row]["type"].stringValue == "ajout" || self.notifications[indexPath.row]["type"].stringValue == "AcceptSortie" )
            {
                let view = self.storyboard?.instantiateViewController(withIdentifier: "visiteur_profil") as! visiteur_profil
                view.id = self.notifications[indexPath.row]["sender"]["id"].stringValue
                parentNavigationController?.pushViewController(view, animated: true)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getNotifications()
    }
    
    /*
     * get all user notification
     */
    func getNotifications ()
    {
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.afficherNotifs , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                print ("**********")
                print (response)
                print("***********")
                
                let b = JSON(response.data)
                
                
                if b["status"].boolValue {
                    if (b["data"].arrayObject?.count == 0)
                    {
                        print ("feraa8")
                        self.tableview.isHidden = true
                        self.btnSupp.isHidden = true
                        self.noNotification.isHidden = false
                    }
                    else {
                        self.btnSupp.isHidden = false
                        self.noNotification.isHidden = true
                        self.notifications = b["data"]
                        self.tableview.reloadData()
                        
                    }
                    
                    
                    
                }
        }
    }
    
    /*
     * delete all notification action
     */
    @IBAction func suppAllNotif(_ sender: Any) {
        _ = SweetAlert().showAlert(Localization("areYouSure"), subTitle: Localization("deleteNotif"), style: AlertStyle.warning, buttonTitle:Localization("cancel"), buttonColor: UIColor.gray , otherButtonTitle:  Localization("yes"), otherButtonColor: UIColor.red) { (isOtherButton) -> Void in
            if isOtherButton == false {
                let ab = UserDefaults.standard.value(forKey: "User") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : a["value"].stringValue
                ]
                Alamofire.request(ScriptBase.sharedInstance.suppAllNotif , method: .get, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        LoaderAlert.shared.dismiss()
                        
                        let b = JSON(response.data)
                        
                        if b["status"].boolValue {
                            
                            self.notifications = []
                            self.tableview.isHidden = true
                            self.noNotification.isHidden = false
                            self.btnSupp.isHidden = true
                            
                        }
                }
            }
        }
    }
}

