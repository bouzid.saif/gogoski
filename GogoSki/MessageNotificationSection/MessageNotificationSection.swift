//
//  ViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 15/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import PageMenu
import SwiftyJSON

class MessageNotificationSection: InternetCheckViewControllers , CAPSPageMenuDelegate {
    /*
     * title of this page
     */
    @IBOutlet weak var Message_Notif_Title: UILabel!
    /*
     * the view witch contains the controllers
     */
    @IBOutlet weak var container: UIView!
    /*
     * profile picture that you can touch to go profil
     */
    @IBOutlet weak var imageToProfileNavBar: RoundedUIImageView!
    /*
     * the menu used for segmented controller
     */
    var pageMenu : CAPSPageMenu?
    /*
     * parametres of the page menu
     */
    var parameters : [CAPSPageMenuOption] = []
    /*
     * controllers of the page menu
     */
    var controllerArray : [UIViewController] = []
    /*
     * test if first enter or not
     */
    var first = true
    override func viewDidLoad() {
        super.viewDidLoad()
        print("MessageNotificationSection")
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if let urlImgUser = URL(string : a["user"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imageToProfileNavBar.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let controller1 = MessagesViewController.shared
        controller1.title = "MESSAGES"
        controller1.parentNavigationController = self.navigationController
        controllerArray.append(controller1)
        let controller2 = self.storyboard?.instantiateViewController(withIdentifier: "NootificationViewController") as! NootificationViewController
        controller2.title = "NOTIFICATIONS"
        controller2.parentNavigationController = self.navigationController
        controllerArray.append(controller2)
        
        parameters = [
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(UIColor(red: 18.0/255.0, green: 150.0/255.0, blue: 225.0/255.0, alpha: 1.0)),
            .menuMargin(20),
            .menuHeight(50.0),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor(red: 40.0/255.0, green: 40.0/255.0, blue: 40.0/255.0, alpha: 1.0)),
            .menuItemSeparatorWidth(0)
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x : 0.0, y : 83.0, width : self.view.frame.width, height : self.view.frame.height - 83.0), pageMenuOptions: parameters)
        
        pageMenu?.delegate = self;
        
        
        self.view.addSubview(pageMenu!.view)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotif(notification:)), name: NSNotification.Name(rawValue: "NotifIsActive"), object: nil)
        
    }
    /*
     * get all conversation
     */
    func LoadMessage(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let controller1 = appDelegate.MessageController as! MessagesViewController
        controller1.loadData(withData: "")
    }
    /*
     * go to the seconde page
     */
    @objc func handleNotif(notification: NSNotification) {
        
        pageMenu!.moveToPage(1)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        pageMenu?.moveToPage(0)
        if first == false {
            // let appDelegate = UIApplication.shared.delegate as! AppDelegate
            pageMenu?.delegate?.didMoveToPage!(MessagesViewController.shared, index: 0)
        }
        first = false
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    /*
     * change title page
     */
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        
        if index == 0 {
            self.Message_Notif_Title.text = "Messages"
            (controller as! MessagesViewController).viewDidLoad()
        }else{
            //headerLBL.text = "Notifications"
            self.Message_Notif_Title.text = "Notifications"
            (controller as! NootificationViewController).viewWillAppear(true)
        }
        
    }
    
    
}

