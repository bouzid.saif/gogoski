//
//  MessagesViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 15/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class MessagesViewController: InternetCheckViewControllers , UITableViewDelegate , UITableViewDataSource {
    /*
     * pattern for static controller
     */
    static let shared = UIStoryboard(name: "Main", bundle:  nil).instantiateViewController(withIdentifier: "MessagesViewController") as! MessagesViewController
    /*
     * text appears when there is no conversations
     */
    @IBOutlet weak var default_conversation: UILabel!
    /*
     * conversations tableview
     */
    @IBOutlet weak var tableview: UITableView!
    /*
     * navigation controller of this controller
     */
    var parentNavigationController : UINavigationController?
    /*
     * variable contains all users
     */
    var users : [String] = []
    /*
     * variable contains all users connected
     */
    var Connected : [String] = []
    /*
     * variable contains all conversations
     */
    var conversation = JSON()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.allowsMultipleSelection = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Connected = appDelegate.ConnectedUser
        if appDelegate.NotifReceive == false {
            loadData2()
        }
        print("MessageViewController")
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDisconnectedUserUpdateNotification(notification:)), name: NSNotification.Name(rawValue: "userWasDisconnectedNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotif4(notification:)), name: NSNotification.Name(rawValue: "MessageIsActiveAhmed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleReload(notification:)), name: NSNotification.Name(rawValue: "ReloadConv"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleReloadView(notification:)), name: NSNotification.Name(rawValue: "ReloadView"), object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(self.DisconnectNow(notification:)), name: NSNotification.Name(rawValue: "UserDisonnectNow"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("viewwillappearMessage")
        if animated == true {
            
        }
    }
    /*
     * notification to reload tableview
     */
    @objc func handleReload(notification: NSNotification) {
        self.tableview.reloadData()
    }
    
    /*
     * notification to reload the view
     */
    @objc func handleReloadView(notification: NSNotification) {
        self.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewdidAppear:",animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if animated {
            if appDelegate.NotifReceive == true {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MessageViewActive"), object: nil)
                appDelegate.NotifReceive = false
                
            }
        }
    }
    /*
     * notification to reload data
     */
    func handleNotifStatic(q:String){
        
        loadData(withData : q)
    }
    
    /*
     * notification to reload the view
     */
    @objc func handleNotif4(notification: NSNotification) {
        
        let q = notification.object as! String
        print("notttttififfff:",notification.name)
        loadData(withData : q)
        // _ = SweetAlert().showAlert("Push", subTitle: "let it go", style: AlertStyle.success)
    }
    /*
     * user connected now
     */
    @objc func connectedNow(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            Connected[0] = "1"
            let q = JSON.parse(typingUsersDictionary[0])
            print(q)
            self.tableview.reloadData()
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            print("userConnectNow:",typingUsersDictionary)
            
            SocketIOManager.sharedInstance.SetOnline(room: q["room"].stringValue, UserId: a["user"]["id"].stringValue)
        }
    }
    /*
     * user disconnected
     */
    @objc func DisconnectNow(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            print("Disconnecting")
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            print("Connected:",appDelegate.ConnectedUser)
            if typingUsersDictionary[0] != a["user"]["id"].stringValue {
                var y = -1
                var x = -1
                print("typingUsersDictionary[0] :",typingUsersDictionary[0])
                for b in  appDelegate.ConnectedUser{
                    y = y + 1
                    if b == typingUsersDictionary[0] {
                        x = y
                    }
                    
                }
                print("x :",x)
                if x != -1 {
                    appDelegate.ConnectedUser.remove(at: x)
                }
                Connected = appDelegate.ConnectedUser
                print("Connected:",appDelegate.ConnectedUser)
                self.tableview.reloadData()
            }
        }
    }
    
    
    @objc func handleDisconnectedUserUpdateNotification(notification: NSNotification) {
        let disconnectedUserNickname = notification.object as! String
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if conversation.count != 0 {
            return conversation.count
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            let img = cell.viewWithTag(50) as! UIImageView
            img.contentMode = .scaleAspectFill
            let radius : CGFloat = img.bounds.size.width / 2
            img.layer.cornerRadius = radius
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "messages", for: indexPath)
        let labelName : UILabel = cell.viewWithTag(1) as! UILabel
        let labelMsg : UILabel = cell.viewWithTag(2) as! UILabel
        let labelTime : UILabel = cell.viewWithTag(3) as! UILabel
        let Badge : UIImageView = cell.viewWithTag(40) as! UIImageView
        let ImageConv : UIImageView = cell.viewWithTag(50) as! UIImageView
        
        Badge.isHidden = false
        var ab : Bool = false
        var temp = JSON(conversation[indexPath.row]["users"].arrayObject)
        let btnSupp : CustomButton = cell.viewWithTag(20) as! CustomButton
        btnSupp.index = indexPath.row
        btnSupp.indexPath = indexPath
        btnSupp.addTarget(self, action: #selector(self.suppActivite), for: .touchUpInside)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Connected = appDelegate.ConnectedUser
        let abc = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = abc.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var accc = JSON(data: dataFromString!)
        for i in 0...(temp.arrayObject?.count)! - 1 {
            
            for a in Connected{
                
                if a == temp[i]["id"].stringValue && temp[i]["id"].stringValue != accc["user"]["id"].stringValue {
                    ab = true
                }
                
            }
            
        }
        
        if conversation[indexPath.row]["titre"].stringValue != "" {
            labelName.text = conversation[indexPath.row]["titre"].stringValue
            if conversation[indexPath.row]["photo"].stringValue != "" {
                if conversation[indexPath.row]["photo"].stringValue.first == "h" {
            ImageConv.setImage(withUrl: URL(string: conversation[indexPath.row]["photo"].stringValue)!, placeholder: UIImage(named: "user_placeholder.png"), crossFadePlaceholder: false, cacheScaled: true, completion: nil)
                }else{
                    ImageConv.image = UIImage(named: "gg")
                     //ImageConv.setImage(withUrl: URL(string: "https://www.glisshop.com/gfx/graphic/glisshop/navs/350x250xps_freestyle.jpg.pagespeed.ic.FQWChf2et4.jpg")!, placeholder: UIImage(named: "user_placeholder.png"), crossFadePlaceholder: false, cacheScaled: true, completion: nil)
                }
            }else{
                ImageConv.image = UIImage(named: "gg")
               //ImageConv.setImage(withUrl: URL(string: "https://www.glisshop.com/gfx/graphic/glisshop/navs/350x250xps_freestyle.jpg.pagespeed.ic.FQWChf2et4.jpg")!, placeholder: UIImage(named: "user_placeholder.png"), crossFadePlaceholder: false, cacheScaled: true, completion: nil)
            }
        }else {
            
                if conversation[indexPath.row]["users"][1]["id"].stringValue == accc["user"]["id"].stringValue {
                    labelName.text = conversation[indexPath.row]["users"][0]["prenom"].stringValue + " " + conversation[indexPath.row]["users"][0]["nom"].stringValue
                    if (conversation[indexPath.row]["users"][0]["photo"].stringValue).hasPrefix("h") {
                        let urlImgUser = NSURL(string : conversation[indexPath.row]["users"][0]["photo"].stringValue)
                        let placeholder =  UIImage(named: "user_placeholder.png")
                        ImageConv.setImage(withUrl: urlImgUser! as URL , placeholder: placeholder,cacheScaled: true)
                        
                    } else {
                        ImageConv.image =  UIImage(named: "user_placeholder.png")
                    }
                }else {
                    labelName.text = conversation[indexPath.row]["users"][1]["prenom"].stringValue + " " + conversation[indexPath.row]["users"][1]["nom"].stringValue
                    if (conversation[indexPath.row]["users"][1]["photo"].stringValue).hasPrefix("h") {
                        let urlImgUser = NSURL(string : conversation[indexPath.row]["users"][1]["photo"].stringValue)
                        let placeholder =  UIImage(named: "user_placeholder.png")
                        ImageConv.setImage(withUrl: urlImgUser! as URL , placeholder: placeholder)
                        
                    } else {
                        ImageConv.image =  UIImage(named: "user_placeholder.png")
                    }
                }
            
            
        }
        Badge.image = (ab == false) ? UIImage(named:"offline_ahmed") : UIImage(named:"online_ahmed")
        // let photo : UIImageView = cell.viewWithTag(4) as! UIImageView
        if (conversation[indexPath.row]["last_message"].stringValue != "" )
        {
            labelMsg.text = conversation[indexPath.row]["last_message"].stringValue
            let Time = conversation[indexPath.row]["last_message_date"].stringValue
            let Time2 = conversation[indexPath.row]["date"].stringValue
            if (Time != "")
            {
                let x = Time.split(separator: "T")
                let c = x[1].split(separator: ":")
                labelTime.text = c[0] + ":" + c[1]
            }else {
                let x = Time2.split(separator: "T")
                let c = x[1].split(separator: ":")
                labelTime.text = c[0] + ":" + c[1]
            }
        }
        else {
            labelMsg.text = ""
            labelTime.text = ""
        }
        
        
        return cell
    }
    /*
     * get all conversation
     */
    func loadData(withData :String){
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.afficherConversation , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                let b = JSON(response.data)
                print("b:",b)
                var c = JSON(b["data"].arrayObject)
                if c.count != 0 {
                    var tab : [Int] = []
                    for i in 0 ... (c.arrayObject?.count)! - 1 {
                        if c[i]["titre"].stringValue == "" {
                            for j in 0 ... (c[i]["users"].arrayObject?.count)! - 1 {
                                if StaticPositionController.shared.ListeNoir.contains(c[i]["users"][j]["id"].stringValue) || (c[i]["users"].arrayObject?.count == 1) {
                                    tab.append(i)
                                }
                            }
                            
                        }
                    }
                    print("******beforeremove :",c.arrayObject?.count)
                    print("items:",tab)
                    if tab.count != 0 {
                        for i in ( 0 ... tab.count - 1 ).reversed() {
                            
                            c.arrayObject?.remove(at: tab[i])
                        }
                    }
                    tab = []
                    print("******afterremove :",c.arrayObject?.count)
                    self.conversation = c
                    self.default_conversation.isHidden = true
                    self.tableview.isHidden = false
                    
                    
                    print("withData:",withData)
                    if withData != "" {
                        self.tableview.reloadData()
                        for  i in 0...self.conversation.count - 1 {
                            
                            if self.conversation[i]["id"].stringValue == withData {
                                print("exist")
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.NotifReceive =  false
                                self.tableview.scrollToRow(at: IndexPath(row: i, section: 0), at: UITableViewScrollPosition.none , animated: false)
                                self.tableview.delegate?.tableView!(self.tableview, didSelectRowAt: IndexPath(row: i, section: 0))
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ResetPost"), object: nil)
                                
                            }
                        }
                    }else {
                        self.tableview.reloadData()
                    }
                    
                }
                else
                {
                    self.default_conversation.isHidden = false
                    self.tableview.isHidden = true
                }
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                
        }
        
        
    }
    
    /*
     * get all conversation 2 without notification
     */
    func loadData2(){
        
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.afficherConversation , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                let b = JSON(response.data)
                print("messages : ::  :::",b)
                var c = JSON(b["data"].arrayObject)
                
                if c.count != 0 {
                    var tab : [Int] = []
                    for i in 0 ... (c.arrayObject?.count)! - 1 {
                        if c[i]["titre"].stringValue == "" {
                            for j in 0 ... (c[i]["users"].arrayObject?.count)! - 1 {
                                if StaticPositionController.shared.ListeNoir.contains(c[i]["users"][j]["id"].stringValue) || (c[i]["users"].arrayObject?.count == 1) {
                                   tab.append(i)
                                }
                            }
                            
                        }
                    }
                    print("******beforeremove :",c.arrayObject?.count)
                    print("items:",tab)
                    if tab.count != 0 {
                        for i in ( 0 ... tab.count - 1 ).reversed() {
                            
                        c.arrayObject?.remove(at: tab[i])
                        }
                    }
                    tab = []
                    print("******afterremove :",c.arrayObject?.count)
                    self.conversation = c
                    //self.default_conversation.isHidden = true
                    self.tableview.isHidden = false
                    
                    
                    
                    self.tableview.reloadData()
                    
                    
                }
                else
                {
                    self.default_conversation.isHidden = false
                    self.tableview.isHidden = true
                }
                
                
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
        let labelName : UILabel = cell!.viewWithTag(1) as! UILabel
        let view = self.storyboard?.instantiateViewController(withIdentifier: "ConversationViewController") as! ConversationViewController
        view.name = labelName.text!
        view.parentNavigationController = self.parentNavigationController
        view.Conversation = conversation[indexPath.row]
        parentNavigationController?.pushViewController(view, animated: true)
    }
    
    /*
     * delete conversation
     */
    @objc func suppActivite (sender : CustomButton)
    {
        self.tableview.isUserInteractionEnabled = false
        
        SweetAlert().showAlert(Localization("areYouSure"), subTitle: Localization("suppConv"), style: AlertStyle.warning, buttonTitle:Localization("cancel"), buttonColor: UIColor.gray , otherButtonTitle:  Localization("yes"), otherButtonColor: UIColor.red) { (isOtherButton) -> Void in
            if isOtherButton == false {
                LoaderAlert.show()
                let ab = UserDefaults.standard.value(forKey: "User") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : a["value"].stringValue
                ]
                let params: Parameters = [
                    "idConv": self.conversation[sender.index!]["id"].stringValue
                    
                ]
                print (params)
                // print ("********* " , self.enCours[sender.index!]["id"].stringValue)
                Alamofire.request(ScriptBase.sharedInstance.suppMessages , method: .post , parameters : params, encoding: JSONEncoding.default,headers : header)
                    .responseString { response in
                        LoaderAlert.dismiss()
                        self.tableview.isUserInteractionEnabled = true
                        let b = JSON(response.data)
                        print ("messages : "  , response)
                        if b["status"].boolValue {
                            
                            _ = SweetAlert().showAlert("", subTitle: Localization("suppDesc"), style: AlertStyle.success)
                            SocketIOManager.sharedInstance.unscubscribe(Conv: self.conversation[sender.index!]["id"].stringValue, nickname: a["user"]["id"].stringValue)
                            if (self.conversation.count != 1)
                            {
                                self.conversation.arrayObject?.remove(at: sender.index!)
                            }
                            else {
                                self.conversation = []
                                
                            }
                            
                            self.tableview.reloadData()
                            
                        }else{
                            
                            _ = SweetAlert().showAlert(Localization("Supp"), subTitle: Localization("erreur"), style: AlertStyle.error)
                            
                        }
                        
                        
                }
            }
            else {
                self.tableview.isUserInteractionEnabled = true
            }
        }
        
        
        
        
    }
    
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "MessageIsActiveAhmed"), object: nil)
    }
    func configureViewFromLocalisation() {
        
        default_conversation.text = Localization("default_conversation")
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
}

