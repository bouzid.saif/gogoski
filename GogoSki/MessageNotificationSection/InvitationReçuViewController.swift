//
//  InvitationReçuViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 16/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import MapKit

class InvitationRec_uViewController: InternetCheckViewControllers ,MKMapViewDelegate{
    /*
     *hangout title
     *
     */
    @IBOutlet weak var titleSortie: UILabel!
    /*
     *practice type LB
     *
     */
    @IBOutlet weak var pratiqueSport: UILabel!
    @IBOutlet weak var map: MKMapView!
    /*
     *iamge if the user
     *
     */
    @IBOutlet weak var photoUser: RoundedUIImageView!
    /*
     *image of the station
     *
     */
    @IBOutlet weak var photoStation: RoundedUIImageView!
    /*
     *level type
     *
     */
    @IBOutlet weak var niveauSport: UILabel!
    /*
     *user name
     *
     */
    @IBOutlet weak var nomUser: UILabel!
    /*
     *title text
     *
     */
    @IBOutlet weak var titre: UILabel!
    /*
     *validate / revert view
     *
     */
    @IBOutlet weak var validerAnnulerView: UIView!
    /*
     *invitation view
     *
     */
    @IBOutlet weak var invitation: UIView!
    /*
     *accept button
     *
     */
    @IBOutlet weak var btnAccepter: UIButton!
    /*
     *revert button
     *
     */
    @IBOutlet weak var btnRefuser: UIButton!
    /*
     *iamge of the actual user
     *
     */
    @IBOutlet weak var imageToProfileNavBar: RoundedUIImageView!
    /*
     *hangout name
     *
     */
    @IBOutlet weak var NomSortie: UILabel!
    /*
     *participants data
     *
     */
    var participants : JSON = []
    /*
     *name received from the preview view
     *
     */
    var nom = ""
    /*
     *id hangout received from the preview view
     *
     */
    var idSortie = ""
    /*
     *user data
     *
     */
    var user : JSON = []
    /*
     *latitude received from the preview view
     *
     */
    var latitude  = ""
    /*
     *longitude received from the preview view
     *
     */
    var longitiude = ""
    /*
     *apointement id received from the preview view
     *
     */
    var idRdv = ""
    /*
     *hangout id received from the preview view
     *
     */
    var id = ""
    /*
     *massif received from the preview view
     *
     */
    var massif = ""
    /*
     *department received from the preview view
     *
     */
    var departement = ""
    /*
     *station received from the preview view
     *
     */
    var station = ""
    var prestations : JSON = []
    var heureDepart : String = ""
    var dateDepart : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        
        invitation.layer.cornerRadius = 40
        invitation.layer.masksToBounds = false
        invitation.layer.shadowColor = UIColor.black.cgColor
        invitation.layer.shadowOpacity = 0.8
        invitation.layer.shadowOffset = CGSize(width: 0, height: 0)
        invitation.layer.shadowRadius = 2
        
        nomUser.text = self.user["prenom"].stringValue + " " + self.user["nom"].stringValue
        pratiqueSport.text = self.user["sports"][0]["pratique"].stringValue
        niveauSport.text = self.user["sports"][0]["niveau"].stringValue
        if ( self.user["sports"][0]["niveau"].stringValue == "Confirmé" )
        {
            niveauSport.textColor = UIColor("#FF3E53")
        }
        else if (self.user["sports"][0]["niveau"].stringValue == "Débutant")
        {
            niveauSport.textColor = UIColor("#76EC9E")
        }
        else if (self.user["sports"][0]["niveau"].stringValue == "Intermédiaire")
        {
            niveauSport.textColor = UIColor("#75A7FF")
        }
        else if (self.user["sports"][0]["niveau"].stringValue == "Expert")
        {
            niveauSport.textColor = UIColor("#4D4D4D")
        }
        else {
            niveauSport.textColor = UIColor("#76EC9E")
        }
        if let urlImgUser = URL(string : self.user["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            photoUser.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if let urlImgUser = URL(string : a["user"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imageToProfileNavBar.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        var q = ""
        if ( self.prestations["prestataire"]["id"].stringValue != "" )
        {
            if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr"
            {
                titleSortie.text = "invited you to a lesson."
                q = "Massif : " + massif + "\n"
                q = q + "Department : " + departement + "-" + station
            }
            else {
                titleSortie.text = "Vous propose une leçon."
                q = "Massif : " + massif + "\n"
                q = q + "Département : " + departement + "-" + station
            }
        }
        else {
            if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr"
            {
                titleSortie.text = "invited you to a hangout."
                q = "Massif : " + massif + "\n"
                q = q + "Department : " + departement + "-" + station
            }
            else {
                titleSortie.text = "Vous propose une sortie."
                q = "Massif : " + massif + "\n"
                q = q + "Département : " + departement + "-" + station
            }
        }
        NomSortie.text = q
        map.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDisconnectedUserUpdateNotification(notification:)), name: NSNotification.Name(rawValue: "PinView"), object: nil)
        let Report = ImageAnnotation()
        if ( longitiude.prefix(1) == " ")
        {
            longitiude = String(longitiude.dropFirst())
        }
        if (latitude.suffix(1) == " " )
        {
            latitude = String(latitude.dropLast())
        }
        Report.coordinate = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!)
        Report.image1 = "default2"
        StaticImageAnnotationRDV.sharedInstance.add(im:Report)
        map.addAnnotation(Report)
        for annotation in map.annotations {
            
            if annotation.isKind(of: MKPointAnnotation.self) {
                let pinView : ImageAnnotationView = map.view(for: annotation) as! ImageAnnotationView
                formatAnnotationView(pinView, for: map)
                
            }
        }
        map.zoomIn(coordinate: CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!))
        configureTileOverlay()
        // Do any additional setup after loading the view.
    }
    private func configureTileOverlay() {
        // We first need to have the path of the overlay configuration JSON
        guard let overlayFileURLString = Bundle.main.path(forResource: "Overlay", ofType: "json") else {
            return
        }
        let overlayFileURL = URL(fileURLWithPath: overlayFileURLString)
        
        // After that, you can create the tile overlay using MapKitGoogleStyler
        guard let tileOverlay = try? MapKitGoogleStyler.buildOverlay(with: overlayFileURL) else {
            return
        }
        
        // And finally add it to your MKMapView
        map.add(tileOverlay)
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let tileOverlay = overlay as? MKTileOverlay {
            return MKTileOverlayRenderer(tileOverlay: tileOverlay)
        } else {
            return MKOverlayRenderer(overlay: overlay)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     *back action
     *
     */
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    /*
     *configure text
     *
     */
    func configureViewFromLocalisation() {
        titre.text = Localization("InvitationTitre")
        btnAccepter.setTitle(Localization("BtnAccepter"), for: .normal)
        btnRefuser.setTitle(Localization("BtnRefuser"), for: .normal)
        titleSortie.text = Localization("proposeSortie")
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /*
     *refuse action
     *
     */
    @IBAction func refuserAction(_ sender: Any) {
        self.btnRefuser.isEnabled = false
        self.btnAccepter.isEnabled = false
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        
        let params: Parameters = [
            "id": self.id ,
            "respond" : false ,
            "actid" : self.idSortie
        ]
        print ("***********" , params)
        
        Alamofire.request(ScriptBase.sharedInstance.acceptRefuseInvit , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                
                let b = JSON(response.data)
                print ("invite friends : " , b)
                if b["status"].boolValue {
                    
                    _ = SweetAlert().showAlert(Localization("BtnRefuser"), subTitle: Localization("invitRefuse"), style: AlertStyle.success , buttonTitle: "OK" , action: { (otherButton) in
                        let ab = UserDefaults.standard.value(forKey: "User") as! String
                        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                        var a = JSON(data: dataFromString!)
                        let header: HTTPHeaders = [
                            "Content-Type" : "application/json",
                            "x-Auth-Token" : a["value"].stringValue
                        ]
                        
                        let params: Parameters = [
                            "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " a rejeté votre invitation" ,
                            "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " rejected your invitation" ,
                            "userids" : ["\(self.user["id"].stringValue)"] ,
                            "type" : "RejectSortie" ,
                            "titre" : "Invitation" ,
                            "photo" : "default" ,
                            "activite" : self.idSortie ,
                            "sender" : a["user"]["id"].stringValue ,
                            "rdv" : self.idRdv
                        ]
                        Alamofire.request(ScriptBase.sharedInstance.creeNotif , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                            .responseJSON { response in
                                print (response)
                                self.btnRefuser.isEnabled = true
                                self.btnAccepter.isEnabled = true
                        }
                        
                        
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    
                    
                }else{
                    self.btnRefuser.isEnabled = true
                    self.btnAccepter.isEnabled = true
                    _ = SweetAlert().showAlert(Localization("BtnRefuser"), subTitle: Localization("erreur"), style: AlertStyle.error)
                    
                }
                
                
        }
    }
    /*
     *accept action
     *
     */
    @IBAction func accepterAction(_ sender: Any) {
        if self.prestations["prestataire"]["id"].stringValue == ""
        {
            self.btnRefuser.isEnabled = false
            self.btnAccepter.isEnabled = false
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : a["value"].stringValue
            ]
            
            let params: Parameters = [
                "id": self.id ,
                "respond" : true ,
                "actid" : self.idSortie
            ]
            print ("***********" , params)
            Alamofire.request(ScriptBase.sharedInstance.acceptRefuseInvit , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    
                    let b = JSON(response.data)
                    print ("invite friends : " , response)
                    if b["status"].boolValue {
                        
                        _ = SweetAlert().showAlert(Localization("BtnAccepter"), subTitle: Localization("invitAccept"), style: AlertStyle.success , buttonTitle: "OK" , action: { (otherButton) in
                            let ab = UserDefaults.standard.value(forKey: "User") as! String
                            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                            var a = JSON(data: dataFromString!)
                            let header: HTTPHeaders = [
                                "Content-Type" : "application/json",
                                "x-Auth-Token" : a["value"].stringValue
                            ]
                            if ( self.participants.count != 0)
                            {
                                
                                let group = DispatchGroup()
                                
                                for i in 0 ... (self.participants.count - 1)
                                {
                                    group.enter()
                                    let params: Parameters = [
                                        "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " vient de rejoindre " + self.nom ,
                                        "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " had joined " + self.nom ,
                                        "userids" : ["\(self.participants[i]["id"].stringValue)"] ,
                                        "type" : "AcceptSortie" ,
                                        "titre" : "Invitation" ,
                                        "photo" : "default" ,
                                        "activite" : self.idSortie ,
                                        "sender" : a["user"]["id"].stringValue ,
                                        "rdv" : self.idRdv
                                    ]
                                    print (" azerezarzaerzaer " , params )
                                    Alamofire.request(ScriptBase.sharedInstance.creeNotif , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                                        .responseJSON { response in
                                            print (response)
                                            group.leave()
                                    }
                                }}
                            
                            self.navigationController?.popViewController(animated: true)
                        })
                        
                        
                        
                    }else{
                        self.btnRefuser.isEnabled = true
                        self.btnAccepter.isEnabled = true
                        _ = SweetAlert().showAlert(Localization("BtnAccepter"), subTitle: Localization("erreur"), style: AlertStyle.error)
                        
                    }
                    
                    
            }
        }
        else {
            let view = self.storyboard?.instantiateViewController(withIdentifier: "Panier") as! PanierKissouViewController
            view.activity = self.prestations
            view.heureDep = self.heureDepart
            view.dateDep = self.dateDepart
            view.id = self.idSortie
            view.inviter = true
            view.idNotif = self.id
            view.titleLec = self.nom
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
    
    /*
     *custom annotation to add on the map
     *
     */
    @objc func handleDisconnectedUserUpdateNotification(notification: NSNotification){
        let coord = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!)
        let annot = Artwork(title: "", locationName: "", discipline: "", coordinate: coord,color: UIColor.blue)
        
        self.map.addAnnotation(annot)
        // self.validerAnnulerView.isHidden = false
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        
        
        if annotation.isKind(of: MKUserLocation.self){
            return nil
        }else if annotation is ImageAnnotation {
            reuseId = "Pin"
            var pinView = map.dequeueReusableAnnotationView(withIdentifier: reuseId) as? ImageAnnotationViewRDV
            
            
            pinView = ImageAnnotationViewRDV(annotation: annotation, reuseIdentifier: reuseId)
            /*  switch now_image  {
             case 0 : pinView?.image = UIImage(named:"exemple")
             break
             case  1 : pinView?.image = UIImage(named:"aymen")
             break
             default : pinView?.image = UIImage(named:"haddar")
             break
             } */
            for x in StaticImageAnnotationRDV.sharedInstance.AImages {
                if isEqual(withCoordinate: x.coordinate,withAnotherCoordinate: annotation.coordinate) {
                    pinView?.image1 = x.image1
                    pinView?.setimageview()
                }
            }
            
            
            
            
            return pinView
            
        }else {
            reuseId = "Pin2"
            let saifPin =  MKAnnotationView(annotation: annotation, reuseIdentifier: "PINA")
            saifPin.isEnabled = true
            saifPin.canShowCallout = true
            
            saifPin.image = UIImage(named: "Pin_saif")
            
            // saifPin.tintColor = MKAnnotationView.blueColor()
            // saifPin.tintAdjustmentMode = .normal
            return saifPin
        }
        
    }
    func isEqual(withCoordinate location1: CLLocationCoordinate2D, withAnotherCoordinate location2: CLLocationCoordinate2D) -> Bool {
        let locationString1 = "\(location1.latitude), \(location1.longitude)"
        let locationString2 = "\(location2.latitude), \(location2.longitude)"
        if (locationString1 == locationString2) {
            return true
        }
        else {
            return false
        }
    }
    func formatAnnotationView(_ pinView: ImageAnnotationView, for aMapView: MKMapView) {
        if pinView != nil {
            
            let zoomLevel: Double = aMapView.zoomLevel
            let scale = Double(-1 * sqrt(Double(1 - pow((zoomLevel / 20.0), 2.0))) + 1.1)
            // This is a circular scale function where at zoom level 0 scale is 0.1 and at zoom level 20 scale is 1.1
            // Option #1
            pinView.transform = CGAffineTransform(scaleX: CGFloat(scale), y: CGFloat(scale))
            // Option #2
            let pinImage = UIImage(named: "exemple")
            let pinImage2 = UIImage(named: "skii")
            /* pinView.image = resizeImage(image: pinImage!,targetSize: CGSize(width: Double((pinImage?.size.width)!) * scale, height:  Double((pinImage?.size.height)!) * scale)) */
            /*pinView.image2 = resizeImage(image: pinImage2!,targetSize: CGSize(width: Double(((pinImage?.size.width)! / 3)) * scale, height:  Double(((pinImage?.size.height)! / 3)) * scale)) */
        }
    }
}

