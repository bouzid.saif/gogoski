//
//  TestFile.swift
//  GogoSki
//
//  Created by Bouzid saif on 16/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import DownPicker
class TestFile : UIViewController {
    @IBOutlet weak var down: UITextField!
    var down_picker = DownPicker()
   var bandArray : NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
        bandArray.add("Saif")
        bandArray.add("Ahmed")
        bandArray.add("Kissou")
        bandArray.add("praty")
        self.down_picker = DownPicker(textField: down, withData: bandArray as! [Any])
        self.down_picker.setArrowImage(UIImage(named:"downArrow_saif"))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
