//
//  PassProtocolUser.swift
//  GogoSki
//
//  Created by Bouzid saif on 05/12/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
/*
 *protocol to use to pass data from view to view
 *
 */
protocol PassProtocolUser {
    func pass(data: SportOBJUser)
}
