//
//  AjouterSportUser.swift
//  GogoSki
//
//  Created by Bouzid saif on 05/12/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AjouterSportUser: UIViewController {
    
    @IBOutlet var dissmissView: UIView!
    /*
     *Table to use for  sports  and levels
     *
     */
    @IBOutlet weak var table: UITableView!
    /*
     *the name of the view
     *
     */
    @IBOutlet weak var titleLBL: UILabel!
    /*
     *this Protocol will be used to pass the sport data to the preview view
     *
     */
    var delegate: PassProtocolUser?
    ///////
    /*
     *Levels in French
     *
     */
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     *Levels in English
     *
     */
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     *Sports in French
     *
     */
    var SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     *Sports in English
     *
     */
    var SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     *Image for sports
     *
     */
    var imageArray = [#imageLiteral(resourceName: "ski-alpin-kissou"),#imageLiteral(resourceName: "Snowboard-kissou"),#imageLiteral(resourceName: "Ski-fond-kissou"),#imageLiteral(resourceName: "Handi-ski-kissou"),#imageLiteral(resourceName: "Raquettes-kissou"),#imageLiteral(resourceName: "Randonnée-ski-kissou")]
    /*
     *Defaults levels to use when passing Data
     *
     */
    let niveauArray = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     *Defaults levels color
     *
     */
    let niveauColor = [UIColor("#D3D3D3"),UIColor("#76EC9E"),UIColor("#75A7FF"),UIColor("#FF3E53"),UIColor("#4D4D4D")]
    /*
     *this flag to know when to reload the tableview with new itms ( false = sports , true = levels)
     *
     */
    var flag : Bool! = false
    /*
     *this the  var  where to save the choosed sport
     *
     */
    var sportChoosed : String!
    /*
     *this the  var  where to save the choosed level
     *
     */
    var niveauChoosed : String!
    /*
     *this the  var  where to save the choosed sport image
     *
     */
    var imageSportChoosed : UIImage!
    /*
     *this the  sports of the connected user
     *
     */
    var json : JSON!
    /*
     *this to know if the user has choosen a new sport and level Default to false
     *
     */
    var ok : Bool! = false
    /*
     *this is the SportUser entity array
     *
     */
    var sportTable = [SportOBJUser]()
    /*
     *load the users sports and remove all the sports that have been already be included
     *
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if sportTable.count != 0 {
        for i in  0 ... ((sportTable.count) - 1) {
            //print(a["user"]["sports"][i]["pratique"].stringValue)
            switch sportTable[i].pratique {
            case SportSaifFR[0] :
                self.SportSaifFR.remove(at: 0)
                self.SportSaifEN.remove(at: 0)
                self.imageArray.remove(at: 0)
            case SportSaifFR[1] :
                self.SportSaifFR.remove(at: 1)
                self.SportSaifEN.remove(at: 1)
                self.imageArray.remove(at: 1)
            case SportSaifFR[2] :
                self.SportSaifFR.remove(at: 2)
                self.SportSaifEN.remove(at: 2)
                self.imageArray.remove(at: 2)
            case SportSaifFR[3] :
                self.SportSaifFR.remove(at: 3)
                self.SportSaifEN.remove(at: 3)
                self.imageArray.remove(at: 3)
            case SportSaifFR[4] :
                self.SportSaifFR.remove(at: 4)
                self.SportSaifEN.remove(at: 4)
                self.imageArray.remove(at: 4)
            case SportSaifFR[5] :
                self.SportSaifFR.remove(at: 5)
                self.SportSaifEN.remove(at: 5)
                self.imageArray.remove(at: 5)
            default :
                print("no one")
            }
        }
             self.table.reloadData()
        }
       
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dissmissV(_:)))
        gesture.numberOfTapsRequired = 1
        gesture.numberOfTouchesRequired = 1
        self.dissmissView.addGestureRecognizer(gesture)
        self.dissmissView.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
    }
    /*
     *dissmiss the view
     *
     */
    @objc func dissmissV(_ gesture : UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
    }
    /*
     *pass the data to the preview view
     *
     */
    override func viewWillDisappear(_ animated: Bool) {
        if ok {
            var sport = SportOBJUser()
            sport.niveau = niveauChoosed
            sport.pratique = sportChoosed
            self.delegate?.pass(data: sport)
        }
    }
    /*
     *add the new sport to the database
     *
     */
    func ajouterSport(){
        LoaderAlert.shared.show()
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : json["value"].stringValue
        ]
        let params: Parameters = [
            "niveau": niveauChoosed,
            "pratique" : sportChoosed,
            "photo" : "default"
        ]
        Alamofire.request(ScriptBase.sharedInstance.ajouterSport , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                let b = JSON(response.data)
                print("b:",b)
                if b["status"].boolValue {
                    self.ok = true
                }
                self.dismiss(animated: true, completion: {
                    if b["status"].boolValue {
                        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                            _ = SweetAlert().showAlert("Ajout Sport", subTitle: "Ajout effectué avec succés,veuillez appliquer les modifications", style: AlertStyle.success)
                        }else {
                            _ = SweetAlert().showAlert("Sport Add", subTitle: "Add Done With Success,please save your changes", style: AlertStyle.success)
                        }
                    }else{
                        if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr" {
                            _ = SweetAlert().showAlert("Sport Add", subTitle: "An Error Has Occured", style: AlertStyle.error)
                        }else {
                            _ = SweetAlert().showAlert("Ajout Sport", subTitle: "Une erreur est survenue", style: AlertStyle.error)
                        }
                    }
                })
        }
    }
    
    func getData(){
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        json = JSON(data: dataFromString!)
    }
}

extension AjouterSportUser: UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (flag ){
            return NiveauSaifFR.count
        }
        return SportSaifFR.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "ajoutSportCell", for: indexPath)
        let image = cell.viewWithTag(1) as! UIImageView
        let textLbL = cell.viewWithTag(2) as! UILabel
        if (!flag){
            
            image.image = imageArray[indexPath.row]
            image.tintColor = UIColor("#BCBBBB")
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                textLbL.text = SportSaifFR[indexPath.row]
            }else {
                textLbL.text = SportSaifEN[indexPath.row]
            }
            return cell
        }
        image.image = imageSportChoosed
        image.tintColor = niveauColor[indexPath.row]
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            textLbL.text = NiveauSaifFR[indexPath.row]
        }else {
            textLbL.text = NiveauSaifEN[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !flag {
            sportChoosed = SportSaifFR[indexPath.row]
            imageSportChoosed = imageArray[indexPath.row]
            flag = true
            tableView.reloadData()
            
        }else{
            titleLBL.text = "NIVEAU"
            print("sport choosed  :  " , self.sportChoosed , "  niveau choosed : " , self.niveauArray[indexPath.row] )
            niveauChoosed = self.NiveauSaifFR[indexPath.row]
            ajouterSport()
        }
    }
}


