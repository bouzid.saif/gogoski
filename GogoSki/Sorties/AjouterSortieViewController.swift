//
//  AjouterSortieViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 16/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import DownPicker
import UIColor_Hex_Swift
import SwiftyJSON
import Alamofire
class AjouterSortieViewController: InternetCheckViewControllers , UITextViewDelegate {
    /*
     *Levels in French
     *
     */
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     *Levels in English
     *
     */
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     *Practice in French
     *
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     *Practice in English
     *
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     *practice type
     *
     */
    @IBOutlet weak var typePratique: UITextField!
    /*
     *practice picker
     *
     */
    var typePratique_downPicker : DownPicker!
    /*
     *department TF
     *
     */
    @IBOutlet weak var departement: UITextField!
    /*
     *to show the view where to choose your apointment place
     *
     */
    @IBOutlet weak var btnLocation: UIButton!
    /*
     *department picker
     *
     */
    var departement_downPicker : DownPicker!
    /*
     *level TF
     *
     */
    @IBOutlet weak var niveau: UITextField!
    /*
     *level picker
     *
     */
    var niveau_downPicker : DownPicker!
    /*
     *station TF
     *
     */
    @IBOutlet weak var station: UITextField!
    /*
     *station picker
     *
     */
    var station_downPicker : DownPicker!
    /*
     *hour TF
     *
     */
    @IBOutlet weak var heure: UITextField!
    /*
     *massif picker
     *
     */
    var massif_downPicker : DownPicker!
    /*
     *massif TF
     *
     */
    @IBOutlet weak var massif: UITextField!
    /*
     *apointment place label
     *
     */
    @IBOutlet weak var lieuRdv: UILabel!
    /*
     *this view combine the place and the duration
     *
     */
    @IBOutlet weak var placeDuree: UIView!
    /*
     *from place of the organizer
     *
     */
    @IBOutlet weak var lieuDepartOrganisateur: UITextField!
    /*
     *date TF
     *
     */
    @IBOutlet weak var date: UITextField!
    /*
     *this view combine the level and the practice
     *
     */
    @IBOutlet weak var typePratiqueNiveau: UIView!
    /*
     *name of the hangout TF
     *
     */
    @IBOutlet weak var nomSortie: PaddingTextField!
    /*
     *details about the hangout TV
     *
     */
    @IBOutlet weak var detailsSortie: UITextView!
    /*
     *duration picker
     *
     */
    var duree_downPicker : DownPicker!
    /*
     *duration TF
     *
     */
    @IBOutlet weak var duree: UITextField!
    /*
     *place picker
     *
     */
    var place_downPicker : DownPicker!
    /*
     *place TF
     *
     */
    @IBOutlet weak var place: UITextField!
    /*
     *flat rate checkbox
     *
     */
    @IBOutlet weak var forfait: VKCheckbox!
    /*
     *title of the view
     *
     */
    @IBOutlet weak var titre: UILabel!
    /*
     *button to create the hangout
     *
     */
    @IBOutlet weak var btnCreerSortie: UIButton!
    /*
     *iamge of the actual user
     *
     */
    @IBOutlet weak var imageToProfileNavBar: RoundedUIImageView!
    /*
     *flat rate label
     *
     */
    @IBOutlet weak var labelForfait: UILabel!
    /*
     *view where it combine the place organizer and his button
     *
     */
    @IBOutlet weak var viewLocation: UIView!
    /*
     *latitude to pass it to the next view
     *
     */
    var latitude = ""
    /*
     *longitude to pass it to the next view
     *
     */
    var longitude = ""
    /*
     *station title to pass it to the next view
     *
     */
    var TitreStations = ""
    /*
     *massif to pass it to the next view
     *
     */
    var TitreMassif = ""
    /*
     *organizer apointment place to pass it to the next view
     *
     */
    var TitreDepart = ""
    /*
     *id of the hangout if it's created byt the renew button to pass it to the next view
     *
     */
    var id = ""
    /*
     *station data
     *
     */
    var StationObject : JSON = []
    /*
     *hangout data
     *
     */
    var sortie : JSON = []
    /*
     *duration array
     *
     */
    var bandArray5 : NSMutableArray = []
    /*
     *massif array
     *
     */
    var massifArray : [String] = []
    /*
     *date picker
     *
     */
    let datePicker = UIDatePicker()
    /*
     *hour picker
     *
     */
    let datePicker1 = UIDatePicker()
    /*
     *apointment list
     *
     */
    var RDVPoint : JSON = []
    var dateSelected : Date = Date()
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDisconnectedUserUpdateNotification(notification:)), name: NSNotification.Name(rawValue: "SetLieu"), object: nil)
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if let urlImgUser = URL(string : a["user"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imageToProfileNavBar.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        detailsSortie.delegate = self
        //showDatePicker()
        //showTimePicker()
        self.heure.isUserInteractionEnabled = false
        typePratiqueNiveau.layer.cornerRadius = 10
        placeDuree.layer.cornerRadius = 10
        detailsSortie.layer.cornerRadius = 10
        detailsSortie.textContainerInset = UIEdgeInsetsMake(10, 13, 10, 13)
        let img = UIImageView()
        img.image = UIImage(named: "calender_ahmed")
        img.frame = CGRect(x: CGFloat(date.frame.size.width - 20), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        date.rightView = img
        date.rightViewMode = .always
        
        
        let img1 = UIImageView()
        img1.image = UIImage(named: "time_ahmed")
        img1.frame = CGRect(x: CGFloat(heure.frame.size.width - 20), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        
        heure.rightView = img1
        heure.rightViewMode = .always
        
        
        lieuRdv.isUserInteractionEnabled = false
        lieuRdv.isEnabled = true
        let bandArray : NSMutableArray = []
        bandArray.add("")
        bandArray.add("Ski alpin")
        bandArray.add("Snowboard")
        bandArray.add("Ski de randonnée")
        bandArray.add("Handiski")
        bandArray.add("Raquette")
        bandArray.add("Ski de fond")
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            typePratique_downPicker = DownPicker(textField: typePratique, withData: SportSaifFR)
        }else {
            typePratique_downPicker = DownPicker(textField: typePratique, withData: SportSaifEN)
        }
        typePratique_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        
        let bandArray1 : NSMutableArray = []
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            bandArray1.add("Veuillez choisir un massif")
        }else {
            bandArray1.add("Please select a massif")
        }
        departement_downPicker = DownPicker(textField: departement, withData: bandArray1 as! [Any])
        departement_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        let bandArray2 : NSMutableArray = []
        bandArray2.add("")
        bandArray2.add("Débutant")
        bandArray2.add("Intermédiaire")
        bandArray2.add("Confirmé")
        bandArray2.add("Expert")
        ////sporttt
        
        
        
        //
        
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            niveau_downPicker = DownPicker(textField: niveau, withData: NiveauSaifFR)
        } else {
            niveau_downPicker = DownPicker(textField: niveau, withData: NiveauSaifEN)
        }
        niveau_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        let bandArray3 : NSMutableArray = []
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            bandArray3.add("Veuillez choisir un départment")
        }else {
            bandArray3.add("Please select a department")
        }
        station_downPicker = DownPicker(textField: station, withData: bandArray3 as! [Any])
        station_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        let bandArray4 : NSMutableArray = []
        massif_downPicker = DownPicker(textField: massif, withData: bandArray4 as! [Any])
        massif_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        
        
        duree_downPicker = DownPicker(textField: duree, withData: bandArray5 as! [Any])
        duree_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        let bandArray6 : NSMutableArray = []
        for i in 0...20 {
            bandArray6.add(String(i))
        }
        bandArray6.add("illimité")
        place_downPicker = DownPicker(textField: place, withData: bandArray6 as! [Any])
        place_downPicker.setArrowImage(UIImage(named:"downArrow"))
        
        
        forfait.borderWidth = 1
        forfait.bgColorSelected = UIColor("#5FB6FF")
        forfait.borderColor = UIColor.gray
        forfait.bgColor = UIColor.white
        forfait.isHidden = true
        labelForfait.isHidden = true
        viewLocation.layer.cornerRadius = 5
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        massif_downPicker.addTarget(self, action: #selector(self.massifDidChange), for : .valueChanged )
        departement_downPicker.addTarget(self, action: #selector(self.departementDidChange), for : .valueChanged )
        station_downPicker.addTarget(self, action: #selector(self.stationDidChange), for : .valueChanged )
        LoadSortie()
    }
    /*
     *this notif is called when the user had chosen an apointment
     *
     */
    @objc func handleDisconnectedUserUpdateNotification(notification: NSNotification){
        
        self.lieuRdv.text = self.station.text
        self.id = notification.object as! String
    }
    /*
     *user choosen a massif
     *
     */
    @objc func massifDidChange() {
        print("massif changed")
        var departementx : [String] = []
        if self.massif.text! != "" {
            for i in 0...StationObject["data"].arrayObject!.count - 1 {
                if StationObject["data"][i]["massif"].stringValue == self.massif.text! {
                    departementx.append(StationObject["data"][i]["dept"].stringValue)
                }
            }
            departementx = departementx.removeDuplicates()
            let bandArray4 : NSMutableArray = []
            bandArray4.addObjects(from: departementx)
            self.departement_downPicker = DownPicker(textField: self.departement, withData: bandArray4 as! [Any])
            self.departement.text = ""
            departement_downPicker.setPlaceholder(Localization("Departement"))
            self.station.text = ""
            station_downPicker.setPlaceholder(Localization("Station"))
            departement_downPicker.addTarget(self, action: #selector(self.departementDidChange), for : .valueChanged )
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                self.departement_downPicker.setToolbarDoneButtonText("Terminé")
                self.departement_downPicker.setToolbarCancelButtonText("Annuler")
                self.departement_downPicker.setPlaceholder(Localization("Departement"))
            }
            else {
                self.departement_downPicker.setToolbarDoneButtonText("Done")
                self.departement_downPicker.setToolbarCancelButtonText("Cancel")
                self.departement_downPicker.setPlaceholder(Localization("Departement"))
            }
        }
        
        
    }
    /*
     *user chosen a department
     *
     */
    @objc func departementDidChange() {
        print("massif changed")
        var stationsx : [String] = []
        print("true")
        if self.massif.text! != "" && self.departement.text! != "" {
            for i in 0...StationObject["data"].arrayObject!.count - 1 {
                print("massif:",StationObject["data"][i]["massif"].stringValue)
                print("dep:",StationObject["data"][i]["dept"].stringValue)
                if StationObject["data"][i]["massif"].stringValue == self.massif.text! && StationObject["data"][i]["dept"].stringValue == self.departement.text! {
                    print("false")
                    stationsx.append(StationObject["data"][i]["nom"].stringValue)
                }
            }
            stationsx = stationsx.removeDuplicates()
            let bandArray4 : NSMutableArray = []
            bandArray4.addObjects(from: stationsx)
            self.station_downPicker = DownPicker(textField: self.station, withData: bandArray4 as! [Any])
            self.station.text = ""
            station_downPicker.setPlaceholder(Localization("Station"))
            station_downPicker.addTarget(self, action: #selector(self.stationDidChange), for : .valueChanged )
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                self.station_downPicker.setToolbarDoneButtonText("Terminé")
                self.station_downPicker.setToolbarCancelButtonText("Annuler")
                self.station_downPicker.setPlaceholder(Localization("Station"))
            }
            else {
                self.station_downPicker.setToolbarDoneButtonText("Done")
                self.station_downPicker.setToolbarCancelButtonText("Cancel")
                self.station_downPicker.setPlaceholder(Localization("Station"))
            }
        }
    }
    /*
     *user chosen a station
     *
     */
    @objc func stationDidChange() {
        print("station changed")
        if self.massif.text! != "" && self.departement.text! != "" && self.station.text != "" {
            for i in 0...StationObject["data"].arrayObject!.count - 1 {
                if StationObject["data"][i]["massif"].stringValue == self.massif.text! && StationObject["data"][i]["dept"].stringValue == self.departement.text! && StationObject["data"][i]["nom"].stringValue == self.station.text{
                    print("false")
                    print("lat:",StationObject["data"][i])
                    if StationObject["data"][i]["listpointsrdv"].arrayObject?.count != 0 {
                        self.latitude = StationObject["data"][i]["listpointsrdv"][0]["latitude"].stringValue
                        self.RDVPoint = StationObject["data"][i]["listpointsrdv"]
                        print("RDVTEST:",self.RDVPoint)
                    }else{
                        //self.latitude = "48.956467"
                        self.RDVPoint = []
                        
                    }
                    print("lat:",self.latitude)
                    if StationObject["data"][i]["listpointsrdv"].arrayObject?.count != 0 {
                        self.longitude = StationObject["data"][i]["listpointsrdv"][0]["longitude"].stringValue
                        self.RDVPoint = StationObject["data"][i]["listpointsrdv"]
                        
                    }else {
                        // self.longitude = "4.358637"
                        self.RDVPoint = []
                    }
                    self.TitreStations = self.station.text!
                    self.TitreMassif = self.massif.text!
                    self.TitreDepart = self.departement.text!
                    if StationObject["data"][i]["listpointsrdv"].arrayObject?.count != 0 {
                        // self.id = StationObject["data"][i]["listpointsrdv"][0]["id"].stringValue
                        self.btnLocation.isEnabled = true
                    }else {
                        // self.id = "default"
                        _ = SweetAlert().showAlert(Localization("Station"), subTitle: Localization("pointRDVMessage") ,style: AlertStyle.error)
                        self.btnLocation.isEnabled = false
                    }
                    
                }
            }
            //stationsx = stationsx.removeDuplicates()
        }
    }
    /*
     *load all the stations
     *
     */
    func loadData(){
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.Stations , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                self.massifArray = []
                let a = JSON(response.data)
                print ( "station creer " , a)
                self.StationObject = a
                for i in 0...a["data"].arrayObject!.count - 1 {
                    self.massifArray.append(a["data"][i]["massif"].stringValue)
                }
                self.massifArray = self.massifArray.removeDuplicates()
                print("Stations: ",a)
                let bandArray4 : NSMutableArray = []
                bandArray4.addObjects(from: self.massifArray)
                self.massif_downPicker = DownPicker(textField: self.massif, withData: bandArray4 as! [Any])
                self.massif_downPicker.addTarget(self, action: #selector(self.massifDidChange), for : .valueChanged )
                if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                    self.massif_downPicker.setToolbarDoneButtonText("Terminé")
                    self.massif_downPicker.setToolbarCancelButtonText("Annuler")
                    self.massif_downPicker.setPlaceholder(Localization("Massif"))
                }
                else {
                    self.massif_downPicker.setToolbarDoneButtonText("Done")
                    self.massif_downPicker.setToolbarCancelButtonText("Cencel")
                    self.massif_downPicker.setPlaceholder(Localization("Massif"))
                }
                print("now:",self.massif.text)
                /*   if a["status"].boolValue == false {
                 if a["message"].stringValue == "L utilisateur n existe pas" || a["message"].stringValue == "Mot de passe invalide" {
                 
                 _ = SweetAlert().showAlert(Localization("ConnexionChargement"), subTitle: Localization("ConnexionErreur"), style: AlertStyle.error)
                 }
                 }else{
                 self.nomUser.text = a["data"]["prenom"].stringValue + " " + a["data"]["nom"].stringValue
                 if let urlImgUser = URL(string : a["data"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
                 self.photo_profil.setImage(withUrl: urlImgUser , placeholder: placeholder)
                 }
                 /*  if (a["data"]["sports"].arrayObject?.count != 0)
                 {
                 for i in 0 ... ((a["data"]["sports"].arrayObject?.count)! - 1)
                 {
                 if (a["data"]["sports"][i]["is_default"].boolValue)
                 {
                 self.sportUser.setTitle(a["data"]["sports"][i]["pratique"].stringValue, for: .normal)
                 }
                 }
                 } */
                 } */
                
                
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
     *back action
     *
     */
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
     *date action
     *
     */
    func showDatePicker(){
        
        datePicker.datePickerMode = .date
        let dateLioum = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result = formatter.string(from: dateLioum)
        let calendar = Calendar.current
      
    
        let hour = String(calendar.component(.hour, from: dateLioum))
       let minutes = String(calendar.component(.minute, from: dateLioum))
       if hour == "19" && Int(minutes)! > 0
        {
           let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: formatter.date(from: result)!)
           datePicker.minimumDate = tomorrow
          }
        else {
           datePicker.minimumDate = formatter.date(from: result)
         }
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            datePicker.locale = NSLocale(localeIdentifier: "fr_FR") as Locale
            let doneButton = UIBarButtonItem(title: "Terminé", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.donedatePicker))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Annuler", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelDatePicker))
            toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        }else {
            datePicker.locale = NSLocale(localeIdentifier: "en_EN") as Locale
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.donedatePicker))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelDatePicker))
            toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        }
        
        date.inputAccessoryView = toolbar
        date.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        date.text = formatter.string(from: datePicker.date)
        showTimePicker()
        if date.text != "" {
            self.heure.isUserInteractionEnabled = true
        }
        
        dateSelected = datePicker.date
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    /*
     *hour action
     *
     */
    func showTimePicker(){
        let date = Date()
        //Formate Date
        datePicker1.datePickerMode = .time
        datePicker1.minuteInterval = 30
        let formatter = DateFormatter()
        //   formatter.timeStyle = .medium
        formatter.dateFormat = "HH:mm"
        formatter.timeZone = TimeZone(identifier: "GMT+1")
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "dd/MM/yyyy"
        var dateSelectedString = formatter1.string(from: dateSelected)
        var dateLioum = formatter1.string(from: date)
        var s = formatter.date(from: "08:00")
        
        if dateSelectedString == dateLioum
        {
            
            let calendar = Calendar.current
            
            var hour = String(calendar.component(.hour, from: date))
            let minutes = String(calendar.component(.minute, from: date))
            if ( hour == "19" && minutes == "0")
            {
                s = formatter.date(from: "19:00")!
            }
            else if Int(hour)! < 19 && Int(hour)! > 8 {
                if ( hour == "8" || hour == "9")
                {
                    hour = "0" + hour
                }
                if ( minutes == "0")
                {
                     s = formatter.date(from: hour + ":00")!
                }
            }
            else if Int(hour)! > 19{
                heure.isUserInteractionEnabled = false
            }
            
        
        }
        datePicker1.minimumDate = s
        
        var s2 = formatter.date(from: "19:00")
        datePicker1.maximumDate = s2
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            datePicker1.locale = NSLocale(localeIdentifier: "fr_FR") as Locale
            let doneButton = UIBarButtonItem(title: "Terminé", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneTimePicker))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Annuler", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelTimePicker))
            toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        }else {
            datePicker1.locale = NSLocale(localeIdentifier: "en_EN") as Locale
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneTimePicker))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelTimePicker))
            toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        }
        
        heure.inputAccessoryView = toolbar
        heure.inputView = datePicker1
        
    }
    
    @objc func doneTimePicker(){
        
        let formatter = DateFormatter()
        //   formatter.timeStyle = .medium
        formatter.dateFormat = "HH:mm"
        formatter.timeZone = TimeZone(identifier: "GMT+1")
        heure.text = formatter.string(from: datePicker1.date)
        var characters = Array(heure.text!)
        characters[3] = "0"
        heure.text = String(characters)
        
        self.view.endEditing(true)
        bandArray5 = []
        switch heure.text {
        case "19:00"? :
            bandArray5.add("01:00")
        case "18:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
        case "17:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
        case "16:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
        case "15:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
        case "14:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
            bandArray5.add("05:30")
            bandArray5.add("06:00")
        case "13:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
            bandArray5.add("05:30")
            bandArray5.add("06:00")
            bandArray5.add("06:30")
            bandArray5.add("07:00")
        case "12:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
            bandArray5.add("05:30")
            bandArray5.add("06:00")
            bandArray5.add("06:30")
            bandArray5.add("07:00")
            bandArray5.add("07:30")
            bandArray5.add("08:00")
        case "11:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
            bandArray5.add("05:30")
            bandArray5.add("06:00")
            bandArray5.add("06:30")
            bandArray5.add("07:00")
            bandArray5.add("07:30")
            bandArray5.add("08:00")
            bandArray5.add("08:30")
            bandArray5.add("09:00")
        case "10:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
            bandArray5.add("05:30")
            bandArray5.add("06:00")
            bandArray5.add("06:30")
            bandArray5.add("07:00")
            bandArray5.add("07:30")
            bandArray5.add("08:00")
            bandArray5.add("08:30")
            bandArray5.add("09:00")
            bandArray5.add("09:30")
            bandArray5.add("10:00")
        case "09:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
            bandArray5.add("05:30")
            bandArray5.add("06:00")
            bandArray5.add("06:30")
            bandArray5.add("07:00")
            bandArray5.add("07:30")
            bandArray5.add("08:00")
            bandArray5.add("08:30")
            bandArray5.add("09:00")
            bandArray5.add("09:30")
            bandArray5.add("10:00")
            bandArray5.add("10:30")
            bandArray5.add("11:00")
        case "08:00"? :
            bandArray5.add("01:00")
            bandArray5.add("01:30")
            bandArray5.add("02:00")
            bandArray5.add("02:30")
            bandArray5.add("03:00")
            bandArray5.add("03:30")
            bandArray5.add("04:00")
            bandArray5.add("04:30")
            bandArray5.add("05:00")
            bandArray5.add("05:30")
            bandArray5.add("06:00")
            bandArray5.add("06:30")
            bandArray5.add("07:00")
            bandArray5.add("07:30")
            bandArray5.add("08:00")
            bandArray5.add("08:30")
            bandArray5.add("09:00")
            bandArray5.add("09:30")
            bandArray5.add("10:00")
            bandArray5.add("10:30")
            bandArray5.add("11:00")
            bandArray5.add("11:30")
            bandArray5.add("12:00")
        default : print ("hata chay")
        }
        duree_downPicker = DownPicker(textField: duree, withData: bandArray5 as! [Any])
        duree_downPicker.setPlaceholder(Localization("Duree"))
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            self.duree_downPicker.setToolbarDoneButtonText("Terminé")
            self.duree_downPicker.setToolbarCancelButtonText("Annuler")
           // self.duree_downPicker.setPlaceholder(Localization("Station"))
        }
        else {
            self.duree_downPicker.setToolbarDoneButtonText("Done")
            self.duree_downPicker.setToolbarCancelButtonText("Cencel")
            //self.duree_downPicker.setPlaceholder(Localization("Station"))
        }
    }
    
    @objc func cancelTimePicker(){
        self.view.endEditing(true)
    }
    /*
     *apointment button action
     *
     */
    @IBAction func pickLocation (_ sender: Any)
    {
        if longitude != "" {
            let view = self.storyboard?.instantiateViewController(withIdentifier: "choisirLieu") as! ChoisirLieuViewController
            view.longitiude = self.longitude
            view.latitude = self.latitude
            view.titreSt = self.TitreStations
            view.massif = self.TitreMassif
            view.depart = self.TitreDepart
            view.RDVApointment = self.RDVPoint
            self.navigationController?.present(view, animated: true, completion: nil)
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("CréerSortieTitre")
        massif_downPicker.setPlaceholder(Localization("Massif"))
        niveau_downPicker.setPlaceholder(Localization("Niveau"))
        departement_downPicker.setPlaceholder(Localization("Departement"))
        station_downPicker.setPlaceholder(Localization("Station"))
        typePratique_downPicker.setPlaceholder(Localization("TypePratique"))
        lieuRdv.text = Localization("LieuRdv")
        lieuRdv.textColor = UIColor.lightGray
        detailsSortie.text = Localization("DetailsSortie")
        duree_downPicker.setPlaceholder(Localization("Duree"))
        place_downPicker.setPlaceholder(Localization("Place"))
        labelForfait.text = Localization("forfait")
        btnCreerSortie.setTitle(Localization("BtnAperçu"), for: .normal)
        lieuDepartOrganisateur.placeholder = Localization("lieuDepartOrganisateur")
        nomSortie.placeholder = Localization("nomSortie")
        date.placeholder = Localization("Date")
        heure.placeholder = Localization("heure")
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            massif_downPicker.setToolbarDoneButtonText("Terminé")
            massif_downPicker.setToolbarCancelButtonText("Annuler")
            niveau_downPicker.setToolbarDoneButtonText("Terminé")
            niveau_downPicker.setToolbarCancelButtonText("Annuler")
            typePratique_downPicker.setToolbarDoneButtonText("Terminé")
            typePratique_downPicker.setToolbarCancelButtonText("Annuler")
            departement_downPicker.setToolbarDoneButtonText("Terminé")
            departement_downPicker.setToolbarCancelButtonText("Annuler")
            station_downPicker.setToolbarDoneButtonText("Terminé")
            station_downPicker.setToolbarCancelButtonText("Annuler")
            duree_downPicker.setToolbarDoneButtonText("Terminé")
            duree_downPicker.setToolbarCancelButtonText("Annuler")
            place_downPicker.setToolbarDoneButtonText("Terminé")
            place_downPicker.setToolbarCancelButtonText("Annuler")
        }else {
            massif_downPicker.setToolbarDoneButtonText("Done")
            massif_downPicker.setToolbarCancelButtonText("Cancel")
            niveau_downPicker.setToolbarDoneButtonText("Done")
            niveau_downPicker.setToolbarCancelButtonText("Cancel")
            typePratique_downPicker.setToolbarDoneButtonText("Done")
            typePratique_downPicker.setToolbarCancelButtonText("Cancel")
            departement_downPicker.setToolbarDoneButtonText("Done")
            departement_downPicker.setToolbarCancelButtonText("Cancel")
            station_downPicker.setToolbarDoneButtonText("Done")
            station_downPicker.setToolbarCancelButtonText("Cancel")
            duree_downPicker.setToolbarDoneButtonText("Done")
            duree_downPicker.setToolbarCancelButtonText("Cancel")
            place_downPicker.setToolbarDoneButtonText("Done")
            place_downPicker.setToolbarCancelButtonText("Cancel")
        }
        showDatePicker()
        
        //Localisator.sharedInstance.saveInUserDefaults = true
        
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /*
     *create the hangout action
     *
     */
    @IBAction func creerSortie(_ sender: Any) {
        if ( massif.text! != "" && niveau.text! != "" && departement.text! != "" && station.text! != "" && lieuRdv.text! != "" && typePratique.text! != "" && detailsSortie.text! != "" && duree.text! != "" && place.text! != "" && lieuDepartOrganisateur.text! != "" && nomSortie.text! != "" && date.text! != "" && heure.text! != "" )
        {
            let view = self.storyboard?.instantiateViewController(withIdentifier: "CreerSortie") as! CreerSortie2ViewController
            // view.id = self.sorties[indexPath.row]["id"].stringValue
            view.massif = massif.text!
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                view.niveau = niveau.text!
            }else {
                if (self.sortie == [] )
                {
                    view.niveau = NiveauSaifFR[niveau_downPicker.selectedIndex]
                }
                else {
                    view.niveau = niveau.text!
                }
            }
            view.departement = departement.text!
            view.station = station.text!
            view.lieuRdv = self.id
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                view.typepratique = typePratique.text!
            }else {
                if (self.sortie == [] )
                {
                    view.typepratique = SportSaifFR[typePratique_downPicker.selectedIndex]
                }
                else {
                    view.typepratique = typePratique.text!
                }
            }
            view.detailSortie = detailsSortie.text!
            view.duree = duree.text!
            view.place = place.text!
            view.lieuDepartOrganisateur = lieuDepartOrganisateur.text!
            view.nomSortie = nomSortie.text!
            view.dateDep = date.text!
            view.heureDep = heure.text!
            view.lat = latitude
            view.long = longitude
            if self.sortie["list_participants"] != []
            {
                view.participants = self.sortie["list_participants"]
            }
            self.navigationController?.pushViewController(view, animated: true)
        }
        else {
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                _ = SweetAlert().showAlert(Localization("CréerSortieTitre"), subTitle: "Veuillez remplir tous les champs" ,style: AlertStyle.error)
            }else{
                _ = SweetAlert().showAlert(Localization("CréerSortieTitre"), subTitle: "Please fill all in the blanks" ,style: AlertStyle.error)
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if  detailsSortie.text == Localization("DetailsSortie")
        {
            detailsSortie.text = ""
            detailsSortie.textColor = UIColor.black
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        if detailsSortie.text == "" {
            detailsSortie.textColor = UIColor.lightGray
            detailsSortie.text = Localization("DetailsSortie")
            detailsSortie.resignFirstResponder()
        }
    }
    /*
     *load the hangout if user had tapped the renew action
     *
     */
    func LoadSortie ()
    {
        if self.sortie != []
        {
            if  detailsSortie.text == Localization("DetailsSortie")
            {
                detailsSortie.text = self.sortie["detail"].stringValue
            }
            massif.text = self.sortie["point"]["station"]["massif"].stringValue
            niveau.text = self.sortie["sport"]["niveau"].stringValue
            departement.text = self.sortie["point"]["station"]["dept"].stringValue
            station.text = self.sortie["point"]["station"]["nom"].stringValue
            typePratique.text = self.sortie["sport"]["pratique"].stringValue
            var heureA = String(self.sortie["duree"].intValue / 60)
            var minute = String (self.sortie["duree"].intValue - ( (self.sortie["duree"].intValue / 60) * 60 ))
            if Int(minute)! < 10
            {
                minute = "0" + minute
            }
            if Int(heureA)! < 10
            {
                heureA = "0" + heureA
            }
            duree.text = heureA + ":" + minute
            place.text = self.sortie["nb_place_maximum"].stringValue
            lieuDepartOrganisateur.text = self.sortie["lieu_depart"].stringValue
            nomSortie.text = self.sortie["titre"].stringValue
            let s = self.sortie["date_debut"].stringValue.suffix(8)
            let toArray = s.components(separatedBy: "/")
            let backToString = toArray.joined(separator: ":")
            heure.text = backToString.prefix(5) + ""
            // if StationObject["data"][i]["listpointsrdv"].arrayObject?.count != 0 {
            if ( self.sortie["point"] != nil)
            {
                latitude = self.sortie["point"]["latitude"].stringValue
                longitude = self.sortie["point"]["longitude"].stringValue
            }
            else {
                latitude = "48.956467"
                longitude = "4.358637"
            }
            id = self.sortie["point"]["id"].stringValue
            if (lieuRdv.text == Localization("LieuRdv"))
            {
                lieuRdv.text = self.sortie["point"]["station"]["nom"].stringValue
            }
            detailsSortie.isUserInteractionEnabled = false
            massif.isUserInteractionEnabled = false
            niveau.isUserInteractionEnabled = false
            departement.isUserInteractionEnabled = false
            station.isUserInteractionEnabled = false
            typePratique.isUserInteractionEnabled = false
            duree.isUserInteractionEnabled = false
            place.isUserInteractionEnabled = false
            nomSortie.isUserInteractionEnabled = false
            heure.isUserInteractionEnabled = false
            lieuRdv.isUserInteractionEnabled = false
            btnLocation.isEnabled = false
        }
    }
}
extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
}

