//
//  ApercuFicheViewController.swift
//  GogoSki
//
//  Created by AYMEN on 11/16/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyJSON

class CreerSortie2ViewController: InternetCheckViewControllers,MKMapViewDelegate {
    
    // var participants : JSON = []
    // var id : String = ""
    //bav bar
    /*
     *Level in French
     *
     */
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     *Level in English
     *
     */
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     *Practice in French
     *
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     *Practice in English
     *
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     *Title of the navigation bar
     *
     */
    @IBOutlet weak var titreNavBarLBL: UILabel!
    /*
     *image of the actual user
     *
     */
    @IBOutlet weak var imageToProfileNavBar: UIImageView!
    ///header
    /*
     *massif text to be sent to the next step
     *
     */
    var massif = ""
    /*
     *level text to be sent to the next step
     *
     */
    var niveau = ""
    /*
     *department text to be sent to the next step
     *
     */
    var departement = ""
    /*
     *station text to be sent to the next step
     *
     */
    var station = ""
    /*
     *apponitement text to be sent to the next step
     *
     */
    var lieuRdv = ""
    /*
     *practice type  text to be sent to the next step
     *
     */
    var typepratique = ""
    /*
     *details of the hangout  to be sent to the next step
     *
     */
    var detailSortie = ""
    /*
     *duration text to be sent to the next step
     *
     */
    var duree = ""
    /*
     *place text to be sent to the next step
     *
     */
    var place = ""
    /*
     *appointement of the organizer text to be sent to the next step
     *
     */
    var lieuDepartOrganisateur = ""
    /*
     *hangout name text to be sent to the next step
     *
     */
    var nomSortie = ""
    /*
     *from date to be sent to the next step
     *
     */
    var dateDep = ""
    /*
     *from hour to be sent to the next step
     *
     */
    var heureDep = ""
    /*
     *latitude text to be sent to the next step
     *
     */
    var lat = ""
    /*
     *longitude text to be sent to the next step
     *
     */
    var long = ""
    /*
     *participants data
     *
     */
    var participants : JSON = []
    /*
     *station title label
     *
     */
    @IBOutlet weak var stationTitleLBL: UILabel!
    /*
     *share button
     *
     */
    @IBOutlet weak var partageBTN: UIButton!
    /*
     *hangout  image
     *
     */
    @IBOutlet weak var imageSortie: UIImageView!
    /*
     *houngout name  label
     *
     */
    @IBOutlet weak var sortieTitreLBL: UILabel!
    /*
     *practice type title
     *
     */
    @IBOutlet weak var sportTypeBTN: UIButton!
    /*
     *level sport title
     *
     */
    @IBOutlet weak var sportNiveauBTN: UIButton!
    //informations
    /*
     *from date  label
     *
     */
    @IBOutlet weak var departDateLBL: UILabel!
    /*
     *from hour  label
     *
     */
    @IBOutlet weak var departHeureLBL: UILabel!
    /*
     *period in hours  label
     *
     */
    @IBOutlet weak var periodeHeuresLBL: UILabel!
    /*
     *number of participants  label
     *
     */
    @IBOutlet weak var nombreParticipantLBL: UILabel!
    /*
     *number of remaining place  label
     *
     */
    @IBOutlet weak var nombrePlacesRestantesLBL: UILabel!
    @IBOutlet weak var map: MKMapView!
    //description
    /*
     *the container of the description height
     *
     */
    @IBOutlet weak var theWholeContainerHeight: NSLayoutConstraint!
    /*
     *the description height
     *
     */
    @IBOutlet weak var descriptionContainerHeight: NSLayoutConstraint!
    /*
     *the description label
     *
     */
    @IBOutlet weak var descriptionLBL: UILabel!
    /*
     *the show more button
     *
     */
    @IBOutlet weak var plusBtn: UIButton!
    /*
     *the practice label width
     *
     */
    @IBOutlet weak var SportUserContWidth: NSLayoutConstraint!
    //participants
    // @IBOutlet weak var placeRestantePHLBL: UILabel!
    // @IBOutlet weak var messagePHBTN: UIButton!
    // @IBOutlet weak var ajouterPHBTN: UIButton!
    // @IBOutlet weak var participantsTV: UITableView!
    /*
     *create the hangout button
     *
     */
    @IBOutlet weak var validerBTN: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    /*
     *the custom image of the pinview
     *
     */
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let saifPin =  MKAnnotationView(annotation: annotation, reuseIdentifier: "PINA")
        saifPin.isEnabled = true
        saifPin.canShowCallout = true
        
        saifPin.image = UIImage(named: "Pin_saif")
        
        // saifPin.tintColor = MKAnnotationView.blueColor()
        // saifPin.tintAdjustmentMode = .normal
        return saifPin
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.map.delegate = self
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if let urlImgUser = URL(string : a["user"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imageToProfileNavBar.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        stationTitleLBL.text = station
        sortieTitreLBL.text = nomSortie
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            sportTypeBTN.setTitle(typepratique, for: .normal)
            
        }else {
            switch typepratique {
            case SportSaifFR[0] :
                sportTypeBTN.setTitle(SportSaifEN[0], for: .normal)
            case SportSaifFR[1] :
                
                sportTypeBTN.setTitle(SportSaifEN[1], for: .normal)
            case SportSaifFR[2] :
                
                sportTypeBTN.setTitle(SportSaifEN[2], for: .normal)
            case SportSaifFR[3] :
                
                sportTypeBTN.setTitle(SportSaifEN[3], for: .normal)
            case SportSaifFR[4] :
                
                sportTypeBTN.setTitle(SportSaifEN[4], for: .normal)
            case SportSaifFR[5] :
                
                sportTypeBTN.setTitle(SportSaifEN[5], for: .normal)
            default :
                
                sportTypeBTN.setTitle("", for: .normal)
            }
        }
        if ((self.sportTypeBTN.title(for: .normal)?.count)! > 19)
        {
            SportUserContWidth.constant = 200
        }else if ((self.sportTypeBTN.title(for: .normal)?.count)! > 15)
        {
            SportUserContWidth.constant = 150
        }else if ((self.sportTypeBTN.title(for: .normal)?.count)! > 10) {
            SportUserContWidth.constant = 120
        }
        else {
            SportUserContWidth.constant = 100
        }
        if ( niveau == "Confirmé" )
        {
            sportNiveauBTN.setTitleColor(UIColor("#FF3E53"), for: .normal)
        }
        else if (niveau == "Débutant")
        {
            sportNiveauBTN.setTitleColor(UIColor("#D3D3D3"), for: .normal)
        }
        else if (niveau == "Intermédiaire")
        {
            sportNiveauBTN.setTitleColor(UIColor("#75A7FF"), for: .normal)
        }
        else if (niveau == "Débrouillard" ){
            sportNiveauBTN.setTitleColor(UIColor("#76EC9E"), for: .normal)
        }
        else {
            sportNiveauBTN.setTitleColor(UIColor("#4D4D4D"), for: .normal)
        }
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            sportNiveauBTN.setTitle(niveau, for: .normal)
        }else {
            switch niveau {
            case NiveauSaifFR[0] :
                sportNiveauBTN.setTitle(NiveauSaifEN[0], for: .normal)
            case NiveauSaifFR[1] :
                
                sportNiveauBTN.setTitle(NiveauSaifEN[1], for: .normal)
            case NiveauSaifFR[2] :
                
                sportNiveauBTN.setTitle(NiveauSaifEN[2], for: .normal)
            case NiveauSaifFR[3] :
                
                sportNiveauBTN.setTitle(NiveauSaifEN[3], for: .normal)
            case NiveauSaifFR[4] :
                
                sportNiveauBTN.setTitle(NiveauSaifEN[4], for: .normal)
                
            default :
                
                sportNiveauBTN.setTitle("", for: .normal)
            }
            
            
        }
        
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            departDateLBL.text = "Départ le : " + dateDep
            departHeureLBL.text = "à " + heureDep
        }else {
            departDateLBL.text = "Departure on : " + dateDep
            departHeureLBL.text = "at " + heureDep
        }
        
        duree = duree.replacingOccurrences(of: ":", with: "h")
        periodeHeuresLBL.text = duree
        
        if place != "illimité" && place != "limitless" {
            nombreParticipantLBL.text = place + " pers."
        }
        else {
            nombreParticipantLBL.text = place
        }
        
        descriptionLBL.text = detailSortie
        if self.descriptionLBL.text!.count < 98 {
            self.plusBtn.setTitle(Localization("plusP"), for: .normal)
            self.plusBtn.isHidden = true
        }else{
            self.plusBtn.setTitle(Localization("plusP"), for: .normal)
            self.plusBtn.isHidden = false
        }
        if (long.first == " ")
        {
            long = String(long.dropFirst())
        }
        let point = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!)
        let annotation = ColorPointAnnotation(pinColor: UIColor.blue)
        annotation.coordinate = point
        
        self.map.addAnnotation(annotation)
        self.map.setCenter(point, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        
    }
    
    // actions
    /*
     *create the hangout action
     *
     */
    @IBAction func validerAction(_ sender: Any) {
        
        //  dateDep.replacingCharacters(in: "/", with: "-")
        dateDep = dateDep.replacingOccurrences(of: "/", with: "-")
        //   var mySubstring1 = ""
        var minute = ""
        var heure = ""
        var str = ""
        /*  if (heureDep.characters.count == 8)
         {
         mySubstring1 = heureDep.prefix(5) + "0:00"
         }
         else {
         mySubstring1     = "0" + heureDep.prefix(4) + ":00"
         }*/
        
        heure = duree.prefix(2) + ""
        minute = duree.suffix(2) + ""
        
        str = String ( (Int (heure)! * 60) + Int (minute)!)
        if self.participants.count == 0
        {
            if place != "illimité" && place != "limitless" {
                place = String(Int(place)! + 1)
            }
            else {
                place = "100"
            }
        }
        var point = ""
        if long == "48.956467" && lat == "4.358637" {
            point = "default"
        }else {
            point = lieuRdv
        }
        let params: Parameters = [
            "datedebut" : dateDep + " " + heureDep,
            "detail" : detailSortie ,
            "titre" : nomSortie ,
            "station" : station ,
            "pratique" : typepratique ,
            "niveau" : niveau ,
            "massif" : massif ,
            "departement" : departement ,
            //   "lieurdv" : lieuRdv ,
            "lieudepart" : lieuDepartOrganisateur ,
            "duree" : str ,
            "nbMax" : place,
            "point" : point
        ]
        //lieuRdv
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.addSortie , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print (response)
                let act = JSON(response.data)
                print ("*******")
                print ("add : " , act)
                if act["status"].boolValue {
                    
                    if self.participants.count != 0
                    {
                        var point = ""
                        if self.long == "48.956467" && self.lat == "4.358637" {
                            point = "default"
                        }else {
                            point = self.lieuRdv
                        }
                        let group = DispatchGroup()
                        
                        for i in 0 ... (self.participants.count - 1)
                        {
                            
                            if (self.participants[i]["id"].stringValue != a["user"]["id"].stringValue )
                            {
                                var params: Parameters = [:]
                                
                                params = [
                                    "idSortie": act["data"].stringValue ,
                                    "idParticipant" : self.participants[i]["id"].stringValue
                                ]
                                
                                
                                print ("***********" , params)
                                
                                Alamofire.request(ScriptBase.sharedInstance.invitParticipant , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                                    .responseJSON { response in
                                        
                                        let b = JSON(response.data)
                                        print ("invite friends : " , response)
                                        if b["status"].boolValue {
                                            if ( b["message"].stringValue == "user already participating")
                                            {
                                                _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("alreadyUser"), style: AlertStyle.error)
                                                return
                                            }
                                            else {
                                                
                                                let ab = UserDefaults.standard.value(forKey: "User") as! String
                                                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                                                var a = JSON(data: dataFromString!)
                                                let header: HTTPHeaders = [
                                                    "Content-Type" : "application/json",
                                                    "x-Auth-Token" : a["value"].stringValue
                                                ]
                                                
                                                let params: Parameters = [
                                                    "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " vous a invité a une sortie" ,
                                                    "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " invited you to join hungout" ,
                                                    "userids" : ["\(self.participants[i]["id"].stringValue)"] ,
                                                    "type" : "rdv" ,
                                                    "titre" : "Invitation" ,
                                                    "photo" : "default" ,
                                                    "activite" : act["data"]["id"].stringValue ,
                                                    "sender" : a["user"]["id"].stringValue ,
                                                    "rdv" : point
                                                ]
                                                group.enter()
                                                Alamofire.request(ScriptBase.sharedInstance.creeNotif , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                                                    .responseJSON { response in
                                                        
                                                        if i == (self.participants.count - 1)
                                                        {
                                                            print ("haddar:",response)
                                                            /*  _ = SweetAlert().showAlert(Localization("addSortie"), subTitle: Localization("descAddSortie"), style: AlertStyle.success)
                                                             let tabBarViewControllers = self.tabBarController?.viewControllers
                                                             let adController = tabBarViewControllers![1] as! UINavigationController
                                                             
                                                             let adminTVC = adController.viewControllers[0] as! SortiesViewController
                                                             
                                                             _ = adminTVC.navigationController?.popToRootViewController(animated: false) */
                                                            //    DispatchQueue.main.async(execute: {
                                                            //       NotificationCenter.default.post(name: NSNotification.Name(rawValue: "POPUP"), object: nil )
                                                            // })
                                                            
                                                        }
                                                        
                                                        group.leave()
                                                }
                                                
                                            }
                                            
                                        }else{
                                            
                                            _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("erreur"), style: AlertStyle.error)
                                            
                                        }
                                        
                                        
                                        
                                }
                                
                                // group.wait()
                            }
                            group.notify(queue: DispatchQueue.main, execute: {
                                print("jajajaja")
                                DispatchQueue.main.async(execute: {
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "POPUP"), object: nil )
                                })
                            })
                            
                        }
                        
                    }
                    else {
                        _ = SweetAlert().showAlert(Localization("addSortie"), subTitle: Localization("descAddSortie"), style: AlertStyle.success)
                        let tabBarViewControllers = self.tabBarController?.viewControllers
                        let adController = tabBarViewControllers![1] as! UINavigationController
                        
                        let adminTVC = adController.viewControllers[0] as! SortiesViewController
                        
                        _ = adminTVC.navigationController?.popToRootViewController(animated: false)
                    }
                }else{
                    
                    _ = SweetAlert().showAlert(Localization("addSortie"), subTitle: Localization("erreur"), style: AlertStyle.error)
                    
                }
                
                
        }
    }
    /*
     *share action
     *
     */
    @IBAction func shareAction(_ sender: Any) {
    
    }
    /*
     * return UILabel height
     *
     */
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    /*
     *show more/less action
     *
     */
    @IBAction func expandCollapseDescription(_ sender: Any) {
        
        if plusBtn.title(for: .normal) == Localization("plusP") {
            descriptionContainerHeight.constant -= self.descriptionLBL.frame.height
            theWholeContainerHeight.constant -= self.descriptionLBL.frame.height
            descriptionContainerHeight.constant += heightForView(text: self.descriptionLBL.text!, font: self.descriptionLBL.font, width: self.descriptionLBL.frame.width)
            descriptionLBL.numberOfLines = 0
            print(theWholeContainerHeight.constant)
            
            theWholeContainerHeight.constant =  descriptionContainerHeight.constant + 48.5
            
            plusBtn.setTitle(Localization("moinsP"), for: .normal)
        }else {
            descriptionContainerHeight.constant = 433.0
            theWholeContainerHeight.constant = 433.0
            descriptionLBL.numberOfLines = 3
            plusBtn.setTitle(Localization("plusP"), for: .normal)
        }
   
    }
    /*
     *back action
     *
     */
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /////////********language"**********///////
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titreNavBarLBL.text = Localization("FicheLeconTitre")
        
        validerBTN.setTitle(Localization("BtnValiderSortie"), for: .normal)
        
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
    
    
    
    
}







