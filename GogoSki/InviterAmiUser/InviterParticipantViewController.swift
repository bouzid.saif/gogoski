//
//  InviterParticipantViewController.swift
//  GogoSki
//
//  Created by Ahmed Haddar on 13/12/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class InviterParticipantViewController: InternetCheckViewControllers,UITableViewDelegate,UITableViewDataSource {
    /*
     * variable contains your favorites
     */
    var Favoris : JSON = []
    /*
     * variable contains the activity passed from other page
     */
    var sortie : JSON = []
    /*
     * variable contains wich is the page that passed parametres
     */
    var chkoun = ""
    /*
     * text appears when there is no favorites
     */
    @IBOutlet weak var default_Inviter: UILabel!
    /*
     * title of this page
     */
    @IBOutlet weak var inviter_title: UILabel!
    /*
     * invite button
     */
    @IBOutlet weak var inviter_btn: UIButton!
    /*
     * the index of user selected
     */
    var indexSelected = 0
    /*
     * indexPath of the user selected
     */
    var indexPathSelected = IndexPath()
    /*
     * test if there is just one cell selected
     */
    var selected  = true
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Favoris.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let labelNom : UILabel = cell.viewWithTag(1) as! UILabel
        let imgUser : UIImageView = cell.viewWithTag(2) as! UIImageView
        labelNom.text = self.Favoris[indexPath.row]["prenom"].stringValue + " " + self.Favoris[indexPath.row]["nom"].stringValue
        if let urlImgUser = URL(string : self.Favoris[indexPath.row]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imgUser.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (selected)
        {
            let cell = tableView.cellForRow(at: indexPath)
            cell?.accessoryType = UITableViewCellAccessoryType.checkmark
            indexSelected = indexPath.row
            indexPathSelected = indexPath
            selected = false
        }
        else {
            let cell1 = tableView.cellForRow(at: indexPathSelected)
            cell1?.accessoryType = UITableViewCellAccessoryType.none
            tableView.deselectRow(at: indexPathSelected, animated: true)
            indexSelected = indexPath.row
            indexPathSelected = indexPath
            let cell = tableView.cellForRow(at: indexPath)
            cell?.accessoryType = UITableViewCellAccessoryType.checkmark
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.none
        indexSelected = 0
        indexPathSelected = []
        selected = true
    }
    /*
     * favorite tableview
     */
    @IBOutlet weak var table : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        table.allowsMultipleSelection = false
        getFavoris()
        print ("sortieeeeee " , self.sortie)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     * get all my favorites
     */
    func getFavoris ()
    {
        
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.afficherFavoris , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                
                let b = JSON(response.data)
                
                
                if b["status"].boolValue {
                    if (b["message"].stringValue == "List vide")
                    {
                        print ("fera8")
                        self.default_Inviter.isHidden = false
                        self.table.isHidden = true
                        self.Favoris = []
                    }
                    else {
                        self.default_Inviter.isHidden = true
                        self.table.isHidden = false
                        self.Favoris = b["data"]
                        self.table.reloadData()
                        
                    }
                    
                }
                else {
                    _ = SweetAlert().showAlert("Liste Favoris", subTitle: Localization("erreur"), style: AlertStyle.error)
                }
        }
    }
    /*
     * back action
     */
    @IBAction func go_back(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     * invite a user to the activity and send notification
     */
    @IBAction func invitAmi(_ sender: Any) {
        if (!selected)
        {
            
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : a["value"].stringValue
            ]
            var params: Parameters = [:]
            if chkoun == "lesson"
            {
                params = [
                    "idLecon": self.sortie["id"].stringValue ,
                    "idParticipant" : self.Favoris[indexSelected]["id"].stringValue
                ]
            }
            else if chkoun != "" {
                params = [
                    "idSortie": self.sortie["id"].stringValue ,
                    "idParticipant" : self.Favoris[indexSelected]["id"].stringValue
                ]
            }
            else {
                params = [
                    "idSortie": self.sortie["sortie"]["id"].stringValue ,
                    "idParticipant" : self.Favoris[indexSelected]["id"].stringValue
                ]
            }
            print ("***********" , params)
            if chkoun == "lesson"
            {
                Alamofire.request(ScriptBase.sharedInstance.invitLesson , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                    .responseString { response in
                        
                        let b = JSON(response.data)
                        print ("invite friends : " , response)
                        if b["status"].boolValue {
                            if ( b["message"].stringValue == "user already participating")
                            {
                                _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("alreadyUser"), style: AlertStyle.error)
                            }
                            else {
                                _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("invit"), style: AlertStyle.success , buttonTitle: "OK" , action: { (otherButton) in
                                    let ab = UserDefaults.standard.value(forKey: "User") as! String
                                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                                    var a = JSON(data: dataFromString!)
                                    let header: HTTPHeaders = [
                                        "Content-Type" : "application/json",
                                        "x-Auth-Token" : a["value"].stringValue
                                    ]
                                    var params: Parameters = [:]
                                    
                                    params = [
                                        "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " vous a invité a un leçon" ,
                                        "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " invited you to join lesson" ,
                                        "userids" : ["\(self.Favoris[self.indexSelected]["id"].stringValue)"] ,
                                        "type" : "rdv" ,
                                        "titre" : "Invitation" ,
                                        "photo" : "default" ,
                                        "activite" : self.sortie["id"].stringValue ,
                                        "sender" : a["user"]["id"].stringValue ,
                                        "rdv" : self.sortie["prestation"]["prestataire"]["point"]["id"].stringValue
                                    ]
                                    
                                    Alamofire.request(ScriptBase.sharedInstance.creeNotif , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                                        .responseJSON { response in
                                            print (response)
                                    }
                                    self.dismiss(animated: true, completion: nil)
                                })
                            }
                            
                            
                        }else{
                            
                            _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("erreur"), style: AlertStyle.error)
                            
                        }
                        
                        
                }
            }
            else {
                Alamofire.request(ScriptBase.sharedInstance.invitParticipant , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                    .responseString { response in
                        
                        let b = JSON(response.data)
                        print ("invite friends : " , response)
                        if b["status"].boolValue {
                            if ( b["message"].stringValue == "user already participating")
                            {
                                _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("alreadyUser"), style: AlertStyle.error)
                            }
                            else {
                                _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("invit"), style: AlertStyle.success , buttonTitle: "OK" , action: { (otherButton) in
                                    let ab = UserDefaults.standard.value(forKey: "User") as! String
                                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                                    var a = JSON(data: dataFromString!)
                                    let header: HTTPHeaders = [
                                        "Content-Type" : "application/json",
                                        "x-Auth-Token" : a["value"].stringValue
                                    ]
                                    var params: Parameters = [:]
                                    if self.chkoun == "" {
                                        
                                        
                                        params = [
                                            "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " vous a invité a une sortie" ,
                                            "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " invited you to join hungout" ,
                                            "userids" : ["\(self.Favoris[self.indexSelected]["id"].stringValue)"] ,
                                            "type" : "rdv" ,
                                            "titre" : "Invitation" ,
                                            "photo" : "default" ,
                                            "activite" : self.sortie["id"].stringValue ,
                                            "sender" : a["user"]["id"].stringValue ,
                                            "rdv" : self.sortie["point"]["id"].stringValue
                                        ]
                                    }
                                    else {
                                        params = [
                                            "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " vous a invité a une sortie" ,
                                            "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " invited you to join hungout" ,
                                            "userids" : ["\(self.Favoris[self.indexSelected]["id"].stringValue)"] ,
                                            "type" : "rdv" ,
                                            "titre" : "Invitation" ,
                                            "photo" : "default" ,
                                            "activite" : self.sortie["sortie"]["id"].stringValue ,
                                            "sender" : a["user"]["id"].stringValue ,
                                            "rdv" : self.sortie["sortie"]["point"]["id"].stringValue
                                        ]
                                    }
                                    Alamofire.request(ScriptBase.sharedInstance.creeNotif , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                                        .responseJSON { response in
                                            print (response)
                                    }
                                    self.dismiss(animated: true, completion: nil)
                                })
                            }
                            
                            
                        }else{
                            
                            _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("erreur"), style: AlertStyle.error)
                            
                        }
                        
                        
                }
                
            }
        }
        
    }
    
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        
        default_Inviter.text = Localization("default_Inviter")
        
        //  Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
}

