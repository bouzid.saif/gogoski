//
//  InvitationRdvReçuViewController.swift
//  GogoSki
//
//  Created by Ahmed Haddar on 21/12/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import MapKit

class InvitationRdvRec_uViewController: InternetCheckViewControllers ,MKMapViewDelegate{
    
    /*
     * label of type of invite
     */
    @IBOutlet weak var titleSortie: UILabel!
    /*
     * sport of the sender
     */
    @IBOutlet weak var pratiqueSport: UILabel!
    /*
     * the map in this view
     */
    @IBOutlet weak var map: MKMapView!
    /*
     * picture of the sender
     */
    @IBOutlet weak var photoUser: RoundedUIImageView!
    /*
     * picture of the station
     */
    @IBOutlet weak var photoStation: RoundedUIImageView!
    /*
     * level of the sender
     */
    @IBOutlet weak var niveauSport: UILabel!
    /*
     * name of the sender
     */
    @IBOutlet weak var nomUser: UILabel!
    /*
     * title of this page
     */
    @IBOutlet weak var titre: UILabel!
    /*
     * the bottom view witch contains the accept and refuse buttons
     */
    @IBOutlet weak var validerAnnulerView: UIView!
    /*
     * the whole view
     */
    @IBOutlet weak var invitation: UIView!
    /*
     * accept button
     */
    @IBOutlet weak var btnAccepter: UIButton!
    /*
     * refuse button
     */
    @IBOutlet weak var btnRefuser: UIButton!
    /*
     * user picture that you can click to go to profil
     */
    @IBOutlet weak var imageToProfileNavBar: RoundedUIImageView!
    /*
     * massif + department + station label
     */
    @IBOutlet weak var NomSortie: UILabel!
    /*
     * variable of the sender
     */
    var user : JSON = []
    /*
     * latitude of the appointment
     */
    var latitude  = ""
    /*
     * longitude of the appointment
     */
    var longitiude = ""
    /*
     * variable contains the id of the appointment
     */
    var idRdv = ""
    /*
     * massif of the appointment
     */
    var massif = ""
    /*
     * department of the appointment
     */
    var departement = ""
    /*
     * station of the appointment
     */
    var station = ""
    /*
     * establishment of the appointment
     */
    var nom_etab = ""
    var photo = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        var q = ""
        if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr"
        {
            titleSortie.text = "invited you to an appointment."
            q = "Establishment name : " + nom_etab + "\n"
           
        }
        else {
            titleSortie.text = "Vous propose un rendez-vous."
            q = "Nom d'établissement : " + nom_etab + "\n"
            
        }
        NomSortie.text = q
        invitation.layer.cornerRadius = 40
        invitation.layer.masksToBounds = false
        invitation.layer.shadowColor = UIColor.black.cgColor
        invitation.layer.shadowOpacity = 0.8
        invitation.layer.shadowOffset = CGSize(width: 0, height: 0)
        invitation.layer.shadowRadius = 2
        
        
        nomUser.text = self.user["prenom"].stringValue + " " + self.user["nom"].stringValue
        pratiqueSport.text = self.user["sports"][0]["pratique"].stringValue
        niveauSport.text = " - " + self.user["sports"][0]["niveau"].stringValue
        if ( self.user["sports"][0]["niveau"].stringValue == "Confirmé" )
        {
            niveauSport.textColor = UIColor("#FF3E53")
        }
        else if (self.user["sports"][0]["niveau"].stringValue == "Débutant")
        {
            niveauSport.textColor = UIColor("#76EC9E")
        }
        else if (self.user["sports"][0]["niveau"].stringValue == "Intermédiaire")
        {
            niveauSport.textColor = UIColor("#75A7FF")
        }
        else if (self.user["sports"][0]["niveau"].stringValue == "Expert")
        {
            niveauSport.textColor = UIColor("#D4D4D4")
        }
        else {
            niveauSport.textColor = UIColor("#76EC9E")
        }
        if let urlImgUser = URL(string : self.user["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            photoUser.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if let urlImgUser = URL(string : a["user"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imageToProfileNavBar.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        
        map.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDisconnectedUserUpdateNotification(notification:)), name: NSNotification.Name(rawValue: "PinView"), object: nil)
        let Report = ImageAnnotation()
        longitiude = String(longitiude.dropFirst())
        
        Report.coordinate = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!)
        Report.image1 = "default2"
        if photo != "" {
            Report.image1 = photo
        }
        StaticImageAnnotationRDV.sharedInstance.add(im:Report)
        map.addAnnotation(Report)
        for annotation in map.annotations {
            
            if annotation.isKind(of: MKPointAnnotation.self) {
                let pinView : ImageAnnotationView = map.view(for: annotation) as! ImageAnnotationView
                formatAnnotationView(pinView, for: map)
                
            }
        }
        map.zoomIn(coordinate: CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!))
        configureTileOverlay()
        // Do any additional setup after loading the view.
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let tileOverlay = overlay as? MKTileOverlay {
            return MKTileOverlayRenderer(tileOverlay: tileOverlay)
        } else {
            return MKOverlayRenderer(overlay: overlay)
        }
    }
    private func configureTileOverlay() {
        // We first need to have the path of the overlay configuration JSON
        guard let overlayFileURLString = Bundle.main.path(forResource: "Overlay", ofType: "json") else {
            return
        }
        let overlayFileURL = URL(fileURLWithPath: overlayFileURLString)
        
        // After that, you can create the tile overlay using MapKitGoogleStyler
        guard let tileOverlay = try? MapKitGoogleStyler.buildOverlay(with: overlayFileURL) else {
            return
        }
        
        // And finally add it to your MKMapView
        map.add(tileOverlay)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     * back action
     */
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("InvitationTitre")
        btnAccepter.setTitle(Localization("BtnAccepter"), for: .normal)
        btnRefuser.setTitle(Localization("BtnRefuser"), for: .normal)
        titleSortie.text = Localization("proposeRdv")
        //Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    
    /*
     * refuse the appointment action
     */
    @IBAction func refuserAction(_ sender: Any) {
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        
        let params: Parameters = [
            "id": self.idRdv ,
            "respond" : false
        ]
        print ("***********" , params)
        Alamofire.request(ScriptBase.sharedInstance.repondreRdv , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                
                let b = JSON(response.data)
                print ("Refus friends : " , response)
                if b["status"].boolValue {
                    
                    _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("invit"), style: AlertStyle.success , buttonTitle: "OK" , action: { (otherButton) in
                        let ab = UserDefaults.standard.value(forKey: "User") as! String
                        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                        var a = JSON(data: dataFromString!)
                        let header: HTTPHeaders = [
                            "Content-Type" : "application/json",
                            "x-Auth-Token" : a["value"].stringValue
                        ]
                        
                        let params: Parameters = [
                            "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " a rejeté votre invitation" ,
                            "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " rejected your invitation" ,
                            "userids" : ["\(self.user["id"].stringValue)"] ,
                            "type" : "RejectRdv" ,
                            "titre" : "Invitation" ,
                            "photo" : "default" ,
                            "activite" : "" ,
                            "sender" : a["user"]["id"].stringValue ,
                            "rdv" : self.idRdv
                        ]
                        Alamofire.request(ScriptBase.sharedInstance.creeNotif , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                            .responseJSON { response in
                                print (response)
                        }
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    
                    
                }else{
                    
                    _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("erreur"), style: AlertStyle.error)
                    
                }
                
                
        }
    }
    /*
     * accept the appointment action
     */
    @IBAction func accepterAction(_ sender: Any) {
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        
        let params: Parameters = [
            "id": self.idRdv ,
            "respond" : true
        ]
        print ("***********" , params)
        Alamofire.request(ScriptBase.sharedInstance.repondreRdv , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseString { response in
                
                let b = JSON(response.data)
                print ("Accept friends : " , response)
                if b["status"].boolValue {
                    
                    _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("invit"), style: AlertStyle.success , buttonTitle: "OK" , action: { (otherButton) in
                        let ab = UserDefaults.standard.value(forKey: "User") as! String
                        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                        var a = JSON(data: dataFromString!)
                        let header: HTTPHeaders = [
                            "Content-Type" : "application/json",
                            "x-Auth-Token" : a["value"].stringValue
                        ]
                        
                        let params: Parameters = [
                            "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " a accepté votre invitation" ,
                            "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " accepted your invitation" ,
                            "userids" : ["\(self.user["id"].stringValue)"] ,
                            "type" : "AcceptRdv" ,
                            "titre" : "Invitation" ,
                            "photo" : "default" ,
                            "activite" : "" ,
                            "sender" : a["user"]["id"].stringValue ,
                            "rdv" : self.idRdv
                        ]
                        Alamofire.request(ScriptBase.sharedInstance.creeNotif , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                            .responseJSON { response in
                                print (response)
                        }
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    
                    
                }else{
                    
                    _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("erreur"), style: AlertStyle.error)
                    
                }
                
                
        }
    }
    /*
     * get the blue pin that you can click on a station to accept or refuse
     */
    @objc func handleDisconnectedUserUpdateNotification(notification: NSNotification){
        let coord = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!)
        let annot = Artwork(title: "", locationName: "", discipline: "", coordinate: coord,color: UIColor.blue)
        
        self.map.addAnnotation(annot)
        // self.validerAnnulerView.isHidden = false
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        
        
        if annotation.isKind(of: MKUserLocation.self){
            return nil
        }else if annotation is ImageAnnotation {
            reuseId = "Pin"
            var pinView = map.dequeueReusableAnnotationView(withIdentifier: reuseId) as? ImageAnnotationViewRDV
            
            
            pinView = ImageAnnotationViewRDV(annotation: annotation, reuseIdentifier: reuseId)
            
            for x in StaticImageAnnotationRDV.sharedInstance.AImages {
                if isEqual(withCoordinate: x.coordinate,withAnotherCoordinate: annotation.coordinate) {
                    pinView?.image1 = x.image1
                    pinView?.setimageview()
                }
            }
            
            
            
            
            return pinView
            
        }else {
            reuseId = "Pin2"
            let saifPin =  MKAnnotationView(annotation: annotation, reuseIdentifier: "PINA")
            saifPin.isEnabled = true
            saifPin.canShowCallout = true
            
            saifPin.image = UIImage(named: "Pin_saif")
            
            // saifPin.tintColor = MKAnnotationView.blueColor()
            // saifPin.tintAdjustmentMode = .normal
            return saifPin
        }
        
    }
    /*
     * function that return true if 2 locations are the same
     */
    func isEqual(withCoordinate location1: CLLocationCoordinate2D, withAnotherCoordinate location2: CLLocationCoordinate2D) -> Bool {
        let locationString1 = "\(location1.latitude), \(location1.longitude)"
        let locationString2 = "\(location2.latitude), \(location2.longitude)"
        if (locationString1 == locationString2) {
            return true
        }
        else {
            return false
        }
    }
    /*
     * resize image when zoom the map
     */
    func formatAnnotationView(_ pinView: ImageAnnotationView, for aMapView: MKMapView) {
        if pinView != nil {
            
            let zoomLevel: Double = aMapView.zoomLevel
            let scale = Double(-1 * sqrt(Double(1 - pow((zoomLevel / 20.0), 2.0))) + 1.1)
            // This is a circular scale function where at zoom level 0 scale is 0.1 and at zoom level 20 scale is 1.1
            // Option #1
            pinView.transform = CGAffineTransform(scaleX: CGFloat(scale), y: CGFloat(scale))
            // Option #2
            
            
        }
    }
}




