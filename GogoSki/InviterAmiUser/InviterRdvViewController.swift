//
//  InviterRdvViewController.swift
//  GogoSki
//
//  Created by Ahmed Haddar on 21/12/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit
extension InviterRdvViewController : FBClusteringManagerDelegate {
    func cellSizeFactor(forCoordinator coordinator: FBClusteringManager) -> CGFloat {
        return 1.0
    }
}
class InviterRdvViewController: InternetCheckViewControllers ,MKMapViewDelegate{
    
    /*
     * map of all station
     */
    @IBOutlet weak var map: MKMapView!
    /*
     * validate button
     */
    @IBOutlet weak var btnValider: UIButton!
    /*
     * cancel button
     */
    @IBOutlet weak var btnAnnuler: UIButton!
    /*
     * title of this page
     */
    @IBOutlet weak var titre: UILabel!
    /*
     * massif + department + station label
     */
    @IBOutlet weak var nomStation: UILabel!
    /*
     * the bottom view witch contains validate and cancel button
     */
    @IBOutlet weak var validerAnnulerView: UIView!
    /*
     * latitude of the station selected
     */
    var latitude  = ""
    /*
     * initialize the cluster
     */
    let clusteringManger = FBClusteringManager()
    /*
     * longitude of the station selected
     */
    var longitiude = ""
    /*
     * massif of the station selected
     */
    var massif = ""
    /*
     * department of the station selected
     */
    var dept = ""
    /*
     * name station of the station selected
     */
    var station = ""
    /*
     * variable contains the id of the user who you will invite him to the appointment
     */
    var idUser = ""
    /*
     * variable contains the id of the station selected
     */
    var idRdv = ""
    /*
     * variable contains all the stations
     */
    var stations : JSON = []
    override func viewDidLoad() {
        super.viewDidLoad()
        map.delegate = self
        validerAnnulerView.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDisconnectedUserUpdateNotification(notification:)), name: NSNotification.Name(rawValue: "PinView"), object: nil)
        // print ( self.stations)
        
        var arry: [FBAnnotation] = []
        var ReportAnnotation = FBAnnotation()
        for i in 0 ... ((stations.arrayObject?.count)! - 1)
        {
            
            
            if (self.stations[i]["listpointsrdv"].count != 0 )
            {
                for j in 0 ... self.stations[i]["listpointsrdv"].count - 1
                {
                latitude = self.stations[i]["listpointsrdv"][j]["latitude"].stringValue
                    
                longitiude = self.stations[i]["listpointsrdv"][j]["longitude"].stringValue
        
                    if (longitiude.prefix(1) == " " )
                    {
                        longitiude = String(longitiude.dropFirst())
                    }
                if (latitude.suffix(1) == " " )
                {
                    latitude = String(latitude.dropLast())
                }
            
                   
                let Report = ImageAnnotation()
                Report.coordinate = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!)
                Report.title = self.stations[i]["nom"].stringValue
                Report.subtitle = String(self.stations[i]["listpointsrdv"][j]["id"].intValue)
                Report.image1 = "default2"
                StaticImageAnnotationRDVInvit.sharedInstance.add(im:Report)
                
                //map.addAnnotation(Report)
                ReportAnnotation = FBAnnotation()
                var q = ""
                if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr"
                {
                    q = "Establishment name : " + self.stations[i]["listpointsrdv"][j]["prestataire"]["nom_etablissement"].stringValue + "\n"
                   
                }
                else {
                    q = "Nom d'établissement : " + self.stations[i]["listpointsrdv"][j]["prestataire"]["nom_etablissement"].stringValue + "\n"
                    
                }
                ReportAnnotation.title = q
                ReportAnnotation.subtitle = String(self.stations[i]["listpointsrdv"][j]["id"].intValue)
                
                ReportAnnotation.coordinate = Report.coordinate
                arry.append(ReportAnnotation)
               
                self.clusteringManger.removeAll()
                self.clusteringManger.add(annotations: arry)
                self.clusteringManger.delegate = self
                self.refreshPosition()
                //       }
                     }
            }
            
            
        }
        // arry = []
        
        //  map.zoomIn(coordinate: CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!))
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
          configureTileOverlay()
        // Do any additional setup after loading the view.
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let tileOverlay = overlay as? MKTileOverlay {
            return MKTileOverlayRenderer(tileOverlay: tileOverlay)
        } else {
            return MKOverlayRenderer(overlay: overlay)
        }
    }
    /*
     * function of clustering
     */
    func refreshPosition (){
        DispatchQueue.main.async(execute:  {
            let mapBoundsWidth = Double(self.map.bounds.size.width)
            let mapRectWidth = self.map.visibleMapRect.size.width
            let scale = mapBoundsWidth / mapRectWidth
            
            let annotationArray = self.clusteringManger.clusteredAnnotations(withinMapRect: self.map.visibleMapRect, zoomScale:scale)
            
            DispatchQueue.main.async {
                self.clusteringManger.display(annotations: annotationArray, onMapView:self.map)
            }
        } )
        for annotation in map.annotations {
            
            /*  if annotation.isKind(of: MKUserLocation.self){
             return
             }*/
            if annotation.isKind(of: ImageAnnotation.self) {
                let pinView : ImageAnnotationViewRDVInvit = map.view(for: annotation) as! ImageAnnotationViewRDVInvit
                formatAnnotationView(pinView, for: map)
                
            }
        }
       
    }
    private func configureTileOverlay() {
        // We first need to have the path of the overlay configuration JSON
        guard let overlayFileURLString = Bundle.main.path(forResource: "Overlay", ofType: "json") else {
            return
        }
        let overlayFileURL = URL(fileURLWithPath: overlayFileURLString)
        
        // After that, you can create the tile overlay using MapKitGoogleStyler
        guard let tileOverlay = try? MapKitGoogleStyler.buildOverlay(with: overlayFileURL) else {
            return
        }
        
        // And finally add it to your MKMapView
        map.add(tileOverlay)
    }
    /*
     * get the blue pin that you can click on a station to validate or cancel
     */
    @objc func handleDisconnectedUserUpdateNotification(notification: NSNotification){
        let coord = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!)
        let annot = Artwork(title: "", locationName: "", discipline: "", coordinate: coord,color: UIColor.blue)
        
        self.map.addAnnotation(annot)
        self.validerAnnulerView.isHidden = false
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        if annotation is FBAnnotationCluster {
            reuseId = "Cluster"
            if annotation.isKind(of: MKUserLocation.self){
                return nil
            }else{
                var clusterView = map.dequeueReusableAnnotationView(withIdentifier: reuseId)
                if clusterView == nil {
                    clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId, configuration: FBAnnotationClusterViewConfiguration.default())
                } else {
                    clusterView?.annotation = annotation
                }
                return clusterView
            }
        } else {
            reuseId = "Pin"
            if annotation.isKind(of: MKUserLocation.self){
                return nil
            }else{
                var pinView = map.dequeueReusableAnnotationView(withIdentifier: reuseId) as? ImageAnnotationViewRDVInvit
                
                let zoomlevel = mapView.zoomLevel
                let scale = 0.06263158 * zoomlevel
                
                
                pinView = ImageAnnotationViewRDVInvit(annotation: annotation, reuseIdentifier: reuseId)
                pinView?.transform = CGAffineTransform(scaleX: CGFloat(scale), y: CGFloat(scale))
                /*  switch now_image  {
                 case 0 : pinView?.image = UIImage(named:"exemple")
                 break
                 case  1 : pinView?.image = UIImage(named:"aymen")
                 break
                 default : pinView?.image = UIImage(named:"haddar")
                 break
                 } */
                for x in StaticImageAnnotationRDVInvit.sharedInstance.AImages {
                    if isEqual(withCoordinate: x.coordinate,withAnotherCoordinate: annotation.coordinate) {
                        pinView?.image1 = x.image1
                        pinView?.setimageview()
                    }
                }
                
                
                
                
                return pinView
            }
        }
    }
    /*
     * function that return true if 2 locations are the same
     */
    func isEqual(withCoordinate location1: CLLocationCoordinate2D, withAnotherCoordinate location2: CLLocationCoordinate2D) -> Bool {
        let locationString1 = "\(location1.latitude), \(location1.longitude)"
        let locationString2 = "\(location2.latitude), \(location2.longitude)"
        if (locationString1 == locationString2) {
            return true
        }
        else {
            return false
        }
    }
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        DispatchQueue.main.async(execute: {
            let mapBoundsWidth = Double(self.map.bounds.size.width)
            let mapRectWidth = self.map.visibleMapRect.size.width
            let scale = mapBoundsWidth / mapRectWidth
            
            let annotationArray = self.clusteringManger.clusteredAnnotations(withinMapRect: self.map.visibleMapRect, zoomScale:scale)
            /////kisou bch
            
            
            
            
            DispatchQueue.main.async {
                self.clusteringManger.display(annotations: annotationArray, onMapView:self.map)
            }
        } )
        for annotation in map.annotations {
            
            if annotation.isKind(of: MKUserLocation.self){
                return
            }
            if annotation.isKind(of: ImageAnnotation.self) {
                let pinView : ImageAnnotationViewRDVInvit = map.view(for: annotation) as! ImageAnnotationViewRDVInvit
                let mapRectWidth = self.map.visibleMapRect.size.width
                let zoomFactor = Int(log2(mapRectWidth)) - 9
                //  pinView.bounds.size.width =  pinView.frame.width / CGFloat(zoomFactor)
                //  pinView.bounds.size.height =  pinView.frame.height / CGFloat(zoomFactor)
                formatAnnotationView(pinView, for: map)
                
            }
        }
        
    }
    /*
     * resize image when zoom the map
     */
    func formatAnnotationView(_ pinView: ImageAnnotationViewRDVInvit, for aMapView: MKMapView) {
        if pinView != nil {
            
            let zoomLevel: Double = aMapView.zoomLevel
            let scale = Double(-1 * sqrt(Double(1 - pow((zoomLevel / 20.0), 2.0))) + 1.1)
            // This is a circular scale function where at zoom level 0 scale is 0.1 and at zoom level 20 scale is 1.1
            // Option #1
            //pinView.transform = CGAffineTransform(scaleX: CGFloat(scale), y: CGFloat(scale))
            // Option #2
            let pinImage = UIImage(named: "exemple")
            let pinImage2 = UIImage(named: "skii")
            /* pinView.image = resizeImage(image: pinImage!,targetSize: CGSize(width: Double((pinImage?.size.width)!) * scale, height:  Double((pinImage?.size.height)!) * scale)) */
            /*pinView.image2 = resizeImage(image: pinImage2!,targetSize: CGSize(width: Double(((pinImage?.size.width)! / 3)) * scale, height:  Double(((pinImage?.size.height)! / 3)) * scale)) */
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     * back action
     */
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("InviterMembreTitre")
        btnValider.setTitle(Localization("btnValider"), for: .normal)
        btnAnnuler.setTitle(Localization("BtnAnnuler"), for: .normal)
        
        //  Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /*
     * send invitation of appointment to the user
     */
    @IBAction func validerInvtation(_ sender: Any) {
        
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        let dateLioum = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(identifier : "GMT+1")
        let dateString = dateFormatter.string(from: dateLioum)
        let params: Parameters = [
            "sender": a["user"]["id"].stringValue ,
            "receiver" : self.idUser ,
            "point" : self.idRdv ,
            "daterdv" : dateString
        ]
        print ("***********" , params)
        Alamofire.request(ScriptBase.sharedInstance.sendRdv , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                
                let b = JSON(response.data)
                print ("invite friends : " , response)
                if b["status"].boolValue {
                    
                    _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("invit"), style: AlertStyle.success , buttonTitle: "OK" , action: { (otherButton) in
                        let ab = UserDefaults.standard.value(forKey: "User") as! String
                        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                        var a = JSON(data: dataFromString!)
                        let header: HTTPHeaders = [
                            "Content-Type" : "application/json",
                            "x-Auth-Token" : a["value"].stringValue
                        ]
                        
                        let params: Parameters = [
                            "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " vous a invité a un rdv" ,
                            "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " invited you to an Appointment" ,
                            "userids" : ["\(self.idUser)"] ,
                            "type" : "rdv" ,
                            "titre" : "Invitation" ,
                            "photo" : "default" ,
                            "activite" : "" ,
                            "sender" : a["user"]["id"].stringValue ,
                            "rdv" : self.idRdv
                        ]
                        Alamofire.request(ScriptBase.sharedInstance.creeNotif , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                            .responseJSON { response in
                                print (response)
                        }
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    
                    
                }else{
                    
                    _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("erreur"), style: AlertStyle.error)
                    
                }
                
                
        }
        
    }
    /*
     * Cancel invitation
     *
     */
    @IBAction func annulerInvitation(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let ViewAnnot = view as? ImageAnnotationViewRDVInvit {
            print ("annot    : " , ViewAnnot.annotation?.title , " " , ViewAnnot.annotation?.subtitle)
            self.nomStation.text = ViewAnnot.annotation!.title!
            self.idRdv = ViewAnnot.annotation!.subtitle as! String
        }
    }
}

