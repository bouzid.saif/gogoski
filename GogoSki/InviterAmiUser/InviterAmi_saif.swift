//
//  InviterAmi_saif.swift
//  GogoSki
//
//  Created by Bouzid saif on 21/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import ContactsUI
import MessageUI
class InviterAmi_saif: InternetCheckViewControllers,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate {
    @IBOutlet weak var table : UITableView!
    @IBOutlet weak var titre: UILabel!
    @IBOutlet weak var labelMailtrouve: UILabel!
    @IBOutlet weak var nb_email : UILabel!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var nb_slect : UILabel!
    var contacts : [CNContact] = []
    var selectedContacts : [CNContact] = []
    var nb_temp =  0
    var tableData = Array<Bool>()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? UITableViewCell {
        
        let name = cell.viewWithTag(1) as! UILabel
        let image = cell.viewWithTag(2) as! UIButton
        let btn = cell.viewWithTag(3) as! UIButton
            let selectionFlag = tableData[indexPath.row]
            switch selectionFlag {
            case true : cell.accessoryType = UITableViewCellAccessoryType.checkmark
            case false : cell.accessoryType = UITableViewCellAccessoryType.none
            }
        name.text = self.contacts[indexPath.row].givenName + " " + self.contacts[indexPath.row].familyName
            if self.contacts[indexPath.row].givenName != "" {
                 image.setTitle("\(String(self.contacts[indexPath.row].givenName.first!).uppercased())", for: .normal)
            }else if self.contacts[indexPath.row].familyName != "" {
               image.setTitle("\(String(self.contacts[indexPath.row].familyName.first!).uppercased())", for: .normal)
            }else{
              image.setTitle("\(String((self.contacts[indexPath.row].emailAddresses[0].value as String).first!).uppercased())", for: .normal)
                name.text = self.contacts[indexPath.row].emailAddresses[0].value as String
            }
        
        return cell
        }else{
            return UITableViewCell()
        }
    }
    @IBAction func inviteFriends (_ sender : Any ) {
        print("count : " ,self.selectedContacts.count)
        let subject = "Invitation GOGOSKI"
        let body = "http://www.gogoski.fr/"
        var email = "mailto:("
        for a in self.selectedContacts {
            email = email + (a.emailAddresses[0].value as String)  + ";"
        }
        email.removeLast()
        email = email + ")?subject=\(subject)&body=\(body)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
       
        //let url = NSURL(string: "mailto:\(email)")
        // UIApplication.shared.openURL(url as! URL)
        if let url = URL(string: "mailto:\(email)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        if (cell?.accessoryType == UITableViewCellAccessoryType.checkmark) {
            cell!.accessoryType = UITableViewCellAccessoryType.none
            tableData[indexPath.row] = false
            tableView.deselectRow(at: indexPath, animated: true)
            if nb_temp == 1 {
            nb_temp = 0
            }else{
                nb_temp = nb_temp - 1
            }
            if nb_temp > 0 {
                btnInvite.isEnabled = true
            }else {
                btnInvite.isEnabled = false
            }
             nb_slect.text = "\(nb_temp)"
            for i in 0 ... self.selectedContacts.count - 1 {
                if self.selectedContacts[i].emailAddresses[0].value == self.contacts[indexPath.row].emailAddresses[0].value {
                    self.selectedContacts.remove(at: i)
                    return
                }
            }
           
        }else{
            cell!.accessoryType = UITableViewCellAccessoryType.checkmark
            tableData[indexPath.row] = true
            nb_temp = nb_temp + 1
            nb_slect.text = "\(nb_temp)"
            self.selectedContacts.append(self.contacts[indexPath.row])
            if nb_temp > 0 {
                btnInvite.isEnabled = true
            }else {
                btnInvite.isEnabled = false
            }
        }
       
    }
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        mailComposerVC.setToRecipients(["someone@somewhere.com"])
        mailComposerVC.setSubject("Sending you an in-app e-mail...")
        mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)
        
        return mailComposerVC
    }
    func showSendMailErrorAlert() {
       /* _ = SweetAlert.
        let alert = CDAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", type: .error)
        let doneAction = CDAlertViewAction(title: "Okay! 💪")
        alert.add(action: doneAction)
        DispatchQueue.main.async(execute: {
            alert.show()
        }) */
        
        
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    @IBAction func go_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        let store = CNContactStore()
        //CNContactPhoneNumbersKey, CNContactFamilyNameKey, CNContactGivenNameKey, CNContactNicknameKey, CNContactPhoneNumbersKey ,
        let keys = [ CNContactEmailAddressesKey,CNContactFamilyNameKey,CNContactGivenNameKey]
        let request1 = CNContactFetchRequest(keysToFetch: keys  as [CNKeyDescriptor])
        store.requestAccess(for: .contacts) { (isGranted, error) in
            // Check the isGranted flag and proceed if true
            if isGranted {
                try? store.enumerateContacts(with: request1) { (contact, error) in
                    
                    if !contact.emailAddresses.isEmpty {
                      print( "******************")
                        print(contact.givenName)
                        self.contacts.append(contact)
                        print("*****************")
                    }
                }
                print("done")
                DispatchQueue.main.async(execute: {
                    for a in self.contacts {
                        self.tableData.append(false)
                    }
                    self.table.reloadData()
                    self.nb_email.text = "\(self.contacts.count)"
                })
               
            }
        }
        btnInvite.isEnabled = false
        
        nb_temp = 0
        nb_slect.text = "0"
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("InviterAmiTitre")
        labelMailtrouve.text = Localization("labelMailtrouve")
        btnInvite.setTitle(Localization("btnInvite"), for: .normal)
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
}
