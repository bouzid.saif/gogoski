//
//  PageBienvenue.swift
//  GogoSki
//
//  Created by Bouzid saif on 28/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import EFInternetIndicator
class PageBienvenue: InternetCheckViewControllers {
    /*
     *description of the logo
     *
     */
    @IBOutlet weak var desc: UILabel!
    /*
     *space pro label
     *
     */
    @IBOutlet weak var espacePro: UILabel!
    /*
     *sign in button
     *
     */
    @IBOutlet weak var btnConnexion: UIButton!
    /*
     *sign up button
     *
     */
    @IBOutlet weak var btnInscription: UIButton!
    /*
     *flag language choosen
     *
     */
    @IBOutlet weak var btnFlag : UIButton!
    /*
     *prepare the language of the app
     *
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        let pre = Locale.preferredLanguages[0]
        print("Language:",pre)
        if Localisator.sharedInstance.currentLanguage != "French_fr" &&  Localisator.sharedInstance.currentLanguage != "English_en" {
        if String(pre.prefix(2)) == "fr" {
            SetLanguage("French_fr")
        }else if String(pre.prefix(2)) == "en" {
            SetLanguage("English_en")
        }else {
             SetLanguage("English_en")
        }
        }
       // self.startMonitoringInternet(backgroundColor:UIColor.blue, style: .statusLine, textColor:UIColor.white, message:"Merci de vérifier votre connexion à internet")
    }
    /*
     *remove language observer
     *
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    /*
     *configure texts
     *
     */
    func configureViewFromLocalisation() {
        espacePro.text = Localization("espacePro")
        espacePro.textAlignment = .right
        desc.text = Localization("DescriptionPageBienvenue")
        btnConnexion.setTitle(Localization("BtnConex"), for: .normal)
        btnInscription.setTitle(Localization("BtnInscri"), for: .normal)
        btnFlag.setBackgroundImage(UIImage(named: Localization("flag")), for: .normal)
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    /*
     *notification reveived of language
     *
     */
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /*
     *go to space pro view
     *
     */
    @IBAction func Go_to_prestataire(_ sender: Any) {
       // let nav = UIStoryboard(name: "Prestataire", bundle: nil).instantiateViewController(withIdentifier: "nav") as! UINavigationController
        let conect_prest = UIStoryboard(name: "Prestataire", bundle : nil).instantiateViewController(withIdentifier: "PageBienvenuePrest") as! HomaPrestataireViewController
        //conect_prest.navigationController = nav
        self.navigationController?.pushViewController(conect_prest, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
