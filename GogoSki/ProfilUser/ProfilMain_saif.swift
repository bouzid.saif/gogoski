//
//  ProfilMain_saif.swift
//  GogoSki
//
//  Created by Bouzid saif on 19/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import SJSegmentedScrollView
class ProfilMain : SJSegmentedViewController{
    
    /*
     * variable of the segment selected
     */
    var selectedSegment: SJSegmentTab?
    /*
     * informations tab constraint
     */
    var height_informations : CGFloat = 0
    /*
     * activity tab constraint
     */
    var height_Activity : CGFloat = 0
    /*
     * activity controller
     */
    let secondViewController =  mesActivites_saif.shared
    /*
     * information controller
     */
    let firstViewController = Informations_saif.shared
    /*
     * header controller
     */
    let headerController = ProfilSection.shared
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("willdis")
        //self.segmentedScrollViewColor
        segmentedScrollViewScroll = true
    }
    override func viewDidLoad() {
        /*
         * Reload all views
         */
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserConnectNow2(notification:)), name: NSNotification.Name(rawValue: "reloadView"), object: nil)
        /*
         * Reload scrollView
         */
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadScrollView(notification:)), name: NSNotification.Name(rawValue: "reloadScrollProfilMain"), object: nil)
        if let storyboard = self.storyboard {
            
            //let headerController = storyboard
            // .instantiateViewController(withIdentifier: "ProfileSection_saif") as! ProfilSection
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            firstViewController.title = Localization("LabelInformations")
            
            height_informations = firstViewController.view.frame.height
            
            secondViewController.title = Localization("LabelMesActivité")
            secondViewController.scrollenabled = false
            height_Activity = secondViewController.view.frame.height
            
            /*
             * initialize controllers
             */
            headerViewController = headerController
            segmentControllers = [firstViewController,
                                  secondViewController]
            
            /*
             * configure segmented controlers
             */
            headerViewHeight = 322
            selectedSegmentViewHeight = 5.0
            headerViewOffsetHeight = 31.0
            segmentTitleColor = .white
            selectedSegmentViewColor = UIColor(red: 99/255.0, green: 164/255, blue: 255/255, alpha: 1)
            segmentShadow = SJShadow.light()
            showsHorizontalScrollIndicator = false
            showsVerticalScrollIndicator = false
            segmentBackgroundColor = UIColor.white.withAlphaComponent(0.3)
            segmentViewHeight = 50.0
            
            
            delegate = self
            self.segmentBounces = false
            
        }
        
        
        title = "Segment"
        /*
         * configure language with notification if language changes
         */
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureViewFromLocalisation()
       
        headerViewHeight = 322
        selectedSegmentViewHeight = 5.0
        headerViewOffsetHeight = 31.0
         segmentViewHeight = 50.0
         height_informations = firstViewController.view.frame.height
        height_Activity = secondViewController.view.frame.height
    }
    /*
     * function of reload all views
     */
    @objc func handleUserConnectNow2(notification: NSNotification) {
        firstViewController.viewDidLoad()
        secondViewController.viewDidLoad()
        headerController.viewDidLoad()
        
        print("***haddar****")
        configureViewFromLocalisation()
        self.segments[0].button.setTitle(Localization("LabelInformations"), for: .normal)
        self.segments[1].button.setTitle(Localization("LabelMesActivité"), for: .normal)
    }
    /*
     * function of reload the scrollView
     */
    @objc func reloadScrollView(notification: NSNotification) {
         segmentedScrollViewScroll = true
    }
    /*
     * configure language
     */
    func configureViewFromLocalisation() {
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            firstViewController.title = "INFORMATIONS"
            secondViewController.title = "MES ACTIVITES"
            
        }else {
            firstViewController.title = "INFORMATIONS"
            secondViewController.title = "MY ACTIVITIES"
        }
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension ProfilMain: SJSegmentedViewControllerDelegate {
    /*
     * when user select tab in segmented tab
     */
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        
        if selectedSegment != nil {
            selectedSegment?.titleColor(.white)
            
        }
        
        if segments.count > 0 {
            
            selectedSegment = segments[index]
            selectedSegment?.titleColor(UIColor.white)
            if index == 0 {
                firstViewController.viewDidLoad()
            }else {
                secondViewController.viewDidLoad()
            }
            
        }
    }
    /*
     * change view size
     */
    func getSize(_ view:Int) -> CGSize {
        if view == 1 {
            return firstViewController.myScrollView.contentSize
        }else{
            return secondViewController.myScrollView.contentSize
        }
    }
    /*
     * update the scroll
     */
    func updateScroll(_ scroll: Bool) {
        print("Profil main receive call")
        if scroll == false {
            firstViewController.resetscroll()
            secondViewController.resetscroll()
        }
        firstViewController.updateScroll(scroll: scroll)
        secondViewController.updateScroll(scroll: scroll)
    }
    
}

