//
//  Informations_saif.swift
//  GogoSki
//
//  Created by Bouzid saif on 17/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import SJSegmentedScrollView
import SwiftyJSON
import Alamofire
class Informations_saif : UIViewController , UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate{
    /*
     * about label
     */
    @IBOutlet weak var labelPropos: UILabel!
    /*
     * array for levels in french
     */
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     * array for levels in English
     */
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     * array for sports in French
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     * array for sports in English
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     * the height of in about view
     */
    @IBOutlet weak var height_viewApropos: NSLayoutConstraint!
    /*
     * NSLayoutConstraint of bottom about view
     */
    @IBOutlet weak var apropos_bot: NSLayoutConstraint!
    /*
     * NSLayoutConstraint of top about view
     */
    @IBOutlet weak var apropo_top: NSLayoutConstraint!
    /*
     * about view
     */
    @IBOutlet weak var view_apropos: UIView!
    /*
     * widht of sport label
     */
    @IBOutlet weak var sport_height: NSLayoutConstraint!
    /*
     * sport label
     */
    @IBOutlet weak var labelSport: UILabel!
    
    /*
     * sport tableview hight
     */
    @IBOutlet weak var tableviewHeight: NSLayoutConstraint!
    /*
     * variable to test if the user has checked the checkbox or not
     */
    var ekhdem = false
    /*
     * variable of selected index
     */
    var indexThenya : IndexPath = IndexPath()
    /*
     * about label
     */
    @IBOutlet weak var apropos: UILabel!
    /*
     * variable of user's sports
     */
    var sportsJ : JSON = []
    /*
     * pattern for static controller
     */
    static let shared = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Informations_saif") as! Informations_saif
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.sportsJ.count != 0 {
            return self.sportsJ.count
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        sport_height.constant = self.tableview.contentSize.height
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? UITableViewCell {
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                (cell.viewWithTag(1) as! UILabel).text = self.sportsJ[indexPath.row]["pratique"].stringValue
                (cell.viewWithTag(2) as! UILabel).text = "Niveau " + self.sportsJ[indexPath.row]["niveau"].stringValue
            }else {
                switch self.sportsJ[indexPath.row]["pratique"].stringValue {
                case SportSaifFR[0] :
                    
                    (cell.viewWithTag(1) as! UILabel).text = SportSaifEN[0]
                case SportSaifFR[1] :
                    (cell.viewWithTag(1) as! UILabel).text = SportSaifEN[1]
                    
                case SportSaifFR[2] :
                    (cell.viewWithTag(1) as! UILabel).text = SportSaifEN[2]
                    
                case SportSaifFR[3] :
                    (cell.viewWithTag(1) as! UILabel).text = SportSaifEN[3]
                    
                case SportSaifFR[4] :
                    (cell.viewWithTag(1) as! UILabel).text = SportSaifEN[4]
                case SportSaifFR[5] :
                    (cell.viewWithTag(1) as! UILabel).text = SportSaifEN[5]
                    
                default :
                    
                    (cell.viewWithTag(1) as! UILabel).text = ""
                    
                }
                switch self.sportsJ[indexPath.row]["niveau"].stringValue {
                case NiveauSaifFR[0] :
                    
                    (cell.viewWithTag(2) as! UILabel).text = "Level " + NiveauSaifEN[0]
                case NiveauSaifFR[1] :
                    (cell.viewWithTag(2) as! UILabel).text = "Level " + NiveauSaifEN[1]
                    
                case NiveauSaifFR[2] :
                    (cell.viewWithTag(2) as! UILabel).text = "Level " + NiveauSaifEN[2]
                    
                case NiveauSaifFR[3] :
                    (cell.viewWithTag(2) as! UILabel).text = "Level " + NiveauSaifEN[3]
                case NiveauSaifFR[4] :
                    (cell.viewWithTag(2) as! UILabel).text = "Level " + NiveauSaifEN[4]
                    
                    
                default :
                    
                    (cell.viewWithTag(2) as! UILabel).text = ""
                    
                }
                
            }
            
            (cell.viewWithTag(3) as! UILabel).text = ""
            (cell.viewWithTag(3) as! UILabel).textColor = UIColor.lightGray
            
            switch self.sportsJ[indexPath.row]["pratique"].stringValue {
            case "Snowboard":
                (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "Snowboard-kissou")
                
            case "Ski Alpin":
                (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "ski-alpin-kissou")
                
            case "Ski de fond" :
                (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "Ski-fond-kissou")
                
            case "Handiski" :
                (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "Handi-ski-kissou")
                
            case "Raquettes" :
                (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "Raquettes-kissou")
                
            case "Ski de randonnée" :
                (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "Randonnée-ski-kissou")
                
            default:
                (cell.viewWithTag(4) as! UIImageView).image = UIImage(named: "Snow débutant_saif")
            }
            
            if self.sportsJ[indexPath.row]["niveau"].stringValue == "Débutant"
            {
                (cell.viewWithTag(2) as! UILabel).textColor = UIColor("#D3D3D3")
                (cell.viewWithTag(4) as! UIImageView).tintColor = UIColor("#D3D3D3")
            }
            else if self.sportsJ[indexPath.row]["niveau"].stringValue == "Intermédiaire"
            {
                (cell.viewWithTag(2) as! UILabel).textColor = UIColor("#75A7FF")
                (cell.viewWithTag(4) as! UIImageView).tintColor = UIColor("#75A7FF")
            }
            else if self.sportsJ[indexPath.row]["niveau"].stringValue == "Confirmé"
            {
                (cell.viewWithTag(2) as! UILabel).textColor = UIColor("#FF3E53")
                (cell.viewWithTag(4) as! UIImageView).tintColor = UIColor("#FF3E53")
            }
            else if self.sportsJ[indexPath.row]["niveau"].stringValue == "Expert"
            {
                (cell.viewWithTag(2) as! UILabel).textColor = UIColor("#4D4D4D")
                (cell.viewWithTag(4) as! UIImageView).tintColor = UIColor("#4D4D4D")
            }
            else
            {
                (cell.viewWithTag(2) as! UILabel).textColor = UIColor("#76EC9E")
                (cell.viewWithTag(4) as! UIImageView).tintColor = UIColor("#76EC9E")
            }
            (cell.viewWithTag(5) as! VKCheckbox).line = .normal
            (cell.viewWithTag(5) as! VKCheckbox).bgColorSelected = UIColor(red: 95/255, green: 182/255, blue: 255/255, alpha: 1)
            (cell.viewWithTag(5) as! VKCheckbox).bgColor          = UIColor.white
            (cell.viewWithTag(5) as! VKCheckbox).color            = UIColor.white
            (cell.viewWithTag(5) as! VKCheckbox).borderColor      = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
            (cell.viewWithTag(5) as! VKCheckbox).borderWidth      = 2
            // check1.cornerRadius     = check1.frame.height / 2
            if indexThenya == indexPath
            {
                print("indexThenya :" , indexThenya)
                
                (cell.viewWithTag(5) as! VKCheckbox).setOn(true, animated: false,user : false)
                (cell.viewWithTag(5) as! VKCheckbox).borderWidth      = 0
                
                (cell.viewWithTag(5) as! VKCheckbox).borderColor      = UIColor.white
                //  indexLoula = indexPath
            }
            else
            {
                (cell.viewWithTag(5) as! VKCheckbox).setOn(false)
            }
            (cell.viewWithTag(5) as! VKCheckbox).checkboxValueChangedBlock = {
                isOn in
                print("Custom checkbox is \(isOn ? "ON" : "OFF")")
                if isOn {
                    
                    if self.ekhdem == false {
                        self.ekhdem = true
                        print("indexPath:",indexPath.row)
                        self.changeDefaultSport(index: indexPath.row,animated:true)
                        (cell.viewWithTag(5) as! VKCheckbox).borderWidth      = 0
                        
                        (cell.viewWithTag(5) as! VKCheckbox).borderColor      = UIColor.white
                    }
                    
                    
                } else{
                    (cell.viewWithTag(5) as! VKCheckbox).borderWidth      = 2
                    (cell.viewWithTag(5) as! VKCheckbox).borderColor      = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
                }
            }
            let btnSupp : CustomButton = cell.viewWithTag(40) as! CustomButton
            btnSupp.indexPath = indexPath
            btnSupp.index = indexPath.row
            btnSupp.addTarget(self, action: #selector(self.updateActivite), for: .touchUpInside)
            return cell
        }else {
            return UITableViewCell()
        }
    }
    /*
     * delete sport action
     */
    @objc func updateActivite(sender : CustomButton)
    {
        
        sender.isEnabled = false
        if self.sportsJ.arrayObject?.count != 1 {
            SweetAlert().showAlert(Localization("areYouSure"), subTitle: Localization("suppSport"), style: AlertStyle.warning, buttonTitle:Localization("cancel"), buttonColor: UIColor.gray , otherButtonTitle:  Localization("yes"), otherButtonColor: UIColor.red) { (isOtherButton) -> Void in
                if isOtherButton == false {
                    LoaderAlert.shared.show()
                    let ab = UserDefaults.standard.value(forKey: "User") as! String
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    var a = JSON(data: dataFromString!)
                    let header: HTTPHeaders = [
                        "Content-Type" : "application/json",
                        "x-Auth-Token" : a["value"].stringValue
                    ]
                    let params : Parameters = [
                        "idSport" : self.sportsJ[sender.index!]["id"].stringValue
                        
                    ]
                    Alamofire.request(ScriptBase.sharedInstance.supprimerSport , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                        .responseJSON { response in
                            LoaderAlert.shared.dismiss()
                            
                            let b = JSON(response.data!)
                            
                            if b["status"].boolValue {
                                _ = SweetAlert().showAlert("Supression", subTitle: Localization("oki"), style: AlertStyle.success, buttonTitle: "OK" )
                                let ab = UserDefaults.standard.value(forKey: "User") as! String
                                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                                var a = JSON(data: dataFromString!)
                                if self.sportsJ[sender.index!]["is_default"].boolValue {
                                    if sender.index! != 0 {
                                        self.changeDefaultSport(index: 0,animated: false)
                                    }else {
                                        self.changeDefaultSport(index: 1,animated:false)
                                    }
                                }
                                self.sportsJ.arrayObject?.remove(at: sender.index!)
                                
                                a["user"]["sports"] = self.sportsJ
                                UserDefaults.standard.setValue(a.rawString(), forKey: "User")
                                UserDefaults.standard.synchronize()
                                self.tableview.reloadData()
                                
                            }
                            else {
                                _ = SweetAlert().showAlert("SPORTS", subTitle: Localization("erreur"), style: AlertStyle.error)
                            }
                    }
                    
                }else {
                    sender.isEnabled = true
                }
            }
        } else {
            _ = SweetAlert().showAlert("Supression", subTitle: Localization("unSport"), style: AlertStyle.error, buttonTitle: "OK" )
            
        }
        
    }
    
    /*
     * scrolling in view
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == myScrollView {
            if scrollView.contentOffset != CGPoint.zero {
                scrollView.isScrollEnabled = true
            }else{
                scrollView.isScrollEnabled = false
            }
        }
    }
    /*
     * sport tableview
     */
    @IBOutlet weak var tableview: UITableView!
    /*
     * scroll of the view
     */
    @IBOutlet weak var myScrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            myScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
        }
        print("hi3")
        myScrollView.isScrollEnabled = false
        tableview.delegate = self
        tableview.dataSource = self
        tableview.allowsMultipleSelection = false
        self.tableview.isScrollEnabled = false
        myScrollView.bounces = false
        myScrollView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        
        
    }
    /*
     * get user information
     */
    func loadData(){
        if  UserDefaults.standard.value(forKey: "User") != nil {
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.AfficherProfil , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                
                let a = JSON(response.data)
                //  print("saif:",a)
                
                if a["status"].boolValue == false {
                    if a["message"].stringValue == "L utilisateur n existe pas" || a["message"].stringValue == "Mot de passe invalide" {
                        
                        _ = SweetAlert().showAlert(Localization("ConnexionChargement"), subTitle: Localization("ConnexionErreur"), style: AlertStyle.error)
                    }
                }else{
                    print(a)
                    let c = a["data"]["sports"].arrayObject
                    self.sportsJ = JSON(c)
                    print(self.sportsJ)
                    if (self.sportsJ.count != 0)
                    {
                        for i in 0 ... (self.sportsJ.count - 1)
                        {
                            if (self.sportsJ[i]["is_default"].boolValue)
                            {
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeSport"), object: self.sportsJ[i])
                                self.indexThenya = IndexPath(row: i, section: 0)
                            }
                        }
                        self.tableview.reloadData()
                        self.tableviewHeight.constant = self.tableview.contentSize.height + 50.0
                    }
                }
                
                
        }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let a = JSON(data: dataFromString!)
        
    if a["user"]["description"].description !=  ""{
            apropos.text = a["user"]["description"].stringValue
            self.apropo_top.constant = 19.0
            self.apropos_bot.constant = 35.0
            self.height_viewApropos.constant = 63.0
            
        }else {
            self.apropo_top.constant = -1
            self.apropos_bot.constant = -1
            self.apropos.text = ""
            self.height_viewApropos.constant = -1
            self.view_apropos.isHidden = true
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        resetscroll()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*
     * update scrolling
     */
    func updateScroll(scroll : Bool){
        myScrollView.isScrollEnabled = scroll
    }
    /*
     * disable scrolling
     */
    func resetscroll(){
        self.myScrollView.setContentOffset(CGPoint.zero, animated: false)
        myScrollView.isScrollEnabled = false
    }
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        labelPropos.text = Localization("labelPropos")
        labelSport.text = Localization("labelSport")
        loadData()
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /*
     * change default action sport
     */
    func changeDefaultSport( index : Int,animated:Bool)
    {
        LoaderAlert.show(withStatus: Localization("changement"))
        let params: Parameters = [
            "pratique": self.sportsJ[index]["pratique"].stringValue
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.changeSportDefault , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                print ("sport : " , b)
                if b["status"].boolValue {
                    StaticPositionController.shared.niveau = b["data"]["niveau"].stringValue
                    StaticPositionController.shared.practice = b["data"]["pratique"].stringValue
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "changeSport"), object: self.sportsJ[index])
                    self.indexThenya = IndexPath(row: index, section: 0)
                    self.tableview.reloadData()
                    
                    
                    if animated  {
                        self.ekhdem = false
                        _ = SweetAlert().showAlert(Localization("changeSport"), subTitle: Localization("changeSport2"), style: AlertStyle.success , buttonTitle: "OK" , action: { (otherButton) in
                            self.ekhdem = false
                            print ( "c bon ")
                        })
                    }else {
                        self.ekhdem = false
                    }
                    
                }else{
                    if animated  {
                        _ = SweetAlert().showAlert(Localization("changeSport"), subTitle: Localization("erreur"), style: AlertStyle.error)
                    }
                    
                }
                
                
        }
    }
}

