//
//  File.swift
//  GogoSki
//
//  Created by Bouzid saif on 17/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import SJSegmentedScrollView
import Alamofire
import SwiftyJSON
class mesActivites_saif : UIViewController,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate {
    /*
     * array for levels in french
     */
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     * array for levels in English
     */
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     * array for sports in French
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     * array for sports in English
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     * variable of activities in progress
     */
    var enCours = [JSON]()
    /*
     * variable of historique activities
     */
    var historiques  = [JSON]()
    /*
     * the height of historique tableview
     */
    @IBOutlet weak var historique_height: NSLayoutConstraint!
    /*
     * the height of in progress view
     */
    @IBOutlet weak var encours_height_view: NSLayoutConstraint!
    /*
     * in progress label
     */
    @IBOutlet weak var labelEnCours: UILabel!
    /*
     * the height of in progress tableview
     */
    @IBOutlet weak var encours_height: NSLayoutConstraint!
    /*
     * the height of historique view
     */
    @IBOutlet weak var historique_height_view: NSLayoutConstraint!
    /*
     * activities label
     */
    @IBOutlet weak var btnActivité: UILabel!
    /*
     * historique label
     */
    @IBOutlet weak var btnHistorique: UILabel!
    /*
     * tableview in progress
     */
    @IBOutlet weak var encours: UITableView!
    /*
     * the scroll of the view
     */
    @IBOutlet weak var myScrollView: UIScrollView!
    /*
     * pattern for static controller
     */
    static let shared = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mesActivites_saif") as! mesActivites_saif
    /*
     * tableview historique
     */
    @IBOutlet weak var historique: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == encours {
            return enCours.count
        }else if tableView == historique {
            return (historiques.count)
        }else{
            return 0
        }
    }
    /*
     * scrolling to the top
     */
    func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        
        if scrollView == myScrollView{
            
            myScrollView.isScrollEnabled = false
        }
    }
    /*
     * scrolling in view
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == myScrollView {
            if scrollView.contentOffset != CGPoint.zero {
                scrollView.isScrollEnabled = true
            }else{
                scrollView.isScrollEnabled = false
            }
        }
    }
    var scrollenabled = false
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == encours {
            let cell = encours.dequeueReusableCell(withIdentifier: "cell_encours", for: indexPath)
            let labelSport : UILabel = cell.viewWithTag(1) as! UILabel
            let labelDate : UILabel = cell.viewWithTag(2) as! UILabel
            let labelDuree : UILabel = cell.viewWithTag(3) as! UILabel
            let labelPlace : UILabel = cell.viewWithTag(4) as! UILabel
            let labelPlaceRestantes : UILabel = cell.viewWithTag(5) as! UILabel
            let labelDepartement : UILabel = cell.viewWithTag(7) as! UILabel
            //  let imgSortie : UIImageView = cell.viewWithTag(10) as UIImageView
            let btnSupp : CustomButton = cell.viewWithTag(20) as! CustomButton
            let labelmin : UILabel = cell.viewWithTag(11) as! UILabel
            let labelmax : UILabel = cell.viewWithTag(12) as! UILabel
            if self.enCours[indexPath.row]["discr"].stringValue == "Lecon" {
                labelmin.text = self.enCours[indexPath.row]["prestation"]["tarifs"][0]["prix_min"].stringValue + " € TTC"
                labelmax.text = self.enCours[indexPath.row]["prestation"]["tarifs"][0]["prix_max"].stringValue + " € TTC"
                labelmin.layer.cornerRadius = 5
                labelmin.layer.masksToBounds = true
                labelmax.layer.cornerRadius = 5
                labelmax.layer.masksToBounds = true
                labelmin.isHidden = false
                labelmax.isHidden = false
                labelDepartement.text = self.enCours[indexPath.row]["point"]["station"]["dept"].stringValue
            }else {
                labelDepartement.text = self.enCours[indexPath.row]["departement"].stringValue
                labelmin.isHidden = true
                labelmax.isHidden = true
            }
            btnSupp.index = indexPath.row
            btnSupp.indexPath = indexPath
            labelSport.adjustsFontSizeToFitWidth = true
            labelDepartement.numberOfLines = 2
            labelDepartement.lineBreakMode = .byWordWrapping
            if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr" {
                switch self.enCours[indexPath.row]["sport"]["pratique"].stringValue {
                case SportSaifFR[0] :
                    
                    labelSport.text = SportSaifEN[0]
                case SportSaifFR[1] :
                    labelSport.text = SportSaifEN[1]
                    
                case SportSaifFR[2] :
                    labelSport.text = SportSaifEN[2]
                    
                case SportSaifFR[3] :
                    labelSport.text  = SportSaifEN[3]
                    
                case SportSaifFR[4] :
                    labelSport.text = SportSaifEN[4]
                case SportSaifFR[5] :
                    labelSport.text = SportSaifEN[5]
                    
                default :
                    
                    labelSport.text  = ""
                    
                }
            }else {
                labelSport.text = self.enCours[indexPath.row]["sport"]["pratique"].stringValue
            }
            if ( self.enCours[indexPath.row]["sport"]["niveau"].stringValue == "Confirmé" )
            {
                labelSport.backgroundColor = UIColor("#FF3E53")
            }
            else if (self.enCours[indexPath.row]["sport"]["niveau"].stringValue == "Débutant")
            {
                labelSport.backgroundColor = UIColor("#D3D3D3")
            }
            else if (self.enCours[indexPath.row]["sport"]["niveau"].stringValue == "Intermédiaire")
            {
                labelSport.backgroundColor = UIColor("#75A7FF")
            }
            else if (self.enCours[indexPath.row]["sport"]["niveau"].stringValue == "Expert")
            {
                labelSport.backgroundColor = UIColor("#4D4D4D")
            }
            else {
                labelSport.backgroundColor = UIColor("#76EC9E")
            }
            var s = self.enCours[indexPath.row]["date_debut"].stringValue.suffix(8)
            let toArray = s.components(separatedBy: "/")
            let backToString = toArray.joined(separator: ":")
            
            let sr = self.enCours[indexPath.row]["date_debut"].stringValue.prefix(10)
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                labelDate.text = sr + " à " + backToString.prefix(5)
            }
            else {
                labelDate.text = sr + " at " + backToString.prefix(5)
            }
            let heure = String(self.enCours[indexPath.row]["duree"].intValue / 60)
            var minute = String (self.enCours[indexPath.row]["duree"].intValue - ( (self.enCours[indexPath.row]["duree"].intValue / 60) * 60 ))
            if (minute == "0")
            {
                minute = ""
            }
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                labelDuree.text = "Durée : " + heure + "h" + minute
            }else {
                labelDuree.text = "Duration : " + heure + "h" + minute
            }
            
            if (self.enCours[indexPath.row]["nb_place_maximum"].stringValue != "100")
            {
                if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                    if (enCours[indexPath.row]["nb_place_maximum"].intValue - enCours[indexPath.row]["nb_participant"].intValue) > 0 {
                        labelPlace.text = self.enCours[indexPath.row]["nb_place_maximum"].stringValue + " pers./"
                        labelPlaceRestantes.text = String(enCours[indexPath.row]["nb_place_maximum"].intValue - enCours[indexPath.row]["nb_participant"].intValue) + " restante(s)"
                    } else {
                        labelPlace.text = self.enCours[indexPath.row]["nb_place_maximum"].stringValue + " pers./"
                        labelPlaceRestantes.text = " " + "0" + " restante(s)"
                    }
                    
                }else {
                    if (enCours[indexPath.row]["nb_place_maximum"].intValue - enCours[indexPath.row]["nb_participant"].intValue) > 0 {
                        labelPlace.text = self.enCours[indexPath.row]["nb_place_maximum"].stringValue + " pers./"
                        labelPlaceRestantes.text = String(enCours[indexPath.row]["nb_place_maximum"].intValue - enCours[indexPath.row]["nb_participant"].intValue) + " remaining"
                    } else {
                        labelPlace.text = self.enCours[indexPath.row]["nb_place_maximum"].stringValue + " pers./"
                        labelPlaceRestantes.text = " " + "0" + " remaining"
                    }
                    
                    
                }
            }
            else {
                if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                    labelPlace.text = "Illimité"
                    labelPlaceRestantes.text = ""
                }
                else {
                    labelPlace.text = "Limitless"
                    labelPlaceRestantes.text = ""
                }
            }
            
            
            btnSupp.addTarget(self, action: #selector(self.suppActivite), for: .touchUpInside)
            let btnAjout : CustomButton = cell.viewWithTag(30) as! CustomButton
            btnAjout.index = indexPath.row
            btnAjout.indexPath = indexPath
            btnAjout.addTarget(self, action: #selector(self.inviteAction), for: .touchUpInside)
            let btnMessages : CustomButton = cell.viewWithTag(100) as! CustomButton
            btnMessages.index = indexPath.row
            btnMessages.indexPath = indexPath
            btnMessages.addTarget(self, action: #selector(self.messageAction), for: .touchUpInside)
            return cell
        }else if tableView == historique {
            
            let cell = historique.dequeueReusableCell(withIdentifier: "cell_historique", for: indexPath)
            let labelSport : UILabel = cell.viewWithTag(1) as! UILabel
            let labelDate : UILabel = cell.viewWithTag(2) as! UILabel
            let labelDuree : UILabel = cell.viewWithTag(3) as! UILabel
            let labelPlace : UILabel = cell.viewWithTag(4) as! UILabel
            let labelPlaceRestantes : UILabel = cell.viewWithTag(5) as! UILabel
            let labelDepartement : UILabel = cell.viewWithTag(7) as! UILabel
            let labelmin : UILabel = cell.viewWithTag(11) as! UILabel
            let labelmax : UILabel = cell.viewWithTag(12) as! UILabel
            if self.historiques[indexPath.row]["discr"].stringValue == "Lecon" {
                labelmin.text = self.historiques[indexPath.row]["prestation"]["tarifs"][0]["prix_min"].stringValue + " € TTC"
                labelmax.text = self.historiques[indexPath.row]["prestation"]["tarifs"][0]["prix_max"].stringValue + " € TTC"
                labelmin.layer.cornerRadius = 5
                labelmin.layer.masksToBounds = true
                labelmax.layer.cornerRadius = 5
                labelmax.layer.masksToBounds = true
                labelmin.isHidden = false
                labelmax.isHidden = false
            }else {
                labelmin.isHidden = true
                labelmax.isHidden = true
            }
            labelDepartement.adjustsFontSizeToFitWidth = true
            labelSport.adjustsFontSizeToFitWidth = true
            if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr" {
                switch self.historiques[indexPath.row]["sport"]["pratique"].stringValue {
                case SportSaifFR[0] :
                    
                    labelSport.text = SportSaifEN[0]
                case SportSaifFR[1] :
                    labelSport.text = SportSaifEN[1]
                    
                case SportSaifFR[2] :
                    labelSport.text = SportSaifEN[2]
                    
                case SportSaifFR[3] :
                    labelSport.text  = SportSaifEN[3]
                    
                case SportSaifFR[4] :
                    labelSport.text = SportSaifEN[4]
                case SportSaifFR[5] :
                    labelSport.text = SportSaifEN[5]
                    
                default :
                    
                    labelSport.text  = ""
                    
                }
            }else {
                labelSport.text = self.historiques[indexPath.row]["sport"]["pratique"].stringValue
            }
            labelSport.backgroundColor = UIColor.gray
            var s = self.historiques[indexPath.row]["date_debut"].stringValue.suffix(8)
            let toArray = s.components(separatedBy: "/")
            let backToString = toArray.joined(separator: ":")
            
            let sr = self.historiques[indexPath.row]["date_debut"].stringValue.prefix(10)
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                labelDate.text = sr + " à " + backToString.prefix(5)
            }
            else {
                labelDate.text = sr + " at " + backToString.prefix(5)
            }
            let heure = String(self.historiques[indexPath.row]["duree"].intValue / 60)
            var minute = String (self.historiques[indexPath.row]["duree"].intValue - ( (self.historiques[indexPath.row]["duree"].intValue / 60) * 60 ))
            if (minute == "0")
            {
                minute = ""
            }
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                labelDuree.text = "Durée : " + heure + "h" + minute
            }else {
                labelDuree.text = "Duration : " + heure + "h" + minute
            }
            
            
            
            if (self.historiques[indexPath.row]["nb_place_maximum"].intValue != 100)
            {
                if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                    if (historiques[indexPath.row]["nb_place_maximum"].intValue - historiques[indexPath.row]["nb_participant"].intValue) > 0 {
                        labelPlace.text = self.historiques[indexPath.row]["nb_place_maximum"].stringValue + " pers. / "
                        labelPlaceRestantes.text = String(historiques[indexPath.row]["nb_place_maximum"].intValue - historiques[indexPath.row]["nb_participant"].intValue) + " restante(s)"
                    } else {
                        labelPlace.text = self.historiques[indexPath.row]["nb_place_maximum"].stringValue + " pers. / "
                        labelPlaceRestantes.text = " " + "0" + " restante(s)"
                    }
                    
                }else {
                    if (historiques[indexPath.row]["nb_place_maximum"].intValue - historiques[indexPath.row]["nb_participant"].intValue) > 0 {
                        labelPlace.text = self.historiques[indexPath.row]["nb_place_maximum"].stringValue + " pers. / "
                        labelPlaceRestantes.text = String(historiques[indexPath.row]["nb_place_maximum"].intValue - historiques[indexPath.row]["nb_participant"].intValue) + " remaining"
                    } else {
                        labelPlace.text = self.historiques[indexPath.row]["nb_place_maximum"].stringValue + " pers. / "
                        labelPlaceRestantes.text = " " + "0" + " remaining"
                    }
                    
                    
                }
            }
            else {
                if Localisator.sharedInstance.currentLanguage != "French"  || Localisator.sharedInstance.currentLanguage != "Français"  || Localisator.sharedInstance.currentLanguage != "French_fr" {
                    labelPlace.text = "Limitless"
                    labelPlaceRestantes.text = ""
                }
                else {
                    labelPlace.text = "Illimité"
                    labelPlaceRestantes.text = ""
                }
            }
            labelDepartement.text = self.historiques[indexPath.row]["departement"].stringValue
            let btnUpdate : CustomButton = cell.viewWithTag(10) as! CustomButton
            btnUpdate.index = indexPath.row
            btnUpdate.indexPath = indexPath
            btnUpdate.addTarget(self, action: #selector(self.updateActivite), for: .touchUpInside)
            
            return cell
            
        }
        else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if self.historiques[indexPath.row]["discr"].stringValue == "Lecon" {
            let view = self.storyboard?.instantiateViewController(withIdentifier: "FicheLeçon") as! FicheLec_onViewController
            view.id = self.historiques[indexPath.row]["id"].stringValue
            self.navigationController?.pushViewController(view, animated: true)
         }else{
            let view = self.storyboard?.instantiateViewController(withIdentifier: "ApercuSortie") as! ApercuFicheViewController
            view.id = self.historiques[indexPath.row]["id"].stringValue
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
    override func viewDidLoad() {
        if #available(iOS 11.0, *) {
            myScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
        }
        myScrollView.isScrollEnabled = scrollenabled
        encours.isScrollEnabled = false
        historique.isScrollEnabled = false
        myScrollView.bounces = false
        myScrollView.delegate = self
        super.viewDidLoad()
        encours.allowsMultipleSelection = false
        historique.allowsMultipleSelection = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.DeleteNotifVar(notification:)), name: NSNotification.Name(rawValue: "SkaerNotif"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        loadData()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == encours{
            //  if indexPath.row == 2 {
            encours_height.constant = encours.contentSize.height
            //   }
        }else if tableView == historique {
            //     if indexPath.row == 1 {
            historique_height.constant = historique.contentSize.height
            //  }
        }
    }
    /*
     * update scrolling
     */
    func updateScroll(scroll : Bool){
        
        myScrollView.isScrollEnabled = scroll
    }
    /*
     * disable scrolling
     */
    func resetscroll(){
        self.myScrollView.setContentOffset(CGPoint.zero, animated: false)
        myScrollView.isScrollEnabled = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        resetscroll()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        labelEnCours.text = Localization("labelEncours")
        btnActivité.text = Localization("labelActivité")
        btnHistorique.text = Localization("labelHistorique")
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    
    @objc func DeleteNotifVar(notification:NSNotification) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.NotifReceive =  false
    }
    /*
     * get all activities
     */
    func loadData(){
        self.historiques = []
        self.enCours = []
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.mesActivites , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                //     LoaderAlert.shared.dismiss()
                
                let a = JSON(response.data)
                print(a)
                /*   var i = 1
                 let dict = ["aa" : i]
                 
                 if JSONSerialization.isValidJSONObject(dict) { // True
                 do {
                 let rawData = try JSONSerialization.data(withJSONObject : dict, options: .prettyPrinted)
                 print ("*****")
                 print (JSON(rawData))
                 
                 print ( "*****")
                 } catch {
                 // Handle Error
                 }
                 }*/
                
                
                if a["status"].boolValue == false {
                    
                    
                }else{
                    if (a["data"].arrayObject?.count != 0)
                    {
                        for i in 0...((a["data"].arrayObject?.count)! - 1)
                        {
                            
                            let c = a["data"][i]
                            let date = Date()
                            
                            let date_debutString = c["date_debut"].stringValue
                            
                            
                            let date_debut = self.convertDateFormater(date: date_debutString)
                            
                            let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: date_debut)
                            
                            
                            if (date > tomorrow!)
                            {
                                
                                
                                self.historiques.append(c)
                                
                                
                            }
                            else {
                                
                                self.enCours.append(c)
                            }
                        }
                        self.encours.reloadData()
                        self.historique.reloadData()
                        if self.enCours.count == 0
                        {
                            
                            self.encours_height_view.constant = 0
                            self.encours_height.constant = 0
                            self.labelEnCours.isHidden = true
                        }
                        else {
                            self.encours_height_view.constant = 60
                            self.labelEnCours.isHidden = false
                            self.encours_height.constant = self.encours.contentSize.height
                        }
                        
                        if self.historiques.count == 0
                        {
                            
                            
                            self.historique_height_view.constant = 0
                            self.historique_height.constant = 0
                            self.btnHistorique.isHidden = true
                            self.btnActivité.isHidden = true
                        }
                        else {
                            self.historique_height_view.constant = 60
                            self.historique_height.constant = self.historique.contentSize.height
                            self.btnHistorique.isHidden = false
                            self.btnActivité.isHidden = false
                        }
                        //   self.historique_height.constant = self.historique.contentSize.height
                        
                        
                        
                    }
                    else {
                        
                        print ("en courssss :" , self.enCours.count)
                        if self.enCours.count == 0
                        {
                            print ("aazeezarzeartrat")
                            self.encours_height_view.constant = 0
                            self.encours_height.constant = 0
                            self.labelEnCours.isHidden = true
                        }
                        print ("histttt :" , self.historiques.count)
                        if self.historiques.count == 0
                        {
                            
                            print ("aazeezarzeartrat")
                            self.historique_height_view.constant = 0
                            self.historique_height.constant = 0
                            self.btnHistorique.isHidden = true
                            self.btnActivité.isHidden = true
                        }
                    }
                }
                
                
                
        }
    }
    /*
     * convertir string to date
     */
    func convertDateFormater(date: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd' 'HH/mm/ss"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        
        guard let date = dateFormatter.date(from: date) else {
            assert(false, "no date from string")
            return Date()
        }
        return date
        
    }
    /*
     * unfollow from activity action
     */
    @objc func suppActivite (sender : CustomButton)
    {
        
        SweetAlert().showAlert(Localization("areYouSure"), subTitle: Localization("youWillExitActivity"), style: AlertStyle.warning, buttonTitle:Localization("cancel"), buttonColor: UIColor.gray , otherButtonTitle:  Localization("yes"), otherButtonColor: UIColor.red) { (isOtherButton) -> Void in
            if isOtherButton == false {
                let ab = UserDefaults.standard.value(forKey: "User") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : a["value"].stringValue
                ]
                
                Alamofire.request(ScriptBase.sharedInstance.suppActivity + self.enCours[sender.index!]["id"].stringValue + "/desabonne" , method: .post, encoding: JSONEncoding.default,headers : header)
                    .responseString { response in
                        
                        let b = JSON(response.data)
                        print ("desabonne "  , response)
                        if b["status"].boolValue {
                            
                            _ = SweetAlert().showAlert(Localization("Supp"), subTitle: Localization("youExitActivity"), style: AlertStyle.success)
                            if (self.enCours.count != 1)
                            {
                                self.enCours.remove(at: sender.index!)
                            }
                            else {
                                self.enCours.removeAll()
                                self.encours_height_view.constant = 0
                                self.encours_height.constant = 0
                                self.labelEnCours.isHidden = true
                                
                            }
                            
                            self.encours.reloadData()
                            self.encours_height.constant = self.encours.contentSize.height
                        }else{
                            
                            _ = SweetAlert().showAlert(Localization("Supp"), subTitle: Localization("erreur"), style: AlertStyle.error)
                            
                        }
                        
                        
                }
            }
        }
    }
    /*
     * invite friend to an activity
     */
    @objc func inviteAction(sender : CustomButton) {
        sender.isEnabled = false
        let view = self.storyboard?.instantiateViewController(withIdentifier: "InviterParticipant") as! InviterParticipantViewController
        view.sortie = self.enCours[sender.index!]
        view.chkoun = "bklabalb"
        sender.isEnabled = true
        self.navigationController?.present(view, animated: true, completion: nil)
    }
    /*
     * open activity conversation
     */
    @objc func messageAction(sender : CustomButton) {
        LoaderAlert.show()
        sender.isEnabled = false
        let params: Parameters = [
            "idConv": self.enCours[sender.index!]["conversation"]["id"].stringValue
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.joinConvActivite , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                print ("add conv "  , b)
                if b["status"].boolValue {
                    let ConvId = self.enCours[sender.index!]["conversation"]["id"].stringValue
                    print("ConvID",ConvId)
                    SocketIOManager.sharedInstance.connectToServerWithNickname(nickname: ConvId, userid: a["user"]["id"].stringValue, first: false)
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.NotifAhmed =  true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToMessageForomNoWhere2"), object: ConvId )
                }
                sender.isEnabled = true
                
                
        }
        
    }
    
    /*
     * renew activity
     */
    @objc func updateActivite (sender : CustomButton)
    {
        sender.isEnabled = false
        if self.historiques[sender.index!]["discr"].stringValue == "Lecon" {
            print ("lesson selected : " , self.historiques[sender.index!])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GotoCreateLesson"), object: self.historiques[sender.index!])
        }else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GotoCreateSortie"), object: self.historiques[sender.index!])
        }
        
        
        sender.isEnabled = true
    }
}

