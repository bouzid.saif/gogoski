//
//  ProfilSection_saif.swift
//  GogoSki
//
//  Created by Bouzid saif on 17/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import SJSegmentedScrollView
import SwiftyJSON
import Alamofire
import OneSignal
import CoreLocation
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
class ProfilSection : InternetCheckViewControllers {
    @IBOutlet weak var btnsettings: UIButton!
    /*
     * pattern for static controller
     */
    static let shared = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileSection_saif") as! ProfilSection
    /*
     * array for levels in french
     */
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     * array for levels in English
     */
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     * array for sports in French
     */
    let SportSaifFR = ["Pratique","Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     * array for sports in English
     */
    let SportSaifEN = ["Practice","Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     * contacts label
     */
    @IBOutlet weak var labelContacts: UILabel!
    /*
     * button preferences
     */
    @IBOutlet weak var btnPreferences: UIButton!
    /*
     * button modify profil
     */
    @IBOutlet weak var btnModif: UIButton!
    /*
     * button logout
     */
    @IBOutlet weak var btnDeco: UIButton!
    /*
     * sport user label
     */
    @IBOutlet weak var sportUser: UIButton!
    /*
     * sport icon
     */
    @IBOutlet weak var sportPicture: UIImageView!
    /*
     * user name
     */
    @IBOutlet weak var nomUser: UILabel!
    /*
     * user picture
     */
    @IBOutlet weak var photo_profil: RoundedUIImageView!
    /*
     * popup view
     */
    @IBOutlet weak var popup: UIView!
    /*
     * button contacts
     */
    @IBOutlet weak var btnContact: UIButton!
    /*
     * width of sport label
     */
    @IBOutlet weak var SportUserContWidth: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnContact.isEnabled = false
        btnPreferences.isEnabled = false
        btnModif.isEnabled = false
        btnDeco.isEnabled = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserConnectNow2(notification:)), name: NSNotification.Name(rawValue: "changeSport"), object: nil)
        popup.layer.cornerRadius = 10
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        self.nomUser.text = ""
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureViewFromLocalisation()
        
    }
    /*
     * change sports french and english in real time
     */
    @objc func handleUserConnectNow2(notification: NSNotification) {
        let notif = notification.object as! JSON
        switch notif["pratique"].stringValue {
        case "Snowboard":
            //  sportPicture.image = UIImage(named: "Snowboard-kissou")
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                self.sportUser.setTitle("Snowboard", for: .normal)
            }else {
                self.sportUser.setTitle("Snowboard", for: .normal)
            }
        case "Ski Alpin":
            //   sportPicture.image = UIImage(named: "ski-alpin-kissou")
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                self.sportUser.setTitle("Ski Alpin", for: .normal)
            }else {
                self.sportUser.setTitle("Alpine skiing", for: .normal)
            }
        case "Ski de fond" :
            sportPicture.image = UIImage(named: "Ski-fond-kissou")
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                self.sportUser.setTitle("Ski de fond", for: .normal)
            }else {
                self.sportUser.setTitle("Cross-country skiing", for: .normal)
            }
        case "Handiski" :
            sportPicture.image = UIImage(named: "Handi-ski-kissou")
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                self.sportUser.setTitle("Handiski", for: .normal)
            }else {
                self.sportUser.setTitle("Handiski", for: .normal)
            }
        case "Raquette" :
            sportPicture.image = UIImage(named: "Raquettes-kissou")
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                self.sportUser.setTitle("Raquette", for: .normal)
            }else{
                self.sportUser.setTitle("Racket", for: .normal)
            }
        case "Ski de randonnée" :
            sportPicture.image = UIImage(named: "Randonnée-ski-kissou")
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                self.sportUser.setTitle("Ski de randonnée", for: .normal)
            } else {
                self.sportUser.setTitle("Nordic skiing", for: .normal)
            }
        default:
            sportPicture.image = UIImage(named: "Snow débutant_saif")
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                self.sportUser.setTitle("Snowboard", for: .normal)
            }else {
                self.sportUser.setTitle("Snowboard", for: .normal)
            }
        }
        // self.sportUser.titleLabel?.minimumScaleFactor = 0.5
        // self.sportUser.titleLabel?.numberOfLines = 1
        
        if ((self.sportUser.title(for: .normal)?.count)! > 19)
        {
            SportUserContWidth.constant = 200
        }else if ((self.sportUser.title(for: .normal)?.count)! > 15)
        {
            SportUserContWidth.constant = 150
        }else if ((self.sportUser.title(for: .normal)?.count)! > 10) {
            SportUserContWidth.constant = 120
        }
        else {
            SportUserContWidth.constant = 100
        }
        
        if notif["niveau"].stringValue == "Débutant" && notif["pratique"].stringValue == "Snowboard"
        {
            sportPicture.image = UIImage(named: "Snowboard Débutant")
        }
        else if notif["niveau"].stringValue == "Intermédiaire" && notif["pratique"].stringValue == "Snowboard"
        {
            sportPicture.image = UIImage(named: "Snowboard Intermédiaire")
            
        }
        else if notif["niveau"].stringValue == "Confirmé" && notif["pratique"].stringValue == "Snowboard"
        {
            sportPicture.image = UIImage(named: "Snowboard Confirmé")
            
        }
        else if notif["niveau"].stringValue == "Expert" && notif["pratique"].stringValue == "Snowboard"
        {
            sportPicture.image = UIImage(named: "Snowboard Expert")
            
        }
        else if notif["niveau"].stringValue == "Débrouillard" && notif["pratique"].stringValue == "Snowboard"
        {
            sportPicture.image = UIImage(named: "Snowboard Débrouillard")
            
        }
        else if notif["niveau"].stringValue == "Débutant" && notif["pratique"].stringValue == "Ski Alpin"
        {
            sportPicture.image = UIImage(named: "Ski Alpin Débutant")
        }
        else if notif["niveau"].stringValue == "Intermédiaire" && notif["pratique"].stringValue == "Ski Alpin"
        {
            sportPicture.image = UIImage(named: "Ski Alpin Intermédiaire")
            
        }
        else if notif["niveau"].stringValue == "Confirmé" && notif["pratique"].stringValue == "Ski Alpin"
        {
            sportPicture.image = UIImage(named: "Ski Alpin Confirmé")
            
        }
        else if notif["niveau"].stringValue == "Expert" && notif["pratique"].stringValue == "Ski Alpin"
        {
            sportPicture.image = UIImage(named: "Ski Alpin Expert")
        }
        else if notif["niveau"].stringValue == "Débrouillard" && notif["pratique"].stringValue == "Ski Alpin"
        {
            sportPicture.image = UIImage(named: "Ski Alpin Débrouillard")
        }
        else if notif["niveau"].stringValue == "Débutant" && notif["pratique"].stringValue == "Ski de fond"
        {
            sportPicture.image = UIImage(named: "Ski de fond Débutant")
        }
        else if notif["niveau"].stringValue == "Intermédiaire" && notif["pratique"].stringValue == "Ski de fond"
        {
            sportPicture.image = UIImage(named: "Ski de fond Intermédiaire")
            
        }
        else if notif["niveau"].stringValue == "Confirmé" && notif["pratique"].stringValue == "Ski de fond"
        {
            sportPicture.image = UIImage(named: "Ski de fond Confirmé")
            
        }
        else if notif["niveau"].stringValue == "Expert" && notif["pratique"].stringValue == "Ski de fond"
        {
            sportPicture.image = UIImage(named: "Ski de fond Expert")
            
            
        }
        else if notif["niveau"].stringValue == "Débrouillard" && notif["pratique"].stringValue == "Ski de fond"
        {
            sportPicture.image = UIImage(named: "Ski de fond Débrouillard")
            
        }
        else if notif["niveau"].stringValue == "Débutant" && notif["pratique"].stringValue == "Handiski"
        {
            sportPicture.image = UIImage(named: "Handiski Débutant")
        }
        else if notif["niveau"].stringValue == "Intermédiaire" && notif["pratique"].stringValue == "Handiski"
        {
            sportPicture.image = UIImage(named: "Handiski intermédiaire")
            
        }
        else if notif["niveau"].stringValue == "Confirmé" && notif["pratique"].stringValue == "Handiski"
        {
            sportPicture.image = UIImage(named: "Handiski Confirmé")
            
        }
        else if notif["niveau"].stringValue == "Expert" && notif["pratique"].stringValue == "Handiski"
        {
            sportPicture.image = UIImage(named: "Handiski Expert")
            
        }
        else if notif["niveau"].stringValue == "Débrouillard" && notif["pratique"].stringValue == "Handiski"
        {
            sportPicture.image = UIImage(named: "Handiski Débrouillard")
            
        }
        else if notif["niveau"].stringValue == "Débutant" && notif["pratique"].stringValue == "Raquette"
        {
            sportPicture.image = UIImage(named: "Raquettes Débutant")
        }
        else if notif["niveau"].stringValue == "Intermédiaire" && notif["pratique"].stringValue == "Raquette"
        {
            sportPicture.image = UIImage(named: "Raquettes Intermédiaire")
            
        }
        else if notif["niveau"].stringValue == "Confirmé" && notif["pratique"].stringValue == "Raquette"
        {
            sportPicture.image = UIImage(named: "Raquettes Confirmé")
            
        }
        else if notif["niveau"].stringValue == "Expert" && notif["pratique"].stringValue == "Raquette"
        {
            sportPicture.image = UIImage(named: "Raquettes Expert")
            
        }
        else if notif["niveau"].stringValue == "Débrouillard" && notif["pratique"].stringValue == "Raquette"
        {
            sportPicture.image = UIImage(named: "Raquettes  Débrouillard")
            
        }
        else if notif["niveau"].stringValue == "Débutant" && notif["pratique"].stringValue == "Ski de randonnée"
        {
            sportPicture.image = UIImage(named: "Ski de randonnée Débutant")
        }
        else if notif["niveau"].stringValue == "Intermédiaire" && notif["pratique"].stringValue == "Ski de randonnée"
        {
            sportPicture.image = UIImage(named: "Ski de randonnée Intermédiaire")
            
        }
        else if notif["niveau"].stringValue == "Confirmé" && notif["pratique"].stringValue == "Ski de randonnée"
        {
            sportPicture.image = UIImage(named: "Ski de randonnée Confirmé")
            
        }
        else if notif["niveau"].stringValue == "Expert" && notif["pratique"].stringValue == "Ski de randonnée"
        {
            sportPicture.image = UIImage(named: "Ski de randonnée Expert")
            
        }
        else if notif["niveau"].stringValue == "Débrouillard" && notif["pratique"].stringValue == "Ski de randonnée"
        {
            sportPicture.image = UIImage(named: "Ski de randonnée Débrouillard")
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    /*
     * get user profil
     */
    func loadData(){
        if  UserDefaults.standard.value(forKey: "User") != nil {
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.AfficherProfil , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                
                let a = JSON(response.data)
                //   print("saif 1: ",a)
                
                if a["status"].boolValue == false {
                    if a["message"].stringValue == "L utilisateur n existe pas" || a["message"].stringValue == "Mot de passe invalide" {
                        
                        _ = SweetAlert().showAlert(Localization("ConnexionChargement"), subTitle: Localization("ConnexionErreur"), style: AlertStyle.error)
                    }
                }else{
                    self.btnContact.isEnabled = true
                    self.btnPreferences.isEnabled = true
                    self.btnModif.isEnabled = true
                    self.btnDeco.isEnabled = true
                    self.nomUser.text = a["data"]["prenom"].stringValue + " " + a["data"]["nom"].stringValue
                    
                    let placeholder =  UIImage(named: "user_placeholder.png")
                    
                    if (a["data"]["photo"].stringValue).hasPrefix("h") {
                        self.photo_profil.setImage(withUrl: URL(string : a["data"]["photo"].stringValue)! , placeholder: placeholder)
                    }else {
                        self.photo_profil.image = UIImage(named: "user_placeholder.png")
                    }
                    
                    /*  if (a["data"]["sports"].arrayObject?.count != 0)
                     {
                     for i in 0 ... ((a["data"]["sports"].arrayObject?.count)! - 1)
                     {
                     if (a["data"]["sports"][i]["is_default"].boolValue)
                     {
                     self.sportUser.setTitle(a["data"]["sports"][i]["pratique"].stringValue, for: .normal)
                     }
                     }
                     } */
                }
                
                
        }
    }
    }
    /*
     * go to preferences action
     */
    @IBAction func go_to_preferences(_ sender: Any) {
        self.popup.isHidden = true
        let pref = self.storyboard?.instantiateViewController(withIdentifier: "mes_preferencesx") as! Mes_preferences_saif
        self.navigationController?.pushViewController(pref, animated: true)
    }
    /*
     * logout action
     */
    @IBAction func go_to_login(_ sender: Any) {
        self.popup.isHidden = true
        
        SweetAlert().showAlert(Localization("areYouSure"), subTitle: Localization("logout"), style: AlertStyle.warning, buttonTitle:Localization("cancel"), buttonColor: UIColor.gray , otherButtonTitle:  Localization("yes"), otherButtonColor: UIColor.red) { (isOtherButton) -> Void in
            if isOtherButton == false {
                LoaderAlert.shared.show(withStatus: Localization("logout2"))
                let ab = UserDefaults.standard.value(forKey: "User") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : a["value"].stringValue
                ]
                Alamofire.request(ScriptBase.sharedInstance.Logout , method: .get, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        LoaderAlert.shared.dismiss()
                        
                        let b = JSON(response.data!)
                        
                        if b["status"].boolValue {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "saker"), object: nil)
                            self.quitMessanger()
                            StaticPositionController.shared.StopLocation()
                            SocketIOManager.sharedInstance.closeConnection()
                            OneSignal.setSubscription(false)
                            UserDefaults.standard.removeObject(forKey: "User")
                            UserDefaults.standard.synchronize()
                            let login = self.storyboard?.instantiateViewController(withIdentifier: "PageBienvenue") as! PageBienvenue
                            // let presentaion = UIPresentationController(presentedViewController: self, presenting: login)
                            let transition = CATransition()
                            transition.duration = 0.5
                            transition.type = kCATransitionPush
                            transition.subtype = kCATransitionFromLeft
                            //login.view.window!.layer.add(transition, forKey: kCATransition)
                            let navigation = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
                            self.navigationController?.view.window?.layer.add(transition, forKey: kCATransition)
                            self.navigationController?.tabBarController?.tabBar.isHidden = true
                            self.navigationController?.pushViewController(login, animated: true)
                            
                        }
                        else {
                            _ = SweetAlert().showAlert(Localization("logout3"), subTitle: Localization("erreur"), style: AlertStyle.error)
                        }
                }
                
            }
        }
        
        /*  present((UIApplication.shared.delegate as! AppDelegate).navigation, animated: false, completion: {
         
         //login.view.window!.layer.removeAllAnimations()
         }) */
        //(UIApplication.shared.delegate as! AppDelegate).navigation.modalTransitionStyle = .flipHorizontal
        // self.present((UIApplication.shared.delegate as! AppDelegate).navigation, animated: true, completion: nil)
        
        //self.navigationController?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        //self.navigationController?.present((UIApplication.shared.delegate as! AppDelegate).navigation, animated: true)
        // self.navigationController?.present((UIApplication.shared.delegate as! AppDelegate).navigation, animated: true)
        // self.navigationController?.presene
    }
    /*
     * exit node
     */
    func quitMessanger(){
        if (UserDefaults.standard.value(forKey: "User") != nil ) {
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : a["value"].stringValue
            ]
            Alamofire.request(ScriptBase.sharedInstance.afficherConversation , method: .get, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    
                    let b = JSON(response.data)
                    print("b:",b)
                    let c = JSON(b["data"].arrayObject)
                    if c.count != 0 {
                        
                        
                        print("count:",c.count)
                        
                        
                        for  i in 0...c.count - 1 {
                            SocketIOManager.sharedInstance.unscubscribe(Conv: c[i]["id"].stringValue, nickname: a["user"]["id"].stringValue)
                        }
                        
                        
                    }
                    
            }
        }
        else if ( UserDefaults.standard.value(forKey: "Prest") != nil)
        {
            let ab = UserDefaults.standard.value(forKey: "Prest") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : a["value"].stringValue
            ]
            Alamofire.request(ScriptBase.sharedInstance.afficherConversation , method: .get, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    
                    let b = JSON(response.data)
                    print("b:",b)
                    let c = JSON(b["data"].arrayObject)
                    if c.count != 0 {
                        
                        
                        print("count:",c.count)
                        
                        
                        for  i in 0...c.count - 1 {
                            SocketIOManager.sharedInstance.unscubscribe(Conv: c[i]["id"].stringValue, nickname: a["user"]["id"].stringValue)
                        }
                        
                        
                        
                    }
                    
                    
            }
        }
        
    }
    /*
     * go to modify profil
     */
    @IBAction func go_to_ModiferProfil(_ sender: Any) {
        self.popup.isHidden = true
        let modif = self.storyboard?.instantiateViewController(withIdentifier: "modifierprofiluser") as! ModifierProfilUser
        self.navigationController?.pushViewController(modif, animated: true)
    }
    /*
     * hide and show popup
     */
    @IBAction func bring_hide_popup(_ sender: Any) {
        if self.popup.isHidden == true {
            self.popup.isHidden = false
            UIView.animate(withDuration: 0.5/*Animation Duration second*/, animations: {
                self.popup.alpha = 1
            }, completion: nil)
        }else{
            UIView.animate(withDuration: 0.5/*Animation Duration second*/, animations: {
                self.popup.alpha = 0
            }, completion:  {
                (value: Bool) in
                self.popup.isHidden = true
            })
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            if self.btnsettings.transform == .identity {
                self.btnsettings.transform = CGAffineTransform(rotationAngle: CGFloat(.pi * 0.5))
            } else {
                self.btnsettings.transform = .identity
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        labelContacts.text = Localization("labelContact")
        btnPreferences.setTitle(Localization("btnPreferences"), for: .normal)
        btnModif.setTitle(Localization("btnModif"), for: .normal)
        btnDeco.setTitle(Localization("btnDeco"), for: .normal)
        loadData()
        
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
}

