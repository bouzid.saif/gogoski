//
//  ForfaitViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 18/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit

class ForfaitViewController: InternetCheckViewControllers {
    
    @IBOutlet weak var btnStandard: UIButton!
    @IBOutlet weak var btnSaison: UIButton!
    @IBOutlet weak var carteforfait: VKCheckbox!
    @IBOutlet weak var adultes: UILabel!
    @IBOutlet weak var labelCarte: UILabel!
    @IBOutlet weak var dateDepart: PaddingTextField!
    @IBOutlet weak var domaineCouvert: PaddingTextField!
    @IBOutlet weak var dureeForfait: PaddingTextField!
    @IBOutlet weak var nombreForfait: UILabel!
    @IBOutlet weak var children: UILabel!
    @IBOutlet weak var titre: UILabel!
    @IBOutlet weak var labelAdulte: UILabel!
    @IBOutlet weak var ados: UILabel!
    @IBOutlet weak var labelAdos: UILabel!
    @IBOutlet weak var labelEnfant: UILabel!
    @IBOutlet weak var btnPasser: UIButton!
    @IBOutlet weak var btnContinuer: UIButton!
    var testSaison = false
    var testStandard = false
    var nbrAdultes = 0
    var nbrAdos = 0
    var nbrChildren = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        
        carteforfait.line = .normal
        carteforfait.bgColorSelected = UIColor(red: 95/255, green: 182/255, blue: 255/255, alpha: 1)
        carteforfait.bgColor          = UIColor.white
        carteforfait.color            = UIColor.white
        carteforfait.borderColor      = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
        carteforfait.borderWidth      = 2
        // check1.cornerRadius     = check1.frame.height / 2
        carteforfait.checkboxValueChangedBlock = {
            isOn in
            print("Custom checkbox is \(isOn ? "ON" : "OFF")")
            if isOn {
                self.carteforfait.borderWidth      = 0
                self.carteforfait.borderColor      = UIColor.white
            } else{
                self.carteforfait.borderWidth      = 2
                self.carteforfait.borderColor      = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func SaisonClick(_ sender: Any) {
        testSaison = true
        if (testStandard)
        {
            btnStandard.setBackgroundImage(UIImage(named : "Rectangle_ahmed"), for: .normal)
            btnStandard.setTitleColor(.black, for: .normal)
        }
        btnSaison.setBackgroundImage(UIImage(named : "RectangleBlue_ahmed"), for: .normal)
        btnSaison.setTitleColor(.white, for: .normal)
    }
    @IBAction func StandardClick(_ sender: Any) {
        testStandard = true
        if (testSaison)
        {
            btnSaison.setBackgroundImage(UIImage(named : "Rectangle_ahmed"), for: .normal)
            btnSaison.setTitleColor(.black, for: .normal)
        }
        btnStandard.setBackgroundImage(UIImage(named : "RectangleBlue_ahmed"), for: .normal)
        btnStandard.setTitleColor(.white, for: .normal)
    }
    
    @IBAction func removeAdulte(_ sender: Any) {
        if (nbrAdultes > 0)
        {
            nbrAdultes = nbrAdultes - 1
            adultes.text = String (nbrAdultes)
        }
    }
    @IBAction func addAdulte(_ sender: Any) {
        nbrAdultes = nbrAdultes + 1
        adultes.text = String (nbrAdultes)
    }
    @IBAction func addAdos(_ sender: Any) {
        nbrAdos = nbrAdos + 1
        ados.text = String (nbrAdos)
    }
    @IBAction func removeAdos(_ sender: Any) {
        if (nbrAdos > 0)
        {
            nbrAdos = nbrAdos - 1
            ados.text = String (nbrAdos)
        }
    }
    @IBAction func addChild(_ sender: Any) {
        nbrChildren = nbrChildren + 1
        children.text = String (nbrChildren)
    }
    @IBAction func removeChild(_ sender: Any) {
        if (nbrChildren > 0)
        {
            nbrChildren = nbrChildren - 1
            children.text = String (nbrChildren)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("ForfaitTitre")
        labelAdulte.text = Localization("Adultes")
        labelAdos.text = Localization("Ados")
        labelEnfant.text = Localization("Enfant")
        labelCarte.text = Localization("CarteForfait")
        btnSaison.setTitle(Localization("BtnSaison"), for: .normal)
        btnStandard.setTitle(Localization("BtnStandard"), for: .normal)
        btnContinuer.setTitle(Localization("BtnContinuer"), for: .normal)
        btnPasser.setTitle(Localization("BtnPasser"), for: .normal)
        nombreForfait.text = Localization("NombreForfait")
            dureeForfait.placeholder = Localization("DuréeForfait")
            domaineCouvert.placeholder = ("DomaineCouvert")
            dateDepart.placeholder = ("DateDep")
       
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
}

