//
//  ProfilPrestataireVC.swift
//  GoGoSkiUiKissou
//
//  Created by AYMEN on 10/16/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import MapleBacon
import OneSignal
class ProfilPrestataireVC: InternetCheckRed ,WWCalendarTimeSelectorProtocol , UIScrollViewDelegate  {
    /*
     *
     */
    @IBOutlet weak var scrollView: UIScrollView!
    ///header
    /*
     *
     */
    @IBOutlet weak var btnDéconex: UIButton!
    /*
     *
     */
    @IBOutlet weak var deconnectionLBL: UILabel!
    /*
     *
     */
    @IBOutlet weak var btnModifierProfil: UIButton!
    /*
     *
     */
    @IBOutlet weak var décoBtn: UIButton!
    /*
     *
     */
    @IBOutlet weak var modifierProfilBtn: UIButton!
    /*
     *
     */
    @IBOutlet weak var modProfileLBL: UILabel!
    /*
     *
     */
    @IBOutlet weak var menuBTN: UIButton!
    /*
     *
     */
    @IBOutlet weak var preferenceLBL: UILabel!
    /*
     *
     */
    @IBOutlet weak var preferenceBTN: UIButton!
    /*
     *
     */
    @IBOutlet weak var profileTopLBL: UILabel!
    /*
     *
     */
    @IBOutlet weak var aproposLBL: UILabel!
    /*
     *
     */
    @IBOutlet weak var mesIndiponibiliteLBL: UILabel!
    /*
     *
     */
    @IBOutlet weak var ajouterUneIndisLBL: UILabel!
    /*
     *
     */
    @IBOutlet weak var prestationProposeLBL: UILabel!
    /*
     *
     */
    @IBOutlet weak var ajouterPrestationLBL: UILabel!
    /*
     *
     */
    @IBOutlet weak var MoniteurLBL: UILabel!
    /*
     *
     */
    @IBOutlet weak var ajouterMoniteurLBL: UILabel!
    /*
     *
     */
    @IBOutlet weak var aproposTV: UILabel!
    /*
     *
     */
    @IBOutlet weak var aproposViewContainerHeight: NSLayoutConstraint!
    /*
     *
     */
    @IBOutlet weak var theWholeViewContainetHeight: NSLayoutConstraint!
    /*
     *
     */
    @IBOutlet weak var couverutrePic: UIImageView!
    /*
     *
     */
    @IBOutlet weak var profilePic: UIImageView!
    /*
     *
     */
    @IBOutlet weak var titleLBL: UILabel!
    /*
     *
     */
    @IBOutlet weak var popUpOptionsView: UIView!
    /*
     *
     */
    @IBOutlet weak var prestationProposeTV: UITableView!
    /*
     *
     */
    @IBOutlet var indisTableView: UITableView!
    /*
     *
     */
    @IBOutlet weak var moniteursTV: UITableView!
    /*
     *
     */
    @IBOutlet weak var indisTableHeight: NSLayoutConstraint!
    /*
     *
     */
    @IBOutlet weak var monitorTableHeight: NSLayoutConstraint!
    /*
     *
     */
    @IBOutlet weak var prestationTVHeight: NSLayoutConstraint!
    /*
     *
     */
    @IBOutlet weak var aproposView: UIView!
    
    
    var json : JSON!
    var MonProfileJson : JSON!
    var indisDataSource : DatasourceIndis!
    var moniteurDataSource : DataSourceMoniteur!
    
    var listPrestation = [PrestationOBJ]()
    var listMoniteur = [MoniteurOBJ]()
    var listIndisponibilite = [IndisponibiliteOBJ]()
    
    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
    let niveauColor = [UIColor("#76EC9E"),UIColor("#76EC9E"),UIColor("#75A7FF"),UIColor("#FF3E53"),UIColor("#4D4D4D")]
    
    var thisProfilePictureString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        profilePic.layer.borderColor = UIColor.white.cgColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        
        indisDataSource = DatasourceIndis()
        moniteurDataSource = DataSourceMoniteur()
        
        scrollView.delegate = self
        // prestationProposeTV.indicatorStyle =
        //  indisTableView
        //  moniteursTV
        
        configureViewFromLocalisation()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let previousHeightsss = indisTableHeight.constant + monitorTableHeight.constant + prestationTVHeight.constant
        if listIndisponibilite.count > 2 {
            UIView.animate(withDuration: 0.5, animations: {
                self.indisTableHeight.constant = CGFloat(2 * 110)
            })
        }
        
        if listMoniteur.count > 2 {
            UIView.animate(withDuration: 0.5, animations: {
                self.monitorTableHeight.constant = CGFloat(2 * 116)
            })
        }
        
        if listPrestation.count > 2 {
            UIView.animate(withDuration: 0.5, animations: {
                self.prestationTVHeight.constant = CGFloat(2 * 160)
            })
        }
        
        let currentHeights = indisTableHeight.constant + monitorTableHeight.constant + prestationTVHeight.constant
        
        theWholeViewContainetHeight.constant =  theWholeViewContainetHeight.constant - previousHeightsss + currentHeights
        getMonProfil()
        closeMenuIfAllreadyOpened()
    }
    
    func getMonProfil(){
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json" ,
            "x-Auth-Token" : json["value"].stringValue
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.monProfilPres , method: .get , encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                let json = JSON(response.data)
                print(json)
                if json["status"].boolValue == false {
                    _ = SweetAlert().showAlert("", subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                }else{
                    // _ = SweetAlert().showAlert("get profile", subTitle: "Déconnecté", style: AlertStyle.warning)
                    self.listMoniteur.removeAll()
                    self.listIndisponibilite.removeAll()
                    self.listPrestation.removeAll()
                    
                    let moiteurList = json["data"]["list_moniteur"]
                    let indisList = json["data"]["list_indispo"]
                    let prestationLis = json["data"]["list_prestation"]
                   
                    
                    //looping through all the json objects in the array teams
                    for i in 0 ..< moiteurList.count{
                        
                        let newMoniteur = MoniteurOBJ()
                        
                        newMoniteur.id = moiteurList[i]["id"].int!
                        newMoniteur.nom = moiteurList[i]["nom"].stringValue
                        newMoniteur.prenom = moiteurList[i]["prenom"].stringValue
                        newMoniteur.photo = moiteurList[i]["photo"].stringValue
                        
                        print(moiteurList[i]["priorite"].description)
                        
                        if let priorite = moiteurList[i]["priorite"].int {
                            newMoniteur.priorite = priorite
                            print("heyy yaaaw",priorite)
                        }
                        
                        for j in 0 ..< moiteurList[i]["sports"].count{
                            let newSport = SportOBJ()
                            newSport.id = moiteurList[i]["sports"][j]["id"].int!
                            newSport.pratique = moiteurList[i]["sports"][j]["pratique"].stringValue
                            newSport.niveau = moiteurList[i]["sports"][j]["niveau"].stringValue
                            
                            newMoniteur.sports.append(newSport)
                        }
                        self.listMoniteur.append(newMoniteur)
                    }
                    
                    for i in 0 ..< indisList.count{
                        
                        let newIndis = IndisponibiliteOBJ()
                        let dateDebutString = indisList[i]["date_debut"].stringValue
                        let dateFinString = indisList[i]["date_fin"].stringValue
                        
                        newIndis.dateDebut = self.convertDateFormater(String(dateDebutString.prefix(10)))
                        newIndis.dateFin = self.convertDateFormater(String(dateFinString.prefix(10)))
                        newIndis.id = indisList[i]["id"].int!
                        self.listIndisponibilite.append(newIndis)
                    }
                    
                    for i in 0 ..< prestationLis.count{
                        
                        let newPrestation = PrestationOBJ()
                        
                        let newTarif = TarifOBJ()
                        newTarif.prix = prestationLis[i]["tarifs"][0]["prix_max"].doubleValue
                        newPrestation.tarifs = newTarif
                        
                        let newSport = SportOBJ()
                        newSport.niveau = prestationLis[i]["sport"]["niveau"].stringValue
                        newSport.pratique = prestationLis[i]["sport"]["pratique"].stringValue
                        newPrestation.sport = newSport
                        
                        newPrestation.id = prestationLis[i]["id"].intValue
                        newPrestation.titre = prestationLis[i]["titre"].stringValue
                        
                        newPrestation.nombreParticipantMax = prestationLis[i]["nombre_participant"].intValue
                        newPrestation.nombreParticipantMin = prestationLis[i]["nombre_participant_min"].intValue
                        
                        newPrestation.duree = prestationLis[i]["duree"].stringValue
                        newPrestation.nombreParticipantMin = prestationLis[i]["nombre_participant_min"].intValue
                        
                        if newPrestation.titre == "prattttt"{
                            print("prattttttt dureeeee",newPrestation.duree)
                        }
                        self.listPrestation.append(newPrestation)
                    }
                    
                    self.setTablesDelegate()
                    self.setApropos(apropos : json["data"]["description_prestataire"].stringValue)
                    self.titleLBL.text = json["data"]["nom_etablissement"].string!
                    if (json["data"]["photo"].string!.first! == "h" ){
                        print("profilepic url",json["data"]["photo"].string!)
                        self.profilePic.setImage(withUrl: URL(string: json["data"]["photo"].string!)!, placeholder: #imageLiteral(resourceName: "user_placeholder") , crossFadePlaceholder: false, cacheScaled: true, completion: nil)
                        self.thisProfilePictureString =  json["data"]["photo"].string!
                    }else{
                        print("nothing happening with the photo")
                        self.profilePic.image = #imageLiteral(resourceName: "user_placeholder")
                    }
                }
        }
    }
    
    func setTablesDelegate(){
        
        refreshSizesIndis(indisT: listIndisponibilite)
        refreshSizesMonitor(monitorT: listMoniteur)
        let xh = CGFloat(self.theWholeViewContainetHeight.constant)

        refreshSizesPrestation(prestationT: listPrestation)
        
        indisDataSource.tableIndis.removeAll()
        indisDataSource.tableIndis.append(contentsOf: listIndisponibilite)
        indisDataSource.previousVCSet(presVC : self)
        indisTableView.dataSource = indisDataSource
        moniteurDataSource.arrayMoniteurs.removeAll()
        moniteurDataSource.arrayMoniteurs = listMoniteur
        moniteurDataSource.previousVCSet(presVC: self)
        moniteursTV.dataSource = moniteurDataSource
        
        moniteursTV.reloadData()
        indisTableView.reloadData()
        prestationProposeTV.reloadData()

    }
    
    func refreshSizesIndis(indisT : [IndisponibiliteOBJ] ){
        let previousHeightss = indisTableHeight.constant
        
        if indisT.count > 2 {
            UIView.animate(withDuration: 0.5, animations: {
                self.indisTableHeight.constant = CGFloat(indisT.count * 110)
            })
        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.indisTableHeight.constant = CGFloat(2 * 110)
            })
        }
        
        let currentHeightss = indisTableHeight.constant
        
        theWholeViewContainetHeight.constant =  theWholeViewContainetHeight.constant - previousHeightss + currentHeightss
    }
    
    func refreshSizesMonitor(monitorT : [MoniteurOBJ] ){
        let previousHeightss =  monitorTableHeight.constant
        let monitorCount = monitorT.count
        if monitorCount > 2 {
            UIView.animate(withDuration: 0.5, animations: {
                self.monitorTableHeight.constant = CGFloat(monitorCount * 116)
            })
        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.monitorTableHeight.constant = CGFloat(2 * 116)
            })
        }
        let currentHeightss =  monitorTableHeight.constant
        
        theWholeViewContainetHeight.constant =  theWholeViewContainetHeight.constant - previousHeightss + currentHeightss
    }
    
    func refreshSizesPrestation(prestationT : [PrestationOBJ]  ){
        let previousHeightss =  prestationTVHeight.constant
        let prestationCount = prestationT.count
        if prestationCount > 2 {
            UIView.animate(withDuration: 0.5, animations: {
                self.prestationTVHeight.constant = CGFloat(prestationCount * 160)
            })
        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.prestationTVHeight.constant = CGFloat(2 * 160)
            })
        }
        let currentHeightss = prestationTVHeight.constant
        
        theWholeViewContainetHeight.constant =  theWholeViewContainetHeight.constant - previousHeightss + currentHeightss
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      /*  if segue.identifier == "modifierPrestation"{
            if let prestation = selectedPrestationToUpdate {
                let receiver = ModifierPrestationViewController()
                receiver.prestationAModifier = prestation
                print("pppppppppppppp", prestation.titre , prestation.duree)
            }else{
                print("nnnnnnnnnnn")
            }
        }
 */
    }

    ///*********************************** Indiponibilite Picker *******************************/
    func showCalendar() {
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
        selector.delegate = self
        
        selector.optionSelectionType = WWCalendarTimeSelectorSelection.range
        
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        
        selector.optionCalendarFontColorMonth = UIColor.black
        selector.optionSelectorPanelBackgroundColor = UIColor("#F44336")
        selector.optionCalendarBackgroundColorPastDatesHighlight = UIColor("#BA000D")
        selector.optionCalendarBackgroundColorFutureDatesHighlight = UIColor("#BA000D")
        selector.optionCalendarBackgroundColorTodayHighlight = UIColor("#BA000D")
        selector.optionTopPanelBackgroundColor = UIColor("#BA000D")
        selector.optionButtonFontColorDone =  UIColor("#F44336")
        
        selector.optionButtonShowCancel = true
        selector.optionButtonFontColorCancel =  UIColor("#F44336")
        
        // selector.verysho
        present(selector, animated: true, completion: nil)
    }
    
    func WWCalendarTimeSelectorDone(selector: WWCalendarTimeSelector, date: Date) {
        print(date)
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
        print("Selected Multiple Dates \n\(dates)\n---")
        if let date = dates.first {
            singleDate = date
            //  dateLabel.text = date.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
        }
        else {
            //    dateLabel.text = "No Date Selected"
        }
        multipleDates.removeAll()
        multipleDates = dates
        print("Selected Multiple Dates \n\(dates)\n---")
        print(multipleDates)
        print("multiple Dates ")
        ajouterIndisToServer()
    }
    
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        if date <= Date(){
            return false
        }
        return true
    }
    
    //********* indis add***////////
    func ajouterIndisToServer(){
        
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : json["value"].stringValue
        ]
        print("multiple dates count : ", multipleDates.count)
        var dateD , dateF : String!
        
        
        let params: Parameters = [
            "date_debut" : multipleDates.first?.stringFromFormat("yyyy-MM-dd") as! String,
            "date_fin" : multipleDates.last?.stringFromFormat("yyyy-MM-dd") as! String
        ]
        Alamofire.request(ScriptBase.sharedInstance.creerIndis , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                if let bb = response.data {
                    let b  = JSON(bb)
                    print("indisponibilité: ",b)
                    
                    if b["status"].boolValue {
                        
                        if b["data"].boolValue {
                            
                            _ = SweetAlert().showAlert(Localization("ajoutIndis"), subTitle: Localization("ajoutSucess"), style: AlertStyle.success)
                            self.getMonProfil()
                        }else{
                            _ = SweetAlert().showAlert(Localization("ajoutIndis"), subTitle: Localization("ajoutIndisContrainte"), style: AlertStyle.error)
                        }
                    }else{
                        _ = SweetAlert().showAlert(Localization("ajoutIndis"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                    }
                }
        }
        LoaderAlert.shared.dismiss()
    }
    //********* indis add***////////
    
    ///*********************************** Indiponibilite Picker *******************************/
    
    func getData(){
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        json = JSON(data: dataFromString!)
        // setApropos()
    }
    
    func setApropos(apropos : String){
        if apropos.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
           
          
            let x = 125 - aproposTV.frame.height
            let y = aproposViewContainerHeight.constant
            aproposTV.numberOfLines = 0
            aproposTV.text = apropos
            aproposTV.sizeToFit()
            UIView.animate(withDuration: 0.5, animations: {
                self.aproposViewContainerHeight.constant = x + self.aproposTV.frame.height
                self.theWholeViewContainetHeight.constant = self.theWholeViewContainetHeight.constant - y +  self.aproposViewContainerHeight.constant
                self.aproposView.alpha = 1
            })

        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.aproposViewContainerHeight.constant = 0
                self.aproposView.alpha = 0
            })
        }
    }
    
    @IBAction func ajouterIndiponibilite(_ sender: Any) {
        showCalendar()
    }
    
    @IBAction func showHideMenu(_ sender: Any) {
        if popUpOptionsView.isHidden {
            popUpOptionsView.isHidden = false
            UIView.animate(withDuration: 0.5/*Animation Duration second*/, animations: {
                self.popUpOptionsView.alpha = 1
            }, completion: nil)
        }else{
            UIView.animate(withDuration: 0.5/*Animation Duration second*/, animations: {
                self.popUpOptionsView.alpha = 0
            }, completion:  {
                (value: Bool) in
                self.popUpOptionsView.isHidden = true
            })
        }
        let myButton = sender as! UIButton
        UIView.animate(withDuration: 0.5, animations: {
            if myButton.transform == .identity {
                myButton.transform = CGAffineTransform(rotationAngle: CGFloat(.pi * 0.5))
            } else {
                myButton.transform = .identity
            }
        })
    }
    
    @IBAction func goToPreferences(_ sender: Any) {
    }
    
    @IBAction func goToModifierProfil(_ sender: Any) {
        
    }
    
    @IBAction func disconnect(_ sender: Any) {
        
        
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json" ,
            "x-Auth-Token" : json["value"].stringValue
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.disconnectPres , method: .get , encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                if let bb = response.data {
                    let json = JSON(bb)
                    if json["status"].boolValue == false {
                        print("response 1 : " , response)

                        _ = SweetAlert().showAlert(Localization("deconnecxion"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                    }else{
                        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
                       // appDelegate.quitMessanger()
                       // _ = SweetAlert().showAlert(Localization("deconnecxion"), subTitle: Localization("deconnecxionTermine"), style: AlertStyle.success)
                        
                        let login = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PageBienvenue") as! PageBienvenue
                        // let presentaion = UIPresentationController(presentedViewController: self, presenting: login)
                        let transition = CATransition()
                        transition.duration = 0.5
                        transition.type = kCATransitionPush
                        transition.subtype = kCATransitionFromLeft
                        //login.view.window!.layer.add(transition, forKey: kCATransition)
                        //  let navigation = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
                       SocketIOManager.sharedInstance.closeConnection()
                        let view = self.storyboard?.instantiateViewController(withIdentifier: "ConnextionPrestataire") as! ConnextionPrestataire
                        view.DisconnectFromPush()
                         //OneSignal.remove(ConnexionUser as OSSubscriptionObserver)
                       OneSignal.setSubscription(false)
                        UserDefaults.standard.removeObject(forKey: "presConn")
                        UserDefaults.standard.synchronize()
                        self.navigationController?.view.window?.layer.add(transition, forKey: kCATransition)
                        self.navigationController?.tabBarController?.tabBar.isHidden = true
                        self.navigationController?.pushViewController(login, animated: true)
                      
                    }
                }else{
                    print("response : " , response)
                    _ = SweetAlert().showAlert(Localization("deconnecxion"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                }
        }
    }
    
    /////////********langue"**********///////
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    
    func configureViewFromLocalisation() {
        
        profileTopLBL.text = Localization("ProfilPrestataire")
        aproposLBL.text = Localization("ProfilPrestataireAPropos")
        
        mesIndiponibiliteLBL.text = Localization("ProfilPrestataireMesIndis")
        ajouterUneIndisLBL.text = Localization("ProfilPrestataireAjouterIndis")
        
        prestationProposeLBL.text = Localization("ProfilPrestatairePrestationPropose")
        ajouterPrestationLBL.text = Localization("ProfilPrestataireAjouterUnePrestation")
        
        MoniteurLBL.text = Localization("ProfilPrestataireMoniteur")
        ajouterMoniteurLBL.text = Localization("ProfilPrestataireAjouterMoniteur")
        
        preferenceLBL.text = Localization("ProfilPrestatairePreference")
        modProfileLBL.text = Localization("ProfilPrestataireModifierProfile")
        deconnectionLBL.text = Localization("ProfilPrestataireDeconn")
        
        Localisator.sharedInstance.saveInUserDefaults = true
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        closeMenuIfAllreadyOpened()
    }
    
    func closeMenuIfAllreadyOpened(){
        
        if !popUpOptionsView.isHidden {
            UIView.animate(withDuration: 0.5/*Animation Duration second*/, animations: {
                self.popUpOptionsView.alpha = 0
            }, completion:  {
                (value: Bool) in
                self.popUpOptionsView.isHidden = true
            })
            UIView.animate(withDuration: 0.5, animations: {
                if self.menuBTN.transform == .identity {
                    self.menuBTN.transform = CGAffineTransform(rotationAngle: CGFloat(.pi * 0.5))
                } else {
                    self.menuBTN.transform = .identity
                }
            })
        }
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd-MM-yy"
        let formatWanted = dateFormatter.string(from: date!)
        
        return  formatWanted.replacingOccurrences(of: "-", with: " / ")
    }
}

extension ProfilPrestataireVC : UITableViewDelegate , UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listPrestation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let presCell = tableView.dequeueReusableCell(withIdentifier: "prestationProposeCell", for: indexPath) as! PrestationProposeCell

        if ( thisProfilePictureString != ""  ){
            if (thisProfilePictureString.first! == "h" ){
                presCell.imagePres.setImage(withUrl: (URL(string : thisProfilePictureString))!, placeholder: #imageLiteral(resourceName: "user_placeholder") , crossFadePlaceholder: false, cacheScaled: true, completion: nil)
            }else{
                print("nothing happening with the photo")
                presCell.imagePres.image = #imageLiteral(resourceName: "user_placeholder")
            }
        }else{
            presCell.imagePres.image = #imageLiteral(resourceName: "user_placeholder")
        }

        presCell.nomPres.text = listPrestation[indexPath.row].titre
        presCell.dureePres.text = Localization("Duree") + " :" + listPrestation[indexPath.row].duree
        presCell.nmbrePersonnePRes.text = String(listPrestation[indexPath.row].nombreParticipantMax) + " " +  Localization("personne")
        let tarifPerHour = getTarifToShow(tarif : listPrestation[indexPath.row].tarifs.prix , duree: listPrestation[indexPath.row].duree)
        presCell.prixParHeure.setTitle(String(format: "%.2f", arguments: [tarifPerHour]) + " €/h", for: .normal)
        
        presCell.sportLBL.text = listPrestation[indexPath.row].sport.pratique
        
        switch listPrestation[indexPath.row].sport.niveau {
        case "Débutant"? : presCell.sportNiveauView.backgroundColor = niveauColor[0]
        case "Débrouillard"? : presCell.sportNiveauView.backgroundColor = niveauColor[1]
        case "Intermédiaire"? : presCell.sportNiveauView.backgroundColor = niveauColor[2]
        case "Confirmé"? : presCell.sportNiveauView.backgroundColor = niveauColor[3]
        case "Expert"? : presCell.sportNiveauView.backgroundColor = niveauColor[4]
        default : break
        }
        
        //delete
        presCell.deletePres.tag = indexPath.row
        presCell.deletePres.addTarget(self, action: #selector(supprimerPrestationSelector(sender: )) , for: .touchUpInside)
        
        presCell.editPres.tag = indexPath.row
        presCell.editPres.addTarget(self, action: #selector(modifierMoniteurAction(sender: )) , for: .touchUpInside)
        
        return presCell
    }
    
}
////prestation job

extension ProfilPrestataireVC {
    
    @objc func modifierMoniteurAction(sender : UIButton){
        sender.isEnabled = false
        let modPresVC = UIStoryboard(name: "Prestataire", bundle : nil).instantiateViewController(withIdentifier: "modifierPrestationVC") as! ModifierPrestationViewController
        modPresVC.prestationAModifier = listPrestation[sender.tag]
        self.navigationController?.pushViewController(modPresVC, animated: true)
        sender.isEnabled = true
    }
    
    @objc func supprimerPrestationSelector(sender : UIButton){

          SweetAlert().showAlert(Localization("SupressionPresT"), subTitle: Localization("suppPres"), style: AlertStyle.warning, buttonTitle:Localization("annuler"), buttonColor: UIColor.gray , otherButtonTitle:  Localization("continuer"), otherButtonColor: UIColor.red) { (isOtherButton) -> Void in
            
            if (!isOtherButton) {
            //  let xh = CGFloat(self.theWholeViewContainetHeight.constant)
                            LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
                            sender.isEnabled = false
                
                            let ab = UserDefaults.standard.value(forKey: "pres") as! String
                            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                            self.json = JSON(data: dataFromString!)
                
                            let id : Int = self.listPrestation[sender.tag].id
                
                            let header: HTTPHeaders = [
                                "Content-Type" : "application/json" ,
                                "x-Auth-Token" : self.json["value"].stringValue
                            ]
                
                            let paramsz: Parameters = [
                                "id" : id
                            ]
                
                            Alamofire.request(ScriptBase.sharedInstance.supprimerPrestation , method: .post, parameters: paramsz, encoding: JSONEncoding.default,headers : header)
                                .responseJSON { response in
                                    LoaderAlert.shared.dismiss()
                                    if let bb = response.data {
                                        let json = JSON(bb)
                                        if json["status"].boolValue == false {
                                            print(json)
                                            _ = SweetAlert().showAlert(Localization("SupressionPresT"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                                        }else{
                                            _ = SweetAlert().showAlert(Localization("SupressionPresT"), subTitle: Localization("presSupprime"), style: AlertStyle.success)
                                            self.listPrestation.remove(at: sender.tag)
                                            self.prestationProposeTV.reloadData()
                                            self.refreshSizesPrestation(prestationT: self.listPrestation)

                                        }
                                        sender.isEnabled = true
                                    }else{
                                        sender.isEnabled = true
                                    }
                            }
            }
        }
    }
    
    func getTarifToShow(tarif : Double , duree : String) -> Double{
        //var const = 1.0
        if let dureeInt = Double(duree){
            
           return (tarif / (dureeInt / 60))
        }
        return tarif
    }
    
}

