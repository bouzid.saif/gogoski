//
//  MonthCalendarViewController.swift
//  GoGoSkiUiKissou
//
//  Created by AYMEN on 10/28/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import JTAppleCalendar
import Presentr
class MonthCalendarViewController: InternetCheckRed {

    @IBOutlet weak var calendarCollection: JTAppleCalendarView!
    @IBOutlet weak var montYearLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var bottomView: UIView!
    let myCalendar = Calendar.current

    var todayIndexPath : IndexPath!
    var todaycell : JTAppleCell!
    var todayDate : Date!
    var todayCellState : CellState!
    let numberOfRowsCalendar : Int! = 6
    let xarray = ["1","7","9","8","2","3","1","5","6","8"]
    let formatter  = DateFormatter()
    var weekView  = ClickInsideUIVIEW()
    var drawOnce = true
    var testCalendar = Calendar.current
    var thisVisibleDate : DateSegmentInfo!
    var deselect : Bool = true
    
    var passedParam : Date!
    ///
 //   let customTransitionDelegate = TransitionnngDelegate()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        modalPresentationStyle = .custom
       // transitioningDelegate = customTransitionDelegate
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calendarCollection.visibleDates { visibleDates in
            self.switchMonthYear(from: visibleDates)
        }
        calendarCollection.scrollToDate(passedParam)
        calendarCollection.selectDates([passedParam])

        
  /*      let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeUp.direction = UISwipeGestureRecognizerDirection.up
       // self.calendarCollection.addGestureRecognizer(swipeUp)
        let tapgesture = UITapGestureRecognizer(target: self,action: #selector(self.tapthere))
        tapgesture.numberOfTapsRequired = 1
        self.bottomView.addGestureRecognizer(tapgesture)
 */
        let panUp = UIPanGestureRecognizer(target: self, action: #selector(self.handleGesture(_:)))
        panUp.delegate = self
        self.view.addGestureRecognizer(panUp)
       
        let tapgesture = UITapGestureRecognizer(target: self,action: #selector(self.tapthere))
        tapgesture.numberOfTapsRequired = 1
        self.bottomView.addGestureRecognizer(tapgesture)
        
    }
   
    
    @objc func tapthere(gesture: UIGestureRecognizer){
        if let tapGesture = gesture as? UITapGestureRecognizer{
            if tapGesture.state == UIGestureRecognizerState.recognized{
                print(tapGesture.location(in: tapGesture.view))
               // if tapGesture.location(in: tapGesture.view).y > 511.0 {
                
                    dismiss(animated: true, completion: nil)
               // }
            }
        }
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
             
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
                var initialFrame = self.view.frame
              //  self.view.frame.origin.y = 0 - initialFrame.size.height
               //dismiss(animated: true, completion: nil)
                
            default:
                break
            }
        }
    }
    
    var interactionController: UIPercentDrivenInteractiveTransition?
   
    @objc func handleGesture(_ gesture: UIPanGestureRecognizer) {
        let translate = gesture.translation(in: gesture.view)
        let percent   = -translate.y / gesture.view!.bounds.size.height
        
        if gesture.state == .began {
            interactionController = UIPercentDrivenInteractiveTransition()
        //    customTransitionDelegate.interactionController = interactionController
            
            dismiss(animated: true)
        } else if gesture.state == .changed {
            interactionController?.update(percent)
        } else if gesture.state == .ended {
            let velocity = gesture.velocity(in: gesture.view)
            if (percent > 0.5 && velocity.y == 0) || velocity.y < 0 {
                interactionController?.finish()
            } else {
                interactionController?.cancel()
            }
            interactionController = nil
        }
    }
    
  
    
    func handleSelectedCellBackground(_ view: JTAppleCell? , cellstate: CellState){
        
        guard  let validCellMonth = view as? CustomCellCalendar else {
            return
        }
            if validCellMonth.isSelected {
                validCellMonth.selectedView.isHidden = false
            }else{
                validCellMonth.selectedView.isHidden = true
            }
     
        // this one here to make sure that we don't lose constraints on runtime :in this case i am pointing to space between uitableview and our headerviewcontainer (UIView)
    }
    
    func handleDaysLabelsColor(_ view: JTAppleCell? , cellstate: CellState){
        let red = self.hexStringToUIColor(hex: "FF3C00" , alphaa: 1.0)
        let redTrans = self.hexStringToUIColor(hex: "FF3C00" , alphaa: 0.3)
        let redDark = self.hexStringToUIColor(hex: "BA000D" , alphaa: 1.0)
        let whiteTrans = self.hexStringToUIColor(hex: "FFFFFF" , alphaa: 0.3)
        
        guard let validCell = view as? CustomCellCalendar else { return }
            
            if validCell.isSelected {
                validCell.eventBackground.backgroundColor = redDark
                validCell.labelCell.textColor = red
                validCell.eventsNumber.textColor = UIColor.white
            }else{
                if cellstate.dateBelongsTo == .thisMonth {
                    validCell.labelCell.textColor = UIColor.white
                    validCell.eventsNumber.textColor = red
                    validCell.eventBackground.backgroundColor = UIColor.white
                    
                }else{
                    validCell.eventBackground.backgroundColor = whiteTrans
                    validCell.labelCell.textColor = whiteTrans
                    validCell.eventsNumber.textColor = redTrans
                }
            }
    
    }
    
    func switchMonthYear(from visibleDates :DateSegmentInfo){
        
        let date = visibleDates.monthDates.first!.date
        self.formatter.dateFormat = "MMMM dd yyyy"
        self.montYearLabel.text = self.formatter.string(from: date)
    }
    
   
    
    @IBAction func scrollToNextMonth(_ sender: Any) {
        calendarCollection.scrollToSegment(.next)
        weekView.isHidden = true
    }
    
    @IBAction func scrollToPreviousMonth(_ sender: Any) {
        calendarCollection.scrollToSegment(.previous)
        weekView.isHidden = true
    }
}


extension MonthCalendarViewController :  JTAppleCalendarViewDelegate{
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        
        
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "YYYY MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        let startDate = formatter.date(from: "2016 01 01")!
        let endDate = formatter.date(from: "2019 12 31")!
        
        let parameters = ConfigurationParameters(startDate: startDate, endDate: endDate , numberOfRows: numberOfRowsCalendar , generateInDates: .forAllMonths,
                                                 generateOutDates: .tillEndOfRow,
                                                 firstDayOfWeek: .sunday)
        return parameters
    }
}

extension MonthCalendarViewController : JTAppleCalendarViewDataSource {
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        var cellFrame : CGRect!
        
        let cellMonth  : CustomCellCalendar! = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCellCalendar", for: indexPath) as! CustomCellCalendar
        cellMonth.todayView.isHidden = true
        
        cellMonth.labelCell.text = cellState.text
        cellMonth.eventsNumber.text = xarray[ indexPath.row % 10 ]
        handleSelectedCellBackground(cellMonth , cellstate: cellState)
        handleDaysLabelsColor(cellMonth , cellstate: cellState)
        
        if myCalendar.isDateInToday(date) {

            cellFrame = cellMonth.frame
            // drawView on this week
            cellMonth.todayView.isHidden = false
            cellMonth.todayView.backgroundColor = self.hexStringToUIColor(hex: "BA000D" , alphaa: 1.0)
            
          //  cellMonth.selectedView.isHidden = false
            todayIndexPath = indexPath
            todaycell = cellMonth
            todayDate = date
            todayCellState = cellState
            drawTranslateWeekMask(cellFrame: cellFrame)
        }
        
        if passedParam == date {
          //  drawTranslateWeekMask(cellFrame: cellFrame)
           // cellMonth.isSelected = true
        }
        
        return cellMonth
    }
    
    func drawTranslateWeekMask(cellFrame : CGRect){
        weekView.isHidden = false
        
        if drawOnce {
            let testFrame : CGRect = CGRect(x: 8 , y: cellFrame.minY + 35, width: headerView.frame.width - 16, height: cellFrame.height )
            
            weekView = ClickInsideUIVIEW(frame: testFrame)
            weekView.backgroundColor = UIColor.white
            weekView.alpha = 0.2
            weekView.layer.cornerRadius = 10
            headerView.addSubview(weekView)
            weekView.isHidden = false
            drawOnce = false
            
        }else if weekView.frame.minY + 35 != (cellFrame.minY + 35) {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                //Frame Option 1:
                self.weekView.frame = CGRect(x: 8, y: cellFrame.minY + 35 , width: self.weekView.frame.width, height: cellFrame.height)
                
            },completion: nil)
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {

         if cellState.dateBelongsTo != .thisMonth {
         
         return
        }
        
      //  print("date " , date , " cellstate : " ,  cellState.dateSection(CellNow) )
        
        handleSelectedCellBackground(cell , cellstate: cellState)
        handleDaysLabelsColor(cell , cellstate: cellState)
        cell?.bounce()
        if date == passedParam || date == Date(){
            return
        }
        let cell = cell as! CustomCellCalendar
        drawTranslateWeekMask(cellFrame: cell.frame)
        headerlabel(day: cellState.day , datee : date )
    }
    
    func headerlabel(day : DaysOfWeek , datee : Date){
        var plusX : Int = 0
        var minusX : Int = 0
        switch day {
        case .sunday:
            plusX = 6
            minusX = 0
        case .monday:
            plusX = 5
            minusX = 1
            
        case .tuesday:
            plusX = 4
            minusX = 2
        case .wednesday:
            plusX = 3
            minusX = 3
        case .thursday:
            plusX = 2
            minusX = 4
        case .friday:
            plusX = 1
            minusX = 5
        
        case .saturday:
            plusX = 0
            minusX = 6

        default:
            break
        }
        //******
        
            guard let startDate = thisVisibleDate.monthDates.first?.date else {
                return
            }
        guard let endDate = thisVisibleDate.monthDates.last?.date else {
            return
        }
            let month = testCalendar.dateComponents([.month], from: startDate).month!
            let monthName = DateFormatter().monthSymbols[(month-1) % 12]
            // 0 indexed array
            let year = testCalendar.component(.year, from: startDate)
            
            
            ///: in case we are in calendar week mode
            
            let cellStatusssFirst = (self.calendarCollection.cellStatusForDate(at: 0, column: 0))
            let monthNumberFirst = testCalendar.dateComponents([.month], from: cellStatusssFirst!.date).month!
        
            let dateFormatter = DateFormatter()

            dateFormatter.dateFormat = "dd"
        let currentDateString: String = dateFormatter.string(from: datee)
        
        let startDateString: String = dateFormatter.string(from: startDate)
        let endDateString: String = dateFormatter.string(from: endDate)
        
        let startDateInt : Int = Int(startDateString)!
        let endDateInt : Int =  Int(endDateString)!
        
        var currentDateMin = Int(currentDateString)! - minusX
        var currentDateMax = Int(currentDateString)! + plusX
           // print("Current date is \(currentDateMin)" , "     " , currentDateMax)
        if currentDateMin < startDateInt{
            currentDateMin = startDateInt
        }
        
        if currentDateMax > endDateInt {
            currentDateMax = endDateInt
        }

        print("Current date is \(currentDateMin)" , "     " , currentDateMax)
        let monthNameFirst = DateFormatter().monthSymbols[(monthNumberFirst-1) % 12]

        montYearLabel.text = "du " + String(currentDateMin) + " au "  +  String(currentDateMax) + " " + monthNameFirst + " " + String(year)
       
        /*
            let dayFirst = datee.
            let monthNameFirst = DateFormatter().monthSymbols[(monthNumberFirst-1) % 12]
            
            let cellStatusssLast = (self.calendarCollection.cellStatusForDate(at: 0, column: 6))
            let monthNumberLast = testCalendar.dateComponents([.month], from: cellStatusssLast!.date).month!
            
            let dayLast = cellStatusssLast?.text
            let monthNameLast = DateFormatter().monthSymbols[(monthNumberLast-1) % 12]
            print("***" , monthNameFirst , "****" , dayFirst! )
            print("***" , monthNameLast , "****" , dayLast! )
            
            
            
            if monthNameFirst == monthNameLast {
                WeekLabel.text = "du " + dayFirst! + " au "  + dayLast! + " " + monthNameFirst + " " + String(year)
            }else {
                WeekLabel.text = "du " + dayFirst! + " " + monthNameFirst + " au "  + dayLast! + " " + monthNameLast + " " + String(year)
            }
            eventsLabel.text = "5 rendez-vous \n  Aujourd'hui"
            
        */
        /////*****
    }
        
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
       
        handleSelectedCellBackground( cell , cellstate: cellState)
        handleDaysLabelsColor( cell , cellstate: cellState)
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        // switchMonthYear(from: visibleDates)
        
        self.setupViewsOfCalendar(from: visibleDates)
        thisVisibleDate = visibleDates
    }

    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = testCalendar.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = testCalendar.component(.year, from: startDate)
        
            montYearLabel.text = monthName + " " + String(year)
    }
}

extension MonthCalendarViewController {
    func hexStringToUIColor (hex:String , alphaa : CGFloat) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alphaa)
        )
    }
}

extension UIView {
    func bounce(){
        self.transform = CGAffineTransform(scaleX : 0.6 ,  y  : 0.6)
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
            self.transform = CGAffineTransform(scaleX : 1 ,  y  : 1)
        })
    }
}

extension MonthCalendarViewController: UIGestureRecognizerDelegate {
    
    // make sure it only recognizes upward gestures
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let pan = gestureRecognizer as? UIPanGestureRecognizer {
            let translation = pan.translation(in: pan.view)
            let angle = atan2(translation.y, translation.x)
            return abs(angle + .pi / 2.0) < (.pi / 8.0)
        }
        return false
    }
}

