//
//  WeekCalendarViewController.swift
//  GoGoSkiUiKissou
//
//  Created by AYMEN on 10/29/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import JTAppleCalendar
import SwiftyJSON
import Alamofire

class WeekCalendarViewController: InternetCheckRed {
    /*
     * array for levels in french
     */
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     * array for levels in English
     */
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     * array for sports in French
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     * array for sports in English
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]

    @IBOutlet weak var WeekLabel: UILabel!
    @IBOutlet weak var eventsLabel: UILabel!
    @IBOutlet weak var eventsTableView: UITableView!
    @IBOutlet weak var calendarCollection: JTAppleCalendarView!
     let niveauColor = [UIColor("#D3D3D3"),UIColor("#76EC9E"),UIColor("#75A7FF"),UIColor("#FF3E53"),UIColor("#4D4D4D")]
    var dictionary =  [String:[LeconPresOBJ]]()

    let weekDays = ["Lun","Mar","Mer","Jeu","Ven","Sam","Dim"]
    let weekDays2 = ["Mon","Tus","Wed","Thi","Fri","Sat","Sun"]
    var tableviewSize : Int = 5
    let formatter  = DateFormatter()
    
    var shownMonthNumber : Int!
    var shownYear : Int!
    
    var numberOfRowsCalendar : Int! = 1
    var testCalendar = Calendar.current
////////////***
    var paramToPass : Date?
    var interactionController: UIPercentDrivenInteractiveTransition?

    /////
    @IBOutlet weak var calendar_view: UIView!
    
    var selectedCell : JTAppleCell?
    var selectedCellState : CellState?
    var selectedDate : Date?
    
    var eventsJson : JSON?
    
    var eventPerDate = [LeconPresOBJ]()
   // var jsonPres : JSON?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        
        shownMonthNumber =  components.month!
        shownYear = components.year!

        print("month " , shownMonthNumber ," year " , shownYear)
        calendarCollection.visibleDates { visibleDates in
            self.switchMonthYear(from: visibleDates)
        }
        calendarCollection.scrollToDate(Date())
        paramToPass = Date()
   
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        
    //    let panDown = UIPanGestureRecognizer(target: self, action: #selector(handleGesture(_:)))
     //   panDown.delegate = self
     //   self.calendar_view.addGestureRecognizer(panDown)
    }
    
    
    /////////********langue"**********///////
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
       
        Localisator.sharedInstance.saveInUserDefaults = true
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
    
    
    
  /*
    @objc func handleGesture(_ gesture: UIPanGestureRecognizer) {
        let translate = gesture.translation(in: gesture.view)
        let percent   = translate.y / gesture.view!.bounds.size.height
       // performSegue(withIdentifier: "toMonth", sender: self)
    }
    */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     //   getEvents(date : "")
        eventsLabel.text =  "0 " + Localization("rendezVSLBL")
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toMonth"{
            if let secondController = segue.destination as? MonthCalendarViewController {
                secondController.passedParam = paramToPass
            }
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
  
    func handleSelectedCellBackground(_ view: JTAppleCell? , cellstate: CellState){

        guard let validCellWeek = view as? CustomCellCalendarWeek else { return }

            if validCellWeek.isSelected {
                validCellWeek.selectedView.isHidden = false
            }else{
                validCellWeek.selectedView.isHidden = true
            }
    }
    
    func handleDaysLabelsColor(_ view: JTAppleCell? , cellstate: CellState){
  /*      let red = self.hexStringToUIColor(hex: "FF3C00" , alphaa: 1.0)
        let redTrans = self.hexStringToUIColor(hex: "FF3C00" , alphaa: 0.3)
        let redDark = self.hexStringToUIColor(hex: "BA000D" , alphaa: 1.0)
        let whiteTrans = self.hexStringToUIColor(hex: "FFFFFF" , alphaa: 0.3)
     */
            guard let validCell = view as? CustomCellCalendarWeek else { return }
            
            if validCell.isSelected {
                validCell.dayLabel.textColor = UIColor.white
                validCell.dayName.textColor = UIColor.white
            }else{
                if cellstate.dateBelongsTo == .thisMonth {
                    validCell.dayLabel.textColor = UIColor.black
                    validCell.dayName.textColor = UIColor.darkGray
                }else{
                    validCell.dayLabel.textColor = UIColor.gray
                    validCell.dayName.textColor = UIColor.gray
                }
            }
    }
    
    func switchMonthYear(from visibleDates :DateSegmentInfo){
        
        let date = visibleDates.monthDates.first!.date
        self.formatter.dateFormat = "MMMM dd yyyy"
        self.WeekLabel.text = self.formatter.string(from: date)
    }
    
    @IBAction func scrollToNextMonth(_ sender: Any) {
        calendarCollection.scrollToSegment(.next)
      //  calendarCollection.
        if let date = selectedDate{
            print("deseleeeeeeeeect",date)
            //   calendarCollection.deselectAllDates()
            calendarCollection.calendarDelegate?.calendar(calendarCollection, didDeselectDate: date, cell: selectedCell, cellState: selectedCellState!)
        }

        //calendarCollection.da
    }
    
    @IBAction func scrollToPreviousMonth(_ sender: Any) {
        calendarCollection.scrollToSegment(.previous)
        if let date = selectedDate{
          //  calendarCollection.deselectAllDates()

            print("deseleeeeeeeeect",date)
        calendarCollection.calendarDelegate?.calendar(calendarCollection, didDeselectDate: date, cell: selectedCell, cellState: selectedCellState!)
        }
    }
}

extension WeekCalendarViewController :  JTAppleCalendarViewDelegate{
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "YYYY MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        let startDate = formatter.date(from: "2018 1 01")!
        let endDate = formatter.date(from: "2019 12 31")!
        
        let parameters = ConfigurationParameters(startDate: startDate, endDate: endDate , numberOfRows: numberOfRowsCalendar , generateInDates: .forAllMonths,
                                                 generateOutDates: .tillEndOfRow,
                                                 firstDayOfWeek: .monday)
        return parameters
    }
}

extension WeekCalendarViewController : JTAppleCalendarViewDataSource {
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        let myCalendar = Calendar.current
        
            let cellWeek  : CustomCellCalendarWeek! = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "weekCell", for: indexPath) as! CustomCellCalendarWeek
            cellWeek.todayView.isHidden = true
            
            cellWeek.dayLabel.text = cellState.text
             if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            cellWeek.dayName.text = weekDays[indexPath.row]
             }else {
                cellWeek.dayName.text = weekDays2[indexPath.row]
        }
            
            handleSelectedCellBackground(cellWeek , cellstate: cellState)
            handleDaysLabelsColor(cellWeek , cellstate: cellState)
            if myCalendar.isDateInToday(date) {
                cellWeek.todayView.isHidden = false
                  calendarCollection.calendarDelegate?.calendar(calendarCollection, didSelectDate: date, cell: cellWeek, cellState: cellState)
            }
            print("week index " , indexPath)
            return cellWeek
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {

        if cellState.dateBelongsTo != .thisMonth {
            return
        }
        handleSelectedCellBackground(cell , cellstate: cellState)
        handleDaysLabelsColor(cell , cellstate: cellState)
        cell?.bounce()
        print("***********seelected Date***************************")
        
        let trueDate = Calendar.current.date(byAdding: .day, value: 1, to: date)
        print(trueDate)
        
        paramToPass = trueDate!
        selectedCell = cell
        selectedCellState = cellState
        selectedDate = trueDate!
        
        eventPerDate.removeAll()
        eventsTableView.reloadData()
        
       // getEvents()
        let dateStrin = String(String(describing: (trueDate!)).prefix(10))
        if Calendar.current.isDateInToday(date) {
            
            getEvents(date: dateStrin)
            return
        }
        /*DispatchQueue.main.async {
            self.tableviewSize = Int(cell.eventsNumber.text!)!
            print("tableviewsize" , self.tableviewSize)
            self.eventsTableView.reloadData()
        }*/
        
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
       
        
        if ( shownMonthNumber !=  components.month! || shownYear != components.year!){
            shownMonthNumber =  components.month!
            shownYear = components.year!
            getEvents(date : dateStrin)
            
        }else{
            self.showEvent(dateString: dateStrin)
        }
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
        handleSelectedCellBackground( cell , cellstate: cellState)
        handleDaysLabelsColor( cell , cellstate: cellState)
        selectedDate = nil
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        
        self.setupViewsOfCalendar(from: visibleDates)
    }
    
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = testCalendar.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = testCalendar.component(.year, from: startDate)
     
        ///: in case we are in calendar week mode
        
        let cellStatusssFirst = (self.calendarCollection.cellStatusForDate(at: 0, column: 0))
        let monthNumberFirst = testCalendar.dateComponents([.month], from: cellStatusssFirst!.date).month!
        
        let dayFirst = cellStatusssFirst?.text
        let monthNameFirst = DateFormatter().monthSymbols[(monthNumberFirst-1) % 12]
        
        let cellStatusssLast = (self.calendarCollection.cellStatusForDate(at: 0, column: 6))
        let monthNumberLast = testCalendar.dateComponents([.month], from: cellStatusssLast!.date).month!
        
        let dayLast = cellStatusssLast?.text
        let monthNameLast = DateFormatter().monthSymbols[(monthNumberLast-1) % 12]
        
        print("***" , monthNameFirst , "****" , dayFirst! )
        print("***" , monthNameLast , "****" , dayLast! )
        
        let monthNameFirstENFR = Localization(String(monthNameFirst.prefix(3)))
        let monthNameLastENFR = Localization(String(monthNameLast.prefix(3)))
        
        if monthNameFirst == monthNameLast {
            WeekLabel.text =  Localization("from") + " " + dayFirst! + " "  +  Localization("rangeTo") + " "  + dayLast! + " " + monthNameFirstENFR + " " + String(year)
        }else {
            WeekLabel.text = Localization("from") + " " + dayFirst! + " " + monthNameFirstENFR + " "  +  Localization("rangeTo") + " "  + dayLast! + " " + monthNameLastENFR + " " + String(year)
        }
        eventsLabel.text = "0 " + Localization("rendezVSLBL")
    }
}

extension WeekCalendarViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if eventPerDate.count == 0{
            eventsLabel.text =  "0 " + Localization("rendezVSLBL")
        }
        return eventPerDate.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell") as! EventCellTableViewCell
        
        let thisProfilePictureString =  eventPerDate[indexPath.row].photoURL
        if ( thisProfilePictureString != ""  ){
            if (thisProfilePictureString.first! == "h" ){
                cell.imageEvent.af_setImage(withURL: (URL(string : thisProfilePictureString))!)
            }else{
             cell.imageEvent.image = #imageLiteral(resourceName: "user_placeholder")
            }
        }else{
            cell.imageEvent.image = #imageLiteral(resourceName: "user_placeholder")
        }
         if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
        cell.titleEvent.text = eventPerDate[indexPath.row].titre + " « " + eventPerDate[indexPath.row].sport.pratique + " »"
         }else {
            switch eventPerDate[indexPath.row].sport.pratique {
            case SportSaifFR[0] :
                
                cell.titleEvent.text = eventPerDate[indexPath.row].titre + " « " + SportSaifEN[0] + " »"
            case SportSaifFR[1] :
               cell.titleEvent.text = eventPerDate[indexPath.row].titre + " « " + SportSaifEN[1] + " »"
                
            case SportSaifFR[2] :
               cell.titleEvent.text = eventPerDate[indexPath.row].titre + " « " + SportSaifEN[2] + " »"
                
            case SportSaifFR[3] :
               cell.titleEvent.text = eventPerDate[indexPath.row].titre + " « " + SportSaifEN[3] + " »"
                
            case SportSaifFR[4] :
                cell.titleEvent.text = eventPerDate[indexPath.row].titre + " « " + SportSaifEN[4] + " »"
            case SportSaifFR[5] :
               cell.titleEvent.text = eventPerDate[indexPath.row].titre + " « " + SportSaifEN[5] + " »"
                
            default :
                
                cell.titleEvent.text = eventPerDate[indexPath.row].titre + " « " + "" + " »"
                
            }
        }
        
        let sport = eventPerDate[indexPath.row].sport
        switch sport.niveau {
        case "Débutant"? : cell.status.textColor = niveauColor[0]
        case "Débrouillard"? : cell.status.textColor = niveauColor[1]
        case "Intermédiaire"? : cell.status.textColor = niveauColor[2]
        case "Confirmé"? : cell.status.textColor = niveauColor[3]
        case "Expert"? : cell.status.textColor = niveauColor[4]
        default : break
        }
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
        cell.status.text = sport.pratique
        }else {
            switch sport.pratique {
            case SportSaifFR[0] :
                
                cell.status.text = SportSaifEN[0]
            case SportSaifFR[1] :
                cell.status.text = SportSaifEN[1]
                
            case SportSaifFR[2] :
                cell.status.text = SportSaifEN[2]
                
            case SportSaifFR[3] :
                cell.status.text = SportSaifEN[3]
            case SportSaifFR[4] :
                cell.status.text = SportSaifEN[4]
            case SportSaifFR[5] :
                cell.status.text = SportSaifEN[5]
                
            default :
                
               cell.status.text = ""
                
            }
        }
        cell.departEvent.text = eventPerDate[indexPath.row].point_depart
        
        var startHourString = String(eventPerDate[indexPath.row].date_debut.suffix(8))
        startHourString = startHourString.replacingOccurrences(of: "/", with: ":")
        
        print("$$$$$$$ ",startHourString)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let startTime = dateFormatter.date(from: startHourString)
        dateFormatter.dateFormat = "hh:mm"
        let  startTimeString = dateFormatter.string(from: startTime!)
        print("startTime ::" ,startTime!)
        let duree = eventPerDate[indexPath.row].Duree
        let endTime = startTime?.addingTimeInterval(duree * 60.0)
        let endTimeString = dateFormatter.string(from: endTime!)
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
        cell.startClock.text = "de " +  String(describing: startTimeString)
        cell.endClock.text = "à " +  String(describing: endTimeString)
        }else{
            cell.startClock.text = "from " +  String(describing: startTimeString)
            cell.endClock.text = "to " +  String(describing: endTimeString)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ficheVC = UIStoryboard(name: "Prestataire", bundle : nil).instantiateViewController(withIdentifier: "FicheActiviteVC") as! FicheActiviteVC
        ficheVC.idLecon = String(eventPerDate[indexPath.row].id)
       self.navigationController?.show(ficheVC, sender: self)
    }
    
}


extension WeekCalendarViewController {
    func hexStringToUIColor (hex:String , alphaa : CGFloat) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alphaa)
        )
    }
}

extension WeekCalendarViewController: UIGestureRecognizerDelegate {
    
    // make sure it only recognizes downward gestures
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let pan = gestureRecognizer as? UIPanGestureRecognizer {
            let translation = pan.translation(in: pan.view)
            let angle = atan2(translation.y, translation.x)
            return abs(angle - .pi / 2.0) < (.pi / 8.0)
        }
        return false
    }
    
}

extension WeekCalendarViewController {
    
    func convertDateFormater(date: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd' 'HH/mm/ss"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        
        guard let date = dateFormatter.date(from: date) else {
            print(false, "no date from string")
            return Date()
        }
        return date
        // dateFormatter.dateFormat = "yyyy MMM EEEE HH:mm"
        //dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        //let timeStamp = dateFormatter.string(from: date)
        
        // return timeStamp
    }
    func getEvents(date : String){
        
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let jsonPres = JSON(data: dataFromString!)
        
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json" ,
            "x-Auth-Token" : jsonPres["value"].stringValue
        ]
        
        let params : Parameters = [
            "mois" : String(shownMonthNumber),
            "annee" : String(shownYear)
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.presCalendarLecon, method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                if let responseData = response.data {
                    let json = JSON(responseData)
                    print("kissouCalendar\n", json)
                    if json["status"].boolValue == false {
                        _ = SweetAlert().showAlert(Localization("cal"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                    }else{
                        
                        self.eventsJson = json
                        let list = json["data"].count
                        print("data count" , list)
                        if date != ""{
                           self.showEvent(dateString : date)
                        }
                    }
                }else{
                        LoaderAlert.shared.dismiss()
                }
        }
    }
    
    func showEvent(dateString : String){
        print(dateString)
        eventPerDate.removeAll()

        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let jsonPres = JSON(data: dataFromString!)
        
     //   print("hiiiiiii",eventsJson!["data"][dateString].exists())
     //   print("hiiiiiii",eventsJson!["data"]["2018-09-29"].exists())
        
        if eventsJson!["data"][dateString].exists() {
            let json = eventsJson!["data"][dateString]
                let list = json.count
            eventsLabel.text = String(list) + " " + Localization("rendezVSLBL")
            
            print("events 1     " , json )
                for i in 0 ..< list {
                    
                    let newLecon = LeconPresOBJ()
                    
                    newLecon.id = json[i]["id"].intValue
                    newLecon.titre = json[i]["titre"].stringValue
                    newLecon.date_debut = json[i]["date_debut"].stringValue
                    
                    let newSport = SportOBJ()
                    newSport.niveau = json[i]["sport"]["niveau"].stringValue
                    newSport.pratique = json[i]["sport"]["pratique"].stringValue
                    newLecon.sport = newSport
                  
                    newLecon.point_depart = jsonPres["user"]["point"]["station"]["nom"].stringValue
                    newLecon.photoURL = jsonPres["user"]["photo"].stringValue
                    print("point depaart :::::",newLecon.point_depart)
                 ///   let date = String(newLecon.date_debut.prefix(10))
                    newLecon.Duree = json[i]["duree"].doubleValue
                    print("date formatter : ",self.convertDateFormater(date: newLecon.date_debut))
                    self.eventPerDate.append(newLecon)
                    //self.dictionary[date]
                }
            eventsTableView.reloadData()
        }
    }
}

