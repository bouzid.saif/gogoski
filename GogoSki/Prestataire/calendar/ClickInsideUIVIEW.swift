//
//  ClickInsideUIVIEW.swift
//  CalendarTest
//
//  Created by AYMEN on 10/9/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit

class ClickInsideUIVIEW: UIView {

        override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
            for subview in subviews {
                if !subview.isHidden && subview.alpha > 0 && subview.isUserInteractionEnabled && subview.point(inside: convert(point, to: subview), with: event) {
                    return true
                }
            }
            return false
        }
    
}
