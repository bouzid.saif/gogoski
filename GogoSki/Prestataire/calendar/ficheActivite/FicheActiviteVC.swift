//
//  FicheActiviteVC.swift
//  GoGoSkiUiKissou
//
//  Created by AYMEN on 10/23/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import DownPicker
import SwiftyJSON
import Alamofire
import MapKit
import MapleBacon
class FicheActiviteVC: InternetCheckRed ,MKMapViewDelegate {
    /*
     * array for levels in french
     */
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     * array for levels in English
     */
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     * array for sports in French
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     * array for sports in English
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    
    @IBOutlet weak var tableHeightCSTR: NSLayoutConstraint!
    @IBOutlet weak var moniteurIMG: UIImageView!
    
    @IBOutlet weak var mapview: MKMapView!
    @IBOutlet weak var ficheActiviteTitleLBL: UILabel!
    
    @IBOutlet weak var placeRestantesBL: UILabel!
    @IBOutlet weak var prixTitleLBL: UILabel!
    @IBOutlet weak var choixMoniteurTitleLBL: UILabel!
    @IBOutlet weak var validerModificationBtn: UIButton!
    
    @IBOutlet weak var annulerModificationBtn: UIButton!
    @IBOutlet weak var modifierHoraireTitleLBL: UILabel!
    
    @IBOutlet weak var imageActivite: UIImageView!
    @IBOutlet weak var position: UILabel!
    @IBOutlet weak var titreFicheActivite: UILabel!
    
    @IBOutlet weak var type: UIButton!
    @IBOutlet weak var niveau: UIButton!
    
    @IBOutlet weak var departDate: UILabel!
    
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var durée: UILabel!
    
    @IBOutlet weak var nombrePersonne: UILabel!
    
    @IBOutlet weak var placeRestante: UILabel!
    
    @IBOutlet weak var prixMin: UILabel!
    
    @IBOutlet weak var prixMax: UILabel!
    ///////
    @IBOutlet weak var descriptionConstraintTop: NSLayoutConstraint!
    
    @IBOutlet weak var descriptionContainerConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var labeldescription: UILabel!
    @IBOutlet weak var bouttonPlus: UIButton!
    @IBOutlet weak var ViewContainerConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var participantTV: UITableView!
    @IBOutlet weak var horareTF: UITextField!
    
   // @IBOutlet weak var horaireLBL: UILabel!
    @IBOutlet weak var moniteurPicker: UITextField!
   
    var moniteurDownPicker : DownPicker!
    var datepicker : DownPicker!
    var datePicker1 : UIDatePicker! = UIDatePicker()
    
    ///tableView
    var participantCellHeader:ParticipantHeaderView = ParticipantHeaderView()
    var arrStatus:NSMutableArray = []
    /////
    
    var previousSection : Int! = -1
    var idLecon : String!
    var conversationID = 0
    
    var MoniteurList = [MoniteurOBJ]()
    var MoniteurNameList = [String]()
    var MoniteurPhotoList = [String]()
    var timePickerArray = [String]()

    var activityObject = ActivityOBJ()
    let niveauColor = [UIColor("#D3D3D3"),UIColor("#76EC9E"),UIColor("#75A7FF"),UIColor("#FF3E53"),UIColor("#4D4D4D")]

     let dureeArray = ["01:00","01:30","02:00","02:30","03:00","03:30","04:00","04:30","05:00","05:30","06:00","06:30","07:00","07:30","08:00","08:30","09:00","09:30","10:00","10:30","11:00","11:30","12:00"]
    
    var moniteurIdPicked = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //showTimePicker()
        
        imageActivite.layer.cornerRadius =  imageActivite.frame.height / 2
        imageActivite.layer.borderWidth = 0
        imageActivite.clipsToBounds = true
        
        mapview.isUserInteractionEnabled = false
        // Do any additional setup after loading the view.
       // print("id lslslslsl: ",idLecon)
      
      
        initSectionStateTableView(x : 10)
        //$$$$LANGUE
       NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
 
        
       // showTimePicker()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getData()
    }
    
    
    /////////********langue"**********///////
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        
        prixTitleLBL.text = Localization("Prix").uppercased()
        modifierHoraireTitleLBL.text = Localization("modifierhoraire")
        validerModificationBtn.setTitle(Localization("BtnValiderModif"), for: .normal)
        annulerModificationBtn.setTitle(Localization("annulerLeconbtn"), for: .normal)
        choixMoniteurTitleLBL.text = Localization("choixMoniteurPres")
        ficheActiviteTitleLBL.text = Localization("ficheActivitéPres")
        
        Localisator.sharedInstance.saveInUserDefaults = true
    }
    
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
    
    func initSectionStateTableView(x: Int){
        arrStatus.removeAllObjects()
        for _ in 0..<x // pass your array count
        {
            self.arrStatus.add("0")
        }
    }
    
    @IBAction func dismisss(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //******** expand collapse uilabel ****** begin ///////
    @IBAction func plusDescLabel(_ sender: Any) {
        setDescriptionLBL(descString: labeldescription.text!)
    }
    //******** expand collapse uilabel ****** end ///////
    
 
    func getData(){
        
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let jsonPres = JSON(data: dataFromString!)
        
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))

        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : jsonPres["value"].stringValue
        ]
        
        let params: Parameters = [
            "id" : idLecon
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.presLeconById , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                if let responseData = response.data {
                    
                    let b = JSON(responseData)
                    print("haw ja",b)
                    if b["status"].boolValue {
                      
                    //    print("haw ja : ",b)
                        
                        // €
                        let newActivity = ActivityOBJ()
                        
                        let listParticipant = b["data"]["lecon"][0]["list_participants"]
                        for i in 0 ..< listParticipant.count{
                            
                            let newParticipant = ParticipantOBJ()
                            newParticipant.id = listParticipant[i]["id"].intValue
                            newParticipant.nom = listParticipant[i]["nom"].stringValue

                          //  print("easy " , newParticipant.nom )
                            newParticipant.prenom = listParticipant[i]["prenom"].stringValue
                            newParticipant.photo = listParticipant[i]["photo"].stringValue
                            newParticipant.carteBancaire.type = listParticipant[i]["carte_bancaire"]["type"].stringValue
                            newParticipant.carteBancaire.titulaire = listParticipant[i]["carte_bancaire"]["titulaire"].stringValue
                            newParticipant.carteBancaire.numero = listParticipant[i]["carte_bancaire"]["numero"].intValue
                            
                            let sports = listParticipant[i]["sports"]
                            for j in 0 ..< sports.count {
                                if sports[j]["is_default"].boolValue {
                                    
                                        let newSport = SportOBJ()
                                        newSport.niveau = sports[j]["niveau"].stringValue
                                        newSport.pratique = sports[j]["pratique"].stringValue
                                    newParticipant.sport = newSport
                                }
                            }
                            newActivity.participant.append(newParticipant)
                        }
                        
                        let leconJson = b["data"]["lecon"][0]
                        
                        //// conversation ID
                        self.conversationID = leconJson["conversation"]["id"].intValue
                        
                        newActivity.titre = leconJson["titre"].stringValue
                        newActivity.date_depart = leconJson["date_debut"].stringValue
                        newActivity.desc = leconJson["detail"].stringValue
                        newActivity.duree = leconJson["prestation"]["duree"].intValue
                        
                        newActivity.canUpdate = leconJson["canupdate"].boolValue
                        let newTarif = TarifOBJ()
                            newTarif.prixMin = leconJson["prestation"]["tarifs"][0]["prix_min"].doubleValue
                            newTarif.prixMax = leconJson["prestation"]["tarifs"][0]["prix_max"].doubleValue
                        newActivity.Tarif = newTarif
                     //   print("prxxxxxxxx", leconJson["prestation"]["tarifs"][0]["prix_max"].doubleValue)
                        newActivity.nombreParticipantMax = leconJson["nb_place_maximum"].intValue
                        let newSport = SportOBJ()
                            newSport.niveau = leconJson["sport"]["niveau"].stringValue
                            newSport.pratique = leconJson["sport"]["pratique"].stringValue
                        newActivity.sport = newSport

                        let newMoniteur = MoniteurOBJ()
                        newMoniteur.id = b["data"]["Moniteur"][0]["id"].intValue
                        newMoniteur.nom = b["data"]["Moniteur"][0]["nom"].stringValue
                        newMoniteur.prenom = b["data"]["Moniteur"][0]["prenom"].stringValue
                        newMoniteur.photo = b["data"]["Moniteur"][0]["photo"].stringValue
                       
                //       print("hooooooo" + newMoniteur.nom + "  " + newMoniteur.prenom)
                        
                        newActivity.moniteur = newMoniteur
                        self.activityObject = newActivity
                        DispatchQueue.main.async(execute: {
                            self.refreshPage()
                        })

                    }else{
                        _ = SweetAlert().showAlert(Localization("ActT"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                        
                    }
                }else{
                    LoaderAlert.shared.dismiss()

                    _ = SweetAlert().showAlert(Localization("ActT"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                    
                }
        }
    }
    
    
    func refreshPage(){
        let a = activityObject
        
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let jsonPres = JSON(data: dataFromString!)
        
        print("jsonPres : ", jsonPres)
        position.text = "Station " + jsonPres["user"]["point"]["station"]["nom"].stringValue
      //  imageActivite.
        
        let thisProfilePictureString = jsonPres["user"]["photo"].stringValue
        if ( thisProfilePictureString != ""  ){
            if (thisProfilePictureString.first! == "h" ){
                imageActivite.af_setImage(withURL: (URL(string : thisProfilePictureString))!)
            }else{
                print("nothing happening with the photo")
                imageActivite.image = #imageLiteral(resourceName: "user_placeholder")
            }
        }else{
            imageActivite.image = #imageLiteral(resourceName: "user_placeholder")
        }

        titreFicheActivite.text = a.titre
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            type.setTitle(a.sport.pratique, for: .normal)
            niveau.setTitle(a.sport.niveau, for: .normal)
        }else {
            switch a.sport.pratique {
            case SportSaifFR[0] :
                
                type.setTitle(SportSaifEN[0], for: .normal)
            case SportSaifFR[1] :
               type.setTitle(SportSaifEN[1], for: .normal)
                
            case SportSaifFR[2] :
                type.setTitle(SportSaifEN[2], for: .normal)
                
            case SportSaifFR[3] :
                type.setTitle(SportSaifEN[3], for: .normal)
                
            case SportSaifFR[4] :
               type.setTitle(SportSaifEN[4], for: .normal)
            case SportSaifFR[5] :
                type.setTitle(SportSaifEN[5], for: .normal)
                
            default :
                
                type.setTitle("", for: .normal)
                
            }
            switch a.sport.niveau {
            case NiveauSaifFR[0] :
                 niveau.setTitle(NiveauSaifEN[0], for: .normal)
            case NiveauSaifFR[1] :
                 niveau.setTitle(NiveauSaifEN[1], for: .normal)
                
            case NiveauSaifFR[2] :
                 niveau.setTitle(NiveauSaifEN[2], for: .normal)
                
            case NiveauSaifFR[3] :
                 niveau.setTitle(NiveauSaifEN[3], for: .normal)
            case NiveauSaifFR[4] :
                niveau.setTitle(NiveauSaifEN[4], for: .normal)
                
                
            default :
                
                niveau.setTitle("", for: .normal)
                
            }
        }
       
        switch a.sport.niveau {
        case "Débutant"? : niveau.setTitleColor( niveauColor[0], for: .normal)
        case "Débrouillard"? :  niveau.setTitleColor( niveauColor[1], for: .normal)
        case "Intermédiaire"? :  niveau.setTitleColor( niveauColor[2], for: .normal)
        case "Confirmé"? : niveau.setTitleColor( niveauColor[3], for: .normal)
        case "Expert"? :  niveau.setTitleColor( niveauColor[4], for: .normal)
        default : break
        }
        
     
        departDate.text = Localization("FicheLeconDeparLe") + " : " + String(a.date_depart.prefix(10))
        
        var startHourString = String(a.date_depart.suffix(8))
        startHourString = startHourString.replacingOccurrences(of: "/", with: ":")
        
        let Xmen = String(startHourString.prefix(5))
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let startTime = dateFormatter.date(from: startHourString)
        dateFormatter.dateFormat = "hh:mm"
        var startTimeString = dateFormatter.string(from: startTime!)
        startTimeString = startTimeString.replacingOccurrences(of: ":", with: "h")
        print("startTimeString $$$$" , startTimeString)
        
///////////// horaire picker
        if !a.canUpdate {
          self.horareTF.isUserInteractionEnabled = true
          //  self.modifierHoraireTitleLBL.isHidden = false
                    let date1 = Calendar.current.date(byAdding: .minute, value: 30, to: startTime!)
                    let date2 = Calendar.current.date(byAdding: .minute, value: -30, to: startTime!)
        
                    let date1S = dateFormatter.string(from: date1!)
                    let date2S = dateFormatter.string(from: date2!)
                    let date1T = dateFormatter.date(from: date1S)
                    let date2T = dateFormatter.date(from: date2S)
                    let m = dateFormatter.date(from: "08:01")
                    let x = Calendar.current.date(byAdding: .minute, value: -1, to: m!)!
                    let morningTimeLimit = Calendar.current.date(byAdding: .hour, value: 1, to: x)!
                    let eveningTimeLimit = Calendar.current.date(byAdding: .hour, value: 12, to: morningTimeLimit)!
                    print("morningTimeLimit ; ",morningTimeLimit," eveningTimeLimit : ",eveningTimeLimit)
                    print("time 1 : " , date1T! , "time 2 : " , date2T! )
                    print("date 1" , date1S , "date 2" , date2S, "date actuellet" , startTimeString)
        

                    switch Xmen {
                    case "08:00":
                        timePickerArray.append("08:00")
                        timePickerArray.append("08:30")
                    case "08:30":
                        timePickerArray.append("08:00")
                        timePickerArray.append("08:30")
                        timePickerArray.append("09:00")
                    case "09:00":
                        timePickerArray.append("08:30")
                        timePickerArray.append("09:00")
                        timePickerArray.append("09:30")

                    case "09:30":
                        timePickerArray.append("09:00")
                        timePickerArray.append("09:30")
                        timePickerArray.append("10:00")
                    case "10:00":
                        timePickerArray.append("09:30")
                        timePickerArray.append("10:00")
                        timePickerArray.append("10:30")
                    case "10:30":
                        timePickerArray.append("10:00")
                        timePickerArray.append("10:30")
                        timePickerArray.append("11:00")
                    case "11:00":
                        timePickerArray.append("10:30")
                        timePickerArray.append("11:00")
                        timePickerArray.append("11:30")
                    case "11:30":
                        timePickerArray.append("11:00")
                        timePickerArray.append("11:30")
                        timePickerArray.append("12:00")

                    case "12:00":
                        timePickerArray.append("11:30")
                        timePickerArray.append("12:00")
                        timePickerArray.append("12:30")
                    case "12:30":
                        
                        timePickerArray.append("12:00")
                        timePickerArray.append("12:30")
                        timePickerArray.append("13:00")
                    case "13:00":
                        
                        timePickerArray.append("12:30")
                        timePickerArray.append("13:00")
                        timePickerArray.append("13:30")
                    case "13:30":
                        
                        timePickerArray.append("13:00")
                        timePickerArray.append("13:30")
                        timePickerArray.append("14:00")
                    case "14:00":
                        
                        timePickerArray.append("13:30")
                        timePickerArray.append("14:00")
                        timePickerArray.append("14:30")
                    case "14:30":
                        timePickerArray.append("14:00")
                        timePickerArray.append("14:30")
                        timePickerArray.append("15:00")
                    case "15:00":
                        timePickerArray.append("14:30")
                        timePickerArray.append("15:00")
                        timePickerArray.append("15:30")
                    case "15:30":
                        timePickerArray.append("15:00")
                        timePickerArray.append("15:30")
                        timePickerArray.append("16:00")
                    case "16:00":
                        timePickerArray.append("15:30")
                        timePickerArray.append("16:00")
                        timePickerArray.append("16:30")
                    case "16:30":
                        timePickerArray.append("16:00")
                        timePickerArray.append("16:30")
                        timePickerArray.append("17:00")
                    case "17:00":
                        timePickerArray.append("16:30")
                        timePickerArray.append("17:00")
                        timePickerArray.append("17:30")
                    case "17:30":
                        timePickerArray.append("17:00")
                        timePickerArray.append("17:30")
                        timePickerArray.append("18:00")
                    case "18:00":
                        timePickerArray.append("17:30")
                        timePickerArray.append("18:00")
                        timePickerArray.append("18:30")
                    case "18:30":
                        timePickerArray.append("18:00")
                        timePickerArray.append("18:30")
                        timePickerArray.append("19:00")
                    case "19:00":
                        timePickerArray.append("18:30")
                        timePickerArray.append("19:00")
                    case "19:30":
                        timePickerArray.append("19:00")
                        timePickerArray.append("19:30")
                    default : break
                    }
                 /*
                    if startTimeString == "12:00"{
                        timePickerArray.removeAll()

                        timePickerArray.append("11:30")
                        timePickerArray.append("12:00")
                        timePickerArray.append("12:30")

                    }else if startTimeString == "11:30"{
                        timePickerArray.removeAll()

                        timePickerArray.append("11:00")
                        timePickerArray.append("11:30")
                        timePickerArray.append("12:00")
         
         
                    }else if startTimeString == "12:30"{
                        timePickerArray.removeAll()

                        timePickerArray.append("12:00")
                        timePickerArray.append("12:30")
                        timePickerArray.append("13:00")
                    }else{
                        timePickerArray.removeAll()

                        if date1T?.compare(eveningTimeLimit) == .orderedAscending  {
                            timePickerArray.append(date2S)
                        }
                        timePickerArray.append(startTimeString)
                        if date2T?.compare(morningTimeLimit) == .orderedAscending {
                            timePickerArray.append(date1S)
                        }
                    }
                    */
                    self.datepicker = DownPicker(textField: self.horareTF, withData: timePickerArray as [Any])
                    self.datepicker.setArrowImage(nil)
                    self.datepicker.setToolbarDoneButtonText(Localization("termineP"))
                    self.datepicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
                    self.horareTF.text = Xmen
        }else{
        //    self.modifierHoraireTitleLBL.isHidden = true

            self.horareTF.text = Xmen
            self.horareTF.isUserInteractionEnabled = false
        }

///////////// horaire picker

        time.text = Localization("FicheLeconA") + " : " + startTimeString
        durée.text =  Localization("Duree") + " : " + getDureeToShow(duree: String(a.duree))

        if activityObject.Tarif.prixMax != 0.0 {
            let stringP =  String(format: "%.2f", arguments: [activityObject.Tarif.prixMax]) + "€   TTC max."
            let stringPrixMax = NSMutableAttributedString.init(string: stringP)
            stringPrixMax.setAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: 24),
                                         NSAttributedStringKey.foregroundColor: UIColor.white  ],
                                        range: NSMakeRange(0, 7))
            prixMax.attributedText = stringPrixMax
        }
        
        if activityObject.Tarif.prixMin != 0.0 {
            let stringP =  String(format: "%.2f", arguments: [activityObject.Tarif.prixMin]) + "€   TTC min."
            
            let stringPrixMin = NSMutableAttributedString.init(string: stringP)
            stringPrixMin.setAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: 24),
                                         NSAttributedStringKey.foregroundColor: UIColor.white ],
                                     range: NSMakeRange(0, 7))
            prixMin.attributedText = stringPrixMin
        }
        
        nombrePersonne.text = String(a.nombreParticipantMax) + " pers. /"
        let placeRestanteString = String(a.nombreParticipantMax - a.participant.count) + " " +  Localization("placeRestant")
        placeRestante.text = placeRestanteString
        placeRestantesBL.text = placeRestanteString
       // labeldescription.text = a.desc
        setDescriptionLBL(descString: a.desc)
      
        setMap(longg: jsonPres["user"]["point"]["longitude"].stringValue , latt:  jsonPres["user"]["point"]["latitude"].stringValue)
        
        initSectionStateTableView(x : activityObject.participant.count)
        participantTV.reloadData()
        handleTableSize()
        
      /*
        let time1 = String(activityObject.date_depart.suffix(8))
        let time2 = String(time1.prefix(5))
        let time3 = time2.replacingOccurrences(of: "/", with: ":")

        let date = dateFormatter.date(from: time3)
        
        if let da = date{
            datePicker1.date = da
        }
 */
        getMoniteur()
        
        moniteurPicker.text = activityObject.moniteur.nom + " " + activityObject.moniteur.prenom
        
        if moniteurPicker.text!.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                if let thisProfilePictureString = activityObject.moniteur.photo{
                    if ( thisProfilePictureString != ""  ){
                        if (thisProfilePictureString.first! == "h" ){
                            print (" thisProfilePictureString : " , thisProfilePictureString)
                            //    moniteurIMG.af_setImage(withURL: )
                            moniteurIMG.setImage(withUrl: (URL(string : thisProfilePictureString))!, placeholder: #imageLiteral(resourceName: "user_placeholder") , crossFadePlaceholder: false, cacheScaled: true, completion: nil)
                        }else{
                            print("nothing happening with the photo")
                            moniteurIMG.image = #imageLiteral(resourceName: "user_placeholder")
                        }
                    }else{
                        imageActivite.image = #imageLiteral(resourceName: "user_placeholder")
                    }
                }
        }
    }
    
    func setDescriptionLBL(descString: String){
        let labelPreviousHeight = labeldescription.frame.height

        if (labeldescription.numberOfLines == 3)
        {
            labeldescription.numberOfLines = 0
         
            bouttonPlus.setTitle(Localization("moinsP"), for: .normal)
            
            labeldescription.text = descString
            labeldescription.sizeToFit()
            print("liness", lines(label: labeldescription))
            // ViewContainerConstraintHeight.constant = labeldescription.frame.height + 29.0 + btnPlus.frame.height
            descriptionContainerConstraintHeight.constant = 363 + labeldescription.frame.height
            ViewContainerConstraintHeight.constant = (ViewContainerConstraintHeight.constant - labelPreviousHeight) + labeldescription.frame.height
            
            if lines(label: labeldescription) < 3 {
                UIView.animate(withDuration: 0.5, animations: {
                    self.bouttonPlus.alpha = 0
                    self.bouttonPlus.isHidden = true
                })
            }else{
                UIView.animate(withDuration: 0.5, animations: {
                    self.bouttonPlus.alpha = 1
                    self.bouttonPlus.isHidden = false
                })
            }
            
        }else {
            labeldescription.numberOfLines = 3
            bouttonPlus.setTitle(Localization("plusP"), for: .normal)
            //  labelTopConstraint.constant = 16
            labeldescription.text = descString
            labeldescription.sizeToFit()
            descriptionContainerConstraintHeight.constant = 363 + labeldescription.frame.height
            ViewContainerConstraintHeight.constant = (ViewContainerConstraintHeight.constant - labelPreviousHeight) + labeldescription.frame.height
        }
        
    }
    func lines(label: UILabel) -> Int {
        let textSize = CGSize(width: label.frame.size.width, height: CGFloat(Float.infinity))
        let rHeight = lroundf(Float(label.sizeThatFits(textSize).height))
        let charSize = lroundf(Float(label.font.lineHeight))
        let lineCount = rHeight/charSize
        return lineCount
    }
    func handleTableSize(){
        if activityObject.participant.count > 0{
            let previousTVHeight = self.tableHeightCSTR.constant
            let newTableSize = CGFloat(110 * self.activityObject.participant.count)
            
            UIView.animate(withDuration: 0.5) {
                self.tableHeightCSTR.constant = newTableSize
            }
            UIView.animate(withDuration: 0.5) {
                self.ViewContainerConstraintHeight.constant = (self.ViewContainerConstraintHeight.constant - previousTVHeight) + newTableSize
            }
        }
    }
    
    func setMap(longg : String , latt : String){
    
        var long = longg
        var lat = latt
        
        if long.prefix(1) == " "
        {
            long = String (long.dropFirst())
        }
        if lat.suffix(1) == " "{
            lat = String (lat.dropLast())
        }
        
        if lat != "" && long != "" {
            let point = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!)
            let annotation = ColorPointAnnotation(pinColor: UIColor.red)
            annotation.coordinate = point
            
            self.mapview.addAnnotation(annotation)
            self.mapview.setCenter(point, animated: true)
        }
        ////
    }
    
    func getDureeToShow (duree : String) -> String {
        var const = ""
        switch duree {
        case "60":  const = dureeArray[0].replacingOccurrences(of: ":", with: "h")
        case "90":  const = dureeArray[1].replacingOccurrences(of: ":", with: "h")
        case "120":  const = dureeArray[2].replacingOccurrences(of: ":", with: "h")
        case "150":  const = dureeArray[3].replacingOccurrences(of: ":", with: "h")
        case "180":  const = dureeArray[4].replacingOccurrences(of: ":", with: "h")
        case "210":  const = dureeArray[5].replacingOccurrences(of: ":", with: "h")
        case "240":  const = dureeArray[6].replacingOccurrences(of: ":", with: "h")
        case "270":  const = dureeArray[7].replacingOccurrences(of: ":", with: "h")
        case "300":  const = dureeArray[8].replacingOccurrences(of: ":", with: "h")
        case "330":  const = dureeArray[9].replacingOccurrences(of: ":", with: "h")
        case "360":  const = dureeArray[10].replacingOccurrences(of: ":", with: "h")
        case "390":  const = dureeArray[11].replacingOccurrences(of: ":", with: "h")
        case "420":  const = dureeArray[12].replacingOccurrences(of: ":", with: "h")
        case "450":  const = dureeArray[13].replacingOccurrences(of: ":", with: "h")
        case "480":  const = dureeArray[14].replacingOccurrences(of: ":", with: "h")
        case "510":  const = dureeArray[15].replacingOccurrences(of: ":", with: "h")
        case "540":  const = dureeArray[16].replacingOccurrences(of: ":", with: "h")
        case "570":  const = dureeArray[17].replacingOccurrences(of: ":", with: "h")
        case "600":  const = dureeArray[18].replacingOccurrences(of: ":", with: "h")
        case "630":  const = dureeArray[19].replacingOccurrences(of: ":", with: "h")
        case "660":  const = dureeArray[20].replacingOccurrences(of: ":", with: "h")
        case "690":  const = dureeArray[21].replacingOccurrences(of: ":", with: "h")
        case "720":  const = dureeArray[22].replacingOccurrences(of: ":", with: "h")
        default : break
        }
        return const
    }
    
    func getMoniteur(){
        
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let jsonPres = JSON(data: dataFromString!)
        
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))

        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : jsonPres["value"].stringValue
        ]
        
        let params: Parameters = [
            "niveau" : activityObject.sport.niveau,
            "pratique" : activityObject.sport.pratique
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.leconMoniteur , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                if let responseData = response.data {
                    let b = JSON(responseData)
                    print("moniiiiiii",b)
                    LoaderAlert.shared.dismiss()
                    if b["status"].boolValue {
                        
                        for i in 0 ..< b["data"][0].count{
                        
                            let monJson = b["data"][0][i]
                            let newMoniteur = MoniteurOBJ()
                            newMoniteur.id = monJson["id"].intValue
                            newMoniteur.nom = monJson["nom"].stringValue
                            newMoniteur.prenom = monJson["prenom"].stringValue
                            newMoniteur.photo = monJson["photo"].stringValue
                            
                            let newSport = SportOBJ()
                            newSport.niveau = monJson["sports"]["niveau"].stringValue
                            newSport.pratique = monJson["sports"]["pratique"].stringValue
                           // print("moniiiiiii" , b)
                            let moniteurName = newMoniteur.nom + " " + newMoniteur.prenom
                            self.MoniteurNameList.append(moniteurName)
                            newMoniteur.sports.append(newSport)
                            self.MoniteurList.append(newMoniteur)
                            
                        }
                        
                        self.refreshMoniteurPicker()
                        
                    }else{
                        _ = SweetAlert().showAlert(Localization("ModificationMoniteur"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                        LoaderAlert.shared.dismiss()

                    }
                }else{
                    _ = SweetAlert().showAlert(Localization("ModificationMoniteur"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                    LoaderAlert.shared.dismiss()

                }
        }
    }
    
    func refreshMoniteurPicker(){
    
       self.moniteurDownPicker = DownPicker(textField: self.moniteurPicker, withData: MoniteurNameList as [Any])
        moniteurDownPicker.addTarget(self, action: #selector(self.monitorDidChange), for : .valueChanged )
        
        self.moniteurDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.moniteurDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
    }
    
     @objc func monitorDidChange(){
        print("moniteur picked  :  " , moniteurPicker.text! )
        if let  index = self.MoniteurNameList.index(of:  moniteurPicker.text!){
            moniteurIdPicked = MoniteurList[index].id
            print("moniteur picked  :  " , moniteurIdPicked)
        }
        
        if let index = MoniteurNameList.index(of: moniteurPicker.text!){
            if let thisProfilePictureString = self.MoniteurList[index].photo{
                if ( thisProfilePictureString != ""  ){
                    if (thisProfilePictureString.first! == "h" ){
                        print (" thisProfilePictureString : " , thisProfilePictureString)
                    //    moniteurIMG.af_setImage(withURL: )
                        moniteurIMG.setImage(withUrl: (URL(string : thisProfilePictureString))!, placeholder: #imageLiteral(resourceName: "user_placeholder") , crossFadePlaceholder: false, cacheScaled: true, completion: nil)
                    }else{
                        print("nothing happening with the photo")
                        moniteurIMG.image = #imageLiteral(resourceName: "user_placeholder")
                    }
                }else{
                    imageActivite.image = #imageLiteral(resourceName: "user_placeholder")
                }
            }
        }
    }
    
    
    @IBAction func modifierLeconAction(_ sender: Any) {
        
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let jsonPres = JSON(data: dataFromString!)
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : jsonPres["value"].stringValue
        ]
        
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))

        var heure = ""
        if horareTF.text!.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            heure = horareTF.text!
        }
        var startHourString = String(activityObject.date_depart.suffix(8))
        startHourString = startHourString.replacingOccurrences(of: "/", with: ":")
        
        let Xmen = String(startHourString.prefix(5))
        var canUpdate = false
        if heure.trimmingCharacters(in: .whitespacesAndNewlines) != Xmen.trimmingCharacters(in: .whitespacesAndNewlines){
           canUpdate = true
        }
        var thisMonitor = 0
        if moniteurIdPicked == 0{
            thisMonitor = activityObject.moniteur.id
        }else{
            thisMonitor = moniteurIdPicked
        }
        
        print("idLL", separator: "     ", terminator: idLecon)
        let params: Parameters = [
            "idL" : idLecon,
            "id" : String(thisMonitor),
            "heure" : heure,
            "canupdate" : canUpdate
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.modifierLecon , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                if let responseData = response.data {
                    let b = JSON(responseData)
                    print("modifier :  ",b)
                    if b["status"].boolValue {
                        _ = SweetAlert().showAlert(Localization("ActT"), subTitle: Localization("modifierAct"), style: AlertStyle.success)

                    }else{
                    
                        _ = SweetAlert().showAlert(Localization("ActT"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                    }
                }else{
                    _ = SweetAlert().showAlert(Localization("ActT"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                    LoaderAlert.shared.dismiss()

                }
        }
    }
    @IBAction func goToActivityMSG(_ sender: Any) {
          print("click")
        if conversationID != 0 {
            print("clicked")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "gotToActivityMsg"), object: conversationID )
        }
     
    }
    
    @IBAction func annulerActiviteAction(_ sender: Any) {
        
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let jsonPres = JSON(data: dataFromString!)
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : jsonPres["value"].stringValue
        ]
        
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
        
        let params: Parameters = [
            "id" : idLecon
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.annulerLecon , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                if let responseData = response.data {
                    let b = JSON(responseData)
                    print("supprimer   :  ",b)
                    if b["status"].boolValue {
                        
                           _ = SweetAlert().showAlert(Localization("ActT"), subTitle: Localization("annulerAct"), style: AlertStyle.success)
                        DispatchQueue.main.async(execute: {
                            self.navigationController?.popViewController(animated: true)
                        })

                    }else{
                        if b["message"].stringValue == "des partcipant existe deja" {
                            _ = SweetAlert().showAlert(Localization("ActT"), subTitle: Localization("participantExiste"), style: AlertStyle.error)
                        }else{
                            _ = SweetAlert().showAlert(Localization("ActT"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                        }
                    }
                }else{
                    _ = SweetAlert().showAlert(Localization("ActT"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                    LoaderAlert.shared.dismiss()
                    
                }
        }
    }
}

extension FicheActiviteVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        //Return header height as per your header hieght of xib
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        let str:String = arrStatus[section] as! String
        
        if str == "0"
        {
            return 0
        }
        return  1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "participantContentCell", for: indexPath) as! ParticipantContentCell
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return activityObject.participant.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        //Return row height as per your cell in tableview
        return 237
    }
    
    ///// tableView setup header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        
        let participantCellHeader = Bundle.main.loadNibNamed("ParticipantCellHeader", owner: self, options: nil)?[0] as! ParticipantHeaderView
        
       // participantCellHeader.msgBTN.isHidden = true
       
        participantCellHeader.expandCollapseButton.isHidden = true
        
        participantCellHeader.expandCollapseButton.tag = section
        participantCellHeader.expandCollapseButton.addTarget(self, action: #selector(FicheActiviteVC.headerCellButtonTapped(_sender:)), for: UIControlEvents.touchUpInside)
        
        let participant = activityObject.participant[section]
        
        
        participantCellHeader.msgBTN.tag = participant.id
            participantCellHeader.msgBTN.addTarget(self, action: #selector(msgClicked(sender :)), for: .touchUpInside)
        
        participantCellHeader.nomPrenomLBL.text = participant.prenom + " " + participant.nom
         if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
        participantCellHeader.pratique.text = participant.sport.pratique
        participantCellHeader.niveau.text =  " - " + participant.sport.niveau
         }else {
            switch participant.sport.pratique {
            case SportSaifFR[0] :
                participantCellHeader.pratique.text = SportSaifEN[0]
            case SportSaifFR[1] :
                 participantCellHeader.pratique.text = SportSaifEN[1]
            case SportSaifFR[2] :
                 participantCellHeader.pratique.text = SportSaifEN[2]
            case SportSaifFR[3] :
                participantCellHeader.pratique.text = SportSaifEN[3]
            case SportSaifFR[4] :
                 participantCellHeader.pratique.text = SportSaifEN[4]
            case SportSaifFR[5] :
                 participantCellHeader.pratique.text = SportSaifEN[5]
            default :
                participantCellHeader.pratique.text = ""
            }
            switch participant.sport.niveau {
            case NiveauSaifFR[0] :
                participantCellHeader.niveau.text =  " - " + NiveauSaifEN[0]
            case NiveauSaifFR[1] :
                participantCellHeader.niveau.text =  " - " + NiveauSaifEN[1]
            case NiveauSaifFR[2] :
               participantCellHeader.niveau.text =  " - " + NiveauSaifEN[2]
            case NiveauSaifFR[3] :
                participantCellHeader.niveau.text =  " - " + NiveauSaifEN[3]
            case NiveauSaifFR[4] :
               participantCellHeader.niveau.text =  " - " + NiveauSaifEN[4]
            default :
              participantCellHeader.niveau.text =  ""
            }
        }
        
        switch participant.sport.niveau {
        case "Débutant"? : participantCellHeader.niveau.textColor = niveauColor[0]
        case "Débrouillard"? :  participantCellHeader.niveau.textColor = niveauColor[1]
        case "Intermédiaire"? :  participantCellHeader.niveau.textColor = niveauColor[2]
        case "Confirmé"? : participantCellHeader.niveau.textColor = niveauColor[3]
        case "Expert"? :  participantCellHeader.niveau.textColor = niveauColor[4]
        default : break
        }
        
        let str:String = arrStatus[section] as! String
        if str == "0"
        {
            UIView.animate(withDuration: 2) { () -> Void in
                participantCellHeader.expandCollapseButton.setImage(#imageLiteral(resourceName: "expand-infoB-kissou"),for: .normal)
            }
        }
        else
        {
            UIView.animate(withDuration: 2) { () -> Void in
                participantCellHeader.expandCollapseButton.setImage(#imageLiteral(resourceName: "collapse-infoB-kissou"), for: .normal)
            }
        }
        return participantCellHeader
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return  1.1
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return  1.1
    }
    
    @objc func msgClicked(sender : UIButton){
        
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let jsonPres = JSON(data: dataFromString!)
        
        LoaderAlert.show()
        let params: Parameters = [
            "amiId": sender.tag
        ]
      
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : jsonPres["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.addConvPriv , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                print ("add conv "  , b)
                if b["status"].boolValue {
                    
                    if let ConvId = b["data"]["id"].int {
                        DispatchQueue.main.async(execute: {
                             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "gotToActivityMsg"), object: ConvId )
                        })
                    }
                }
        }
    }
    
    @objc func headerCellButtonTapped(_sender: UIButton)
    {
        // initSectionStateTableView()
        let str:String = arrStatus[_sender.tag] as! String
        if str == "0"
        {
            //initSectionStateTableView()
            arrStatus[_sender.tag] = "1"
        } else
        {
            arrStatus[_sender.tag] = "0"
        }
        
        if previousSection >= 0 && self.previousSection !=  _sender.tag {
            //
            //   participantTV.re
            arrStatus[previousSection] = "0"
            
        //    print("sakker    index"  )
            participantTV.beginUpdates()
            participantTV.reloadSections(IndexSet(integer: _sender.tag) as IndexSet, with: UITableViewRowAnimation.automatic)
            participantTV.reloadSections(IndexSet(integer:self.previousSection ) as IndexSet, with: UITableViewRowAnimation.none)
            previousSection = _sender.tag
            participantTV.endUpdates()
            
            return
        }
        
        previousSection = _sender.tag
        //participantTV.reloadData()
        participantTV.beginUpdates()
        participantTV.reloadSections(IndexSet(integer: _sender.tag) as IndexSet, with: UITableViewRowAnimation.automatic)
        participantTV.endUpdates()
    }
    
    
}

