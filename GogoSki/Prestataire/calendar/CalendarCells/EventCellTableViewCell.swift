//
//  EventCellTableViewCell.swift
//  CalendarTest
//
//  Created by AYMEN on 10/4/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit

class EventCellTableViewCell: UITableViewCell {

    @IBOutlet weak var imageEvent: UIImageView!
    @IBOutlet weak var titleEvent: UILabel!
    @IBOutlet weak var departEvent: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var startClock: UILabel!
    @IBOutlet weak var endClock: UILabel!
    

}
