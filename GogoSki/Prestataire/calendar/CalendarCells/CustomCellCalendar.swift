//
//  CustomCellCalendar.swift
//  Events for All
//
//  Created by AYMEN on 9/21/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CustomCellCalendar: JTAppleCell {
    
    @IBOutlet weak var eventBackground: UIView!
    
    @IBOutlet weak var eventsNumber: UILabel!
    //    @IBOutlet weak var eventsNumber: UILabel!
    
    @IBOutlet weak var labelCell : UILabel!
    @IBOutlet weak var selectedView : UIView!
    
    @IBOutlet weak var todayView: UIView!

}
