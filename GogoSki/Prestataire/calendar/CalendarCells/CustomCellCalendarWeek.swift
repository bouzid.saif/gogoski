//
//  CustomCellCalendarWeek.swift
//  CalendarTest
//
//  Created by AYMEN on 10/8/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CustomCellCalendarWeek: JTAppleCell {
   
    @IBOutlet weak var dayName: UILabel!
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var todayView: UIView!
    @IBOutlet weak var dayLabel: UILabel!
    
}
