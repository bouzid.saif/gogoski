//
//  InscriptionPrestataireViewController.swift
//  GogoSki
//
//  Created by AYMEN on 11/14/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DownPicker
import MapKit

class InscriptionPrestataireViewController: InternetCheckRed , UIGestureRecognizerDelegate,PassProtocolEmplacement{
   
    //massif département station
    
    @IBOutlet weak var pickPositionLBL: UILabel!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var stationTF: UITextField!
    @IBOutlet weak var departementTF: UITextField!
    @IBOutlet weak var massifTF: UITextField!
    @IBOutlet weak var backBTN: UIButton!
    @IBOutlet weak var inscriptionNavLBL: UILabel!
    @IBOutlet weak var typePrestationTF: UITextField!
    @IBOutlet weak var passwordTFINSCRIPTION: UITextField!
    @IBOutlet weak var emailTFINSCRIPTION: UITextField!
 
    @IBOutlet weak var inscriptionBTN: UIButton!
    var selectedStation = ""
    let typePrestataireArray: NSMutableArray = ["Prestataire de ski"]
    let typePrestataireArrayEN: NSMutableArray = ["Ski provider"]
    let arrayPrest = ["ROLE_PRES_SKI"]
    
    var massifsArray = [String]()
    var departementsArray = [String]()
    var stationsArray = [String]()
 //   var dicDepStation = Dictionary<String,[String]>()
  //  var dicMAsiif = Dictionary<String,(Dictionary<String,[String]>)>()
    var annotation = MKPointAnnotation()
    var StationObject : JSON = []
    
    var typePrestataireDownPicker : DownPicker!

    var massif_downPicker : DownPicker!
    var departement_downPicker : DownPicker!
    var station_downPicker : DownPicker!
    ///langue
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    
    var array : NSMutableArray!
    var selectedLong : Double! = 0
    var selectedLat : Double! = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        stationTF.placeholder = Localization("Station")
        departementTF.placeholder = Localization("Departement")
        massifTF.placeholder = Localization("Massif")
    }
    
    ///adminEnabled 
    func pass(data: EmplacementObj) {
        print("yeahh")
        print(data.laltitude)
        if data.laltitude != 0 {
            self.selectedLat = data.laltitude
            self.selectedLong = data.longitude
            for annot in map.annotations {
                map.removeAnnotation(annot)
            }
            annotation.coordinate = CLLocationCoordinate2D(latitude: self.selectedLat, longitude: self.selectedLong)
            map.addAnnotation(annotation)
        }else{
             _ = SweetAlert().showAlert(Localization("Emplacement"), subTitle: Localization("devezEmplacement"), style: AlertStyle.warning)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            array = typePrestataireArray
        }else {
            array = typePrestataireArrayEN
        }
        self.typePrestataireDownPicker = DownPicker(textField: self.typePrestationTF, withData: array as! [Any])
        typePrestataireDownPicker.setArrowImage(nil)
        
        self.typePrestataireDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.typePrestataireDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        typePrestataireDownPicker.setPlaceholder(Localization("typePrest"))
        
        self.loadStations()
        
        let point = CLLocationCoordinate2D(latitude: 48.956467, longitude: 4.358637)
        self.map.setCenter(point, animated: true)

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        gestureRecognizer.delegate = self
        map.addGestureRecognizer(gestureRecognizer)
        map.isPitchEnabled = false
        map.isZoomEnabled = false
        map.isRotateEnabled = false
        map.isScrollEnabled = false
        
    }
    
    @objc func handleTap(_ gestureReconizer: UILongPressGestureRecognizer) {
        
      /*  let location = gestureReconizer.location(in: map)
        let coordinate = map.convert(location,toCoordinateFrom: map)
        
        // Add annotation:
        annotation.coordinate = coordinate
        
        selectedLong = coordinate.longitude
        selectedLat = coordinate.latitude
       
        map.addAnnotation(annotation) */
        let view = self.storyboard?.instantiateViewController(withIdentifier: "choisirRDVPres") as! Choisir_Emplacement_Pres
        view.delegate = self
        
            view.ObjToPAss.longitude = self.selectedLong
            view.ObjToPAss.laltitude = self.selectedLat
        
        self.navigationController?.pushViewController(view, animated: true)
    }

    @IBAction func inscrirePrestataire(_ sender: Any) {
        
        if self.emailTFINSCRIPTION.text! != "" && self.passwordTFINSCRIPTION.text! != "" && self.stationTF.text! != "" && self.massifTF.text! != "" && self.passwordTFINSCRIPTION.text! != "" && selectedLat != 0 && selectedLong != 0 {
            
            if isValidEmail(testStr: self.emailTFINSCRIPTION.text!){
                   LoaderAlert.shared.show(withStatus: Localization("ChargementInscription"))
                        self.inscriptionBTN.isEnabled = false
                    self.backBTN.isEnabled = false

                        let params: Parameters = [
                        "email": self.emailTFINSCRIPTION.text!,
                        "password" : self.passwordTFINSCRIPTION.text!,
                        "langue" : getLangue(),
                        "role" : "ROLE_PRES_SKI",
                        "station" : self.selectedStation,
                        "longitude" : selectedLong,
                        "latitude" : selectedLat
                        ]
                        let header: HTTPHeaders = [
                            "Content-Type" : "application/json"
                        ]
                    print(params)
                        Alamofire.request(ScriptBase.sharedInstance.inscriptionPrestataire , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                                .responseJSON { response in
                                    LoaderAlert.shared.dismiss()
                                    self.inscriptionBTN.isEnabled = true
                                    self.backBTN.isEnabled = true
                                    print("response : ",response)
                                    if let responseData = response.data {
                                        let a = JSON(responseData)
                                        if a["status"].intValue == 1 {
                                            self.emailTFINSCRIPTION.text = ""
                                            self.passwordTFINSCRIPTION.text = ""
                                           
                                            _ = SweetAlert().showAlert(Localization("ChargementInscription"), subTitle: Localization("InscriptionErreur"), style: AlertStyle.success)
                                            
                                        }else{
                                            print("neiiin")
                                            if a["message"].stringValue == "email existe déja"{
                                                self.emailTFINSCRIPTION.text = ""
                                                self.passwordTFINSCRIPTION.text = ""
                                                _ = SweetAlert().showAlert(Localization("ChargementInscription"), subTitle: Localization("InscriptionExist"), style: AlertStyle.error)
                                            }
                                        }
                                    }
                    }
                
            }else{
                  _ = SweetAlert().showAlert(Localization("ChargementInscription"), subTitle: Localization("emailInvalidPres"), style: AlertStyle.error)
            }
        }else{
            _ = SweetAlert().showAlert(Localization("ChargementInscription"), subTitle: Localization("infoManquate"), style: AlertStyle.error)
        }
    }
    
    func getStationID() -> String {
        
        for i in 0...StationObject["data"].arrayObject!.count - 1 {
            if StationObject["data"][i]["massif"].stringValue == self.massifTF.text! && StationObject["data"][i]["nom"].stringValue == self.stationTF.text! && StationObject["data"][i]["dept"].stringValue == self.departementTF.text! {
                
                print("id : ",StationObject["data"][i].stringValue)
                return StationObject["data"][i].stringValue
            }
        }
        
        return "0"
    }
    
    
    /*
     *load all the stations
     *
     */
    func loadStations(){
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : "nothing"
        ]
        Alamofire.request(ScriptBase.sharedInstance.StationsPres , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                if let responseData = response.data{
                        let a = JSON(responseData)
                        print("a")
                    if a["status"].boolValue == true{
                        self.StationObject = a
                        for i in 0 ..< a["data"].arrayObject!.count {
                            self.massifsArray.append(a["data"][i]["massif"].stringValue)
                        }
                        self.massifsArray = self.massifsArray.removeDuplicates()
                        print("Stations: ",a)
                        let bandArray4 : NSMutableArray = []
                        bandArray4.addObjects(from: self.massifsArray)
                        self.massif_downPicker = DownPicker(textField: self.massifTF, withData: bandArray4 as! [Any])
                        self.massif_downPicker.addTarget(self, action: #selector(self.massifDidChange), for : .valueChanged )
                        self.massif_downPicker.setArrowImage(nil)
                        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                            self.massif_downPicker.setToolbarDoneButtonText("Terminé")
                            self.massif_downPicker.setToolbarCancelButtonText("Annuler")
                            self.massif_downPicker.setPlaceholder(Localization("Massif"))
                        }
                        else {
                            self.massif_downPicker.setToolbarDoneButtonText("Done")
                            self.massif_downPicker.setToolbarCancelButtonText("Cencel")
                            self.massif_downPicker.setPlaceholder(Localization("Massif"))
                        }
                    }else{
                        SweetAlert().showAlert(Localization("loadingStation"), subTitle: Localization("ModProfExist"), style: AlertStyle.warning, buttonTitle:"ok", buttonColor: UIColor.gray) { (isOtherButton) -> Void in
                            self.navigationController?.popViewController(animated: true)
                        }
                        //ModProfExist
                    }
                }else{
                    ///
                }
        }
    }
    
    /*
     *user choosen a massif
     *
     */
    @objc func massifDidChange() {
        print("massif changed")
        
        departementTF.text = ""
        stationTF.text = ""
        
        
        var departementx : [String] = []
        if self.massifTF.text! != "" {
            for i in 0...StationObject["data"].arrayObject!.count - 1 {
                if StationObject["data"][i]["massif"].stringValue == self.massifTF.text! {
                    departementx.append(StationObject["data"][i]["dept"].stringValue)
                }
            }
            departementx = departementx.removeDuplicates()
            let bandArray4 : NSMutableArray = []
            bandArray4.addObjects(from: departementx)
            self.departement_downPicker = DownPicker(textField: self.departementTF, withData: bandArray4 as! [Any])
            self.departement_downPicker.setArrowImage(nil)

            departement_downPicker.addTarget(self, action: #selector(self.departementDidChange), for : .valueChanged )
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                self.departement_downPicker.setToolbarDoneButtonText("Terminé")
                self.departement_downPicker.setToolbarCancelButtonText("Annuler")
                self.departement_downPicker.setPlaceholder(Localization("Departement"))
            }
            else {
                self.departement_downPicker.setToolbarDoneButtonText("Done")
                self.departement_downPicker.setToolbarCancelButtonText("Cencel")
                self.departement_downPicker.setPlaceholder(Localization("Departement"))
            }
        }        
    }
    
    /*
     *user chosen a department
     *
     */
    @objc func departementDidChange() {
        print("massif changed")
        stationTF.text = ""
        var stationsx : [String] = []
        print("true")
        if self.massifTF.text! != "" && self.departementTF.text! != "" {
            for i in 0...StationObject["data"].arrayObject!.count - 1 {
                print("massif:",StationObject["data"][i]["massif"].stringValue)
                print("dep:",StationObject["data"][i]["dept"].stringValue)
                if StationObject["data"][i]["massif"].stringValue == self.massifTF.text! && StationObject["data"][i]["dept"].stringValue == self.departementTF.text! {
                    print("false")
                    stationsx.append(StationObject["data"][i]["nom"].stringValue)
                }
            }
            //stationsx = stationsx.removeDuplicates()
            let bandArray4 : NSMutableArray = []
            bandArray4.addObjects(from: stationsx)
            self.station_downPicker = DownPicker(textField: self.stationTF, withData: bandArray4 as! [Any])
            self.station_downPicker.setArrowImage(nil)
station_downPicker.addTarget(self, action: #selector(self.stationDidChange), for : .valueChanged )
          //  station_downPicker.addTarget(self, action: #selector(self.stationDidChange), for : .valueChanged )
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                self.station_downPicker.setToolbarDoneButtonText("Terminé")
                self.station_downPicker.setToolbarCancelButtonText("Annuler")
                self.station_downPicker.setPlaceholder(Localization("Station"))
            }
            else {
                self.station_downPicker.setToolbarDoneButtonText("Done")
                self.station_downPicker.setToolbarCancelButtonText("Cencel")
                self.station_downPicker.setPlaceholder(Localization("Station"))
            }
        }
    }
    
     @objc func stationDidChange() {
    if self.massifTF.text! != "" && self.departementTF.text! != "" && self.stationTF.text! != ""{
          for i in 0...StationObject["data"].arrayObject!.count - 1 {
         if StationObject["data"][i]["massif"].stringValue == self.massifTF.text! && StationObject["data"][i]["dept"].stringValue == self.departementTF.text! && StationObject["data"][i]["nom"].stringValue == self.stationTF!.text {
            self.selectedStation = StationObject["data"][i]["id"].stringValue
            }
        }
        }
    }
    
    
    func getLangue() -> String {
        var langue : String! = ""
        if Localisator.sharedInstance.currentLanguage == arrayLanguages[0] {
            if NSLocale.current.languageCode == "en" {
                
                langue = Localization(arrayLanguages[1])
            }else if NSLocale.current.languageCode == "fr"  {
                langue = Localization(arrayLanguages[2])
            }
        }
        else if Localisator.sharedInstance.currentLanguage == arrayLanguages[1]
        {
            langue = Localization(arrayLanguages[1])
        }
        else {
            langue = Localization(arrayLanguages[2])
        }
        return langue
    }
    
    @IBAction func goBackFromInsription(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        
        pickPositionLBL.text = Localization("pickPosPres")
        inscriptionNavLBL.text = Localization("labelInscriptionUser")
        emailTFINSCRIPTION.placeholder = Localization("labelMail")
        inscriptionBTN.setTitle(Localization("btnInscritUser"), for: .normal)

        
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    public func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
}
