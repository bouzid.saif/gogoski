//
//  PAssProtocolEmplacement.swift
//  GogoSki
//
//  Created by macbook on 2018-02-06.
//  Copyright © 2018 Velox-IT. All rights reserved.
//

import Foundation
protocol PassProtocolEmplacement {
    func pass(data: EmplacementObj)
}
