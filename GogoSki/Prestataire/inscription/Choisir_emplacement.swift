//
//  Choisir_emplacement.swift
//  GogoSki
//
//  Created by macbook on 2018-02-06.
//  Copyright © 2018 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class Choisir_Emplacement_Pres : InternetCheckRed,MKMapViewDelegate,UIGestureRecognizerDelegate {
    @IBOutlet weak var Map : MKMapView!
    var selectedLong : Double! = 0
    var selectedLat : Double! = 0
    var annotation = MKPointAnnotation()
    var ObjToPAss = EmplacementObj()
    var delegate: PassProtocolEmplacement?

    override func viewDidLoad() {
        super.viewDidLoad()
        let point = CLLocationCoordinate2D(latitude: 48.956467, longitude: 4.358637)
        self.Map.setCenter(point, animated: true)
        if ObjToPAss.laltitude != 0 {
            annotation.coordinate = CLLocationCoordinate2D(latitude: ObjToPAss.laltitude, longitude: ObjToPAss.longitude)
            Map.addAnnotation(annotation)
        }
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        gestureRecognizer.delegate = self
        Map.addGestureRecognizer(gestureRecognizer)
    }
    @objc func handleTap(_ gestureReconizer: UILongPressGestureRecognizer) {
        for annot in Map.annotations {
            Map.removeAnnotation(annot)
        }
        let location = gestureReconizer.location(in: Map)
        let coordinate = Map.convert(location,toCoordinateFrom: Map)
        
        // Add annotation:
        annotation.coordinate = coordinate
        
        selectedLong = coordinate.longitude
        selectedLat = coordinate.latitude
        
        Map.addAnnotation(annotation)
        ObjToPAss.laltitude = selectedLat
        ObjToPAss.longitude = selectedLong
        self.go_back(self)
    }
    /*
     * back action
     */
    @IBAction func  go_back(_ sender :Any) {
        self.delegate?.pass(data: ObjToPAss)
        self.navigationController?.popViewController(animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
