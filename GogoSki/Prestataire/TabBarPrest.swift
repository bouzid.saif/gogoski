//
//  TabBarControllViewController.swift
//  GogoSki
//
//  Created by Bouzid saif on 06/12/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit

class TabBarPrest: UITabBarController,UITabBarControllerDelegate {
    var ConvId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotifMessagePres(notification:)), name: NSNotification.Name(rawValue: "PushMessagePres"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotifPres(notification:)), name: NSNotification.Name(rawValue: "PushNotifPres"), object: nil)
    //    NotificationCenter.default.addObserver(self, selector: #selector(self.handleMessageActive(notification:)), name: NSNotification.Name(rawValue: "MessageViewActive"), object: nil)
     //   NotificationCenter.default.addObserver(self, selector: #selector(self.handle_go_to_message(notification:)), name: NSNotification.Name(rawValue: "GoToMessageForomNoWhere"), object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(self.gotToMsgAct(notification:)), name: NSNotification.Name(rawValue: "gotToActivityMsg"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.addBadge(notification:)), name: NSNotification.Name(rawValue: "AddBadge2"), object: nil)
        configureViewFromLocalisation()
        // Do any additional setup after loading the view.
    }
    @objc func addBadge (notification: NSNotification)
    {
        
        let tabBarController = self
        tabBarController.delegate = self
        if !tabBarController.viewControllers![1].isBeingPresented {
            if tabBarController.viewControllers![1].tabBarItem.badgeValue == nil {
                tabBarController.viewControllers![1].tabBarItem.badgeValue = "1"
            }else {
                tabBarController.viewControllers![1].tabBarItem.badgeValue = String(Int(tabBarController.viewControllers![1].tabBarItem.badgeValue!)! + 1 )
            }
        }
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    @objc func gotToMsgAct(notification: NSNotification)
    {
        print("got it in tab bar")

        let q = notification.object
        
        self.selectedIndex = 1

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "goMsgAct"), object: q)
        })
        
        
    }
    
    @objc func handle_go_to_message (notification: NSNotification)
    {
   //     popAll()
        let q = notification.object as! String
        ConvId = q
        self.selectedIndex = 1
        
    }
    
    @objc func handleNotifPres(notification: NSNotification) {
    //    popAll()
       
        self.selectedIndex = 1
       
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotifIsActive"), object: self.ConvId as! String)
            })
        
    }
    
    @objc func handleNotifMessagePres(notification: NSNotification) {
      //  popAll()
        print("goMsgAct")
        let q = notification.object as! String
        ConvId = q
        self.selectedIndex = 1
        if let conversationId = Int(q){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "goMsgAct"), object: conversationId)
            })
        }
    }
  /*
    @objc func handleMessageActive(notification: NSNotification) {
        
        print("ConvId:", ConvId)
        
        if ConvId != "0" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MessageIsActive"), object: ConvId as! String)
        }else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotifIsActive"), object: self.ConvId as! String)
            })
        }
        
        ConvId = ""
    }
 */
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        popAll()
    }
    func configureViewFromLocalisation() {
        
        
        let tabBarController = self
        tabBarController.delegate = self
        if let tabBarViewControllers = tabBarController.viewControllers {
            let campusController = tabBarViewControllers[0] as! UINavigationController
            campusController.title = Localization("Agenda")
            let adController = tabBarViewControllers[1] as! UINavigationController
            adController.title = Localization("Messages")
            let LeconVC = tabBarViewControllers[2] as! UINavigationController
            LeconVC.title = Localization("Profile")
           
        }
        
        //Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    func popAll(){
        
        let tabBarController = self
        tabBarController.delegate = self
        
        if let tabBarViewControllers = tabBarController.viewControllers {
            
            let campusController = tabBarViewControllers[0] as! UINavigationController
            
            let campusTVC = campusController.viewControllers[0] as! WeekCalendarViewController
            
            _ = campusTVC.navigationController?.popToRootViewController(animated: false)
            
            let adController = tabBarViewControllers[1] as! UINavigationController
            
            let adminTVC = adController.viewControllers[0] as! PageMenuVC
            
            _ = adminTVC.navigationController?.popToRootViewController(animated: false)
            
              let searchController = tabBarViewControllers[2] as! UINavigationController
            
            let searchTVC = searchController.viewControllers[0] as! ProfilPrestataireVC
            _ = searchTVC.navigationController?.popToRootViewController(animated: false)
            
            /*   let SejourVC = tabBarViewControllers[3] as! UINavigationController
             
             let Sejour = SejourVC.viewControllers[0] as! Sejour
             
             _ = Sejour.navigationController?.popToRootViewController(animated: false) */
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

