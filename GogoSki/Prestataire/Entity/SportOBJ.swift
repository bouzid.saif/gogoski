//
//  SportOBJ.swift
//  GogoSki
//
//  Created by AYMEN on 11/29/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit

class SportOBJ: NSObject {
    var id : Int! = 0
    var pratique : String! = ""
    var niveau : String! = ""
    var isDefault : Int! = 0
    var photo : String! = ""
}
