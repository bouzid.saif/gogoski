//
//  MoniteurOBJ.swift
//  GogoSki
//
//  Created by AYMEN on 12/4/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit

class MoniteurOBJ: NSObject {

    var id : Int! = 0
    var nom : String! = ""
    var prenom : String! = ""
    var photo : String! = ""
    var sports = [SportOBJ]()
    var priorite : Int! = 0

}
