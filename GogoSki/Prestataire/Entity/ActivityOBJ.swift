//
//  ActivityOBJ.swift
//  GogoSki
//
//  Created by AYMEN on 1/11/18.
//  Copyright © 2018 Velox-IT. All rights reserved.
//

import UIKit

class ActivityOBJ: NSObject {

    
    var date_depart = ""
    var titre = ""
    var desc = ""
    var participant = [ParticipantOBJ]()
    var moniteur = MoniteurOBJ()
    var Tarif = TarifOBJ()
    var duree = 0
    var nombreParticipantMax = 0
    var sport = SportOBJ()
    var canUpdate = false
}
