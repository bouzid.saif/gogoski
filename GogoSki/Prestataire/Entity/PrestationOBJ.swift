//
//  PrestationOBJ.swift
//  GogoSki
//
//  Created by AYMEN on 11/29/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit

class PrestationOBJ: NSObject {
    
    var id : Int = 0
    var titre : String = ""
    var nombreParticipantMax : Int = 0
    var nombreParticipantMin : Int = 0
    var stockPrestation : Int = 0
    var duree : String = ""
    var sport : SportOBJ = SportOBJ()
    var tarifs : TarifOBJ = TarifOBJ()
    
}

