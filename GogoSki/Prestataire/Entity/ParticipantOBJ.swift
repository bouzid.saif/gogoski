//
//  ParticipantOBJ.swift
//  GogoSki
//
//  Created by AYMEN on 1/11/18.
//  Copyright © 2018 Velox-IT. All rights reserved.
//

import UIKit

class ParticipantOBJ: NSObject {

    var id = 0
    var nom = ""
    var prenom = ""
    var sport = SportOBJ()
    var carteBancaire = CarteBancaireOBJ()
    var photo = ""
    
}
