//
//  TarifOBJ.swift
//  GogoSki
//
//  Created by AYMEN on 11/29/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit

class TarifOBJ: NSObject {

    var prixMin : Double! = 0.0
    var prixMax : Double! = 0.0
    var prix : Double! = 0.0
    var dateDebut : Date?
    var dateFin : Date?
}
