import Foundation
import SwiftMessages

public protocol InternetStatusIndicable2 : class {
    var internetConnectionIndicator:InternetViewIndicator2? { get set }
    func startMonitoringInternet(backgroundColor:UIColor, style: MessageView.Layout, textColor:UIColor, message:String, remoteHostName: String)
    
}

extension InternetStatusIndicable2 where Self: UIViewController {
    
    public func startMonitoringInternet(backgroundColor: UIColor = UIColor.red, style: MessageView.Layout = .statusLine, textColor: UIColor = UIColor.white, message: String = "Merci de vérifier votre connexion à internet", remoteHostName: String = "apple.com") {
        
        internetConnectionIndicator = InternetViewIndicator2(backgroundColor: backgroundColor, style: style, textColor: textColor, message: message, remoteHostName: remoteHostName)
    
    }
}
