//
//  SubmitProfilePrestataireViewController.swift
//  GogoSki
//
//  Created by AYMEN on 1/29/18.
//  Copyright © 2018 Velox-IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DownPicker

class SubmitProfilePrestataireViewController: InternetCheckRed , UITextViewDelegate , UIImagePickerControllerDelegate , UINavigationControllerDelegate  {

    @IBOutlet weak var topBarTitleLBL: UILabel!
    @IBOutlet weak var userPictureIMG: UIImageView!
    @IBOutlet weak var choosePicBTN: UIButton!
    @IBOutlet weak var langueTitleLBL: UILabel!
    
    @IBOutlet weak var langueTF: UITextField!
    @IBOutlet weak var informationGeneraleLBL: UILabel!
    @IBOutlet weak var informationGeneralTF: UITextField!
    @IBOutlet weak var adresseLBL: UILabel!
    
    @IBOutlet weak var rueTF: UITextField!
    @IBOutlet weak var villeTF: UITextField!
    @IBOutlet weak var codePostalTF: UITextField!
    @IBOutlet weak var paysTF: UITextField!
    @IBOutlet weak var descTitleLBL: UILabel!
    
    @IBOutlet weak var descTF: UITextView!
    @IBOutlet weak var validerModBTN: UIButton!
    
    var langueDownPicker: DownPicker!
    var paysDownPicker: DownPicker!
    
    let langues: NSMutableArray = ["English" , "French"]
    let pays: NSMutableArray = ["Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"]
    var json : JSON!
    
    let bandArray : NSMutableArray = []
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    
    @objc let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bandArray.add(Localization("French_fr"))
        bandArray.add(Localization("English_en"))
        
        self.langueDownPicker = DownPicker(textField: self.langueTF, withData: bandArray as! [Any])
        langueDownPicker.setArrowImage(nil)
        self.paysDownPicker = DownPicker(textField: self.paysTF, withData: pays as! [Any])
        paysDownPicker.setPlaceholder("pay")
        paysDownPicker.setArrowImage(nil)
        
        descTF.delegate = self
        
        descTF.text = Localization("descTextView")
        descTF.textColor = UIColor.lightGray
        
        getData()
        
        imagePicker.delegate = self
      
            getMonProfil()
       
        
        
        choosePicBTN.titleLabel?.minimumScaleFactor = 0.2
        choosePicBTN.titleLabel?.numberOfLines = 1
        choosePicBTN.titleLabel?.adjustsFontSizeToFitWidth = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func choisirPhotoAction(_ sender: Any) {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func submitProfileAction(_ sender: Any) {
        self.myImageUploadRequest()
       
    }
    
    
    func updateProfile(url : String){
     //   LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
        
      
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : json["value"].stringValue
            ]
            let adminEnabledString = "1"
        var desc = ""
        if descTF.text == Localization("descTextView") {
            desc = ""
        }else{
            desc = descTF.text!.trimmingCharacters(in: .whitespaces)
        }
            let params: Parameters = [
                "langue" : langueTF.text!,
                "nomEtablissement" : informationGeneralTF.text!,
                "description"  : desc,
                "address"  : ["pays" : paysTF.text!.trimmingCharacters(in: .whitespaces),
                              "code" : codePostalTF.text!.trimmingCharacters(in: .whitespaces),
                              "rue" : rueTF.text!.trimmingCharacters(in: .whitespaces),
                              "ville" : villeTF.text!.trimmingCharacters(in: .whitespaces)
                ],
                "adminEnabled" : adminEnabledString,
                "photo" : url
            ]
            
            Alamofire.request(ScriptBase.sharedInstance.modifierProfilePrestataire , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseString { response in
                    LoaderAlert.shared.dismiss()
                    print("b:",response)
                    let b = JSON(response.data)
                    
                    
                    if b["status"].boolValue {
                        //       _ = SweetAlert().showAlert("Modification Profil", subTitle: "Changement effectué", style: AlertStyle.success)
                        
                        if (self.langueTF.text == Localization("French_fr"))
                        {
                            SetLanguage(self.arrayLanguages[2])
                        }
                        else {
                            SetLanguage(self.arrayLanguages[1])
                        }
                        Localisator.sharedInstance.saveInUserDefaults = true
                         self.validerModBTN.isEnabled = true
                        SweetAlert().showAlert(Localization("modifProfil"), subTitle: Localization("inscritPresAdmin"), style: AlertStyle.warning, buttonTitle:"ok", buttonColor: UIColor.gray) { (isOtherButton) -> Void in
                            let login = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PageBienvenue") as! PageBienvenue
                            
                            let transition = CATransition()
                            transition.duration = 0.5
                            transition.type = kCATransitionPush
                            transition.subtype = kCATransitionFromLeft
                            
                            self.navigationController?.view.window?.layer.add(transition, forKey: kCATransition)
                            self.navigationController?.tabBarController?.tabBar.isHidden = true
                            self.navigationController?.pushViewController(login, animated: true)
                            
                            return
                            
                        }
                        
                       
                        
                    }else{
                        self.validerModBTN.isEnabled = true
                        LoaderAlert.shared.dismiss()
                        _ = SweetAlert().showAlert(Localization("modifProfil"), subTitle: "Une erreur est survenue", style: AlertStyle.error)
                    }
            }
     
    }
    func getData(){
        
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        json = JSON(data: dataFromString!)
      
    }
    
    func getMonProfil(){
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json" ,
            "x-Auth-Token" : json["value"].stringValue
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.monProfilPres , method: .get , encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                let jsonprofile = JSON(response.data)
                print(jsonprofile)
                if jsonprofile["status"].boolValue == false {
                    
                    _ = SweetAlert().showAlert("", subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                    
                }else{
                    if ( jsonprofile["data"]["langue"].description != "null"){
                        if (jsonprofile["data"]["langue"].string!.first! == "f" || jsonprofile["data"]["langue"].string!.first! == "F"  )
                        {
                            self.langueTF.text = Localization("French_fr")
                        }else {
                            self.langueTF.text = Localization("English_en")
                        }
                        //  = jsonprofile["data"]["langue"].string!
                    }
                 
                }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if descTF.textColor == UIColor.lightGray {
            descTF.text = nil
            descTF.textColor = UIColor("#24253D")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty || textView.text.trimmingCharacters(in: .whitespaces).isEmpty {
            textView.text = "Décrivez-vous en quelques lignes."
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    /////////********langue"**********///////
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        
        self.langueDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.langueDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        self.paysDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.paysDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
      //  modifierProfileTitleLBL.text = Localization("ModifierProfile")
        informationGeneraleLBL.text = Localization("ModifierProfileInformationGen")
        informationGeneralTF.placeholder = Localization("ModifierProfileNomEtablissement")
        
        adresseLBL.text = Localization("ModifierProfileAdresse")
        
        rueTF.placeholder = Localization("ModifierProfileRue")
        villeTF.placeholder = Localization("ModifierProfileVille")
        codePostalTF.placeholder = Localization("ModifierProfileCodePostal")
        paysTF.placeholder = Localization("ModifierProfileCodePay")
        descTitleLBL.text = Localization("ModifierProfileAjouterDescription")
        langueTitleLBL.text = Localization("PageLangueTitle")
        validerModBTN.setTitle(Localization("BtnValiderModif"), for: .normal)
        choosePicBTN.setTitle(Localization("ModifierProfilePhotoBtn"), for: .normal)
        
        Localisator.sharedInstance.saveInUserDefaults = true
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //let user = SharedPreferences.sharedpref.prefs.string(forKey: "id_user")! as String
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            DispatchQueue.main.async(execute: {
                // self.imageView?.contentMode = .redraw
                // SwiftSpinner.show("Uploading...")
                self.userPictureIMG?.image = pickedImage
                // self.uploadnow.setTitle("Upload Now", for: .normal)
                
            })
        }
        dismiss(animated: true, completion: nil)
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        
        dismiss(animated: true, completion: nil)
    }
    
    
    ////
    func myImageUploadRequest()
    {
        validerModBTN.isEnabled = false
        if langueTF.text?.trimmingCharacters(in: .whitespaces) != "" && informationGeneralTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
            rueTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
            villeTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
            paysTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
            codePostalTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
            descTF.text?.trimmingCharacters(in: .whitespaces) != ""
        {
            
            var Url = ""
            //SwiftSpinner.show("Uploading Images...")
            LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
            let myUrl = NSURL(string: ScriptBase.sharedInstance.uploadImage)
            
            let request = NSMutableURLRequest(url:myUrl! as URL);
            request.httpMethod = "POST";
            
            let param = [
                "name"  : "Anonymous"
            ]
            
            let boundary = self.generateBoundaryString()
            let ab = UserDefaults.standard.value(forKey: "pres") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            let imageData: NSData = UIImageJPEGRepresentation(userPictureIMG.image!, 1) as! NSData
            
            if(imageData == nil)  { return }
            
            request.httpBody = self.createBodyWithParameters(parameters: param, filePathKey: "image", imageDataKey: imageData as NSData, boundary: boundary) as Data
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if error != nil {
                    print("error=\(String(describing: error))")
                    return
                }
                
                // You can print out response object
                print("******* response = \(String(describing: response))")
                
                // Print out reponse body
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("****** response data = \(responseString!)")
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    print("JSON:",json as Any)
                    let j = JSON(json)
                    Url = j["url"].stringValue
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.updateProfile(url : Url)
                    })
                    
                }catch
                {
                    print("failed")
                    print(error)
                    Url = "Url Par default"
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.updateProfile(url : Url)
                    })
                }
                LoaderAlert.shared.dismiss()
                
            }
            task.resume()
        }else{
            _ = SweetAlert().showAlert(Localization("modifProfil"), subTitle: Localization("infoManquate"), style: AlertStyle.warning)
            self.validerModBTN.isEnabled = true
        }
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
}
