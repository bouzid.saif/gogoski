//
//  ConnexionPrestataire.swift
//  GogoSki
//
//  Created by Bouzid saif on 31/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import OneSignal

class ConnextionPrestataire: InternetCheckRed, OSSubscriptionObserver {
  
    
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
            // get player ID
            print("userId:" ,  stateChanges.to.userId)
            if stateChanges.to.userId != nil {
                let ab = UserDefaults.standard.value(forKey: "pres") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let params : Parameters = [
                    "os" : "ios" ,
                    "pid" : stateChanges.to.userId
                ]
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json"
                    ,"x-Auth-Token" : a["value"].stringValue
                ]
                Alamofire.request(ScriptBase.sharedInstance.subscribePush , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        //      LoaderAlert.shared.dismiss()
                        
                        let b = JSON(response.data!)
                        print(b)
                        
                        
                        print("subscribed")
                }
                //  else {
                //      _ = SweetAlert().showAlert("Conexion", subTitle: "Veuillez vous connecter", style: AlertStyle.error)
                //}
            }
        }
        stateChanges.to.userId
    }
 
    
  /*
     set checkbox on
    if OneSignal.getPermissionSubscriptionState().subscriptionStatus.subscribed {
        self.check2.setOn(true, animated: false , user: false)
    }
     
     
     on checkbox listenner state
     chenge one signal subscription state 
     OneSignal.setSubscription(true)
 */
    
    @IBOutlet weak var backBTN: UIButton!
    @IBOutlet weak var connexionLBL: UILabel!
    @IBOutlet weak var connexionBTN: UIButton!
    @IBOutlet weak var passwordTFCONX: UITextField!
    @IBOutlet weak var emailTFCONX: UITextField!
    
    //*** change langue
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    //****
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //   emailTFCONX.text = "faresPres@test.tn"
        //  passwordTFCONX.text = "fares"
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func go_back(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func connexionPrestataire(_ sender: Any) {
        if self.emailTFCONX.text != "" && self.passwordTFCONX.text != ""{
            LoaderAlert.shared.show(withStatus: Localization("ChargementConnextion"))
            self.connexionBTN.isEnabled = false
            self.backBTN.isEnabled = false
            let params: Parameters = [
                "email": self.emailTFCONX.text!,
                "password" : self.passwordTFCONX.text!
            ]
            let header: HTTPHeaders = [
                "Content-Type" : "application/json"
            ]
            
            Alamofire.request(ScriptBase.sharedInstance.connexionPrestataire , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    LoaderAlert.shared.dismiss()
                    self.connexionBTN.isEnabled = true
                    self.backBTN.isEnabled = true
                    let json = JSON(response.data)
                    print("response :  ",json)
                    if json["status"].boolValue == false {
                        if json["message"].stringValue == "L utilisateur n existe pas" || json["message"].stringValue == "Mot de passe invalide" {
                            
                            _ = SweetAlert().showAlert(Localization("ConnexionChargement"), subTitle: Localization("ConnexionErreur"), style: AlertStyle.error)
                        }else if json["message"].stringValue == "role Prestataire est exigé" {
                            _ = SweetAlert().showAlert(Localization("ConnexionChargement"), subTitle: Localization("UserInPres"), style: AlertStyle.error)
                        }else if json["message"].stringValue == "Votre compte doit etre valider par l' admin"{
                              _ = SweetAlert().showAlert(Localization("ConnexionChargement"), subTitle: Localization("inscritPresAdmin"), style: AlertStyle.error)
                        }
                        self.emailTFCONX.text = ""
                        self.passwordTFCONX.text = ""
                    }else{
                       
                        UserDefaults.standard.setValue(json["data"].rawString(), forKey: "pres")
                            UserDefaults.standard.synchronize()
                            print("$$$$$$$$$$$$$$$$$$$")
                            print(json["data"].rawString()!)
      
                         //   if json["data"]["user"]["nom_etablissement"].description != "null" {
                                // * change langue
                                if json["data"]["user"]["langue"].stringValue == "English" || json["data"]["user"]["langue"].stringValue == "Anglais" {
                                    print("setting langue",self.arrayLanguages[1])
                                    SetLanguage(self.arrayLanguages[1])
                                }else if json["data"]["user"]["langue"].stringValue == "French" || json["data"]["user"]["langue"].stringValue == "Français"  {
                                    print("setting langue",self.arrayLanguages[2])
                                    SetLanguage(self.arrayLanguages[2])
                                }
                                Localisator.sharedInstance.saveInUserDefaults = true
                             ///msg
                             //   self.ConnectTopush()
                        if json["data"]["user"]["tel_Notif"].boolValue {
                            OneSignal.setSubscription(true)
                        }else {
                             OneSignal.setSubscription(false)
                        }
                        SocketIOManager.sharedInstance.establishConnection()
                        let adminEnabled = json["data"]["user"]["admin_enabled"].stringValue
                        if ( adminEnabled == "0"){
                            let modif = UIStoryboard(name: "Prestataire", bundle: nil).instantiateViewController(withIdentifier: "modifFirst") as! SubmitProfilePrestataireViewController
                           // modif.isFirst = true
                            
                            self.navigationController?.show(modif, sender: true)
                            return
                        }
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userConnection"), object: nil)
                                self.ConnectTopush()

                                //*****
                                self.performSegue(withIdentifier: "homePres", sender: self)
                        }
            }
        }
        
    }
    
    func ConnectTopush(){
        
        OneSignal.add(self as OSSubscriptionObserver)
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.setSubscription(false)
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            OneSignal.setSubscription(true)
            let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
            let userID = status.subscriptionStatus.userId
            print("userID = \(userID)")
            let pushToken = status.subscriptionStatus.pushToken
            print("pushToken = \(pushToken)")
        })
    }
    
    func DisconnectFromPush(){
        OneSignal.remove(self as OSSubscriptionObserver)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        connexionLBL.text = Localization("ConnexionTitre")
        connexionBTN.setTitle(Localization("BtnConex"), for: .normal)
        
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /*
    func ConnectTopush(){
        
        OneSignal.add(self as OSSubscriptionObserver)
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.setSubscription(false)
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            OneSignal.setSubscription(true)
            let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
            let userID = status.subscriptionStatus.userId
            print("userID = \(userID)")
            let pushToken = status.subscriptionStatus.pushToken
            print("pushToken = \(pushToken)")
        })
        
    }
    */
  
    
}

