//
//  ModifierMoniteurViewController.swift
//  GogoSki
//
//  Created by AYMEN on 12/10/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import MapleBacon
import DownPicker

class ModifierMoniteurViewController : InternetCheckRed , PassProtocol , UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    @IBOutlet weak var prioriteTFF: UITextField!
    @IBOutlet weak var backBTNN: UIButton!
    @IBOutlet weak var modifierMoniteurTitleLBL: UILabel!
    @IBOutlet weak var photoMoniteurIMG: UIImageView!
    @IBOutlet weak var choisirPhotoBTN: UIButton!
    @IBOutlet weak var nomMTF: UITextField!
    @IBOutlet weak var prenomMTF: UITextField!
    @IBOutlet weak var sportNiveauTitleMTF: UILabel!
    @IBOutlet weak var ajouterUnSportLBL: UILabel!
    @IBOutlet weak var ajouterUnSportBTN: UIButton!
    @IBOutlet weak var sportTV: UITableView!
    @IBOutlet weak var ModifierBTN: UIButton!
    
    var json : JSON!
    var sports = [SportOBJ]()
    var MoniteurAmodifier : MoniteurOBJ!
    
    @objc let imagePicker = UIImagePickerController()
    var perioriteDownPicker: DownPicker!
    let perioriteArray = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(MoniteurAmodifier.nom)
       
        getData()
        initData()
        imagePicker.delegate = self
        
        self.perioriteDownPicker = DownPicker(textField: self.prioriteTFF, withData: perioriteArray as [Any])
        perioriteDownPicker.setArrowImage(nil)
       
        choisirPhotoBTN.titleLabel?.minimumScaleFactor = 0.2
        choisirPhotoBTN.titleLabel?.numberOfLines = 1
        choisirPhotoBTN.titleLabel?.adjustsFontSizeToFitWidth = true
        
        print("hellooo",MoniteurAmodifier.priorite)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    }
    
    func initData(){
        if(MoniteurAmodifier.photo != nil && MoniteurAmodifier.photo != ""){
            if let photoURlFirst =  MoniteurAmodifier.photo.first {
                if (photoURlFirst == "h" ){
                    self.photoMoniteurIMG.setImage(withUrl: URL(string:String(MoniteurAmodifier.photo))!, placeholder: #imageLiteral(resourceName: "user_placeholder") , crossFadePlaceholder: false, cacheScaled: true, completion: nil)
                }else{
                    print("nothing happening with the photo")
                    self.photoMoniteurIMG.image = #imageLiteral(resourceName: "user_placeholder")
                }
            }
        }
        nomMTF.text =  MoniteurAmodifier.nom
        prenomMTF.text =  MoniteurAmodifier.prenom
        //if MoniteurAmodifier.priorite != 0 {
        prioriteTFF.text = String(MoniteurAmodifier.priorite)
        //}
        sports = MoniteurAmodifier.sports

        sportTV.reloadData()
    }
    
    func getData(){
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        json = JSON(data: dataFromString!)
    }
    
    func pass(data: SportOBJ) { //conforms to protocol
        // implement your own implementation
        print("sport ", data.pratique , "niveau" ,  data.niveau!)
        sports.removeAll()
        sports.append(data)
        sportTV.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! AjouterSportViewController
        vc.delegate = self
    }
    func getSportsString() -> String {
        var stringToReturn = "["
        var ajouterVirgule = false
        for sport in sports {
            if ajouterVirgule{
                stringToReturn += " , "
            }else{
                ajouterVirgule = true
            }
            stringToReturn += "{ \"niveau\" : \"\(sport.niveau!)\" , \"pratique\" : \"\(sport.pratique!)\" }"
        }
        stringToReturn += "]"
        return stringToReturn
    }
    
    @IBAction func backVC(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func choisirPhotoAction(_ sender: Any) {
        backBTNN.isEnabled = false
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
        backBTNN.isEnabled = true

    }
    @IBAction func ajouterUnSportAction(_ sender: Any) {
        performSegue(withIdentifier: "toAjouterSportM", sender: self)

    }
    
    @IBAction func modifierMoniteurAction(_ sender: Any) {
        
        self.ModifierBTN.isEnabled = false
        self.backBTNN.isEnabled = false
        let q = getSportsString()
        let c = JSON.parse(q)
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : json["value"].stringValue
        ]
        
        print(MoniteurAmodifier.id)
        let params: Parameters = [
            "id" : MoniteurAmodifier.id!,
            "nom" : nomMTF.text!,
            "prenom"  : prenomMTF.text!,
            "sports" : c.rawValue,
            "priorite" : prioriteTFF.text!
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.modifierMoniteurPres , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                if let responseData = response.data {
                    let b = JSON(responseData)
                    print("modifierrr",b)
                    if b["status"].boolValue {
                        _ = SweetAlert().showAlert(Localization("ModificationMoniteur"), subTitle: Localization("ModificationMoniteurSuccess"), style: AlertStyle.success)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1 , execute: {
                            self.navigationController?.popViewController(animated: true)
                        })
                        self.ModifierBTN.isEnabled = true
                        self.backBTNN.isEnabled = true


                    }else{
                        _ = SweetAlert().showAlert(Localization("ModificationMoniteur"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                        self.ModifierBTN.isEnabled = true
                        self.backBTNN.isEnabled = true
                    }
                }else{
                    _ = SweetAlert().showAlert(Localization("ModificationMoniteur"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                    self.ModifierBTN.isEnabled = true
                      self.backBTNN.isEnabled = true
                }
        }
    }
    /////////********langue"**********///////
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
       
        self.perioriteDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.perioriteDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        perioriteDownPicker.setPlaceholder(Localization("priorite"))

        modifierMoniteurTitleLBL.text = Localization("ModifierMoniteurTitle")
        choisirPhotoBTN.setTitle(Localization("CreerMoniteurChoisirPhoto"), for: .normal)
        nomMTF.placeholder = Localization("CreerMoniteurNom")
        prenomMTF.placeholder = Localization("CreerMoniteurPrenom")
        sportNiveauTitleMTF.text = Localization("CreerMoniteurSportNiveau")
        ajouterUnSportLBL.text = Localization("CreerMoniteurAjoutSport")
        ModifierBTN.setTitle(Localization("ModifierMoniteurButton"), for: .normal)
        Localisator.sharedInstance.saveInUserDefaults = true
        
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
    
    ////******************************************UPLOAD ************************************************************************************************************/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //let user = SharedPreferences.sharedpref.prefs.string(forKey: "id_user")! as String
        let user = ""
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            DispatchQueue.main.async(execute: {
                // self.imageView?.contentMode = .redraw
                // SwiftSpinner.show("Uploading...")
                self.photoMoniteurIMG?.image = pickedImage
                // self.uploadnow.setTitle("Upload Now", for: .normal)
                self.myImageUploadRequest(id_product: "user")
            })
        }
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    ////
    ////
    func myImageUploadRequest(id_product:String)
    {
        self.backBTNN.isEnabled = false
        self.ModifierBTN.isEnabled = false
        //SwiftSpinner.show("Uploading Images...")
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
        let myUrl = NSURL(string: ScriptBase.sharedInstance.uploadImage)
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        let param = [
            "name"  : "Anonymous"
        ]
        var i = 0
        
        let boundary = self.generateBoundaryString()
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let imageData: NSData = UIImageJPEGRepresentation(photoMoniteurIMG.image!, 1) as! NSData
        
        if(imageData==nil)  { return }
        
        request.httpBody = self.createBodyWithParameters(parameters: param, filePathKey: "image", imageDataKey: imageData as NSData, boundary: boundary) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(error)")
                self.backBTNN.isEnabled = true
                self.ModifierBTN.isEnabled = true
                return
            }
            
            // You can print out response object
            print("******* response = \(response)")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                
                print("JSON:",json as Any )
                let j = JSON(json)
                
                LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
                let params: Parameters = [
                    "id" : self.MoniteurAmodifier.id!,
                    "photo": j["url"].stringValue
                ]
                
                let ab = UserDefaults.standard.value(forKey: "pres") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : a["value"].stringValue
                ]
                Alamofire.request(ScriptBase.sharedInstance.modifierMoniteurPres , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        LoaderAlert.shared.dismiss()
                        self.ModifierBTN.isEnabled = true
                        self.backBTNN.isEnabled = true
                        
                        let b = JSON(response.data)
                        print("b:",b)
                        a["user"]["photo"].stringValue = j["url"].stringValue
                        
                        if b["status"].boolValue {
                            
                            _ = SweetAlert().showAlert(Localization("ModificationMoniteur"), subTitle: Localization("modif"), style: AlertStyle.success)
                        }else{
                            
                            _ = SweetAlert().showAlert(Localization("ModificationMoniteur"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                        }
                }
            }catch
            {
                print(error)
                LoaderAlert.shared.dismiss()
            }
            
        }
        
        task.resume()
        
    }
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
    ////******************************************UPLOAD*******************************************************************************************************/
    
    
}


extension ModifierMoniteurViewController : UITableViewDataSource , UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if sports.count == 0 {
            return 1
        }
        return sports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "sportsCellM") as! UITableViewCell
        let pratiqueLBL = cell.viewWithTag(1) as! UILabel
        let niveauLBL = cell.viewWithTag(2) as! UILabel
        
        if sports.count == 0 {
            return cell
        }
        pratiqueLBL.text = sports[indexPath.row].pratique!
        niveauLBL.text = sports[indexPath.row].niveau!
        
        return cell
    }
}



