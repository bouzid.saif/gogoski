//
//  DataSourceMoniteur.swift
//  GogoSki
//
//  Created by AYMEN on 11/18/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import MapleBacon

protocol SetData {
    func previousVCSet (presVC: ProfilPrestataireVC)
}
class DataSourceMoniteur: NSObject , UITableViewDataSource , UITableViewDelegate,SetData{
    func previousVCSet(presVC: ProfilPrestataireVC) {
        presProfileVC = presVC
    }
    
    var json : JSON!
    var thisTable : UITableView!
    var presProfileVC : ProfilPrestataireVC!
    
    let niveauColor = [UIColor("#76EC9E"),UIColor("#76EC9E"),UIColor("#75A7FF"),UIColor("#FF3E53"),UIColor("#4D4D4D")]

    var arrayMoniteurs  = [MoniteurOBJ]()
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "moniteurCell") as! MoniteurCell
        cell.nomPrenomLBL.text = arrayMoniteurs[indexPath.row].nom as String + " " + arrayMoniteurs[indexPath.row].prenom
        if ( arrayMoniteurs[indexPath.row].photo != "" && arrayMoniteurs[indexPath.row].photo != nil ){
            if ((arrayMoniteurs[indexPath.row].photo!).first! == "h" ){
                cell.imageMoniteur.setImage(withUrl: (URL(string : arrayMoniteurs[indexPath.row].photo))!, placeholder: #imageLiteral(resourceName: "user_placeholder") , crossFadePlaceholder: false, cacheScaled: true, completion: nil)
            }else{
                print("nothing happening with the photo")
                cell.imageMoniteur.image = #imageLiteral(resourceName: "user_placeholder")
            }
        }else{
            cell.imageMoniteur.image = #imageLiteral(resourceName: "user_placeholder")
        }
        
        if arrayMoniteurs[indexPath.row].sports.count > 0 {
            let niveau = arrayMoniteurs[indexPath.row].sports.first?.niveau!.trimmingCharacters(in: .whitespaces)
            cell.niveauLBL.text = niveau
            switch niveau {
            case "Débutant"? : cell.niveauLBL.textColor = niveauColor[0]
            case "Débrouillard"? : cell.niveauLBL.textColor = niveauColor[1]
            case "Intermédiaire"? : cell.niveauLBL.textColor = niveauColor[2]
            case "Confirmé"? : cell.niveauLBL.textColor = niveauColor[3]
            case "Expert"? : cell.niveauLBL.textColor = niveauColor[4]
            default : break
            }
            
            cell.sportLBL.text = arrayMoniteurs[indexPath.row].sports.first?.pratique!
        }
        
        cell.deleteMoniteur.tag = indexPath.row
        cell.deleteMoniteur.addTarget(self, action: #selector(deleteMoniteur(sender: )) , for: .touchUpInside)
        
        cell.updateMoniteur.tag = indexPath.row
        cell.updateMoniteur.addTarget(self, action: #selector(modifierMoniteurAction(sender: )) , for: .touchUpInside)
        
        thisTable = tableView
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (arrayMoniteurs.count == 0)
        {
            return 0
        }
        else {
            return arrayMoniteurs.count
        }
    }
    
    @objc func deleteMoniteur(sender: UIButton) {
        
          SweetAlert().showAlert(Localization("SupressionMoniteurT"), subTitle: Localization("suppMoniteur"), style: AlertStyle.warning, buttonTitle:Localization("annuler"), buttonColor: UIColor.gray , otherButtonTitle:  Localization("continuer"), otherButtonColor: UIColor.red) { (isOtherButton) -> Void in
            if (!isOtherButton) {
                                LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
                                sender.isEnabled = false

                                let ab = UserDefaults.standard.value(forKey: "pres") as! String
                                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                                self.json = JSON(data: dataFromString!)
            
                                let id : Int = self.arrayMoniteurs[sender.tag].id
                                let header: HTTPHeaders = [
                                    "Content-Type" : "application/json" ,
                                    "x-Auth-Token" : self.json["value"].stringValue
                                ]
                                Alamofire.request(ScriptBase.sharedInstance.supprimerMoniteurPres + String(id) , method: .get , encoding: JSONEncoding.default,headers : header)
                                    .responseJSON { response in
                                        LoaderAlert.shared.dismiss()
                                        if let bb = response.data {
                                            let json = JSON(bb)
                                            if json["status"].boolValue == false {
                                                _ = SweetAlert().showAlert(Localization("SupressionMoniteurT"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                                            }else{
                                                _ = SweetAlert().showAlert(Localization("SupressionMoniteurT"), subTitle: Localization("SuppressionMoniteurSuccess"), style: AlertStyle.success)
                                                self.arrayMoniteurs.remove(at: sender.tag)
                                                self.thisTable.reloadData()
                                                self.presProfileVC.refreshSizesMonitor(monitorT : self.arrayMoniteurs )
                                            }
                                            sender.isEnabled = true
                                        }else{
                                            sender.isEnabled = true
                                        }
                                }
            }
            
            }
    }
    
    @objc func modifierMoniteurAction(sender : UIButton){
        
        let modMoniteurVC = UIStoryboard(name: "Prestataire", bundle : nil).instantiateViewController(withIdentifier: "modiferMoniteur") as! ModifierMoniteurViewController
        
        modMoniteurVC.MoniteurAmodifier = arrayMoniteurs[sender.tag]
        
        presProfileVC.navigationController?.pushViewController(modMoniteurVC, animated: true)
    }
}

