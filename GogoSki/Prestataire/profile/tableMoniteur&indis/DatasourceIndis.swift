//
//  DatasourceK.swift
//  GogoSki
//
//  Created by AYMEN on 11/18/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class DatasourceIndis : NSObject , UITableViewDataSource ,SetData{
    func previousVCSet(presVC: ProfilPrestataireVC) {
        pressProfile = presVC
    }
    
    var pressProfile : ProfilPrestataireVC!
    var tableIndis  = [IndisponibiliteOBJ]()
    
    var thisTableI : UITableView!
    var json : JSON!
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "indisponibCell") as! indisponibiliteTVCell
        cell.dateFinLBL.text = Localization("rangeTo") + " " + (tableIndis[indexPath.row].dateFin)!
        cell.dateDebutLBL.text = Localization("from") + " " + tableIndis[indexPath.row].dateDebut!
        cell.removeIndisBTN.tag = indexPath.row
        cell.removeIndisBTN.addTarget(self, action: #selector(removeIndis(sender: )) , for: .touchUpInside)
        thisTableI = tableView
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableIndis.count
    }
    
    @objc func removeIndis(sender : UIButton ){
      

           SweetAlert().showAlert(Localization("SupressionindisT"), subTitle: Localization("suppPres"), style: AlertStyle.warning, buttonTitle:Localization("annuler"), buttonColor: UIColor.gray , otherButtonTitle:  Localization("continuer"), otherButtonColor: UIColor.red) { (isOtherButton) -> Void in
            
            
            
            if (!isOtherButton) {

                        sender.isEnabled = false
                        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
        
                        let ab = UserDefaults.standard.value(forKey: "pres") as! String
                        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                        self.json = JSON(data: dataFromString!)
        
                        let id : Int = self.tableIndis[sender.tag].id
                        print("deleting indis where id  = " , id)
        
                        let header: HTTPHeaders = [
                            "Content-Type" : "application/json" ,
                            "x-Auth-Token" : self.json["value"].stringValue
                        ]
        
                        Alamofire.request(ScriptBase.sharedInstance.supprimerIndis + String(id) , method: .get , encoding: JSONEncoding.default,headers : header)
                            .responseJSON { response in
                                LoaderAlert.shared.dismiss()
                                let json = JSON(response.data)
                               
                                if json["status"].boolValue == false {
                                    _ = SweetAlert().showAlert(Localization("SupressionindisT"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                                }else{
                                    _ = SweetAlert().showAlert(Localization("SupressionindisT"), subTitle: Localization("SuppressionIndisponibilitéSuccess"), style: AlertStyle.success)
                                    self.tableIndis.remove(at: sender.tag)
                                    self.thisTableI.reloadData()
                                    
                                    self.pressProfile.refreshSizesIndis(indisT: self.tableIndis)
                                }
                                sender.isEnabled = false
                        }
            }
        }
        //  ProfilPrestataireVC.getMonProfil()
    }
}

