//
//  ParticipantTVC.swift
//  GoGoSkiUiKissou
//
//  Created by AYMEN on 10/27/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit

class ParticipantHeaderView: UIView {
    
    @IBOutlet weak var expandCollapseButton: UIButton!
    @IBOutlet weak var nomPrenomLBL: UILabel!
    @IBOutlet weak var pratique: UILabel!
    @IBOutlet weak var niveau: UILabel!
    @IBOutlet weak var infoBancairetitleLBL: UILabel!
    @IBOutlet weak var msgBTN: UIButton!
}
