//
//  ModifierPrestationViewController.swift
//  GogoSki
//
//  Created by AYMEN on 1/9/18.
//  Copyright © 2018 Velox-IT. All rights reserved.
//

import UIKit
import DownPicker
import Alamofire
import SwiftyJSON

class ModifierPrestationViewController: InternetCheckRed , UITextFieldDelegate {

    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var titreTF: UITextField!
    @IBOutlet weak var typeTF: UITextField!
    @IBOutlet weak var niveauTF: UITextField!
    @IBOutlet weak var nombreDeParticipantMaxTF: UITextField!
    @IBOutlet weak var nombreDeParticipantMin: UITextField!
    @IBOutlet weak var dureeeTF: UITextField!
    @IBOutlet weak var checkBoxView: VKCheckbox!
    @IBOutlet weak var forfaitLBL: UILabel!
    @IBOutlet weak var bacckBTN: UIButton!
    @IBOutlet weak var modifierBTN: UIButton!
    @IBOutlet weak var stockTF: UITextField!
    @IBOutlet weak var stockView: UIView!
    @IBOutlet weak var tarifTF: UITextField!
    
    
    var typeDownPicker: DownPicker!
    
    var niveauDownPicker: DownPicker!
    var nombreParticipantMaxDownPicker: DownPicker!
    var nombreParticipantMinDownPicker: DownPicker!
    
    var stockDEPrestationDownPicker: DownPicker!
    var dureeTFDownPicker: DownPicker!
    var forfaitDownPicker: DownPicker!
    
    let nombre: NSMutableArray = [ "2", "3", "4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"]
    let dureeArray = ["01:00","01:30","02:00","02:30","03:00","03:30","04:00","04:30","05:00","05:30","06:00","06:30","07:00","07:30","08:00","08:30","09:00","09:30","10:00","10:30","11:00","11:30","12:00"]
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    var json : JSON!
    var prestationAModifier : PrestationOBJ!
    var selectedSport = ""
    var selectedLevel = ""
    
    let limitLengthServiceTitle = 20

    override func viewDidLoad() {
        super.viewDidLoad()
        
        titreTF.delegate = self
        
        initialiseInputs()
        
        stockView.alpha = 0
        //46
      //  stockViewHeight.constant = 0
        print("heloooooooo",prestationAModifier.titre)
        initDownPickers()
        ////objet a modifier
        initData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= limitLengthServiceTitle
    }
    
    func initData(){
       
        titreTF.text =  prestationAModifier.titre
        
        nombreDeParticipantMin.text =  String(prestationAModifier.nombreParticipantMin)
        nombreDeParticipantMaxTF.text =  String(prestationAModifier.nombreParticipantMax)

        dureeeTF.text = getDureeToShow(duree: prestationAModifier.duree)
        tarifTF.text = String(prestationAModifier.tarifs.prix)
        
        niveauTF.text = prestationAModifier.sport.niveau
        typeTF.text = prestationAModifier.sport.pratique
    }
    override func viewWillAppear(_ animated: Bool) {
        getData()
    }
    
    func getData(){
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        json = JSON(data: dataFromString!)
    }
    
    func initDownPickers(){
        //ajouter prestation
        //modifier Moniteur
        self.titreTF.placeholder = Localization("ModifierMoniteurTitle")
        self.stockTF.placeholder = Localization("AjoutPresStockPres")
        
        self.nombreParticipantMaxDownPicker = DownPicker(textField: self.nombreDeParticipantMaxTF, withData: nombre as! [Any])
        nombreParticipantMaxDownPicker.setArrowImage(nil)
        nombreParticipantMaxDownPicker.setPlaceholder(Localization("AjoutPresNbrParticipantsMax"))
        
        self.nombreParticipantMinDownPicker = DownPicker(textField: self.nombreDeParticipantMin, withData: nombre as! [Any] )
        nombreParticipantMinDownPicker.setArrowImage(nil)
        nombreParticipantMinDownPicker.setPlaceholder(Localization("AjoutPresNbrParticipantsMin"))
        
        self.dureeTFDownPicker = DownPicker(textField: self.dureeeTF, withData: dureeArray as [Any] )
        dureeTFDownPicker.setArrowImage(nil)
        dureeTFDownPicker.setPlaceholder(Localization("Duree"))
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            self.typeDownPicker = DownPicker(textField: self.typeTF, withData: SportSaifFR as [Any])
            self.niveauDownPicker = DownPicker(textField: self.niveauTF, withData: NiveauSaifFR as [Any])
        } else {
            self.typeDownPicker = DownPicker(textField: self.typeTF, withData: SportSaifEN as [Any])
            self.niveauDownPicker = DownPicker(textField: self.niveauTF, withData: NiveauSaifEN as [Any])
        }
        typeDownPicker.setArrowImage(nil)
        niveauDownPicker.setArrowImage(nil)
        
        typeDownPicker.setPlaceholder(Localization("AjoutPrestypeDePratique"))
        niveauDownPicker.setPlaceholder(Localization("AjoutPresNiveau"))
    }
    
    func initialiseInputs(){
        checkBoxView.line             = .thin
        checkBoxView.bgColorSelected  = UIColor(red: 255/255, green: 121/255, blue: 97/255, alpha: 1)
        checkBoxView.bgColor          = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        checkBoxView.color            = UIColor.white
        checkBoxView.borderColor      = UIColor.gray
        checkBoxView.borderWidth      = 0
        checkBoxView.setOn(true)
        checkBoxView.isUserInteractionEnabled = false
        
        // Handle custom checkbox callback
        checkBoxView.checkboxValueChangedBlock = {
            isOn in
            if (isOn ){
                self.checkBoxView.borderWidth = 0
            }else{
                self.checkBoxView.borderWidth = 2
            }
            print("Custom checkbox is \(isOn ? "ON" : "OFF")")
        }
    }
    
    /////////********langue"**********///////
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
      
        self.typeDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.typeDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        
        self.niveauDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.niveauDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        self.nombreParticipantMaxDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.nombreParticipantMaxDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        
        self.nombreParticipantMinDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.nombreParticipantMinDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        
        self.dureeTFDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.dureeTFDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        titleLBL
            .text = Localization("ModifPrestationTitle")
        titreTF.placeholder = Localization("AjoutPresTitrePres")
        typeTF.placeholder = Localization("AjoutPrestypeDePratique")
        niveauTF.placeholder = Localization("AjoutPresNiveau")
        nombreDeParticipantMaxTF.placeholder = Localization("AjoutPresNbrParticipantsMax")
        nombreDeParticipantMin.placeholder = Localization("AjoutPresStockPres")
        tarifTF.placeholder = Localization("AjoutPresTarif")
        dureeeTF.placeholder = Localization("Duree")
        forfaitLBL.text = Localization("AjoutPresDejaForfait")
        modifierBTN.setTitle(Localization("BtnValiderModif"), for: .normal)
        
        Localisator.sharedInstance.saveInUserDefaults = true
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
    
    func reinitTextFields(){
        titreTF.text = ""
        nombreDeParticipantMin.text = ""
        nombreDeParticipantMaxTF.text = ""
        typeTF.text = ""
        niveauTF.text = ""
        dureeeTF.text = ""
        tarifTF.text = ""
    }
    
    @IBAction func modifierAction(_ sender: Any) {
        self.modifierBTN.isEnabled = false
        self.bacckBTN.isEnabled = false
        if titreTF.text?.trimmingCharacters(in: .whitespaces) != ""
            ||
            typeTF.text?.trimmingCharacters(in: .whitespaces) != ""
            ||
            niveauTF.text?.trimmingCharacters(in: .whitespaces) != ""
            ||
            nombreDeParticipantMin.text?.trimmingCharacters(in: .whitespaces) != ""
            ||
            nombreDeParticipantMaxTF.text?.trimmingCharacters(in: .whitespaces) != ""
            ||
            dureeeTF.text?.trimmingCharacters(in: .whitespaces) != ""
            ||
            tarifTF.text?.trimmingCharacters(in: .whitespaces) != "" {
            
            
            if Int(nombreDeParticipantMaxTF.text!)! < Int(nombreDeParticipantMin.text!)! {
                _ = SweetAlert().showAlert(Localization("modifierPrestation"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                self.modifierBTN.isEnabled = true
                self.bacckBTN.isEnabled = true
            }else{
                    LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
                    let header: HTTPHeaders = [
                        "Content-Type" : "application/json",
                        "x-Auth-Token" : json["value"].stringValue
                    ]
            
                    var niveau = ""
                    var pratique = ""
                    if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                       
                        niveau = niveauTF.text!
                        pratique = typeTF.text!
                    } else {
                         if niveauTF.text! != ""{
                            if niveauTF.text! == NiveauSaifFR[0] || niveauTF.text! == NiveauSaifFR[1] || niveauTF.text! == NiveauSaifFR[2] || niveauTF.text! == NiveauSaifFR[3] || niveauTF.text! == NiveauSaifFR[4] {
                                niveau = niveauTF.text!
                                pratique = typeTF.text!
                            }else{
                                niveau = NiveauSaifFR[NiveauSaifEN.index(of: niveauTF.text!)!]
                                pratique = SportSaifFR[SportSaifEN.index(of: typeTF.text!)!]
                            }
                        }
                    }
                    let params: Parameters = [
                        "id": prestationAModifier.id,
                        "titre": titreTF.text!,
                        "nbParticipant" : Int(nombreDeParticipantMaxTF.text!)!,
                        "nbParticipantmin" : Int(nombreDeParticipantMin.text!)!,
                        "stock" : 0,
                        "duree" : getDureeToStore(duree: dureeeTF.text!),
                        "tarif" : Double(tarifTF.text!)!,
                        "niveau" : niveau,
                        "pratique" : pratique
                    ]
            
                    Alamofire.request(ScriptBase.sharedInstance.modifierPrestation , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                        .responseJSON { response in
                            LoaderAlert.shared.dismiss()
                            if let bb = response.data{
                                    let b = JSON(bb)
                                    print("b:",b)
                                
                                    if b["status"].boolValue {

                                        _ = SweetAlert().showAlert(Localization("modifierPrestation"), subTitle: Localization("ModificationMoniteurSuccess"), style: AlertStyle.success)
                                        self.reinitTextFields()
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 2 , execute: {
                                            self.navigationController?.popViewController(animated: true)
                                        })
                                    }else{
                                        _ = SweetAlert().showAlert(Localization("modifierPrestation"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                                    }
                                self.modifierBTN.isEnabled = true
                                self.bacckBTN.isEnabled = true
                            }else{
                                _ = SweetAlert().showAlert(Localization("modifierPrestation"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                                self.modifierBTN.isEnabled = true
                                self.bacckBTN.isEnabled = true
                            }
                    }
            
            }
            
        }else{
            _ = SweetAlert().showAlert(Localization("modifierPrestation"), subTitle: Localization("infoManquate"), style: AlertStyle.error)
            self.modifierBTN.isEnabled = true
            self.bacckBTN.isEnabled = true
            LoaderAlert.shared.dismiss()
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getDureeToStore (duree : String) -> String {
        var const = ""
        switch duree {
                case "01:00":  const = "60"
                case "01:30":  const = "90"
                case "02:00":  const = "120"
                case "02:30":  const = "150"
                case "03:00":  const = "180"
                case "03:30":  const = "210"
                case "04:00":  const = "240"
                case "04:30":  const = "270"
                case "05:00":  const = "300"
                case "05:30":  const = "330"
                case "06:00":  const = "360"
                case "06:30":  const = "390"
                case "07:00":  const = "420"
                case "07:30":  const = "450"
                case "08:00":  const = "480"
                case "08:30":  const = "510"
                case "09:00":  const = "540"
                case "09:30":  const = "570"
                case "10:00":  const = "600"
                case "10:30":  const = "630"
                case "11:00":  const = "660"
                case "11:30":  const = "690"
                case "12:00":  const = "720"
            default : break
        }
        return const
    }
    
    func getDureeToShow (duree : String) -> String {
        var const = ""
        switch duree {
        case "60":  const = dureeArray[0]
        case "90":  const = dureeArray[1]
        case "120":  const = dureeArray[2]
        case "150":  const = dureeArray[3]
        case "180":  const = dureeArray[4]
        case "210":  const = dureeArray[5]
        case "240":  const = dureeArray[6]
        case "270":  const = dureeArray[7]
        case "300":  const = dureeArray[8]
        case "330":  const = dureeArray[9]
        case "360":  const = dureeArray[10]
        case "390":  const = dureeArray[11]
        case "420":  const = dureeArray[12]
        case "450":  const = dureeArray[13]
        case "480":  const = dureeArray[14]
        case "510":  const = dureeArray[15]
        case "540":  const = dureeArray[16]
        case "570":  const = dureeArray[17]
        case "600":  const = dureeArray[18]
        case "630":  const = dureeArray[19]
        case "660":  const = dureeArray[20]
        case "690":  const = dureeArray[21]
        case "720":  const = dureeArray[22]
        default : break
        }
        return const
    }
}
