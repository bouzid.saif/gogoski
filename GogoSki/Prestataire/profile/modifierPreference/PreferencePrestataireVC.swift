//
//  PreferencePrestataireVC.swift
//  GoGoSkiUiKissou
//
//  Created by AYMEN on 10/24/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import DownPicker
import SwiftyJSON
import Alamofire
import OneSignal
class PreferencePrestataireVC: InternetCheckRed {

    @IBOutlet weak var contactBL: UILabel!
    @IBOutlet weak var phoneLBL: UILabel!
    @IBOutlet weak var mesPreferenceTitleLBL: UILabel!
    ///
    @IBOutlet weak var langueTitleLBL: UILabel!
    @IBOutlet weak var adresseTitleLBL: UILabel!
    @IBOutlet weak var validerModificationBTN: UIButton!
    @IBOutlet weak var supprimerCompteBTN: UIButton!
    @IBOutlet weak var checkboxTelephone: VKCheckbox!
    @IBOutlet weak var checkboxEmail: VKCheckbox!
    @IBOutlet weak var paysTF: UITextField!
    @IBOutlet weak var langueTF: UITextField!

    @IBOutlet weak var codePostalTF: UITextField!
    @IBOutlet weak var villeTF: UITextField!
    @IBOutlet weak var rueTF: UITextField!
    @IBOutlet weak var ibanTF: UITextField!
    @IBOutlet weak var telephoneTF: UITextField!
    @IBOutlet weak var codeTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    var langueDownPicker: DownPicker!
    var paysDownPicker: DownPicker!
    var codeDownPicker: DownPicker!
    let codes = [  "+1 ",
                   "+7  ",
                   "+20 ",
                   "+27 ",
                   "+30 ",
                   "+31 ",
                   "+32 ",
                   "+33 ",
                   "+34 ",
                   "+36 ",
                   "+39 ",
                   "+40 ",
                   "+41 ",
                   "+43 ",
                   "+44 ",
                   "+45 ",
                   "+46 ",
                   "+47 ",
                   "+47 ",
                   "+48 ",
                   "+49 ",
                   "+51 ",
                   "+52 ",
                   "+53 ",
                   "+54 ",
                   "+55 ",
                   "+56 ",
                   "+57 ",
                   "+58 ",
                   "+60 ",
                   "+61 ",
                   "+62 ",
                   "+63 ",
                   "+64 ",
                   "+65 ",
                   "+66 ",
                   "+77 ",
                   "+81 ",
                   "+82 ",
                   "+84 ",
                   "+86 ",
                   "+90 ",
                   "+91 ",
                   "+92 ",
                   "+93 ",
                   "+94 ",
                   "+95 ",
                   "+98 ",
                   "+211 ",
                   "+212 ",
                   "+213 ",
                   "+216 ",
                   "+218 ",
                   "+220 ",
                   "+221 ",
                   "+222 ",
                   "+223 ",
                   "+224 ",
                   "+225 ",
                   "+226 ",
                   "+227 ",
                   "+228 ",
                   "+229 ",
                   "+230 ",
                   "+231 ",
                   "+232 ",
                   "+233 ",
                   "+234 ",
                   "+235 ",
                   "+236 ",
                   "+237 ",
                   "+238 ",
                   "+239 ",
                   "+240 ",
                   "+241 ",
                   "+242 ",
                   "+243 ",
                   "+244 ",
                   "+245 ",
                   "+246 ",
                   "+248 ",
                   "+249 ",
                   "+250 ",
                   "+251 ",
                   "+252 ",
                   "+253 ",
                   "+254 ",
                   "+255 ",
                   "+256 ",
                   "+257 ",
                   "+258 ",
                   "+260 ",
                   "+261 ",
                   "+262 ",
                   "+262 ",
                   "+263 ",
                   "+264 ",
                   "+265 ",
                   "+266 ",
                   "+267 ",
                   "+268 ",
                   "+269 ",
                   "+290 ",
                   "+291 ",
                   "+297 ",
                   "+298 ",
                   "+299 ",
                   "+ 345 " ,
        "+350 ",
        "+351 ",
        "+352 ",
        "+353 ",
        "+354 ",
        "+355 ",
        "+356 ",
        "+357 ",
        "+358 ",
        "+359 ",
        "+370 ",
        "+371 ",
        "+372 ",
        "+373 ",
        "+374 ",
        "+375 ",
        "+376 ",
        "+377 ",
        "+378 ",
        "+379 ",
        "+380 ",
        "+381 ",
        "+382 ",
        "+385 ",
        "+386 ",
        "+387 ",
        "+389 ",
        "+420 ",
        "+421 ",
        "+423 ",
        "+500 ",
        "+501 ",
        "+502 ",
        "+503 ",
        "+504 ",
        "+505 ",
        "+506 ",
        "+507 ",
        "+508 ",
        "+509 ",
        "+590 ",
        "+591 ",
        "+593 ",
        "+594 ",
        "+595 ",
        "+595 ",
        "+596 ",
        "+597 ",
        "+598 ",
        "+599 ",
        "+670 ",
        "+672 ",
        "+673 ",
        "+674 ",
        "+675 ",
        "+676 ",
        "+677 ",
        "+678 ",
        "+679 ",
        "+680 ",
        "+681 ",
        "+682 ",
        "+683 ",
        "+685 ",
        "+686 ",
        "+687 ",
        "+688 ",
        "+689 ",
        "+690 ",
        "+691 ",
        "+692 ",
        "+850 ",
        "+852 ",
        "+853 ",
        "+855 ",
        "+856 ",
        "+872 ",
        "+880 ",
        "+886 ",
        "+960 ",
        "+961 ",
        "+962 ",
        "+963 ",
        "+964 ",
        "+965 ",
        "+966 ",
        "+967 ",
        "+968 ",
        "+970 ",
        "+971 ",
        "+972 ",
        "+973 ",
        "+974 ",
        "+975 ",
        "+976 ",
        "+977 ",
        "+992 ",
        "+993 ",
        "+994 ",
        "+995 ",
        "+996 ",
        "+998 ",
        "+1242 ",
        "+1246 ",
        "+1264 ",
        "+1268 ",
        "+1284 ",
        "+1340 ",
        "+1441 ",
        "+1473 ",
        "+1649 ",
        "+1664 ",
        "+1670 ",
        "+1671 ",
        "+1684 ",
        "+1758 ",
        "+1767 ",
        "+1784 ",
        "+1849 ",
        "+1868 ",
        "+1869 ",
        "+1876 ",
        "+1939 " ]
    let langues: NSMutableArray = ["English", "French"]
    let pays: NSMutableArray = ["Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"]
    
    var json : JSON!
    
    let bandArray : NSMutableArray = []
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()

    override func viewDidLoad() {
        super.viewDidLoad()

       
 
        initDownPickerAndCheckboxes()
        getData()
        getMonProfil()
        ////***langue setting***/////
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    }
    
    func initDownPickerAndCheckboxes(){
        // Do any additional setup after loading the view.
        //downpickerzzzzzzz
        
        bandArray.add(Localization("French_fr"))
        bandArray.add(Localization("English_en"))
        
        self.langueDownPicker = DownPicker(textField: self.langueTF, withData: bandArray as! [Any] )
        langueDownPicker.setArrowImage(nil)
        self.paysDownPicker = DownPicker(textField: self.paysTF, withData: pays as! [Any])
        paysDownPicker.setPlaceholder("France")
        paysDownPicker.setArrowImage(nil)
        self.codeDownPicker = DownPicker(textField: self.codeTF, withData: codes as! [Any])
        codeDownPicker.setPlaceholder("+33")
        codeDownPicker.setArrowImage(nil)
        
        ///init checkboxezzzzz
        // Customized checkbox
        checkboxTelephone.line = .normal
        checkboxTelephone.bgColorSelected = UIColor(red: 95/255, green: 182/255, blue: 255/255, alpha: 1)
        checkboxTelephone.bgColor          = UIColor.white
        checkboxTelephone.color            = UIColor.white
        checkboxTelephone.borderColor      = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
        checkboxTelephone.borderWidth      = 2
        
        // Handle custom checkbox callback
        checkboxTelephone.checkboxValueChangedBlock = {
            isOn in
            if (isOn ){
                self.checkboxTelephone.borderWidth = 0
                  self.checkboxTelephone.borderColor      = UIColor.white
            }else{
                self.checkboxTelephone.borderWidth = 2
                 self.checkboxTelephone.borderColor      = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
            }
        }
        ///
        checkboxEmail.line = .normal
        checkboxEmail.bgColorSelected = UIColor(red: 95/255, green: 182/255, blue: 255/255, alpha: 1)
        checkboxEmail.bgColor          = UIColor.white
        checkboxEmail.color            = UIColor.white
        checkboxEmail.borderColor      = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
        checkboxEmail.borderWidth      = 2
        
        // Handle custom checkbox callback
        checkboxEmail.checkboxValueChangedBlock = {
            isOn in
            if (isOn ){
                self.checkboxEmail.borderWidth = 0
                  self.checkboxEmail.borderColor      = UIColor.white
            }else{
                self.checkboxEmail.borderWidth = 2
                 self.checkboxEmail.borderColor      = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
            }
        }
       // checkboxEmail.setOn(false)
    }

    @IBAction func backPreviousPage(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func supprimerCompte(_ sender: Any) {
        SweetAlert().showAlert(Localization("SupressionCompte"), subTitle: Localization("SupressionCompteWarning"), style: AlertStyle.warning, buttonTitle:Localization("annuler"), buttonColor: UIColor.gray , otherButtonTitle:  Localization("continuer"), otherButtonColor: UIColor.red) { (isOtherButton) -> Void in
            if isOtherButton == false {
                LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
                let ab = UserDefaults.standard.value(forKey: "pres") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : a["value"].stringValue
                ]
                Alamofire.request(ScriptBase.sharedInstance.supprimerUser , method: .get, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        LoaderAlert.shared.dismiss()
                        
                        let b = JSON(response.data!)
                        
                        if b["status"].boolValue {
                            
                            _ = SweetAlert().showAlert(Localization("SupressionCompte"), subTitle: Localization("compteDesactive"), style: AlertStyle.success, buttonTitle: "OK" , action: { (otherButton) in
                                let login = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PageBienvenue") as! PageBienvenue
                                // let presentaion = UIPresentationController(presentedViewController: self, presenting: login)
                                let transition = CATransition()
                                transition.duration = 0.5
                                transition.type = kCATransitionPush
                                transition.subtype = kCATransitionFromLeft
                                //login.view.window!.layer.add(transition, forKey: kCATransition)
                                let navigation = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
                                self.navigationController?.view.window?.layer.add(transition, forKey: kCATransition)
                                self.navigationController?.tabBarController?.tabBar.isHidden = true
                                self.navigationController?.pushViewController(login, animated: true)
                            } )
                            
                        }
                        else {
                            _ = SweetAlert().showAlert(Localization("SupressionCompte"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                        }
                }
            }
        }
    }
    
 //**********************************************GETDATA*******BEGIN***********************************************************///////
    func getData(){
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        json = JSON(data: dataFromString!)
        
        print("prestataire")
        print(json)
        
    }

    func getMonProfil(){
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json" ,
            "x-Auth-Token" : json["value"].stringValue
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.monProfilPres , method: .get , encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                let jsonprofile = JSON(response.data)
                print(jsonprofile)
                if jsonprofile["status"].boolValue == false {
                    
                    _ = SweetAlert().showAlert("", subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                    
                }else{
                  
                    if ( jsonprofile["data"]["langue"].description != "null"){
                       if (jsonprofile["data"]["langue"].string!.characters.first! == "f" || jsonprofile["data"]["langue"].string!.characters.first! == "F"  )
                        {
                           self.langueTF.text = Localization("French_fr")
                        }else {
                            self.langueTF.text = Localization("English_en")
                        }
                       //  = jsonprofile["data"]["langue"].string!
                    }
                    if ( jsonprofile["data"]["email"].description != "null"){
                        self.emailTF.text = jsonprofile["data"]["email"].string!
                    }
                    if ( jsonprofile["data"]["tel_notif"].boolValue){
                        self.checkboxTelephone.setOn(true, animated: false , user: true)
                    }else {
                       self.checkboxTelephone.setOn(false, animated: false , user: true)
                    }
                    if ( jsonprofile["data"]["mail_notif"].boolValue){
                        self.checkboxEmail.setOn(true, animated: false , user: true)
                    }else {
                        self.checkboxEmail.setOn(false, animated: false , user: true)
                    }
                    if ( jsonprofile["data"]["telephone"].description != "null"){
                        self.telephoneTF.text = String(jsonprofile["data"]["telephone"].int!)
                    }
                    if ( jsonprofile["data"]["prefix"].description != "null"){
                        self.codeTF.text = String(jsonprofile["data"]["prefix"].string!)
                    }
              //      if ( jsonprofile["data"]["code_iban"].description != "null"){
              //          self.ibanTF.text = String(jsonprofile["data"]["code_iban"].int!)
               //     }
                    if ( jsonprofile["data"]["_address"]["code_postal"].description != "null"){
                        self.codePostalTF.text = String(jsonprofile["data"]["_address"]["code_postal"].int!)
                    }
                    if ( jsonprofile["data"]["_address"]["rue"].description != "null"){
                        self.rueTF.text = jsonprofile["data"]["_address"]["rue"].string!
                    }
                    if ( jsonprofile["data"]["_address"]["ville"].description != "null"){
                        self.villeTF.text = jsonprofile["data"]["_address"]["ville"].string!
                    }
                    if ( jsonprofile["data"]["_address"]["pays"].description != "null"){
                        self.paysTF.text = jsonprofile["data"]["_address"]["pays"].string!
                    }

                }
        }
    
    }
//**********************************************GETDATA*******END*********************************************************///////
///************ modifier **********************************///////
    @IBAction func validerModification(_ sender: Any) {
        if langueTF.text?.trimmingCharacters(in: .whitespaces) != "" && emailTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
            codeTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
            telephoneTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
         //   ibanTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
            rueTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
            villeTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
            codePostalTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
            paysTF.text?.trimmingCharacters(in: .whitespaces) != "" {
            LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
            validerModificationBTN.isEnabled = false
            
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : json["value"].stringValue
                ]
            print("MailNotif:",self.checkboxEmail.isOn())
                let params: Parameters = [
                    "langue" : langueTF.text!,
                    "email" : emailTF.text!,
                 //   "IBAN" : ibanTF.text!,
                    "telephone"  : telephoneTF.text!.trimmingCharacters(in: .whitespaces) ,
                    "code" : codeTF.text!.trimmingCharacters(in: .whitespaces),
                    "address"  : ["pays" : paysTF.text!.trimmingCharacters(in: .whitespaces),
                                  "code" : codePostalTF.text!.trimmingCharacters(in: .whitespaces),
                                  "rue" : rueTF.text!.trimmingCharacters(in: .whitespaces),
                                  "ville" : villeTF.text!.trimmingCharacters(in: .whitespaces)
                    ] ,
                    "mailNotif" : self.checkboxEmail.isOn() ? "1" : "0",
                    "telNotif" : self.checkboxTelephone.isOn() ? "1" : "0"
                ]
                
                Alamofire.request(ScriptBase.sharedInstance.modifierPreferencesPres , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        LoaderAlert.shared.dismiss()
                        let b = JSON(response.data)
                      
                        if b["status"].boolValue {
                            
                         //   _ = SweetAlert().showAlert("Modification Profil", subTitle: "Changement effectué", style: AlertStyle.success)
                            //self.getMonProfil()
                            if (self.langueTF.text == Localization("French_fr"))
                            {
                                SetLanguage(self.arrayLanguages[2])
                            }else {
                                SetLanguage(self.arrayLanguages[1])
                            }
                            if self.checkboxTelephone.isOn() {
                                OneSignal.setSubscription(true)
                            }else {
                                OneSignal.setSubscription(false)
                            }
                            Localisator.sharedInstance.saveInUserDefaults = true
                            self.validerModificationBTN.isEnabled = true
                            _ = SweetAlert().showAlert(Localization("modifPreferences"), subTitle: Localization("ModificationMoniteurSuccess"), style: AlertStyle.success)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2 , execute: {
                                self.navigationController?.popViewController(animated: true)
                            })
                        }else{
                            self.validerModificationBTN.isEnabled = true
                            _ = SweetAlert().showAlert(Localization("modifPreferences"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                        }
                }
        }else{
            _ = SweetAlert().showAlert(Localization("modifPreferences"), subTitle: Localization("infoManquate"), style: AlertStyle.warning)
        }
        
    }
    
    /////////********langue"**********///////
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
   
        self.langueDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.langueDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        self.paysDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.paysDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
      
        self.codeDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.codeDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        
        mesPreferenceTitleLBL.text = Localization("MesPreferenceTitle")
        langueTitleLBL.text = Localization("PageLangueTitle")
        emailTF.placeholder = Localization("labelMail")
        codeTF.placeholder = "Code"
        telephoneTF.placeholder = Localization("MesPreferenceTelephone")
        phoneLBL.text = Localization("MesPreferenceTelephone")
       // ibanTF.placeholder = "************"
        adresseTitleLBL.text = Localization("MesPreferenceInformationFacturation")
        rueTF.placeholder = Localization("ModifierProfileRue")
        villeTF.placeholder = Localization("ModifierProfileVille")
        codePostalTF.placeholder = Localization("ModifierProfileCodePostal")
        paysTF.placeholder = Localization("ModifierProfileCodePay")
        validerModificationBTN.setTitle(Localization("BtnValiderModif"), for: .normal)
        supprimerCompteBTN.setTitle(Localization("btnSupp"), for: .normal)
        
        Localisator.sharedInstance.saveInUserDefaults = true
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
}
