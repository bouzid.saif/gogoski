//
//  ModifierProfilePresVC.swift
//  GoGoSkiUiKissou
//
//  Created by AYMEN on 10/25/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import DownPicker
import MapKit
import UIColor_Hex_Swift
import SwiftyJSON
import Alamofire
import MapleBacon
import MapKit

class ModifierProfilePresVC: InternetCheckRed , UITextViewDelegate , UIImagePickerControllerDelegate , UINavigationControllerDelegate , MKMapViewDelegate{

    @IBOutlet weak var modifierProfileTitleLBL: UILabel!
    @IBOutlet weak var choisirPhotoBTN: UIButton!
    @IBOutlet weak var langueTitleLBL: UILabel!
    @IBOutlet weak var informationGeneraleTitleLBL: UILabel!
    @IBOutlet weak var adresseTitleLBL: UILabel!
    @IBOutlet weak var ajouterUndesTitleLBL: UILabel!
    @IBOutlet weak var modifierBTN: UIButton!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var paysTF: UITextField!
    @IBOutlet weak var langueTF: UITextField!
    @IBOutlet weak var nomEtablissementTF: UITextField!
    @IBOutlet weak var codePostalTF: UITextField!
    @IBOutlet weak var villeTF: UITextField!
    @IBOutlet weak var rueTF: UITextField!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var descriptionTF: UITextView!
    
    var langueDownPicker: DownPicker!
    var paysDownPicker: DownPicker!
    
    let langues: NSMutableArray = ["English" , "French"]
    let pays: NSMutableArray = ["Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"]
    var json : JSON!
    
    let bandArray : NSMutableArray = []
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
   
    @objc let imagePicker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

     //   map.showsUserLocation = true
       map.delegate = self
        map.isUserInteractionEnabled = false
      //  let userLoc = map.userLocation
      //  let region = MKCoordinateRegionMakeWithDistance((userLoc.location?.coordinate)!, 2000, 2000)
        
       // map.setRegion(region, animated: true)
        // Do any additional setup after loading the view.
        
        bandArray.add(Localization("French_fr"))
        bandArray.add(Localization("English_en"))
        
        
        self.langueDownPicker = DownPicker(textField: self.langueTF, withData: bandArray as! [Any])
        langueDownPicker.setArrowImage(nil)
        self.paysDownPicker = DownPicker(textField: self.paysTF, withData: pays as! [Any])
        paysDownPicker.setPlaceholder("pay")
        paysDownPicker.setArrowImage(nil)
        
     
        descriptionTF.delegate = self
     
        descriptionTF.text = "Décrivez-vous en quelques lignes."
        descriptionTF.textColor = UIColor.lightGray
        
         getData()
        
        imagePicker.delegate = self
        getMonProfil()
        
        
        choisirPhotoBTN.titleLabel?.minimumScaleFactor = 0.2
        choisirPhotoBTN.titleLabel?.numberOfLines = 1
        choisirPhotoBTN.titleLabel?.adjustsFontSizeToFitWidth = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    }
    
    func setMap(longg : String , latt : String){
        
        var long = longg
        var lat = latt
        
        if long.prefix(1) == " "
        {
            long = String (long.dropFirst())
        }
        if lat.suffix(1) == " "{
            lat = String (lat.dropLast())
        }
        
        if lat != "" && long != "" {
            let point = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!)
            let annotation = ColorPointAnnotation(pinColor: UIColor.red)
            annotation.coordinate = point
            
            self.map.addAnnotation(annotation)
            self.map.setCenter(point, animated: true)
        }
        ////
    }
    
    func getData(){
        
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
         json = JSON(data: dataFromString!)
        setMap(longg: json["user"]["point"]["longitude"].stringValue , latt:  json["user"]["point"]["latitude"].stringValue )

    }
    
    func getMonProfil(){
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json" ,
            "x-Auth-Token" : json["value"].stringValue
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.monProfilPres , method: .get , encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                let jsonprofile = JSON(response.data)
                print(jsonprofile)
                if jsonprofile["status"].boolValue == false {
                    
                    _ = SweetAlert().showAlert("", subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                    
                }else{
                    if ( jsonprofile["data"]["langue"].description != "null"){
                        if (jsonprofile["data"]["langue"].string!.characters.first! == "f" || jsonprofile["data"]["langue"].string!.characters.first! == "F"  )
                        {
                            self.langueTF.text = Localization("French_fr")
                        }else {
                            self.langueTF.text = Localization("English_en")
                        }
                        //  = jsonprofile["data"]["langue"].string!
                    }
                    if ( jsonprofile["data"]["nom_etablissement"].description != "null"){
                        self.nomEtablissementTF.text = jsonprofile["data"]["nom_etablissement"].stringValue
                    }
                    if ( jsonprofile["data"]["description_prestataire"].description != "null"){
                        self.descriptionTF.text = jsonprofile["data"]["description_prestataire"].stringValue
                        self.descriptionTF.textColor = UIColor("#24253D")
                    }
                    if ( jsonprofile["data"]["_address"]["code_postal"].description != "null"){
                        self.codePostalTF.text = String(jsonprofile["data"]["_address"]["code_postal"].int!)
                    }
                    if ( jsonprofile["data"]["_address"]["rue"].description != "null"){
                        self.rueTF.text = jsonprofile["data"]["_address"]["rue"].stringValue
                    }
                    if ( jsonprofile["data"]["_address"]["ville"].description != "null"){
                        self.villeTF.text = jsonprofile["data"]["_address"]["ville"].stringValue
                    }
                    if ( jsonprofile["data"]["_address"]["pays"].description != "null"){
                        self.paysTF.text = jsonprofile["data"]["_address"]["pays"].stringValue
                    }
            
                    if ( jsonprofile["data"]["photo"].string! != "" && jsonprofile["data"]["photo"].string! != nil ){
                           if (jsonprofile["data"]["photo"].string!.first! == "h" ){
                            
                                self.image.setImage(withUrl: URL(string: jsonprofile["data"]["photo"].stringValue)!, placeholder: #imageLiteral(resourceName: "user_placeholder") , crossFadePlaceholder: false, cacheScaled: true, completion: nil)
                           }else{
                                print("nothing happening with the photo")
                                self.image.image = #imageLiteral(resourceName: "user_placeholder")
                            }
                    }
                }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if descriptionTF.textColor == UIColor.lightGray {
            descriptionTF.text = nil
            descriptionTF.textColor = UIColor("#24253D")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty || textView.text.trimmingCharacters(in: .whitespaces).isEmpty {
            textView.text = Localization("descTextView")
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func backPrevious(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func choisirPhoto(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
  
    @IBAction func modifierAction(_ sender: Any) {
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))

        modifierBTN.isEnabled = false
        if langueTF.text?.trimmingCharacters(in: .whitespaces) != "" && nomEtablissementTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
        rueTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
        villeTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
        paysTF.text?.trimmingCharacters(in: .whitespaces) != "" &&
        codePostalTF.text?.trimmingCharacters(in: .whitespaces) != "" //&&
      //      descriptionTF.text?.trimmingCharacters(in: .whitespaces) != ""
            {
          
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : json["value"].stringValue
                ]
                var adminEnabledString = "2"
              
                let params: Parameters = [
                    "langue" : langueTF.text!,
                    "nomEtablissement" : nomEtablissementTF.text!,
                    "description"  : descriptionTF.text!.trimmingCharacters(in: .whitespaces),
                    "address"  : ["pays" : paysTF.text!.trimmingCharacters(in: .whitespaces),
                              "code" : codePostalTF.text!.trimmingCharacters(in: .whitespaces),
                              "rue" : rueTF.text!.trimmingCharacters(in: .whitespaces),
                              "ville" : villeTF.text!.trimmingCharacters(in: .whitespaces)
                    ],
                    "adminEnabled" : adminEnabledString
                ]
                
                Alamofire.request(ScriptBase.sharedInstance.modifierProfilePrestataire , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        LoaderAlert.shared.dismiss()
                        let b = JSON(response.data)
                        print("b:",b)
                        
                        if b["status"].boolValue {
                         //       _ = SweetAlert().showAlert("Modification Profil", subTitle: "Changement effectué", style: AlertStyle.success)
                            
                            if (self.langueTF.text == Localization("French_fr"))
                            {
                                SetLanguage(self.arrayLanguages[2])
                            }else {
                                SetLanguage(self.arrayLanguages[1])
                            }
                            Localisator.sharedInstance.saveInUserDefaults = true

                          
                            _ = SweetAlert().showAlert(Localization("modifProfil"), subTitle: Localization("ModificationMoniteurSuccess"), style: AlertStyle.success)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2 , execute: {
                                self.navigationController?.popViewController(animated: true)
                            })
                            self.modifierBTN.isEnabled = true

                        }else{
                            self.modifierBTN.isEnabled = true
                            LoaderAlert.shared.dismiss()
                            _ = SweetAlert().showAlert(Localization("modifProfil"), subTitle: "Une erreur est survenue", style: AlertStyle.error)
                        }
                }
        /* }else{
         _ = SweetAlert().showAlert("No data changed", subTitle: "update data to save", style: AlertStyle.none)
        } */
        }else{
            _ = SweetAlert().showAlert(Localization("modifProfil"), subTitle: Localization("infoManquate"), style: AlertStyle.warning)
            self.modifierBTN.isEnabled = true
        }
    }
   
    /////////********langue"**********///////
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
      
        
        self.langueDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.langueDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        self.paysDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.paysDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))        
        
        modifierProfileTitleLBL.text = Localization("ModifierProfile")
        informationGeneraleTitleLBL.text = Localization("ModifierProfileInformationGen")
        nomEtablissementTF.placeholder = Localization("ModifierProfileNomEtablissement")
      
        adresseTitleLBL.text = Localization("ModifierProfileAdresse")

        rueTF.placeholder = Localization("ModifierProfileRue")
        villeTF.placeholder = Localization("ModifierProfileVille")
        codePostalTF.placeholder = Localization("ModifierProfileCodePostal")
        paysTF.placeholder = Localization("ModifierProfileCodePay")
        ajouterUndesTitleLBL.text = Localization("ModifierProfileAjouterDescription")
          langueTitleLBL.text = Localization("PageLangueTitle")
        modifierBTN.setTitle(Localization("BtnValiderModif"), for: .normal)
        choisirPhotoBTN.setTitle(Localization("ModifierProfilePhotoBtn"), for: .normal)
       
        Localisator.sharedInstance.saveInUserDefaults = true
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //let user = SharedPreferences.sharedpref.prefs.string(forKey: "id_user")! as String
        let user = ""
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            DispatchQueue.main.async(execute: {
                // self.imageView?.contentMode = .redraw
                // SwiftSpinner.show("Uploading...")
                self.image?.image = pickedImage
                // self.uploadnow.setTitle("Upload Now", for: .normal)
                self.myImageUploadRequest(id_product: user)
                
            })
        }
        dismiss(animated: true, completion: nil)
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        
        dismiss(animated: true, completion: nil)
    }
    
    
    ////
    func myImageUploadRequest(id_product:String)
    {
        //SwiftSpinner.show("Uploading Images...")
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
        let myUrl = NSURL(string: ScriptBase.sharedInstance.uploadImage)
        self.choisirPhotoBTN.isEnabled = false
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        let param = [
            "name"  : "Anonymous"
        ]
        
        var i = 0

        let boundary = self.generateBoundaryString()
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
     
        
        let imageData: NSData = UIImageJPEGRepresentation(image.image!, 1) as! NSData
        
        if(imageData==nil)  { return }
        
        request.httpBody = self.createBodyWithParameters(parameters: param, filePathKey: "image", imageDataKey: imageData as NSData, boundary: boundary) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(error)")
                  LoaderAlert.shared.dismiss()
                return
            }

            // You can print out response object
            print("******* response = \(response)")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                print("1aaaaa")
                print("JSON:",json as Any )
                let j = JSON(json)
                print("2aaaaa")
                LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
                let params: Parameters = [
                    "photo": j["url"].stringValue
                ]
                print("3aaaaa")
                let ab = UserDefaults.standard.value(forKey: "pres") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : a["value"].stringValue
                ]
                print("4aaaa")

                Alamofire.request(ScriptBase.sharedInstance.modifierProfilePrestataire , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        LoaderAlert.shared.dismiss()
                       // self.modifierBTN.isEnabled = true
                        self.choisirPhotoBTN.isEnabled = true

                        let b = JSON(response.data)
                        print("b:",b)
                        a["user"]["photo"].stringValue = j["url"].stringValue
                        print("5aaaaaaa")

                        if b["status"].boolValue {
                            
                            _ = SweetAlert().showAlert(Localization("ModProfilePres"), subTitle: Localization("ModificationMoniteurSuccess"), style: AlertStyle.success)
                        }else{

                            _ = SweetAlert().showAlert(Localization("ModProfilePres"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                        }
                }
            }catch
            {
                print(error)
            }
        }
        task.resume()
        
    }
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
}
