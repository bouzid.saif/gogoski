//
//  CreerMoniteurPresVC.swift
//  GoGoSkiUiKissou
//
//  Created by AYMEN on 10/25/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import DownPicker

class CreerMoniteurPresVC: InternetCheckRed , PassProtocol, UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    @IBOutlet weak var prioriteTF: UITextField!
    @IBOutlet weak var backBTN: UIButton!
    @IBOutlet weak var sportsTable: UITableView!
    @IBOutlet weak var creerBTN: UIButton!
    @IBOutlet weak var ajouterSportLBL: UILabel!
    @IBOutlet weak var creerFicheMoniteurTitleLBL: UILabel!
    @IBOutlet weak var sportNiveauTitle: UILabel!
    @IBOutlet weak var choixPhotoBTN: UIButton!
    @IBOutlet weak var imageMoniteur: UIImageView!
    @IBOutlet weak var prenomTF: UITextField!
    @IBOutlet weak var nomTF: UITextField!
    
    @objc let imagePicker = UIImagePickerController()
    var canUploadPhoto = false
    var json : JSON!
    var sports = [SportOBJ]()
    var isEmptySportTable = false
    
    var perioriteDownPicker: DownPicker!
    let perioriteArray = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        getData()
        imagePicker.delegate = self
        
        self.perioriteDownPicker = DownPicker(textField: self.prioriteTF, withData: perioriteArray as! [Any])
        perioriteDownPicker.setArrowImage(nil)
        
        choixPhotoBTN.titleLabel?.minimumScaleFactor = 0.2
        choixPhotoBTN.titleLabel?.numberOfLines = 1
        choixPhotoBTN.titleLabel?.adjustsFontSizeToFitWidth = true
        

        //*****langue*****/////
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        //*****langue*****/////
    }
    
    override func viewWillAppear(_ animated: Bool) {
        canUploadPhoto = false
    }
    
    func getData(){
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        json = JSON(data: dataFromString!)
    }
    
    @IBAction func choisirPhotoOuLogo(_ sender: Any) {
        self.creerBTN.isEnabled = false
        self.backBTN.isEnabled = false

        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    //////////////
    @IBAction func ajouterUnSport(_ sender: Any) {
        
        performSegue(withIdentifier: "toAjouterSport", sender: self)
    }
    
    func pass(data: SportOBJ) { //conforms to protocol
        // implement your own implementation
        if data.pratique != nil && data.niveau != nil{
            print("sport ", data.pratique , "niveau" ,  data.niveau!)
            sports.removeAll()
            sports.append(data)
            isEmptySportTable = true
            sportsTable.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! AjouterSportViewController
        vc.delegate = self
    }
    
    func getSportsString() -> String {
        var stringToReturn = "["
        var ajouterVirgule = false
        for sport in sports {
            if ajouterVirgule{
                stringToReturn += " , "
            }else{
                ajouterVirgule = true
            }
            stringToReturn += "{ \"niveau\" : \"\(sport.niveau!)\" , \"pratique\" : \"\(sport.pratique!)\" }"
        }
        stringToReturn += "]"
        print("********************")
        print(stringToReturn)
        print("********************")
        return stringToReturn
    }
    
    @IBAction func creerMoniteur(_ sender: Any) {
        self.backBTN.isEnabled = false
        self.creerBTN.isEnabled = false
        self.myImageUploadRequest(id_product: "user")
    }
    
    func ajouterMoniteurAction(url : String){
        
        if nomTF.text?.trimmingCharacters(in: .whitespaces) != "" &&  nomTF.text?.trimmingCharacters(in: .whitespaces) != "" && sports.count > 0{
           
            
            LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
            
            
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : json["value"].stringValue
            ]
            let q = getSportsString()
            let c = JSON.parse(q)
            
            print(c)
            print("JSON ")
            print(url)
            print("saazsaszasazsz",prioriteTF.text!)
            
            let params: Parameters = [
                "photo": url,
                "nom" : nomTF.text!,
                "prenom" : prenomTF.text!,
                "sports" : c.rawValue,
                "priorite" : prioriteTF.text!
            ]
            
            print(params)
            Alamofire.request(ScriptBase.sharedInstance.ajouterMoniteurPres , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    LoaderAlert.shared.dismiss()
                    
                    let b = JSON(response.data)
                    print("b:",b)
                    
                    if b["status"].boolValue {
                        
                        _ = SweetAlert().showAlert(Localization("CreationMoniteur"), subTitle: Localization("ajoutSucess"), style: AlertStyle.success)
                        self.prenomTF.text = ""
                        self.nomTF.text = ""
                        self.prioriteTF.text = ""
                        self.sports.removeAll()
                        self.sportsTable.reloadData()
                        self.canUploadPhoto = false
                        DispatchQueue.main.async {
                            self.backBTN.isEnabled = true
                            self.creerBTN.isEnabled = true
                            self.imageMoniteur.image = #imageLiteral(resourceName: "user_placeholder")
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.backBTN.isEnabled = true
                            self.creerBTN.isEnabled = true
                        }
                        _ = SweetAlert().showAlert(Localization("CreationMoniteur"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                    }
            }
            
        }else{
            self.backBTN.isEnabled = true
            self.creerBTN.isEnabled = true
            _ = SweetAlert().showAlert(Localization("CreationMoniteur"), subTitle: Localization("infoManquate"), style: AlertStyle.error)
        }
    }
    
    @IBAction func dimissModally(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /////////********langue"**********///////
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
      
        self.perioriteDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.perioriteDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        perioriteDownPicker.setPlaceholder(Localization("priorite"))

        creerFicheMoniteurTitleLBL.text = Localization("CreerMoniteurTitle")
        choixPhotoBTN.setTitle(Localization("CreerMoniteurChoisirPhoto"), for: .normal)
        nomTF.placeholder = Localization("CreerMoniteurNom")
        prenomTF.placeholder = Localization("CreerMoniteurPrenom")
        sportNiveauTitle.text = Localization("CreerMoniteurSportNiveau")
        ajouterSportLBL.text = Localization("CreerMoniteurAjoutSport")
        creerBTN.setTitle(Localization("CreerMoniteurCreerBTN"), for: .normal)
        
        Localisator.sharedInstance.saveInUserDefaults = true
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //let user = SharedPreferences.sharedpref.prefs.string(forKey: "id_user")! as String
        let user = ""
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            DispatchQueue.main.async(execute: {
                // self.imageView?.contentMode = .redraw
                // SwiftSpinner.show("Uploading...")
                self.imageMoniteur?.image = pickedImage
                // self.uploadnow.setTitle("Upload Now", for: .normal)
            })
        }
        
        dismiss(animated: true) {
            self.canUploadPhoto = true
            self.creerBTN.isEnabled = true
            self.backBTN.isEnabled = true
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        self.creerBTN.isEnabled = true
        self.backBTN.isEnabled = true
    }
    ////
    
    func myImageUploadRequest(id_product:String)
    {
        if nomTF.text?.trimmingCharacters(in: .whitespaces) != "" &&  nomTF.text?.trimmingCharacters(in: .whitespaces) != "" && sports.count > 0{
            if canUploadPhoto {
                var Url = ""
                //SwiftSpinner.show("Uploading Images...")
                LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
                let myUrl = NSURL(string: ScriptBase.sharedInstance.uploadImage)
                
                let request = NSMutableURLRequest(url:myUrl! as URL);
                request.httpMethod = "POST";
                
                let param = [
                    "name"  : "Anonymous"
                ]
                
                let boundary = self.generateBoundaryString()
                let ab = UserDefaults.standard.value(forKey: "pres") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                
                let imageData: NSData = UIImageJPEGRepresentation(imageMoniteur.image!, 1) as! NSData
                
                if(imageData == nil)  { return }
                
                request.httpBody = self.createBodyWithParameters(parameters: param, filePathKey: "image", imageDataKey: imageData as NSData, boundary: boundary) as Data
                
                let task = URLSession.shared.dataTask(with: request as URLRequest) {
                    data, response, error in
                    
                    if error != nil {
                        print("error=\(String(describing: error))")
                        return
                    }
                    
                    // You can print out response object
                    print("******* response = \(String(describing: response))")
                    
                    // Print out reponse body
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("****** response data = \(responseString!)")
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        print("JSON:",json as Any )
                        let j = JSON(json)
                        Url = j["url"].stringValue
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.ajouterMoniteurAction(url : Url)
                        })
                        
                    }catch
                    {
                        // self.ajouterMoniteurAction(url : )
                        Url = "Url Par default"
                        print("failed")
                        print(error)
                          Url = "Url Par default"
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.ajouterMoniteurAction(url : Url)
                        })
                    }
                    LoaderAlert.shared.dismiss()
                    
                }
                task.resume()
                
            }else{
                self.ajouterMoniteurAction(url : "Url Par default")
            }
        }else{
            self.creerBTN.isEnabled = true
            self.backBTN.isEnabled = true
            _ = SweetAlert().showAlert(Localization("CreationMoniteur"), subTitle: Localization("infoManquate"), style: AlertStyle.error)
        }
        
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}

extension CreerMoniteurPresVC : UITableViewDataSource , UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if sports.count == 0 {
            isEmptySportTable = true
            return 1
        }
        isEmptySportTable = false
        return sports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "sportsCell") as! UITableViewCell
        let pratiqueLBL = cell.viewWithTag(1) as! UILabel
        let niveauLBL = cell.viewWithTag(2) as! UILabel
        
        
        if !isEmptySportTable {
            pratiqueLBL.text = sports[indexPath.row].pratique!
            niveauLBL.text = sports[indexPath.row].niveau!
        }else{
            pratiqueLBL.text = "---"
            niveauLBL.text = "---"
        }
        return cell
    }
}

extension JSON{
    mutating func appendIfArray(json:JSON){
        if var arr = self.array{
            arr.append(json)
            self = JSON(arr);
        }
    }
    mutating func appendIfDictionary(key:String,json:JSON){
        if var dict = self.dictionary{
            dict[key] = json;
            self = JSON(dict);
        }
    }
}

