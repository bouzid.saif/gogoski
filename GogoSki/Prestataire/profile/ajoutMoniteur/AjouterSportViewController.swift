//
//  AjouterSportViewController.swift
//  GogoSki
//
//  Created by AYMEN on 12/2/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AjouterSportViewController: InternetCheckRed {

    @IBOutlet var containerUIVIEW: UIView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var titleLBL: UILabel!
    /////
    @IBOutlet weak var viewBehindTableView: UIView!
    var delegate: PassProtocol?
///////
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
 //   let sportsArray = ["Ski Alpin","Snowboard","Ski de fond","Handiski","Raquettes","Ski de randonnée"]
    let imageArray = [#imageLiteral(resourceName: "ski-alpin-kissou"),#imageLiteral(resourceName: "Snowboard-kissou"),#imageLiteral(resourceName: "Ski-fond-kissou"),#imageLiteral(resourceName: "Handi-ski-kissou"),#imageLiteral(resourceName: "Raquettes-kissou"),#imageLiteral(resourceName: "Randonnée-ski-kissou")]
   // let niveauArray = ["Débutant","Intermédiaire","Confirmé","Expert"]
    let niveauColor = [UIColor("#76EC9E"),UIColor("#76EC9E"),UIColor("#75A7FF"),UIColor("#FF3E53"),UIColor("#4D4D4D")]

    var flag : Bool! = false
    var sportChoosed : String!
    var niveauChoosed : String!
    var imageSportChoosed : UIImage!
    var json : JSON!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapgesture = UITapGestureRecognizer(target: self,action: #selector(self.tapToCancel))
        tapgesture.numberOfTapsRequired = 1
       viewBehindTableView.addGestureRecognizer(tapgesture)
     //*****langue*****/////
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    //*****langue*****/////
        // Do any additional setup after loading the view.
    }
    
    @objc func tapToCancel(sender: UITapGestureRecognizer){
            //react to tap
            self.dismiss(animated: true, completion: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        getData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
            let sport = SportOBJ()
            sport.niveau = niveauChoosed
            sport.pratique = sportChoosed
            self.delegate?.pass(data: sport)
    }
  
    func ajouterSport(){
        LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : json["value"].stringValue
        ]
        let params: Parameters = [
            "niveau": niveauChoosed,
            "pratique" : sportChoosed,
            "photo" : "default"
        ]
        Alamofire.request(ScriptBase.sharedInstance.ajouterSport , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                let b = JSON(response.data)
                print("b:",b)
                self.dismiss(animated: true, completion: {
                    if b["status"].boolValue {
                        _ = SweetAlert().showAlert(Localization("ajoutSportTitle"), subTitle: Localization("ajoutSucess"), style: AlertStyle.success)
                    }else{
                        _ = SweetAlert().showAlert(Localization("ajoutSportTitle"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                    }
                })
        }
    }
    
    func getData(){
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        json = JSON(data: dataFromString!)
    }
    
    /////////********langue"**********///////
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        
        /*
         mesPreferenceTitleLBL.text = Localization("MesPreferenceTitle")
         langueTitleLBL.text = Localization("PageLangueTitle")
         emailTF.placeholder = Localization("labelMail")
         codeTF.placeholder = "Code"
         telephoneTF.placeholder = Localization("MesPreferenceTelephone")
         phoneLBL.text = Localization("MesPreferenceTelephone")
         ibanTF.placeholder = "************"
         adresseTitleLBL.text = Localization("MesPreferenceInformationFacturation")
         rueTF.placeholder = Localization("ModifierProfileRue")
         villeTF.placeholder = Localization("ModifierProfileVille")
         codePostalTF.placeholder = Localization("ModifierProfileCodePostal")
         paysTF.placeholder = Localization("ModifierProfileCodePay")
         validerModificationBTN.setTitle(Localization("BtnValiderModif"), for: .normal)
         supprimerCompteBTN.setTitle(Localization("btnSupp"), for: .normal)
         */
        titleLBL.text = Localization("PRATIQUE")
        Localisator.sharedInstance.saveInUserDefaults = true
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
}

extension AjouterSportViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (flag ){
            return NiveauSaifFR.count
        }
        return SportSaifFR.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "ajoutSportCell", for: indexPath)
        let image = cell.viewWithTag(1) as! UIImageView
        let textLbL = cell.viewWithTag(2) as! UILabel
        if (!flag){
        
        image.image = imageArray[indexPath.row]
        image.tintColor = UIColor("#BCBBBB")
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                textLbL.text = SportSaifFR[indexPath.row]
            }else {
                textLbL.text = SportSaifEN[indexPath.row]
            }
        return cell
        }
        image.image = imageSportChoosed
        image.tintColor = niveauColor[indexPath.row]
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            textLbL.text = NiveauSaifFR[indexPath.row]
        }else {
            textLbL.text = NiveauSaifEN[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if !flag {
            sportChoosed = SportSaifFR[indexPath.row]
            imageSportChoosed = imageArray[indexPath.row]
            flag = true
            tableView.reloadData()
        }else{
            titleLBL.text = "ss"
            niveauChoosed = self.NiveauSaifFR[indexPath.row]
            ajouterSport()
        }
    }
    
}



