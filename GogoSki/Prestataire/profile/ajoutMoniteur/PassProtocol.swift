//
//  PassProtocol.swift
//  GogoSki
//
//  Created by AYMEN on 12/2/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation

protocol PassProtocol {
    func pass(data: SportOBJ)
}
