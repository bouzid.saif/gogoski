//
//  AjouterPrestationVC.swift
//  GoGoSkiUiKissou
//
//  Created by AYMEN on 10/28/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import DownPicker
import Alamofire
import SwiftyJSON

class AjouterPrestationVC: InternetCheckRed, UITextFieldDelegate {
    
    @IBOutlet weak var backBTN: UIButton!
    @IBOutlet weak var validerAjoutBtn: UIButton!
    @IBOutlet weak var jaiDejaForfaitLBL: UILabel!
    @IBOutlet weak var ajouterPrestationTitreLBL: UILabel!
    @IBOutlet weak var titreDePrestationTF: UITextField!
    @IBOutlet weak var typeDePrestationTF: UITextField!
    
    @IBOutlet weak var niveauTF: UITextField!
    @IBOutlet weak var nombreParticipantMaxTF: UITextField!
    @IBOutlet weak var nombreParticipantsMinTF: UITextField!
    
    @IBOutlet weak var stockDEPrestationTF: UITextField!
    @IBOutlet weak var tarifTF: UITextField!
    @IBOutlet weak var dureeTF: UITextField!
    
    @IBOutlet weak var forfaitCheckBox: VKCheckbox!
    
    @IBOutlet weak var stockViewHeight: NSLayoutConstraint!
    @IBOutlet weak var stockView: UIView!
    
    var typeDownPicker: DownPicker!
    
    var niveauDownPicker: DownPicker!
    var nombreParticipantMaxDownPicker: DownPicker!
    var nombreParticipantMinDownPicker: DownPicker!
    
    var stockDEPrestationDownPicker: DownPicker!
    var dureeTFDownPicker: DownPicker!
    var forfaitDownPicker: DownPicker!
    
    let nombre: NSMutableArray = ["2", "3", "4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"]
    let dureeArray: NSMutableArray = ["01:00","01:30","02:00","02:30","03:00","03:30","04:00","04:30","05:00","05:30","06:00","06:30","07:00","07:30","08:00","08:30","09:00","09:30","10:00","10:30","11:00","11:30","12:00"]
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    //  let langues: NSMutableArray = ["1", "2", "3", "4"]
    // let langues: NSMutableArray = ["1", "2", "3", "4"]
    var json : JSON!
    
    let limitLengthServiceTitle = 20

    override func viewDidLoad() {
        super.viewDidLoad()
        
        titreDePrestationTF.delegate = self
        
        initialiseInputs()
        
        stockView.alpha = 0
        //46
        stockViewHeight.constant = 0
        
        initDownPickers()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= limitLengthServiceTitle
    }
    
    func initDownPickers(){
        //ajouter prestation
        //modifier Moniteur
        self.titreDePrestationTF.placeholder = Localization("AjoutPresTitrePres")
        self.stockDEPrestationTF.placeholder = Localization("AjoutPresStockPres")
        
        self.nombreParticipantMaxDownPicker = DownPicker(textField: self.nombreParticipantMaxTF, withData: nombre as! [Any])
        nombreParticipantMaxDownPicker.setArrowImage(nil)
        nombreParticipantMaxDownPicker.setPlaceholder(Localization("AjoutPresNbrParticipantsMax"))
        
        self.nombreParticipantMinDownPicker = DownPicker(textField: self.nombreParticipantsMinTF, withData: nombre as! [Any] )
        nombreParticipantMinDownPicker.setArrowImage(nil)
        nombreParticipantMinDownPicker.setPlaceholder(Localization("AjoutPresNbrParticipantsMin"))
        
        self.dureeTFDownPicker = DownPicker(textField: self.dureeTF, withData: dureeArray as! [Any] )
        dureeTFDownPicker.setArrowImage(nil)
        dureeTFDownPicker.setPlaceholder(Localization("Duree"))
        
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            
            self.typeDownPicker = DownPicker(textField: self.typeDePrestationTF, withData: SportSaifFR as [Any])
            self.niveauDownPicker = DownPicker(textField: self.niveauTF, withData: NiveauSaifFR as [Any])
        } else {
            
            self.typeDownPicker = DownPicker(textField: self.typeDePrestationTF, withData: SportSaifEN as [Any])
            self.niveauDownPicker = DownPicker(textField: self.niveauTF, withData: NiveauSaifEN as [Any])
        }
        
        typeDownPicker.setArrowImage(nil)
        niveauDownPicker.setArrowImage(nil)
        
        typeDownPicker.setPlaceholder(Localization("AjoutPrestypeDePratique"))
        niveauDownPicker.setPlaceholder(Localization("AjoutPresNiveau"))
    }
    
    func getData(){
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        json = JSON(data: dataFromString!)
    }
    
    @IBAction func validerAjout(_ sender: Any) {
        
        
        if titreDePrestationTF.text?.trimmingCharacters(in: .whitespaces) != ""
            ||
            typeDePrestationTF.text?.trimmingCharacters(in: .whitespaces) != ""
            ||
            niveauTF.text?.trimmingCharacters(in: .whitespaces) != ""
            ||
            nombreParticipantsMinTF.text?.trimmingCharacters(in: .whitespaces) != ""
            ||
            nombreParticipantMaxTF.text?.trimmingCharacters(in: .whitespaces) != ""
            ||
            dureeTF.text?.trimmingCharacters(in: .whitespaces) != ""
            ||
            tarifTF.text?.trimmingCharacters(in: .whitespaces) != "" {
            
            self.validerAjoutBtn.isEnabled = false
            self.backBTN.isEnabled = false
            LoaderAlert.shared.show(withStatus: Localization("VeuillezPatienter"))

            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : json["value"].stringValue
            ]
            
            var niveau = ""
            var pratique = ""
            if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
                
                niveau = niveauTF.text!
                pratique = typeDePrestationTF.text!
            } else {
                niveau = NiveauSaifFR[NiveauSaifEN.index(of: niveauTF.text!)!]
                pratique = SportSaifFR[SportSaifEN.index(of: typeDePrestationTF.text!)!]
            }
            
            let dureToStore = getDureeToStore(duree: dureeTF.text!)
            print("fkn dureeeee", dureeTF.text!)
            let params: Parameters = [
                "titre": titreDePrestationTF.text!,
                "nbParticipant" : Int(nombreParticipantMaxTF.text!)!,
                "nbParticipantmin" : Int(nombreParticipantsMinTF.text!)!,
                "stock" : 0,
                "duree" : dureToStore,
                "tarif" : Double(tarifTF.text!)!,
                "niveau" : niveau,
                "pratique" : pratique
            ]
            
            Alamofire.request(ScriptBase.sharedInstance.ajouterPrestation , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    LoaderAlert.shared.dismiss()
                    
                    if let bb = response.data{
                            let b = JSON(bb)
                            print("b:",b)
                            if b["status"].boolValue {
                                  _ = SweetAlert().showAlert(Localization("modifierPrestation"), subTitle: Localization("ajoutSucess"), style: AlertStyle.success)
                                self.validerAjoutBtn.isEnabled = true
                                self.backBTN.isEnabled = true
                                    self.reinitTextFields()
                            }else{
                                _ = SweetAlert().showAlert(Localization("modifierPrestation"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                                self.validerAjoutBtn.isEnabled = true
                                self.backBTN.isEnabled = true
                            }
                    }else{
                        _ = SweetAlert().showAlert(Localization("modifierPrestation"), subTitle: Localization("ajoutFailure"), style: AlertStyle.error)
                        self.validerAjoutBtn.isEnabled = true
                        self.backBTN.isEnabled = true
                    }
            }
        }else{
            _ = SweetAlert().showAlert(Localization("modifierPrestation"), subTitle: Localization("infoManquate"), style: AlertStyle.warning)
            self.validerAjoutBtn.isEnabled = true
            self.backBTN.isEnabled = true
            LoaderAlert.shared.dismiss()

        }
    }
    
    func reinitTextFields(){
        titreDePrestationTF.text = ""
        nombreParticipantsMinTF.text = ""
        nombreParticipantMaxTF.text = ""
        typeDePrestationTF.text = ""
        niveauTF.text = ""
        dureeTF.text = ""
        tarifTF.text = ""
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /////////********langue"**********///////
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        
      
    //    self.forfaitDownPicker.setToolbarDoneButtonText(Localization("termineP"))
     //   self.forfaitDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        self.typeDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.typeDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        self.niveauDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.niveauDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        self.nombreParticipantMaxDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.nombreParticipantMaxDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        self.nombreParticipantMinDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.nombreParticipantMinDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
       // self.stockDEPrestationDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        //self.stockDEPrestationDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        self.dureeTFDownPicker.setToolbarDoneButtonText(Localization("termineP"))
        self.dureeTFDownPicker.setToolbarCancelButtonText(Localization("BtnAnnuler"))
        
        
        
        
        ajouterPrestationTitreLBL
            .text = Localization("ProfilPrestataireAjouterUnePrestation")
        titreDePrestationTF.placeholder = Localization("AjoutPresTitrePres")
        typeDePrestationTF.placeholder = Localization("AjoutPrestypeDePratique")
        niveauTF.placeholder = Localization("AjoutPresNiveau")
        nombreParticipantMaxTF.placeholder = Localization("AjoutPresNbrParticipantsMax")
        stockDEPrestationTF.placeholder = Localization("AjoutPresStockPres")
        tarifTF.placeholder = Localization("AjoutPresTarif")
        dureeTF.placeholder = Localization("Duree")
        jaiDejaForfaitLBL.text = Localization("AjoutPresDejaForfait")
        validerAjoutBtn.setTitle(Localization("AjoutPresValiderAjout"), for: .normal)
        
        Localisator.sharedInstance.saveInUserDefaults = true
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////////********langue"**********///////
    
    
    func initialiseInputs(){
        /*     self.typeDownPicker = DownPicker(textField: self.typeDePrestationTF, withData: langues as! [Any])
         typeDownPicker.setPlaceholder("Type de pratique")
         typeDownPicker.setArrowImage(nil)
         self.niveauDownPicker = DownPicker(textField: self.niveauTF, withData: langues as! [Any])
         niveauDownPicker.setPlaceholder("Niveau")
         niveauDownPicker.setArrowImage(nil)
         self.nombreParticipantDownPicker = DownPicker(textField: self.nombreParticipantMaxTF, withData: langues as! [Any])
         nombreParticipantDownPicker.setPlaceholder("Nombre de participants max")
         nombreParticipantDownPicker.setArrowImage(nil)
         self.stockDEPrestationDownPicker = DownPicker(textField: self.stockDEPrestationTF, withData: langues as! [Any])
         stockDEPrestationDownPicker.setPlaceholder("Stock de la prestation")
         stockDEPrestationDownPicker.setArrowImage(nil)
         self.dureeTFDownPicker = DownPicker(textField: self.tarifTF, withData: langues as! [Any])
         dureeTFDownPicker.setPlaceholder("Tarif")*/
        //dureeTFDownPicker.setArrowImage(nil)
        // self.forfaitDownPicker = DownPicker(textField: self.dureeTF, withData: langues as! [Any])
        //  forfaitDownPicker.setPlaceholder("Durée")
        //   forfaitDownPicker.setArrowImage(nil)
        ///
        forfaitCheckBox.line             = .thin
        forfaitCheckBox.bgColorSelected  = UIColor(red: 255/255, green: 121/255, blue: 97/255, alpha: 1)
        forfaitCheckBox.bgColor          = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        forfaitCheckBox.color            = UIColor.white
        forfaitCheckBox.borderColor      = UIColor.gray
        forfaitCheckBox.borderWidth      = 0
        forfaitCheckBox.setOn(true)
        forfaitCheckBox.isUserInteractionEnabled = false
        // Handle custom checkbox callback
        forfaitCheckBox.checkboxValueChangedBlock = {
            isOn in
            if (isOn ){
                self.forfaitCheckBox.borderWidth = 0
            }else{
                self.forfaitCheckBox.borderWidth = 2
            }
            print("Custom checkbox is \(isOn ? "ON" : "OFF")")
        }
    }
    func getDureeToStore (duree : String) -> String {
        var const = ""
        switch duree {
        case "01:00":  const = "60"
        case "01:30":  const = "90"
        case "02:00":  const = "120"
        case "02:30":  const = "150"
        case "03:00":  const = "180"
        case "03:30":  const = "210"
        case "04:00":  const = "240"
        case "04:30":  const = "270"
        case "05:00":  const = "300"
        case "05:30":  const = "330"
        case "06:00":  const = "360"
        case "06:30":  const = "390"
        case "07:00":  const = "420"
        case "07:30":  const = "450"
        case "08:00":  const = "480"
        case "08:30":  const = "510"
        case "09:00":  const = "540"
        case "09:30":  const = "570"
        case "10:00":  const = "600"
        case "10:30":  const = "630"
        case "11:00":  const = "660"
        case "11:30":  const = "690"
        case "12:00":  const = "720"
        default : break
        }
        return const
    }
    
}

