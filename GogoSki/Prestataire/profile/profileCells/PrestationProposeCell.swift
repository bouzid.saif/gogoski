//
//  PrestationProposeCell.swift
//  GoGoSkiUiKissou
//
//  Created by AYMEN on 10/27/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit

class PrestationProposeCell: UITableViewCell {

    @IBOutlet weak var imagePres: UIImageView!
    
    @IBOutlet weak var nomPres: UILabel!
    @IBOutlet weak var sportLBL: UILabel!
    @IBOutlet weak var sportNiveauView: UIView!
    
    @IBOutlet weak var deletePres: UIButton!
    
    @IBOutlet weak var editPres: UIButton!
    @IBOutlet weak var prixParHeure: UIButton!
    @IBOutlet weak var nmbrePersonnePRes: UILabel!
    @IBOutlet weak var dureePres: UILabel!
    
}
