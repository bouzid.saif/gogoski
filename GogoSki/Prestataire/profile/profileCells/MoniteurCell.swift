//
//  MoniteurCell.swift
//  GogoSki
//
//  Created by AYMEN on 11/18/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit

class MoniteurCell: UITableViewCell {
    
    @IBOutlet weak var imageMoniteur: UIImageView!
    @IBOutlet weak var nomPrenomLBL: UILabel!
    @IBOutlet weak var updateMoniteur: UIButton!
    @IBOutlet weak var sportLBL: UILabel!
    @IBOutlet weak var deleteMoniteur: UIButton!
    @IBOutlet weak var niveauLBL: UILabel!
}
