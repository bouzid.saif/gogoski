//
//  indisponibiliteTVCell.swift
//  GoGoSkiUiKissou
//
//  Created by AYMEN on 10/16/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit

class indisponibiliteTVCell: UITableViewCell {

    @IBOutlet weak var dateDebutLBL: UILabel!
    @IBOutlet weak var dateFinLBL: UILabel!
   
    @IBOutlet weak var removeIndisBTN: UIButton!
    
}
