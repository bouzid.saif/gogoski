//
//  NotificationVC.swift
//  GoGoSkiUiKissou
//
//  Created by AYMEN on 10/17/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NotificationVC: InternetCheckRed , UITableViewDataSource , UITableViewDelegate {

    @IBOutlet weak var suppBTN: UIButton!
    @IBOutlet weak var pasDeNotificationLBL: UILabel!
    @IBOutlet weak var emtyTableMsg: UIView!
    @IBOutlet weak var notificationTableView: UITableView!
    var notificationNumber = 8
      var parentNavigationController : UINavigationController?
    var notifications : JSON = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        emtyTableMsg.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        notificationTableView.delegate = self
        configureViewFromLocalisation()
    }
    
    /////langue**********************
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        pasDeNotificationLBL.text = Localization("pasDeNotif")
   //     pasDeNotificationLBL.sizeToFit()
        //pasDeNotificationLBL.contentMode = .center
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /////langue**********************
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (notifications.arrayObject?.count)! == 0 {
            self.notificationTableView.isHidden = true
            emtyTableMsg.isHidden = false
            suppBTN.isHidden = true
        }else{
            suppBTN.isHidden = false
            emtyTableMsg.isHidden = true
            self.notificationTableView.isHidden = false
        }
        return (notifications.arrayObject?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellNotif = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationTableViewCell
        
   /*     let cell = tableView.dequeueReusableCell(withIdentifier: "notif", for: indexPath)
        let labelTitle : UILabel = cell.viewWithTag(1) as! UILabel
        let labelContent : UILabel = cell.viewWithTag(2) as! UILabel
        let imgNotif : UIImageView = cell.viewWithTag(4) as! UIImageView
        let dateNotif : UILabel = cell.viewWithTag(3) as! UILabel
        */
        cellNotif.titleLBL.text = self.notifications[indexPath.row]["titre"].stringValue
        cellNotif.descLBL.text = self.notifications[indexPath.row]["content"].stringValue
        cellNotif.titleLBL.text = self.notifications[indexPath.row]["date"].stringValue
        cellNotif.id = self.notifications[indexPath.row]["activite"]["id"].stringValue
        if ( self.notifications[indexPath.row]["type"].stringValue == "message" )
        {
            cellNotif.ajoutBTN.image = UIImage(named : "notif_nouveauMsg_ahmed")
            cellNotif.type = "message"
        }
        else if ( self.notifications[indexPath.row]["type"].stringValue == "rdv" ) {
            cellNotif.ajoutBTN.image = UIImage(named : "notif_proposeRdv_ahmed")
              cellNotif.type = "rdv"
        }
        else if (self.notifications[indexPath.row]["type"].stringValue == "create")
        {
            cellNotif.ajoutBTN.image = UIImage(named : "notif_ajoutFavoris_ahmed")
            cellNotif.type = "create"
        }
        else if (self.notifications[indexPath.row]["type"].stringValue == "join")
        {
            cellNotif.ajoutBTN.image = UIImage(named : "notif_changeActivite_ahmed")
            cellNotif.type = "join"
        }
        else if (self.notifications[indexPath.row]["type"].stringValue == "pay"){
          
        }else{
             cellNotif.ajoutBTN.image = UIImage(named : "cash")
              cellNotif.type = "pay"
        }
        if self.notifications[indexPath.row]["photo"].stringValue != "default" {
            cellNotif.imageNotif.setImage(withUrl: URL(string:self.notifications[indexPath.row]["photo"].stringValue)!, placeholder: UIImage(named:"user_placeholder"), crossFadePlaceholder: false, cacheScaled: true, completion: nil)
        }else{
         cellNotif.imageNotif.image = UIImage(named:"user_placeholder")
        }
        let backgroundIMG = cellNotif.viewWithTag(12) as! UIImageView
        if (self.notifications[indexPath.row]["etat"].stringValue != "seen")
        {
            backgroundIMG.image = #imageLiteral(resourceName: "row-bg-non-lu-kissou")
        }else{
            backgroundIMG.image = #imageLiteral(resourceName: "row-bg-kissou")
        }        
       if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
       cellNotif.titleLBL.text = "Leçon Ski « " + self.notifications[indexPath.row]["titre"].stringValue + " »"
        cellNotif.descLBL.text = self.notifications[indexPath.row]["content_fr"].stringValue
        let Time = self.notifications[indexPath.row]["date"].stringValue
        let x = Time.split(separator: " ")
        let c = x[1].split(separator: "/")
        cellNotif.time.text = c[0] + ":" + c[1]
       
       }else{
        cellNotif.titleLBL.text = "Ski Lesson « " + self.notifications[indexPath.row]["titre"].stringValue + " »"
        cellNotif.descLBL.text = self.notifications[indexPath.row]["content_en"].stringValue
        let Time = self.notifications[indexPath.row]["date"].stringValue
        let x = Time.split(separator: " ")
        let c = x[1].split(separator: "/")
        cellNotif.time.text = c[0] + ":" + c[1]
        }
        return cellNotif
    }
    @IBAction func suppNotification(_ sender: Any) {
        
        
        _ = SweetAlert().showAlert(Localization("areYouSure"), subTitle: Localization("deleteNotif"), style: AlertStyle.warning, buttonTitle:Localization("cancel"), buttonColor: UIColor.gray , otherButtonTitle:  Localization("yes"), otherButtonColor: UIColor.red) { (isOtherButton) -> Void in
            if isOtherButton  {
                let ab = UserDefaults.standard.value(forKey: "pres") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : a["value"].stringValue
                ]
                Alamofire.request(ScriptBase.sharedInstance.suppAllNotif , method: .get, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        LoaderAlert.shared.dismiss()
                        
                        let b = JSON(response.data)
                        
                        if b["status"].boolValue {
                            self.emtyTableMsg.isHidden = false
                            self.notificationTableView.isHidden = true
                            self.notificationNumber = 0
                             self.notifications = []
                            self.notificationTableView.reloadData()
                           
                           
                            
                        }
                }
            }
        }
    }
    
    ///******* get notification ***/////
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getNotifications()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! NotificationTableViewCell
        print("lol",cell.id)
        if (cell.type == "create" ||  cell.type == "join") && cell.id != ""  {
            let ficheVC = UIStoryboard(name: "Prestataire", bundle : nil).instantiateViewController(withIdentifier: "FicheActiviteVC") as! FicheActiviteVC
            ficheVC.idLecon = String(cell.id)
            self.parentNavigationController?.show(ficheVC, sender: self)
        }
        
    }
    
    func getNotifications ()
    {
        self.pasDeNotificationLBL.isHidden = true
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.afficherNotifs , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                // self.btnValider.isEnabled = true
                let b = JSON(response.data)
                print(b)
                //  print("b:",b)
                /* a["user"]["prenom"].stringValue =    self.prenom.text!
                 a["user"]["nom"].stringValue = self.nom.text!
                 a["user"]["date_naissance"].stringValue = b["data"]["date_naissance"].stringValue*/
                
                if b["status"].boolValue {
                    if (b["data"].arrayObject?.count == 0)
                    {
                        print("feraa8")
                          self.pasDeNotificationLBL.isHidden = false
                    }
                    else {
                        
                        self.notificationTableView.isHidden = false
                        self.notifications = b["data"]
                        self.notificationTableView.reloadData()
                    }
                    /*   if self.Connex != ""{
                     _ = SweetAlert().showAlert("Modification Profil", subTitle: "Changement effectué", style: AlertStyle.success,buttonTitle: "Passer", action: { (otherButton) in
                     self.performSegue(withIdentifier: "connexion_once", sender: self)
                     } )
                     }else{
                     _ = SweetAlert().showAlert("Modification Profil", subTitle: "Changement effectué", style: AlertStyle.success)
                     }
                     }else{
                     
                     _ = SweetAlert().showAlert("Modification Profil", subTitle: "Une erreur est survenue", style: AlertStyle.error)
                     }*/
                    
                    
                }
        }
        self.notificationTableView.reloadData()

    }
}
