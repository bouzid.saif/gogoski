
//
//  MessageVC.swift
//  GoGoSkiUiKissou
//
//  Created by AYMEN on 10/17/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MessageVC: InternetCheckRed , UITableViewDataSource , UITableViewDelegate{
    
    @IBOutlet weak var pasDeMessageLBL: UILabel!
    @IBOutlet weak var pasDeMessagesView: UIView!
    var parentNavigationController : UINavigationController?
    var time : [String] = ["12:13" , "12:14" , "12,15"]
    var name : [String] = ["ahmed","saif","praty"]
    var users : [String] = []
    var Connected : [String] = []
    var conversation = JSON()
    var indexxToPAss : Int!
    var currentEmail = ""
    
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewdidload")
    //    pasDeMessagesView.isHidden = true
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Connected = appDelegate.ConnectedUser
       
        print("MessageViewController")
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDisconnectedUserUpdateNotification(notification:)), name: NSNotification.Name(rawValue: "userWasDisconnectedNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotif(notification:)), name: NSNotification.Name(rawValue: "MessageIsActive"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.DisconnectNow(notification:)), name: NSNotification.Name(rawValue: "UserDisonnectNow"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleReload(notification:)), name: NSNotification.Name(rawValue: "ReloadConvKissou"), object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(self.fromActToMsg(notification:)), name: NSNotification.Name(rawValue: "goMsgAct"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleReloadView(notification:)), name: NSNotification.Name(rawValue: "ReloadView"), object: nil)
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let jsonPres = JSON(data: dataFromString!)
        
        //print("haaaaih email",jsonPres["user"]["email"].stringValue)
        currentEmail = jsonPres["user"]["email"].stringValue
        self.loadData(withData: "")
    }
    
    @objc func fromActToMsg(notification: NSNotification) {
        
        if let idConvGot = notification.object as? Int{
            loadData(withData : String(idConvGot))
          /*  for i in 0 ..< conversation.count{
                print("aya 3ad ; " , idConvGot)
                print("tawwer   : ",conversation[i] , idConvGot)
                print("id zab " , conversation[i]["id"].intValue)
                
                if conversation[i]["id"].intValue == idConvGot {
                    print("yess we got it at  : " , i)
                   
                    self.tableview.delegate?.tableView!(tableview, didSelectRowAt: IndexPath(row: i, section: 0))
                }
            }*/
        }
    }
    
    @objc func handleReload(notification: NSNotification) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Connected = appDelegate.ConnectedUser
            self.tableview.reloadData()
    }
    
    @objc func handleReloadView(notification: NSNotification) {
        self.loadData(withData: "")
    }
    /////langue**********************
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
      pasDeMessageLBL.text = Localization("pasDeMsg")
      //  pasDeMessageLBL.sizeToFit()
      //  pasDeMessageLBL.contentMode = .center
    }
    
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    
    /////langue**********************
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
           self.navigationController?.tabBarController?.tabBar.isHidden = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Connected = appDelegate.ConnectedUser
       // loadData(withData: "")
        print("will appear " ,Connected)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewdidAppear:",animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if animated {
           /* if appDelegate.NotifReceive == true {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MessageViewActive"), object: nil)
                appDelegate.NotifReceive = false
            } */
        }
    }
    
    @objc func handleUserConnectNow2(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            print("setOnline:",typingUsersDictionary)
        }
    }
    
    @objc func handleNotif(notification: NSNotification) {
        
        let q = notification.object as! String
        //loadData(withData : q)
        // _ = SweetAlert().showAlert("Push", subTitle: "let it go", style: AlertStyle.success)
    }
    @objc func connectedNow(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            Connected[0] = "1"
            let q = JSON.parse(typingUsersDictionary[0])
            print(q)
            ////
                self.tableview.reloadData()
            
            let ab = UserDefaults.standard.value(forKey: "pres") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            print("userConnectNow:",typingUsersDictionary)
            
            SocketIOManager.sharedInstance.SetOnline(room: q["room"].stringValue, UserId: a["user"]["id"].stringValue)
        }
    }
    @objc func DisconnectNow(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            print("Disconnecting")
            let ab = UserDefaults.standard.value(forKey: "pres") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            print("Connected:",appDelegate.ConnectedUser)
            if typingUsersDictionary[0] != a["user"]["id"].stringValue {
                var y = 0
                var x = 0
                for b in  appDelegate.ConnectedUser{
                    
                    if b == typingUsersDictionary[0] {
                        x = y
                    }
                    y = y + 1
                }
                if x != 0 {
                    appDelegate.ConnectedUser.remove(at: x)
                }
                Connected = appDelegate.ConnectedUser
                print("Connected:",appDelegate.ConnectedUser)
                    self.tableview.reloadData()
              
            }
        }
    }
    
    @objc func handleUserTypingNotification(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String: AnyObject] {
            var names = ""
            var totalTypingUsers = 0
            print(typingUsersDictionary)
            /* for (typingUser, _) in typingUsersDictionary {
             if typingUser != nickname {
             names = (names == "") ? typingUser : "\(names), \(typingUser)"
             totalTypingUsers += 1
             }
             }
             
             if totalTypingUsers > 0 {
             let verb = (totalTypingUsers == 1) ? "is" : "are"
             
             lblOtherUserActivityStatus.text = "\(names) \(verb) now typing a message..."
             lblOtherUserActivityStatus.isHidden = false
             }
             else {
             lblOtherUserActivityStatus.isHidden = true
             } */
        }
    }
    
    @objc func handleDisconnectedUserUpdateNotification(notification: NSNotification) {
        let disconnectedUserNickname = notification.object as! String
        // lblNewsBanner.text = "User \(disconnectedUserNickname.uppercased()) has left."
        //showBannerLabelAnimated()
    }
    
    func loadData(withData :String){
        
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.afficherConversation , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                let b = JSON(response.data)
                print("b:",b)
                let c = JSON(b["data"].arrayObject)
                if c.count != 0 {
                    self.conversation = c
                    print("withData:",withData)
                    if withData != "" {
                        for  i in 0...self.conversation.count - 1 {
                            
                            if self.conversation[i]["id"].stringValue == withData {
                                print("exist")
                                self.tableview.delegate?.tableView!(self.tableview, didSelectRowAt: IndexPath(row: i, section: 0))
                                
                            }
                        }
                    }else {
                        self.tableview.reloadData()
                    }
                }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if conversation.count != 0 {
            UIView.animate(withDuration: 0.5, animations: {
                self.pasDeMessagesView.isHidden = true
            })
            return conversation.count
        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.pasDeMessagesView.isHidden = false
            })
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        indexxToPAss = indexPath.row
        let view = self.storyboard?.instantiateViewController(withIdentifier: "ConversationViewController1") as! MessageListViewController
        view.name = conversation[indexxToPAss]["titre"].stringValue
        view.Conversation = conversation[indexxToPAss]
        view.activityId = conversation[indexxToPAss]["activite"]["id"].intValue
        self.parentNavigationController?.pushViewController(view, animated: true)
        //self.performSegue(withIdentifier: "tomsggg", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "tomsggg" {
               let next = segue.destination as! MessageListViewController
                
                next.name = conversation[indexxToPAss]["titre"].stringValue
                next.Conversation = conversation[indexxToPAss]
            }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellMessage = tableView.dequeueReusableCell(withIdentifier: "messageCell", for: indexPath) as! MessageTableViewCell
        
         cellMessage.supprimerConvBTN.tag = indexPath.row
        cellMessage.supprimerConvBTN.addTarget(self, action: #selector(suppMsg(sender:)), for: .touchUpInside)
        
         var ab : Bool = false
         var temp = JSON(conversation[indexPath.row]["users"].arrayObject)
         for i in 0...(temp.arrayObject?.count)! - 1 {
            for a in Connected{
                print("id cncted 1 : " , a , "  id cncted : ", temp[i]["id"].stringValue )
                if a == temp[i]["id"].stringValue {
                    ab = true
                }
            }
         }

        cellMessage.onlineIndicatorIMG.image = (ab == false) ? #imageLiteral(resourceName: "offline_ahmed") : #imageLiteral(resourceName: "online_ahmed")
        
        var indexOtherUser = 0
        if let email = conversation[indexPath.row]["users"][0]["email"].string {
            if email == currentEmail{
                indexOtherUser = 1
            }
        }
        print("indexOtherUser    :   ",indexOtherUser)
        
        if let photoURL = conversation[indexPath.row]["users"][indexOtherUser]["photo"].string {
            if ( photoURL != ""  ){
                if (photoURL.first! == "h" ){
                    cellMessage.imageMessage.af_setImage(withURL: URL(string: photoURL)!)
                }else{
                     cellMessage.imageMessage.image = #imageLiteral(resourceName: "user_placeholder")
                }
            }else{
                 cellMessage.imageMessage.image = #imageLiteral(resourceName: "user_placeholder")
            }
        }else{
            cellMessage.imageMessage.image = #imageLiteral(resourceName: "user_placeholder")
        }
        
        if let conv = conversation[indexPath.row]["titre"].string{
            print("conv title " ,conv)
            if conv.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                cellMessage.titleLBL.text = conv
            }else{
                cellMessage.titleLBL.text = conversation[indexPath.row]["users"][indexOtherUser]["prenom"].stringValue + " " + conversation[indexPath.row]["users"][indexOtherUser]["nom"].stringValue
            }
        }else{
            print("else conv " , conversation[indexPath.row])
            
            let msgTitle = conversation[indexPath.row]["users"][indexOtherUser]["prenom"].stringValue + " " + conversation[indexPath.row]["users"][indexOtherUser]["nom"].stringValue
            if msgTitle.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
               cellMessage.titleLBL.text = msgTitle
            }else{
                cellMessage.titleLBL.text = Localization("gogoskiUser")
            }
            
        }
        if let message = conversation[indexPath.row]["last_message"].string{
            cellMessage.lastMessageLBL.text = message
       
         //   let Time = conversation[indexPath.row]["last_message_date"].stringValue
            let Time = conversation[indexPath.row]["last_message_date"].stringValue
            let x = Time.split(separator: "T")
            let c = x[1].split(separator: ":")
            cellMessage.timeLBL.text = c[0] + ":" + c[1]
        }else{
            cellMessage.timeLBL.text = ""
            cellMessage.lastMessageLBL.text = ""
        }
        return cellMessage
    }
    
    @objc func suppMsg (sender : UIButton)
    {
        self.tableview.isUserInteractionEnabled = false
        
        SweetAlert().showAlert(Localization("areYouSure"), subTitle: Localization("suppConv"), style: AlertStyle.warning, buttonTitle:Localization("cancel"), buttonColor: UIColor.gray , otherButtonTitle:  Localization("yes"), otherButtonColor: UIColor.red) { (isOtherButton) -> Void in
            if isOtherButton == false {
                LoaderAlert.show()
                let ab = UserDefaults.standard.value(forKey: "pres") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json",
                    "x-Auth-Token" : a["value"].stringValue
                ]
                let params: Parameters = [
                    "idConv": self.conversation[sender.tag]["id"].stringValue
                ]
                print (params)
                // print ("********* " , self.enCours[sender.index!]["id"].stringValue)
                Alamofire.request(ScriptBase.sharedInstance.suppMessages , method: .post , parameters : params, encoding: JSONEncoding.default,headers : header)
                    .responseString { response in
                        LoaderAlert.dismiss()
                        self.tableview.isUserInteractionEnabled = true
                        let b = JSON(response.data)
                        print ("messages : "  , response)
                        if b["status"].boolValue {
                            _ = SweetAlert().showAlert(Localization("Supp"), subTitle: Localization("suppDesc"), style: AlertStyle.success)
                            if (self.conversation.count != 1)
                            {
                                self.conversation.arrayObject?.remove(at: sender.tag)
                            }else {
                                self.conversation = []
                            }
                            self.tableview.reloadData()
                        }else{
                            _ = SweetAlert().showAlert(Localization("Supp"), subTitle: Localization("erreur"), style: AlertStyle.error)
                        }
                }
            }
            else {
                self.tableview.isUserInteractionEnabled = true
            }
        }
    }
}
