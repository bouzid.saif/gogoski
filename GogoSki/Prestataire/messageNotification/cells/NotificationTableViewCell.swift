//
//  NotificationTableViewCell.swift
//  GogoSki
//
//  Created by BOUZIDMACPRO on 14/12/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var imageNotif: UIImageView!
    @IBOutlet weak var descLBL: UILabel!
    @IBOutlet weak var ajoutBTN: UIImageView!
    @IBOutlet weak var time: UILabel!
    var type = ""
    var id = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
