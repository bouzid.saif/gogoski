//
//  MessageTableViewCell.swift
//  GogoSki
//
//  Created by BOUZIDMACPRO on 14/12/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var onlineIndicatorIMG: RoundedUIImageView!
    @IBOutlet weak var timeLBL: UILabel!
    @IBOutlet weak var supprimerConvBTN: UIButton!
    @IBOutlet weak var lastMessageLBL: UILabel!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var imageMessage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func supprimerConversationAction(_ sender: Any) {
    }
    
}
