//
//  MessageListViewController.swift
//  GogoSki
//
//  Created by BOUZIDMACPRO on 14/12/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class MessageListViewController:InternetCheckRed , UITableViewDataSource , UITableViewDelegate,UITextFieldDelegate{

    @IBOutlet weak var empty_message: UILabel!
    
    @IBOutlet weak var empty_view_message: UIView!
    @IBOutlet weak var titleMessageListLBL: UILabel!
    @IBOutlet weak var userNameLBL: UILabel!
    @IBOutlet weak var userIMG: RoundedUIImageView!
    @IBOutlet weak var tableViewMSG: UITableView!
    var first = true
    var  bloque = false
    var name : String = ""
    var Conversation = JSON()
    var Messages = JSON()
    let button = UIButton(type: .custom)
    
    @IBOutlet weak var bottomtable: NSLayoutConstraint!
    @IBOutlet weak var conversationTextfield: PaddingTextField!
    @IBOutlet weak var TypingName: UILabel!
    
    var activityId : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
       self.navigationController?.tabBarController?.tabBar.isHidden = true
        tableViewMSG.delegate = self
        tableViewMSG.dataSource = self
        ////langue
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        ////langue
        empty_message.isHidden = true
         empty_view_message.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDisconnectedUserUpdateNotification(notification:)), name: NSNotification.Name(rawValue: "userWasDisconnectedNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserConnectNow(notification:)), name: NSNotification.Name(rawValue: "userConnectNow"), object: nil)
        
        userNameLBL.text = name
        TypingName.text = name + " is Typing..."
        button.setImage(UIImage(named: "Shape_ahmed"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.frame = CGRect(x: CGFloat(conversationTextfield.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        button.addTarget(self, action: #selector(self.sendMessages), for: .touchUpInside)
        conversationTextfield.rightView = button
        conversationTextfield.rightViewMode = .always
        loadData(animated: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleConnectedUserUpdateNotification(notification:)), name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserTypingNotification(notification:)), name: NSNotification.Name(rawValue: "userTypingNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserStopTypingNotification(notification:)), name: NSNotification.Name(rawValue: "userStopTypingNotification"), object: nil)
        self.conversationTextfield.delegate = self
        // Do any additional setup after loading the view.
        
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let jsonPres = JSON(data: dataFromString!)
        
        print("haaaaih email",jsonPres)
        let currentEmail = jsonPres["user"]["email"].stringValue
        
        var indexOtherUser = 0
        if let email = Conversation["users"][0]["email"].string {
            if email == currentEmail{
                indexOtherUser = 1
            }
        }
        
        if Conversation["titre"].stringValue == "" {
                let placeholder =  UIImage(named: "user_placeholder.png")
                if let urlImgUser = URL(string : Conversation["users"][indexOtherUser]["photo"].stringValue)
                {
                    userIMG.setImage(withUrl: urlImgUser , placeholder: placeholder)
                }
        }else {
            userIMG.image = UIImage(named:"station")
        }
        print("acitvity Id ,",activityId)
        if activityId != 0 {
            
                let panDown = UITapGestureRecognizer(target: self, action: #selector(handleGesture(_:)))
                userIMG.isUserInteractionEnabled = true
                self.userIMG.addGestureRecognizer(panDown)
        }
    }
    
     @objc func handleGesture(_ gesture: UIPanGestureRecognizer) {
        let ficheVC = UIStoryboard(name: "Prestataire", bundle : nil).instantiateViewController(withIdentifier: "FicheActiviteVC") as! FicheActiviteVC
        ficheVC.idLecon = String(activityId)
        self.navigationController?.show(ficheVC, sender: self)
     }
    
    
    @objc func handleUserConnectNow(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            let ab = UserDefaults.standard.value(forKey: "pres") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            print("userConnectNow:",typingUsersDictionary)
            SocketIOManager.sharedInstance.SetOnline(room: Conversation["id"].stringValue, UserId: a["user"]["id"].stringValue)            
        }
        
    }
    @objc func handleUserConnectNow2(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            print("setOnline:",typingUsersDictionary)
        }
    }
    @objc func handleUserStopTypingNotification(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            var names = ""
            var totalTypingUsers = 0
            let ab = UserDefaults.standard.value(forKey: "pres") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let userId = typingUsersDictionary[1] as! String
            print("user:",typingUsersDictionary)
            if userId != a["user"]["id"].stringValue {
                bottomtable.constant = 0
                self.TypingName.isHidden = true
            }
            /* for (typingUser, _) in typingUsersDictionary {
             if typingUser != nickname {
             names = (names == "") ? typingUser : "\(names), \(typingUser)"
             totalTypingUsers += 1
             }
             }
             
             if totalTypingUsers > 0 {
             let verb = (totalTypingUsers == 1) ? "is" : "are"
             
             lblOtherUserActivityStatus.text = "\(names) \(verb) now typing a message..."
             lblOtherUserActivityStatus.isHidden = false
             }
             else {
             lblOtherUserActivityStatus.isHidden = true
             } */
        }
    }
    @objc func handleUserTypingNotification(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            var names = ""
            var totalTypingUsers = 0
            let ab = UserDefaults.standard.value(forKey: "pres") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let userId = typingUsersDictionary[1] as! String
            print("user:",typingUsersDictionary)
            
            if !(userId.elementsEqual(a["user"]["id"].stringValue)) {
                bottomtable.constant = 20.0
                self.TypingName.isHidden = false
            }
            /* for (typingUser, _) in typingUsersDictionary {
             if typingUser != nickname {
             names = (names == "") ? typingUser : "\(names), \(typingUser)"
             totalTypingUsers += 1
             }
             }
             
             if totalTypingUsers > 0 {
             let verb = (totalTypingUsers == 1) ? "is" : "are"
             
             lblOtherUserActivityStatus.text = "\(names) \(verb) now typing a message..."
             lblOtherUserActivityStatus.isHidden = false
             }
             else {
             lblOtherUserActivityStatus.isHidden = true
             } */
        }
    }
    @objc func handleDisconnectedUserUpdateNotification(notification: NSNotification) {
        let disconnectedUserNickname = notification.object as! String
        
        // lblNewsBanner.text = "User \(disconnectedUserNickname.uppercased()) has left."
        //showBannerLabelAnimated()
    }
    @objc func handleConnectedUserUpdateNotification(notification: NSNotification) {
        let connectedUserInfo = notification.object as! [String: AnyObject]
        let connectedUserNickname = connectedUserInfo["nickname"] as? String
        print(connectedUserNickname)
        //  lblNewsBanner.text = "User \(connectedUserNickname!.uppercased()) was just connected."
        //showBannerLabelAnimated()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
//self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadView"), object: nil)
        
       self.navigationController?.popViewController(animated: true)
         self.navigationController?.tabBarController?.tabBar.isHidden = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Messages.count != 0 {
            if bloque == true {
                return Messages.count + 1
            }else{
                return Messages.count
            }
            
        }else{
            return 0
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SocketIOManager.sharedInstance.getChatMessage { (messageInfo) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                self.empty_message.isHidden = true
                self.tableViewMSG.isHidden = false
                  self.empty_view_message.isHidden  = true
                print("*******************************")
                print(messageInfo as JSON)
                let s = messageInfo as JSON
                var x = " { \"id\" : 400, "
                x = x + " \"etat\" : \"non lu\" ,"
                x = x + " \"message\" : \"\(messageInfo[0]["message"].stringValue)\","
                x = x + "\"conversation\" : {} ,"
                x = x + "\"expediteur\" : { "
                x = x + "\"id\" : \"\(messageInfo[0]["clientid"].stringValue)\" , \"last_message_date\" : \"\(messageInfo[0]["currentDateTime"].stringValue)\" } } "
                print("9bal : ", x)
                let p = JSON.parse(x)
                
                // print("saifMessage ",p.description)
                //self.loadData(animated: false)
                var c = self.Messages.description
                c.characters.popFirst()
                c.characters.popLast()
                print("*******************************")
                print("self.messages ", self.Messages.description)
                self.Messages = JSON.parse("[ " + c + "," + p.description + "]")
                print("*******************************")
                print(self.Messages.description)
                print("*******************************")
                if self.Messages.count > 0{
                self.tableViewMSG.beginUpdates()
                    let indexPath = IndexPath(row: (self.Messages.count)  - 1, section: 0)
                
                self.tableViewMSG.insertRows(at: [indexPath], with: .automatic)
                self.tableViewMSG.endUpdates()
              
                self.tableViewMSG.reloadData()
                self.tableViewMSG.scrollToRow(at: IndexPath(row: self.Messages.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: false)
                }
                print("*******************************")
                // let q = messageInfo as [String]
                //print("message:",q[2])
            })
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        
        if Messages[indexPath.row]["expediteur"]["id"].stringValue != a["user"]["id"].stringValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sender", for: indexPath)
            let labelContent : PaddingLabel = cell.viewWithTag(1) as! PaddingLabel
            labelContent.text = Messages[indexPath.row]["message"].stringValue
            let labelTime : UILabel = cell.viewWithTag(2) as! UILabel
            let Time = Messages[indexPath.row]["date"].stringValue
            if (Time != "")
            {
                let x = Time.split(separator: "T")
                let c = x[1].split(separator: ":")
                labelTime.text = c[0] + ":" + c[1]
            }
            labelContent.setNeedsLayout()
            if #available(iOS 11.0, *) {
                labelContent.clipsToBounds = true
                labelContent.layer.cornerRadius = 10
                labelContent.layer.maskedCorners = [ .layerMinXMinYCorner , .layerMaxXMinYCorner , .layerMaxXMaxYCorner]
            }
            else {
                let path = UIBezierPath(roundedRect:labelContent.bounds,
                                        byRoundingCorners:[.topRight, .bottomRight , .topLeft],
                                        cornerRadii: CGSize(width: 10, height:  10))
                let maskLayer = CAShapeLayer()
                maskLayer.path = path.cgPath
                labelContent.layer.mask = maskLayer
            }
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "receiver", for: indexPath)
            let labelContent : PaddingLabel = cell.viewWithTag(1) as! PaddingLabel
            labelContent.text = Messages[indexPath.row]["message"].stringValue
            let labelTime : UILabel = cell.viewWithTag(2) as! UILabel
            let Time = Messages[indexPath.row]["date"].stringValue
            if (Time != "")
            {
                let x = Time.split(separator: "T")
                let c = x[1].split(separator: ":")
                labelTime.text = c[0] + ":" + c[1]
            }
            labelContent.setNeedsLayout()
            if #available(iOS 11.0, *) {
                labelContent.clipsToBounds = true
                labelContent.layer.cornerRadius = 10
                labelContent.layer.maskedCorners = [ .layerMinXMinYCorner , .layerMaxXMinYCorner , .layerMinXMaxYCorner]
            }
            else {
                let path = UIBezierPath(roundedRect:labelContent.bounds,
                                        byRoundingCorners:[.topRight, .bottomLeft , .topLeft],
                                        cornerRadii: CGSize(width: 10, height:  10))
                let maskLayer = CAShapeLayer()
                maskLayer.path = path.cgPath
                labelContent.layer.mask = maskLayer
            }
            return cell
        }
        
    }
    @objc func sendMessages(_ sender: Any) {
        print ("heey")
        if conversationTextfield.text != "" {
            self.empty_message.isHidden = true
            self.tableViewMSG.isHidden = false
              self.empty_view_message.isHidden  = true
            self.button.isEnabled = false
            let ab = UserDefaults.standard.value(forKey: "pres") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let params: Parameters = [
                "converationId": Conversation["id"].stringValue,
                "message" : self.conversationTextfield.text!
            ]
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : a["value"].stringValue
            ]
            Alamofire.request(ScriptBase.sharedInstance.Send_Message , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    self.button.isEnabled = true
                    
                    let b = JSON(response.data)
                    print("message:",response)
                    SocketIOManager.sharedInstance.sendMessage(idNickname: a["user"]["id"].stringValue, nickname: a["user"]["email"].stringValue, msg: self.conversationTextfield.text!, conv: self.Conversation["id"].stringValue,photo:a["user"]["photo"].stringValue)
                    print("idNickname : " , a["user"]["id"].stringValue , " nickname : " ,  a["user"]["email"].stringValue , "msg" , self.conversationTextfield.text! , " conv : " , self.Conversation["id"].stringValue)
                    self.conversationTextfield.text = ""
                    
                    self.loadData(animated: true)
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titleMessageListLBL.text = Localization("ConversationTitre")
        conversationTextfield.placeholder = Localization("ConversationTextField")
            empty_message.text = Localization("pasDeMsg")
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    func loadData(animated:Bool){
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.afficherMessageConversation + Conversation["id"].stringValue , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                let b = JSON(response.data)
                print("lesMessages:",b)
                let c = JSON(b["data"].arrayObject)
                //previousCount  =
                if c.count != 0 {
                    self.empty_message.isHidden = true
                      self.empty_view_message.isHidden  = true
                    self.Messages = c
                    //print("count:",self.Messages.count)
                    self.tableViewMSG.reloadData()
                    if animated && self.Messages.count > 0 && self.tableViewMSG.numberOfRows(inSection: 0) > 0{
                        self.tableViewMSG.scrollToRow(at: IndexPath(row: self.Messages.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                    }
                    else if self.Messages.count > 0 && self.Messages.count > 0  && self.tableViewMSG.numberOfRows(inSection: 0) > 0{
                        self.tableViewMSG.scrollToRow(at: IndexPath(row: self.Messages.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: false)
                    }
                }else {
                 self.empty_message.isHidden = false
                    self.tableViewMSG.isHidden = true
                      self.empty_view_message.isHidden  = false
                }
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        SocketIOManager.sharedInstance.sendStartTypingMessage(Conv: Conversation["id"].stringValue , nickname: a["user"]["id"].stringValue)
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        let ab = UserDefaults.standard.value(forKey: "pres") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        SocketIOManager.sharedInstance.stopmessage(Conv: Conversation["id"].stringValue , nickname: a["user"]["id"].stringValue)
        return true
    }
    
    
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }

}
