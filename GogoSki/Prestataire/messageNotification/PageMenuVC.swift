//
//  PageMenuVC.swift
//  GoGoSkiUiKissou
//
//  Created by AYMEN on 10/17/17.
//  Copyright © 2017 KISSOU. All rights reserved.
//

import UIKit
import PageMenu


class PageMenuVC: InternetCheckRed , CAPSPageMenuDelegate{

    @IBOutlet weak var headerLBL: UILabel!
    var pageMenu : CAPSPageMenu?
    var MessageVC  : MessageVC!
    var NotificationVC  : NotificationVC!
    var parameters : [CAPSPageMenuOption]!
    var controllerArray : [UIViewController] = []
    
    @IBOutlet weak var viewContainer: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      

        // Do any additional setup after loading the view.
        // Array to keep track of controllers in page menu
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        let MessageVCId = "MessageVC"
        let NotificationVCId = "NotificationVC"
        let storyboardName = "Prestataire"
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)
       
        MessageVC = (storyboard.instantiateViewController(withIdentifier: MessageVCId) as UIViewController!) as! MessageVC!
        MessageVC?.title = "MESSAGES"
        MessageVC?.parentNavigationController = self.navigationController
        controllerArray.append(MessageVC!)

        NotificationVC = (storyboard.instantiateViewController(withIdentifier: NotificationVCId) as UIViewController!) as! NotificationVC!
        NotificationVC?.title = "NOTIFICATIONS"
         NotificationVC?.parentNavigationController = self.navigationController
        controllerArray.append(NotificationVC!)
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        
        // Lastly add page menu as subview of base view controller view
        // or use pageMenu controller in you view hierachy as desired
        
        
        var parameterss: [CAPSPageMenuOption] = [
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor(red: 186.0/255.0, green: 0/255.0, blue: 13.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(UIColor(red: 186.0/255.0, green: 0.0/255.0, blue: 13.0/255.0, alpha: 1.0)),
            .menuMargin(20),
            .menuHeight(50.0),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor(red: 40.0/255.0, green: 40.0/255.0, blue: 40.0/255.0, alpha: 1.0)),
            .menuItemSeparatorWidth(0)
            ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x : 0.0, y : 83.0, width : self.view.frame.width, height : self.view.frame.height - 150), pageMenuOptions: parameterss)
        
        pageMenu?.delegate = self;

        
        self.view.addSubview(pageMenu!.view)
        
        
      //  self.pageMenu?.moveToPage(1)
    //    pageMenu!.delegate = self as! CAPSPageMenuDelegate
    }

    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        print("did move to page")
        
        if index == 0 {
            headerLBL.text = "Messages"
        }else{
            headerLBL.text = "Notifications"

        }
        
    }
}
