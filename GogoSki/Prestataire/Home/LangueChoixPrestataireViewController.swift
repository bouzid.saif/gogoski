//
//  LangueChoixPrestataireViewController.swift
//  GogoSki
//
//  Created by AYMEN on 12/11/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit

class LangueChoixPrestataireViewController: InternetCheckRed ,UITableViewDelegate,UITableViewDataSource {
   
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    var languages : [String] = []
    var selectedIndex : Int = -1
    var indexLangue : IndexPath = []
  
    @IBOutlet weak var tableLangue: UITableView!
    @IBOutlet weak var backBTN: UIButton!
    @IBOutlet weak var langueTitleLBL: UILabel!
    @IBOutlet weak var validerBTN: UIButton!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        tableLangue.delegate = self
        tableLangue.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        languages.append(arrayLanguages[1])
        languages.append(arrayLanguages[2])
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        langueTitleLBL.text = Localization("PageLangueTitle")
        Localisator.sharedInstance.saveInUserDefaults = true
        validerBTN.setTitle(Localization("BtnValider"), for: .normal)
        tableLangue.reloadData()
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        //  let img : UIImageView = cell.viewWithTag(2) as! UIImageView
        let label : UILabel = cell.viewWithTag(1) as! UILabel
        print(NSLocale.current.languageCode == "en")
        if Localisator.sharedInstance.currentLanguage == arrayLanguages[0] {
            if NSLocale.current.languageCode == "en" && indexPath.row == 0 {
                
                indexLangue = indexPath
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }else if NSLocale.current.languageCode == "fr" && indexPath.row == 1 {
                
                indexLangue = indexPath
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            } else if (NSLocale.current.languageCode != "fr" && NSLocale.current.languageCode != "en") && indexPath.row == 1 {
                
                indexLangue = indexPath
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }
        }
        else if (Localisator.sharedInstance.currentLanguage != languages[indexPath.row])
        {
            
            cell.accessoryType = UITableViewCellAccessoryType.none
            // img.isHidden = false
        }
        else {
            
            indexLangue = indexPath
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        }
        
        label.text       = Localization(languages[indexPath.row])
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
        // let img : UIImageView = cell!.viewWithTag(2) as! UIImageView
        //  img.isHidden = false
        if (cell?.accessoryType != UITableViewCellAccessoryType.checkmark) {
            
            cell?.accessoryType = UITableViewCellAccessoryType.checkmark
            let cell1 = tableView.cellForRow(at: indexLangue)
            cell1?.accessoryType = UITableViewCellAccessoryType.none
            tableView.deselectRow(at: indexLangue, animated: true)
            selectedIndex = indexPath.row
            indexLangue = indexPath
            
        }
        /*  else {
         cell?.accessoryType = UITableViewCellAccessoryType.none
         tableView.deselectRow(at: indexPath, animated: true)
         }*/
        
        
    }
   
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func validerAction(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: Localization("languageChangedWarningMessage"), preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            
            SetLanguage(self.languages[self.indexLangue.row])
            
            Localisator.sharedInstance.saveInUserDefaults = true
            self.navigationController?.popViewController(animated: true)
        }
        let cancelAction = UIAlertAction(title: "Cancel" , style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
   
}
