//
//  HomaPrestataireViewController.swift
//  GogoSki
//
//  Created by AYMEN on 12/9/17.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit

class HomaPrestataireViewController:  InternetCheckRed {
    
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var espacePro: UILabel!
    @IBOutlet weak var btnConnexion: UIButton!
    @IBOutlet weak var btnInscription: UIButton!
    @IBOutlet weak var btnFlag : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        self.startMonitoringInternet(backgroundColor: UIColor.red, style: .statusLine, textColor: UIColor.white, message: Localization("connexionperdu"), remoteHostName: "apple.com")
        // self.startMonitoringInternet(backgroundColor:UIColor.blue, style: .statusLine, textColor:UIColor.white, message:"Merci de vérifier votre connexion à internet")
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        espacePro.text = Localization("espaceUtilisateur")
        espacePro.textAlignment = .right
        desc.text = Localization("DescriptionPageBienvenue")
        btnConnexion.setTitle(Localization("BtnConex"), for: .normal)
        btnInscription.setTitle(Localization("BtnInscri"), for: .normal)
        btnFlag.setBackgroundImage(UIImage(named: Localization("flag")), for: .normal)
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    @IBAction func Go_to_prestataire(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

