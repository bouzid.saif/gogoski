//
//  ModifierMotdePasseUser.swift
//  GogoSki
//
//  Created by Bouzid saif on 21/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
class ModifierMotdePasseUser: InternetCheckViewControllers {
    /*
     * profile picture that you can touch to go profil
     */
    @IBOutlet weak var imageToProfileNavBar: RoundedUIImageView!
    /*
     * password textfield
     */
    @IBOutlet weak var password: PaddingTextField!
    /*
     * new password label
     */
    @IBOutlet weak var labelNewPassword: UILabel!
    /*
     * new password textfield
     */
    @IBOutlet weak var newPassword: PaddingTextField!
    /*
     * password label
     */
    @IBOutlet weak var labelMotDePasse: UILabel!
    /*
     * confirm label
     */
    @IBOutlet weak var labelConfirmer: UILabel!
    /*
     * validate modification button
     */
    @IBOutlet weak var btnValider: UIButton!
    /*
     * title of this page
     */
    @IBOutlet weak var titre: UILabel!
    /*
     * new password textfield
     */
    @IBOutlet weak var confirmNewPassword: PaddingTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if let urlImgUser = URL(string : a["users"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imageToProfileNavBar.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*
     * back action
     */
    @IBAction func go_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     * validate modification action
     */
    @IBAction func valider_action(_ sender: Any) {
        if password.text != "" && newPassword.text != "" && confirmNewPassword.text != "" {
            if newPassword.text == confirmNewPassword.text {
                if password.text != newPassword.text {
                    self.btnValider.isEnabled = false
                    LoaderAlert.shared.show(withStatus: "Modification...")
                    let params: Parameters = [
                        "motPassActuel": password.text!,
                        "nouveauMotPass" : newPassword.text!
                    ]
                    let ab = UserDefaults.standard.value(forKey: "User") as! String
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    var a = JSON(data: dataFromString!)
                    let header: HTTPHeaders = [
                        "Content-Type" : "application/json",
                        "x-Auth-Token" : a["value"].stringValue
                    ]
                    Alamofire.request(ScriptBase.sharedInstance.ModiferMP , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                        .responseJSON { response in
                            LoaderAlert.shared.dismiss()
                            self.btnValider.isEnabled = true
                            let b = JSON(response.data)
                            print("b:",b)
                            self.newPassword.text = ""
                            self.password.text = ""
                            self.confirmNewPassword.text = ""
                            if b["status"].boolValue {
                                _ = SweetAlert().showAlert(Localization("mdp"), subTitle: Localization("modif"), style: AlertStyle.success)
                            }else{
                                if b["message"].stringValue == "mot de passe invalide" {
                                    _ = SweetAlert().showAlert(Localization("mdp"), subTitle: Localization("mdp1"), style: AlertStyle.error)
                                }else{
                                    _ = SweetAlert().showAlert(Localization("mdp"), subTitle: Localization("erreur"), style: AlertStyle.error)
                                }
                            }
                    }
                }else{
                    _ = SweetAlert().showAlert(Localization("mdp"), subTitle: Localization("mdp2"), style: AlertStyle.warning)
                }
            }else{
                _ = SweetAlert().showAlert(Localization("mdp"), subTitle: Localization("mdp3"), style: AlertStyle.warning)
            }
        }
    }
    
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("ModifierMotDePasseTitre")
        btnValider.setTitle(Localization("BtnValiderModif"), for: .normal)
        labelConfirmer.text = Localization("labelConfirmer")
        labelMotDePasse.text = Localization("labelPassword")
        labelNewPassword.text = Localization("labelNewPassword")
        password.placeholder = Localization("motDePasse")
        newPassword.placeholder = Localization("motDePasse")
        confirmNewPassword.placeholder = Localization("motDePasse")
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
}

