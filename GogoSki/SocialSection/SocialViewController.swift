//
//  SocialViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 16/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import PageMenu
import SwiftyJSON

class SocialViewController: InternetCheckViewControllers , CAPSPageMenuDelegate{
    /*
     *setting of the page menu
     *
     */
    var parameters : [CAPSPageMenuOption] = []
    /*
     *container of the two views
     *
     */
    var controllerArray : [UIViewController] = []
    /*
     *image of the actual user
     *
     */
    @IBOutlet weak var imageToProfileNavBar: RoundedUIImageView!
    /*
     *title of the view
     *
     */
    @IBOutlet weak var titre: UILabel!
    /*
     *the container view
     *
     */
    @IBOutlet weak var container: UIView!
    /*
     *pagemenu manager
     *
     */
    var pageMenu : CAPSPageMenu?
    override func viewDidLoad() {
        super.viewDidLoad()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if let urlImgUser = URL(string : a["user"]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imageToProfileNavBar.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        let mapcontroller = self.storyboard?.instantiateViewController(withIdentifier: "CarteViewController") as! CarteViewController
        mapcontroller.title = "CARTE"
        mapcontroller.parentNavigationController = self.navigationController
        controllerArray.append(mapcontroller)
        let listeController = self.storyboard?.instantiateViewController(withIdentifier: "ListeViewController") as! ListeViewController
        listeController.title = "LISTE"
        listeController.parentNavigationController = self.navigationController
        controllerArray.append(listeController)
        parameters = [
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(UIColor(red: 18.0/255.0, green: 150.0/255.0, blue: 225.0/255.0, alpha: 1.0)),
            .menuMargin(20),
            .menuHeight(50.0),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor(red: 40.0/255.0, green: 40.0/255.0, blue: 40.0/255.0, alpha: 1.0)),
            .menuItemSeparatorWidth(0)
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x : 0.0, y : 83.0, width : self.view.frame.width, height : self.view.frame.height - 90.0), pageMenuOptions: parameters)
        
        pageMenu?.delegate = self;
        container?.addSubview(pageMenu!.view)
        //self.view.addSubview(pageMenu!.view)
        
    }
    /*
     *func to select the first view
     *
     */
    func selectPage(){
        pageMenu!.moveToPage(0)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        //pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: (container?.frame.width)!, height: (container?.frame.height)!), pageMenuOptions: parameters)
        
        
        pageMenu?.view.frame = container.bounds
        container?.updateConstraintsIfNeeded()
        pageMenu?.viewDidLayoutSubviews()
        
        
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        print("did move to page")
        
        
        
        if index == 0 {
            // self.Message_Notif_Title.text = "Messages"
            (controller as! CarteViewController).viewWillAppear(true)
        }else{
            //headerLBL.text = "Notifications"
            // self.Message_Notif_Title.text = "Notifications"
            UsersStatic.sharedInstance.test = true
            (controller as! ListeViewController).viewWillAppear(true)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("SocialTitre")
        
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
}

