//
//  CarteViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 16/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import UIDropDown
import MapKit
import CoreLocation
import MapleBacon
import SwiftyJSON
import UIColor_Hex_Swift
import Alamofire

extension MKMapView {
    var zoomLevel: Double {
        return log2(360 * ((Double(self.frame.size.width) / 256) / self.region.span.longitudeDelta)) - 1
    }
}
extension CarteViewController : FBClusteringManagerDelegate {
    
    func cellSizeFactor(forCoordinator coordinator:FBClusteringManager) -> CGFloat {
        return 1.0
    }
}
extension MKMapView{
    func zoomIn(coordinate: CLLocationCoordinate2D, withLevel level:CLLocationDistance = 10000){
        let camera =
            MKMapCamera(lookingAtCenter: coordinate, fromEyeCoordinate: coordinate, eyeAltitude: level)
        self.setCamera(camera, animated: true)
    }
}
class CarteViewController: InternetCheckViewControllers,MKMapViewDelegate,UIGestureRecognizerDelegate {
    /*
     *the parent view navigation controller
     *
     */
    var parentNavigationController : UINavigationController?
    /*
     *Level in french
     *
     */
    let NiveauSaifFR = ["Tous les niveaux","Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     *level in english
     *
     */
    let NiveauSaifEN = ["All the levels","Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     *practice in french
     *
     */
    let SportSaifFR = ["Toutes les pratiques","Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     *practice in english
     *
     */
    let SportSaifEN = ["All the practices","Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     *to test if this is the first load of the view
     *
     */
    var First = false
    /*
     *level picker
     *
     */
    @IBOutlet weak var Niveau : UIDropDown!
    /*
     *my friends data
     *
     */
    var mesAmis : JSON = []
    /*
     *my family data
     *
     */
    var mesTribu : JSON = []
    /*
     *cluster manager of annotations
     *
     */
    let clusteringManager = FBClusteringManager()
    /*
     *practice picker
     *
     */
    @IBOutlet weak var Pratique : UIDropDown!
    /*
     *favorites picker
     *
     */
    @IBOutlet weak var Favoris : UIDropDown!
    @IBOutlet weak var map: MKMapView!
    
    @IBOutlet var pinchReconizer : UIPinchGestureRecognizer!
    override func viewDidLoad() {
        super.viewDidLoad()
        StaticPositionController.shared.startNotifications()
        StaticPositionController.shared.fetchBlacklist()
        
        
        
        
        LoadFavorite()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDisconnectedUserUpdateNotification2222222(notification:)), name: NSNotification.Name(rawValue: "viewdidload"), object: nil)
        
        Niveau.cornerRadius = 20
        Pratique.cornerRadius = 20
        Favoris.cornerRadius = 20
        
        
        Favoris.borderColor = UIColor.clear
        Favoris.fontSize = 13.0
        Favoris.animationType = .Classic
        //Favoris.arrowPadding = 12.0
        Favoris.imageSaif = UIImage(named: "downArrow_saif")
        Favoris.tableHeight = 150.0
        Favoris.layer.masksToBounds = false
        Favoris.layer.shadowColor = UIColor.black.cgColor
        Favoris.layer.shadowOpacity = 0.5
        Favoris.layer.shadowOffset = CGSize(width: 0, height: 0)
        Favoris.layer.shadowRadius = 1
        Favoris.hideOptionsWhenSelect = true
        Favoris.contentMode = .left
        
        if Localisator.sharedInstance.currentLanguage == "French_fr" || Localisator.sharedInstance.currentLanguage == "French" || Localisator.sharedInstance.currentLanguage == "Français" {
            self.Favoris.options = ["Tout le monde","Tribu","Amis"]
        }else {
            self.Favoris.options = ["Everyone","Family","Friends"]
        }
        self.Favoris.selectOptions(index: 0)
        self.Favoris.didTouch{ (touch) in
            if touch {
                _ = self.Niveau.resign()
                _ = self.Pratique.resign()
            }
        }
        
        Niveau.contentMode = .left
        Niveau.borderColor = UIColor.clear
        Niveau.layer.masksToBounds = false
        Niveau.layer.shadowColor = UIColor.black.cgColor
        Niveau.layer.shadowOpacity = 0.5
        Niveau.layer.shadowOffset = CGSize(width: 0, height: 0)
        Niveau.layer.shadowRadius = 1;
        Niveau.fontSize = 13.0
        Niveau.animationType = .Classic
        //Niveau.arrowPadding = 12.0
        Niveau.imageSaif = UIImage(named: "downArrow_saif")
        Niveau.tableHeight = 150.0
        Niveau.hideOptionsWhenSelect = true
        Niveau.didTouch{ (touch) in
            if touch {
                _ = self.Pratique.resign()
                _ = self.Favoris.resign()
            }
        }
        Pratique.borderColor = UIColor.clear
        Pratique.layer.masksToBounds = false
        Pratique.layer.shadowColor = UIColor.black.cgColor
        Pratique.layer.shadowOpacity = 0.5
        Pratique.layer.shadowOffset = CGSize(width: 0, height: 0)
        Pratique.layer.shadowRadius = 1
        Pratique.fontSize = 13.0
        Pratique.animationType = .Classic
        Pratique.contentMode = .left
        Pratique.imageSaif = UIImage(named: "downArrow_saif")
        // Pratique.arrowPadding = 12.0
        Pratique.tableHeight = 150.0
        Pratique.hideOptionsWhenSelect = true
        Pratique.didTouch{ (touch) in
            if touch {
                _ = self.Niveau.resign()
                _ = self.Favoris.resign()
            }
        }
        if  Localisator.sharedInstance.currentLanguage == "French_fr" || Localisator.sharedInstance.currentLanguage == "French" || Localisator.sharedInstance.currentLanguage == "Français" {
            Niveau.options = NiveauSaifFR
        } else {
            Niveau.options = NiveauSaifEN
        }
        Niveau.selectOptions(index: 0)
        if  Localisator.sharedInstance.currentLanguage == "French_fr" || Localisator.sharedInstance.currentLanguage == "French" || Localisator.sharedInstance.currentLanguage == "Français" {
            Pratique.options = SportSaifFR
        }else {
            Pratique.options = SportSaifEN
        }
        Pratique.selectOptions(index: 0)
        configureViewFromLocalisation()
        Pratique.didSelect { (value, position) in
            
            self.prepareData(Pratique: position,Niveau : self.Niveau.selectedIndex!,Favoris : self.Favoris.selectedIndex!)
            
        }
        Niveau.didSelect { (value, position) in
            
            self.prepareData(Pratique: self.Pratique.selectedIndex!,Niveau : position,Favoris : self.Favoris.selectedIndex!)
            
        }
        Favoris.didSelect{ (value, position) in
            
            self.prepareData(Pratique: self.Pratique.selectedIndex!,Niveau : self.Niveau.selectedIndex!,Favoris : position)
            
        }
        pinchReconizer   =  UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGesture))
        //let tomtomOverlay = MKTileOverlay(urlTemplate: "https://api.tomtom.com/map/1/tile/basic/main/{z}/{x}/{y}.png?key=bLfvvOXNSRvSDFSUjDivc4NY2pzfnvS4")
        //tomtomOverlay.canReplaceMapContent = true
        //self.map.add(tomtomOverlay, level: MKOverlayLevel.aboveLabels)
        
        self.map.showsTraffic = true
        self.map.showsBuildings = true
        
        self.map.addGestureRecognizer(pinchReconizer)
        pinchReconizer.delegate = self
        self.map.isUserInteractionEnabled = true
        self.map.delegate = self
        configureTileOverlay()
        NotificationCenter.default.addObserver(self, selector: #selector(self.MapLoadData(notification:)), name: NSNotification.Name(rawValue: "MapGetDone"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.MapUpdateLocation(notification:)), name: NSNotification.Name(rawValue: "MapUpdateLocation"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.MapUpdateLocation(notification:)), name: NSNotification.Name(rawValue: "MapRemove"), object: nil)
        StaticPositionController.shared.getData(completionHandler: { (verif) in
            
            if verif {
                self.map.showsUserLocation = true
                //self.map.setUserTrackingMode(.followWithHeading, animated: true)
            }
        })
    }
    private func configureTileOverlay() {
        // We first need to have the path of the overlay configuration JSON
        guard let overlayFileURLString = Bundle.main.path(forResource: "Overlay", ofType: "json") else {
            return
        }
        let overlayFileURL = URL(fileURLWithPath: overlayFileURLString)
        
        // After that, you can create the tile overlay using MapKitGoogleStyler
        guard let tileOverlay = try? MapKitGoogleStyler.buildOverlay(with: overlayFileURL) else {
            return
        }
        
        // And finally add it to your MKMapView
        map.add(tileOverlay)
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let tileOverlay = overlay as? MKTileOverlay {
            return MKTileOverlayRenderer(tileOverlay: tileOverlay)
        } else {
            return MKOverlayRenderer(overlay: overlay)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if First == true {
            if  Localisator.sharedInstance.currentLanguage == "French_fr" || Localisator.sharedInstance.currentLanguage == "French" || Localisator.sharedInstance.currentLanguage == "Français" {
                Niveau.options = NiveauSaifFR
            } else {
                Niveau.options = NiveauSaifEN
            }
            Niveau.selectOptions(index: 0)
            if  Localisator.sharedInstance.currentLanguage == "French_fr" || Localisator.sharedInstance.currentLanguage == "French" || Localisator.sharedInstance.currentLanguage == "Français" {
                Pratique.options = SportSaifFR
            }else {
                Pratique.options = SportSaifEN
            }
            Pratique.selectOptions(index: 0)
            
        }else {
            prepareData(Pratique: 0,Niveau : 0,Favoris : 0)
        }
        
        
        
    }
    @objc func MapRemoveUser(notification:NSNotification) {
        prepareData(Pratique: self.Pratique.selectedIndex!,Niveau : self.Niveau.selectedIndex!,Favoris : self.Favoris.selectedIndex!)
    }
    /*
     *location did update
     *
     */
    @objc func MapUpdateLocation(notification:NSNotification) {
        prepareData(Pratique: self.Pratique.selectedIndex!,Niveau : self.Niveau.selectedIndex!,Favoris : self.Favoris.selectedIndex!)
    }
    /*
     *load the annotation
     *
     */
    @objc func MapLoadData(notification:NSNotification) {
        
        prepareData(Pratique: 0,Niveau : 0,Favoris : 0)
        
    }
    /*
     *the manager of what to show and not
     *
     */
    func prepareData(Pratique: Int,Niveau : Int,Favoris : Int) {
        var result : [(key: String, value: JSON)] = []
        var array: [FBAnnotation] = []
        if !(Pratique == 0 && Niveau == 0 && Favoris == 0) {
            if Pratique == 0 {
                // je teste sur le Niveau  et Favoris
                if Niveau != 0 && Favoris == 0 {
                    //ici filtre que par le niveau
                    var b = StaticPositionController.shared.UsersPos
                    result = b.filter{$0.value["niveau"].stringValue == NiveauSaifFR[Niveau]  }
                    print("ParNiveau:",result.count)
                }else if Niveau == 0 && Favoris != 0 {
                    //ici filtre que par favoris
                    let b = StaticPositionController.shared.UsersPos
                    
                    if Favoris == 2 {
                        if self.mesAmis.count != 0 {
                            result = b.filter{ var match = false; for i in 0 ... self.mesAmis.count - 1  { if $0.value["user_id"].stringValue == self.mesAmis[i]["id"].stringValue { match = true}  }
                                return match
                            }
                        }
                    }else {
                        if self.mesTribu.count != 0 {
                            result = b.filter{ var match = false; for i in 0 ... self.mesTribu.count - 1  { if $0.value["user_id"].stringValue == self.mesTribu[i]["id"].stringValue { match = true}  }
                                return match
                            }
                        }
                    }
                    print("ParFavoris:",result.count)
                } else {
                    // ici filtre par niveau et favoris
                    let b = StaticPositionController.shared.UsersPos
                    
                    if Favoris == 2 {
                        if self.mesAmis.count != 0 {
                            result = b.filter{ var match = false; for i in 0 ... self.mesAmis.count - 1  { if $0.value["user_id"].stringValue == self.mesAmis[i]["id"].stringValue && self.mesAmis[i]["niveau"].stringValue == NiveauSaifFR[Niveau] { match = true}  }
                                return match
                            }
                        }
                    }else if Favoris == 1{
                        if self.mesTribu.count != 0 {
                            result = b.filter{ var match = false; for i in 0 ... self.mesTribu.count - 1  { if $0.value["user_id"].stringValue == self.mesTribu[i]["id"].stringValue && self.mesTribu[i]["niveau"].stringValue == NiveauSaifFR[Niveau] { match = true}  }
                                return match
                            }
                        }
                    }
                    print("Par Niveau et favoris :",result.count)
                }
            }else if Niveau == 0 {
                // ici Pratique est remplis , on veut savoir maintenant Favoris
                if Pratique != 0 && Favoris == 0 {
                    //ici filtre que par pratique
                    let b = StaticPositionController.shared.UsersPos
                    result = b.filter{$0.value["pratique"].stringValue == SportSaifFR[Pratique] }
                    print("ParPratique:",result.count)
                } else {
                    // ici filtre par pratique et favoris
                    let b = StaticPositionController.shared.UsersPos
                    
                    if Favoris == 2 {
                        if self.mesAmis.count != 0 {
                            result = b.filter{ var match = false; for i in 0 ... self.mesAmis.count - 1  { if $0.value["user_id"].stringValue == self.mesAmis[i]["id"].stringValue && self.mesAmis[i]["pratique"].stringValue == SportSaifFR[Pratique] { match = true}  }
                                return match
                            }
                        }
                    }else {
                        if self.mesTribu.count != 0 {
                            result = b.filter{ var match = false; for i in 0 ... self.mesTribu.count - 1  { if $0.value["user_id"].stringValue == self.mesTribu[i]["id"].stringValue && self.mesTribu[i]["pratique"].stringValue == SportSaifFR[Pratique] { match = true}  }
                                return match
                            }
                        }
                    }
                    print("Par Pratique et favoris :",result.count)
                }
            }else if Favoris == 0 {
                // ici Pratique et niveau sont remplis donc filtre par niveau et pratique
                let b = StaticPositionController.shared.UsersPos
                result = b.filter{ ($0.value["pratique"].stringValue == SportSaifFR[Pratique] ) && $0.value["niveau"].stringValue == NiveauSaifFR[Niveau]}
                print("ParPratique et Niveau:",result.count)
            } else {
                //ici filtre par Pratique , niveau , favoris
                let b = StaticPositionController.shared.UsersPos
                
                if Favoris == 2 {
                    if self.mesAmis.count != 0 {
                        result = b.filter{ var match = false; for i in 0 ... self.mesAmis.count - 1  { if $0.value["user_id"].stringValue == self.mesAmis[i]["id"].stringValue && self.mesAmis[i]["pratique"].stringValue == SportSaifFR[Pratique] && self.mesAmis[i]["niveau"].stringValue == NiveauSaifFR[Niveau] { match = true}  }
                            return match
                        }
                    }
                }else {
                    if self.mesTribu.count != 0 {
                        result = b.filter{ var match = false; for i in 0 ... self.mesTribu.count - 1  { if $0.value["user_id"].stringValue == self.mesTribu[i]["id"].stringValue && self.mesTribu[i]["pratique"].stringValue == SportSaifFR[Pratique] && self.mesTribu[i]["niveau"].stringValue == NiveauSaifFR[Niveau] { match = true}  }
                            return match
                        }
                    }
                }
                print("tout les filtres remplis :",result.count)
            }
            
        }else {
            //ici tout le monde
            let b = StaticPositionController.shared.UsersPos
            result = b.filter{ _ in return true}
            print("tout le monde:",result.count)
        }
        StaticImageAnnotation.sharedInstance.remove()
        for (key,value) in result {
            if StaticPositionController.shared.ListeNoir.contains(key) == false {
                /* do { try  self.map.reloadInputViews()
                 }catch {
                 
                 } */
                let ReportAnnotation = FBAnnotation()
                let Report = ImageAnnotation()
                ReportAnnotation.title = key
                ReportAnnotation.subtitle = key
                Report.coordinate = CLLocationCoordinate2D(latitude: Double(value["latitude"].stringValue)!, longitude: Double(value["longitude"].stringValue)!)
                Report.subtitle = key
                ReportAnnotation.coordinate = CLLocationCoordinate2D(latitude: Double(value["latitude"].stringValue)!, longitude: Double(value["longitude"].stringValue)!)
                if value["photo"].stringValue == "Default" {
                    Report.image1 = "Default"
                }else {
                    Report.image1 = value["photo"].stringValue
                }
                Report.image2 = value["pratique"].stringValue
                Report.image4 = value["niveau"].stringValue
                Report.colour = UIColor.white
                StaticImageAnnotation.sharedInstance.add(im:Report)
                array.append(ReportAnnotation)
            }
        }
        self.clusteringManager.removeAll()
        self.clusteringManager.add(annotations: array)
        self.clusteringManager.delegate = self
        self.refreshPosition()
        
    }
    /*
     *select the annotation to go to profile of user
     *
     */
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if let ViewAnnot = view as? ImageAnnotationView {
            print("didselect")
            let iduser = ViewAnnot.annotation!.subtitle as! String
            let view = self.storyboard?.instantiateViewController(withIdentifier: "visiteur_profil") as! visiteur_profil
            view.id = iduser
            view.parentNavigationController = self.parentNavigationController
            parentNavigationController?.pushViewController(view, animated: true)
        }
    }
    /*
     *reload the view
     *
     */
    @objc func handleDisconnectedUserUpdateNotification2222222(notification:NSNotification) {
        self.viewDidLoad()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //coord = FIRDatabase.database().reference().child("map")
        //coord.removeAllObservers()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    /*
     *configure texts
     *
     */
    func configureViewFromLocalisation() {
        
        if Localisator.sharedInstance.currentLanguage == "French_fr" || Localisator.sharedInstance.currentLanguage == "French" || Localisator.sharedInstance.currentLanguage == "Français" {
            Favoris.placeholder = "Favoris"
            
            Niveau.placeholder = "Niveau"
            Pratique.placeholder = "Pratique"
            
        }else {
            Favoris.placeholder = "Favorites"
            
            Niveau.placeholder = "Level"
            Pratique.placeholder = "Practice"
            
        }
        //  Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    /*
     *the action of the pinch gesture
     *
     */
    @objc func handlePinchGesture(_ pinchRecognizer:UIPinchGestureRecognizer){
        
        if (pinchRecognizer.state != UIGestureRecognizerState.changed){
            return
        }
      
        StaticImageAnnotation.sharedInstance.resize(zoomlevel: map.zoomLevel)
        
        var y : [MKAnnotation] = []
        if map.annotations.count != 0 {
            for i in 0 ... map.annotations.count - 1 {
                let annotation = map.annotations[i]
                if annotation.isKind(of: MKUserLocation.self){
                    
                }else if annotation.isKind(of: FBAnnotation.self) {
                    
                    //resize
                    
                    
                 
                    StaticImageAnnotation.sharedInstance.resize(zoomlevel: self.map.zoomLevel)
                    y.append( (map.delegate?.mapView!(map, viewFor: annotation))!.annotation! )
                    
                    
                    
                }
                
            }
            if y.count != 0 {
                
                self.map.removeAnnotations(y)
                self.map.addAnnotations(y)
                
            }
            
        }
    }
    func formatAnnotationView(_ pinView: ImageAnnotationView, for aMapView: MKMapView,zoom:Double,pinch:UIPinchGestureRecognizer) {
        
        //nothing done yet
        
        
        
        
        
        
        
        
    }
    /*
     *resize function
     *
     */
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    /*
     *to refresh the cluster manager
     *
     */
    func refreshPosition (){
        DispatchQueue.main.async(execute:  {
            let mapBoundsWidth = Double(self.map.bounds.size.width)
            let mapRectWidth = self.map.visibleMapRect.size.width
            let scale = mapBoundsWidth / mapRectWidth
            
            let annotationArray = self.clusteringManager.clusteredAnnotations(withinMapRect: self.map.visibleMapRect, zoomScale:scale)
            
            DispatchQueue.main.async {
                self.clusteringManager.display(annotations: annotationArray, onMapView:self.map)
            }
        } )
    }
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        DispatchQueue.main.async(execute: {
            let mapBoundsWidth = Double(self.map.bounds.size.width)
            let mapRectWidth = self.map.visibleMapRect.size.width
            let scale = mapBoundsWidth / mapRectWidth
            
            let annotationArray = self.clusteringManager.clusteredAnnotations(withinMapRect: self.map.visibleMapRect, zoomScale:scale)
            /////kisou bch
            
            
            
            
            DispatchQueue.main.async {
                self.clusteringManager.display(annotations: annotationArray, onMapView:self.map)
            }
        } )
        for annotation in map.annotations {
            
            if annotation.isKind(of: MKUserLocation.self){
                return
            }
            
            
            
            
            
            if annotation.isKind(of: ImageAnnotation.self) {
                
                
            }
        }
        
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        if annotation is FBAnnotationCluster {
            reuseId = "Cluster"
            if annotation.isKind(of: MKUserLocation.self){
                return nil
            }else{
                var clusterView = map.dequeueReusableAnnotationView(withIdentifier: reuseId)
                if clusterView == nil {
                    clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId, configuration: FBAnnotationClusterViewConfiguration.default())
                } else {
                    clusterView?.annotation = annotation
                }
                return clusterView
            }
        } else {
            reuseId = "Pin"
            if annotation.isKind(of: MKUserLocation.self){
                return nil
            }else{
                var pinView = map.dequeueReusableAnnotationView(withIdentifier: reuseId) as? ImageAnnotationView
                
                
                let zoomlevel = mapView.zoomLevel
                
                StaticImageAnnotation.sharedInstance.resize(zoomlevel: mapView.zoomLevel)
                // let scale = Double(-1 * sqrt(Double(1 - pow((zoomlevel / 20.0), 2.0))) + 1.1)
                pinView = ImageAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                
                // let scalex = 0.04293158 * ( zoomlevel )
                
                
                for x in StaticImageAnnotation.sharedInstance.AImages {
                    if isEqual(withCoordinate: x.coordinate,withAnotherCoordinate: annotation.coordinate) {
                        pinView?.image1 = x.image1
                        pinView?.image2 = x.image2
                        pinView?.image4 = x.image4
                        
                        
                        pinView?.setimageview(scale: StaticImageAnnotation.sharedInstance.scale)
                    }
                }
                
                
                
                
                return pinView
            }
        }
    }
    /*
     *another scale function
     *
     */
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        //image.draw(in: CGRectMake(0, 0, newSize.width, newSize.height))
        image.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: newSize.width, height: newSize.height))  )
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.Pratique.HideTables()
        self.Niveau.HideTables()
        if First {
            self.Favoris.HideTables()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func isEqual(withCoordinate location1: CLLocationCoordinate2D, withAnotherCoordinate location2: CLLocationCoordinate2D) -> Bool {
        let locationString1 = "\(location1.latitude), \(location1.longitude)"
        let locationString2 = "\(location2.latitude), \(location2.longitude)"
        if (locationString1 == locationString2) {
            return true
        }
        else {
            return false
        }
    }
    /*
     *load all the favorites
     *
     */
    func LoadFavorite()
    {
        LoaderAlert.show(withStatus: "Chargement ...")
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.afficherFavoris , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                
                let b = JSON(response.data)
                
                
                if b["status"].boolValue {
                    LoaderAlert.dismiss()
                    if (b["message"].stringValue == "List vide")
                    {
                        print ( "vide")
                        
                    }
                    else {
                        for i in 0 ... ((b["data"].arrayObject?.count)! - 1)
                        {
                            if (b["data"][i]["type"].stringValue == "ami" )
                            {
                                self.mesAmis.appendIfArray(json: b["data"][i])
                            }
                            else {
                                self.mesTribu.appendIfArray(json: b["data"][i])
                            }
                            
                        }
                        print("self.mesAmis:",self.mesAmis)
                        print("self.mesTribu:",self.mesTribu)
                    }
                    
                    
                    
                    self.First = true
                    
                }
                else {
                    LoaderAlert.dismiss()
                    
                }
        }
    }
}


