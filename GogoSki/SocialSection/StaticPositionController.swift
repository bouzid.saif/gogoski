//
//  StaticPositionController.swift
//  GogoSki
//
//  Created by Bouzid saif on 19/01/2018.
//  Copyright © 2018 Velox-IT. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftyJSON
import Alamofire
class StaticPositionController : NSObject,CLLocationManagerDelegate  {
    /*
     *static var of this controller
     *
     */
    static var shared = StaticPositionController()
    /*
     *the location manager of the app
     *
     */
    var locationManager:  CLLocationManager
    override init() {
        
        locationManager = CLLocationManager()
    }
    /*
     *all users of the app
     *
     */
    var UsersPos = OrderedDictionary<String,JSON>()
    /*
     *if combine the user_id ad a key and his IndexPath in the list view controller
     *
     */
    var TablrePos = OrderedDictionary<String,IndexPath>()
    var ListeNoir : [String] = []
    /*
     *to know when the the location manager did update position at the first time
     *
     */
    var first = true
    /*
     *to know if UserPos was filled or not
     *
     */
    var count = -1
    /*
     *the location at n moment of the user
     *
     */
    var locationUser = CLLocationCoordinate2D()
    /*
     *the default practice of the user
     *
     */
    var practice = ""
    /*
     *the default level of the user
     *
     */
    var niveau = ""
    /*
     *to verify the blacklist completed task
     *
     */
    var VerifListeNoir = false
    func fetchBlacklist(){
        ListeNoir = []
        let ab = UserDefaults.standard.value(forKey: "UserBlocked") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        if a.arrayObject?.count != 0 {
            for i in 0 ... (a.arrayObject?.count)! - 1 {
                ListeNoir.append(a[i]["i"].stringValue)
            }
        }
    }
    /*
     *preapare the notfications listeners of blacklist
     *
     */
    func startNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.AddToBlackList(notification:)), name: NSNotification.Name(rawValue: "AddToBlackList"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.RemoveFromBlackList(notification:)), name: NSNotification.Name(rawValue: "RemoveFromBlackList"), object: nil)
        
    }
    func disableNotifications(){
        ListeNoir = []
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "AddToBlackList"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RemoveFromBlackList"), object: nil)
    }
    @objc func AddToBlackList(notification: NSNotification) {
        let notif = notification.object as? [String]
        self.ListeNoir.append(notif![0] as! String)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MapGetDone"), object: nil)
        print(ListeNoir)
    }
    @objc func RemoveFromBlackList(notification: NSNotification) {
        let notif = notification.object as? [String]
        print(notif![0])
        for i in 0 ... ListeNoir.count - 1 {
            if ListeNoir[i] ==  notif![0] as! String {
                ListeNoir.remove(at: i)
                print(ListeNoir)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MapGetDone"), object: nil)
                return
            }
        }
        
        
    }
    /*
     *preapare the connexion between the node and the  app
     *
     */
    func getData(completionHandler : @escaping ((Bool) -> Void)) {
        self.locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            ///houni nejbed awel mara
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            var photo = ""
            if (a["user"]["photo"].stringValue).prefix(1).elementsEqual("h"){
                photo = a["user"]["photo"].stringValue
            }else {
                photo = "default"
            }
            
            locationManager.startUpdatingLocation()
            //locationManager.startMonitoringSignificantLocationChanges()
            completionHandler(true)
        }else {
            completionHandler(false)
        }
        
    }
    
    /*
     *to verify if UserPos did finish filling
     *
     */
    func verifData(completionHandler : @escaping ((Bool) -> Void)) {
        completionHandler(UsersPos.count >= 0)
        
    }
    /*
     *to stop the location manager at any time
     *
     */
    func StopLocation(){
        self.locationManager.stopUpdatingLocation()
    }
    /*
     *at first user, connect with the map server and then update the user data
     *
     */
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue : CLLocationCoordinate2D = (manager.location?.coordinate)!
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        var photo = ""
        if (a["user"]["photo"].stringValue).prefix(1).elementsEqual("h"){
            photo = a["user"]["photo"].stringValue
        }else {
            photo = "default"
        }
       
        locationUser = locValue
        if first {
            
            SocketIOManager.sharedInstance.connectToMapServer(id_user: a["user"]["id"].stringValue, nom: a["user"]["nom"].stringValue, prenom: a["user"]["prenom"].stringValue, niveau: (self.niveau == "") ? a["user"]["sports"][0]["niveau"].stringValue : self.niveau, pratique: (self.practice == "")  ? a["user"]["sports"][0]["pratique"].stringValue : self.practice,photo: photo,latitude : String(locValue.latitude) ,longitude :String(locValue.longitude) ,  completionHandler: { (verif) in
                
                SocketIOManager.sharedInstance.listenAllUser()
                SocketIOManager.sharedInstance.listenNewUser()
                SocketIOManager.sharedInstance.listenUserUpdatePosition()
                SocketIOManager.sharedInstance.listenRemoveUser()
                
                
            })
            first = false
        }else {
            SocketIOManager.sharedInstance.updatePosition(id_user: a["user"]["id"].stringValue, nom: a["user"]["nom"].stringValue, prenom: a["user"]["prenom"].stringValue, niveau: (self.niveau == "") ? a["user"]["sports"][0]["niveau"].stringValue : self.niveau, pratique: (self.practice == "")  ? a["user"]["sports"][0]["pratique"].stringValue : self.practice, latitude: String(locValue.latitude), longitude: String(locValue.longitude), photo: photo)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NewDistanceUsers"), object: nil)
        }
        //  print("working")
        //houni nab3ath
    }
    
    
    
    
}
