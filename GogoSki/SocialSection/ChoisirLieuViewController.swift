//
//  ChoisirLieuViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 27/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import MapKit
import SwiftyJSON
class ChoisirLieuViewController: InternetCheckViewControllers,MKMapViewDelegate {
    /*
     *the station name
     *
     */
    @IBOutlet weak var nom_station: UILabel!
    /*
     *the station image
     *
     */
    @IBOutlet weak var image_station: RoundedUIImageView!
    /*
     *accept/decline view
     *
     */
    @IBOutlet weak var accpetRefuseView: UIView!
    @IBOutlet weak var map: MKMapView!
    /*
     *decline button
     *
     */
    @IBOutlet weak var btnAnnuler: UIButton!
    /*
     *accept button
     *
     */
    @IBOutlet weak var btnAccepter: UIButton!
    /*
     *title of the view
     *
     */
    @IBOutlet weak var titre: UILabel!
    /*
     *latitude received from the preview view
     *
     */
    var latitude  = ""
    /*
     *longitude received from the preview view
     *
     */
    var longitiude = ""
    /*
     *station title received from the preview view
     *
     */
    var titreSt = ""
    /*
     *massif received from the preview view
     *
     */
    var massif = ""
    /*
     *department received from the preview view
     *
     */
    var depart = ""
    /*
     *All Apointement available
     *
     */
    var RDVApointment : JSON = []
    /*
     *id of the apointment selected
     *
     */
    var selectedPoint : String = ""
    /*
     *id of the apointment place received from the preview view
     *
     */
    var id = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        map.delegate = self
        configureTileOverlay()
        accpetRefuseView.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDisconnectedUserUpdateNotification(notification:)), name: NSNotification.Name(rawValue: "PinView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        print("RDVVV:",RDVApointment)
        // Do any additional setup after loading the view.
        if RDVApointment.count != 0 {
            for i in 0 ... RDVApointment.count - 1 {
                let Report = ImageAnnotation()
                longitiude = String(longitiude.dropFirst())
                Report.coordinate = CLLocationCoordinate2D(latitude: Double(RDVApointment[i]["latitude"].stringValue)!, longitude: Double(RDVApointment[i]["longitude"].stringValue.dropFirst())!)
                Report.image1 = RDVApointment[i]["photo"].stringValue
                Report.nom_etab = RDVApointment[i]["prestataire"]["nom_etablissement"].stringValue
                Report.id_point = RDVApointment[i]["id"].stringValue
                //nom_station.text = Localization("nomEtab") + RDVApointment[i]["nomEtab"].stringValue
                //nom_station.text = "Massif : " + massif + "\n"
                //nom_station.text = nom_station.text! + "Department : " + depart + "-" + titreSt
                StaticImageAnnotationRDV.sharedInstance.add(im:Report)
                map.addAnnotation(Report)
            }
        }
        /* let Report = ImageAnnotation()
         longitiude = String(longitiude.dropFirst())
         Report.coordinate = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!)
         Report.image1 = "default2"
         nom_station.text = "Massif : " + massif + "\n"
         nom_station.text = nom_station.text! + "Department : " + depart + "-" + titreSt
         StaticImageAnnotationRDV.sharedInstance.add(im:Report)
         map.addAnnotation(Report) */
        /* for annotation in map.annotations {
         
         if annotation.isKind(of: MKPointAnnotation.self) {
         let pinView : ImageAnnotationViewRDV = map.view(for: annotation) as! ImageAnnotationView
         formatAnnotationView(pinView, for: map)
         
         }
         } */
        
        // map.zoomIn(coordinate: CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!))
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let tileOverlay = overlay as? MKTileOverlay {
            return MKTileOverlayRenderer(tileOverlay: tileOverlay)
        } else {
            return MKOverlayRenderer(overlay: overlay)
        }
    }
    private func configureTileOverlay() {
        // We first need to have the path of the overlay configuration JSON
        guard let overlayFileURLString = Bundle.main.path(forResource: "Overlay", ofType: "json") else {
            return
        }
        let overlayFileURL = URL(fileURLWithPath: overlayFileURLString)
        
        // After that, you can create the tile overlay using MapKitGoogleStyler
        guard let tileOverlay = try? MapKitGoogleStyler.buildOverlay(with: overlayFileURL) else {
            return
        }
        
        // And finally add it to your MKMapView
        map.add(tileOverlay)
    }
    /*
     *to put a cutom annotaion on the map
     *
     */
    @objc func handleDisconnectedUserUpdateNotification(notification: NSNotification){
        let data = notification.object as! [String]
        nom_station.text = Localization("nomEtab") + data[1]
        if data[2].first == "h" {
            image_station.setImage(withUrl: URL(string: data[2])!)
        }else {
            image_station.image = UIImage(named: "station")
        }
        
        self.selectedPoint = data[0]
        //let coord = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!)
        //let annot = Artwork(title: "", locationName: "", discipline: "", coordinate: coord,color: UIColor.blue)
        
        //  self.map.addAnnotation(annot)
        self.accpetRefuseView.isHidden = false
    }
    /*
     *configure the custom annotation
     *
     */
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        
        
        if annotation.isKind(of: MKUserLocation.self){
            return nil
        }else if annotation is ImageAnnotation {
            reuseId = "Pin"
            var pinView = map.dequeueReusableAnnotationView(withIdentifier: reuseId) as? ImageAnnotationViewRDV
            
            
            pinView = ImageAnnotationViewRDV(annotation: annotation, reuseIdentifier: reuseId)
            /*  switch now_image  {
             case 0 : pinView?.image = UIImage(named:"exemple")
             break
             case  1 : pinView?.image = UIImage(named:"aymen")
             break
             default : pinView?.image = UIImage(named:"haddar")
             break
             } */
            for x in StaticImageAnnotationRDV.sharedInstance.AImages {
                if isEqual(withCoordinate: x.coordinate,withAnotherCoordinate: annotation.coordinate) {
                    pinView?.image1 = x.image1
                    pinView?.id_point = x.id_point!
                    pinView?.nom_etab = x.nom_etab!
                    pinView?.setimageview()
                }
            }
            
            
            
            
            return pinView
            
        }else {
            reuseId = "Pin2"
            let saifPin =  MKAnnotationView(annotation: annotation, reuseIdentifier: "PINA")
            saifPin.isEnabled = true
            saifPin.canShowCallout = true
            
            saifPin.image = UIImage(named: "Pin_saif")
            
            // saifPin.tintColor = MKAnnotationView.blueColor()
            // saifPin.tintAdjustmentMode = .normal
            return saifPin
        }
        
    }
    func isEqual(withCoordinate location1: CLLocationCoordinate2D, withAnotherCoordinate location2: CLLocationCoordinate2D) -> Bool {
        let locationString1 = "\(location1.latitude), \(location1.longitude)"
        let locationString2 = "\(location2.latitude), \(location2.longitude)"
        if (locationString1 == locationString2) {
            return true
        }
        else {
            return false
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     *accept action
     *
     */
    @IBAction func AcceptAction(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SetLieu"), object: self.selectedPoint)
        self.dismiss(animated: true, completion: nil)
    }
    /*
     *configure 2/2 of the custom annotation
     *
     */
    func formatAnnotationView(_ pinView: ImageAnnotationView, for aMapView: MKMapView) {
        if pinView != nil {
            
            let zoomLevel: Double = aMapView.zoomLevel
            let scale = Double(-1 * sqrt(Double(1 - pow((zoomLevel / 20.0), 2.0))) + 1.1)
            // This is a circular scale function where at zoom level 0 scale is 0.1 and at zoom level 20 scale is 1.1
            // Option #1
            pinView.transform = CGAffineTransform(scaleX: CGFloat(scale), y: CGFloat(scale))
            // Option #2
            let pinImage = UIImage(named: "exemple")
            let pinImage2 = UIImage(named: "skii")
            /* pinView.image = resizeImage(image: pinImage!,targetSize: CGSize(width: Double((pinImage?.size.width)!) * scale, height:  Double((pinImage?.size.height)!) * scale)) */
            /*pinView.image2 = resizeImage(image: pinImage2!,targetSize: CGSize(width: Double(((pinImage?.size.width)! / 3)) * scale, height:  Double(((pinImage?.size.height)! / 3)) * scale)) */
        }
    }
    /*
     *decline action
     *
     */
    @IBAction func RefuseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
     *back action
     *
     */
    @IBAction func Back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     *language config
     *
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("ChoisirLieuTitre")
        btnAccepter.setTitle(Localization("BtnAccepter"), for: .normal)
        btnAnnuler.setTitle(Localization("BtnAnnuler"), for: .normal)
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

