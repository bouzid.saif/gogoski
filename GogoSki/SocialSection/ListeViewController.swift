//
//  ListeViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 16/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import Foundation
import MapKit
import SwiftyJSON
import Alamofire
import AlamofireImage
extension String {
    public func indexOfCharacter(char: Character) -> Int? {
        if let idx = self.index(of: char) {
            return distance(from: self.startIndex, to: idx)
        }
        return nil
    }
}
extension UITableView {
    func refreshTable() {
        let indexPathForSection = NSIndexSet(index: 0)
        UIView.performWithoutAnimation {
            self.reloadSections(indexPathForSection as IndexSet, with: .none)
        }
        
    }
}
struct MapStruct {
    var idUser : String
    var coordinate : CLLocationCoordinate2D
    var username : String
    var url_img : String
    var distance : Double
    var unit : String
    var pratique : String
    var niveau : String
}
struct Platform {
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
    
}
class ListeViewController: InternetCheckViewControllers , UITableViewDataSource , UITableViewDelegate {
    /*
     *the parent view navigation controller
     *
     */
    var parentNavigationController : UINavigationController?
    
    /*
     *to know if this is the first load of the view
     *
     */
    var first = true
    /*
     *to know if loading the stations did done
     *
     */
    var StationEnabled = false
    /*
     *level in french
     *
     */
    let NiveauSaifFR = ["Niveau","Débrouillard","Débutant","Intermédiaire","Confirmé","Expert"]
    /*
     *level in english
     *
     */
    let NiveauSaifEN = ["Level","Resourceful","Beginner","Intermediate","Confirmed","Expert"]
    /*
     *practice in french
     *
     */
    let SportSaifFR = ["Pratique","Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     *practice in english
     *
     */
    let SportSaifEN = ["Practice","Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross_country skiing"]
    /*
     *stations data
     *
     */
    var stations : JSON = []
    @IBOutlet weak var table: UITableView!
    /*
     *to know if all cell has been added
     *
     */
    var finishCells = false
    /*
     *custom object of the list view controller data source
     *
     */
    var map_table2 : [MapStruct] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        table.isUserInteractionEnabled = false
        table.delegate = self
        table.dataSource = self
        table.allowsMultipleSelection = false
        /*coord = FIRDatabase.database().reference().child("map").queryOrderedByKey()
         self.locationManager.requestAlwaysAuthorization()
         self.locationManager.requestWhenInUseAuthorization() */
        NotificationCenter.default.addObserver(self, selector: #selector(self.DeleteNotifVar(notification:)), name: NSNotification.Name(rawValue: "SkaerNotif"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeDistance(notification:)), name: NSNotification.Name(rawValue: "NewDistanceUsers"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeDistanceFromOtherUser(notification:)), name: NSNotification.Name(rawValue: "UserUpdateLocation"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.AddNewUser(notification:)), name: NSNotification.Name(rawValue: "NewUserJoined"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.RemoveUser(notification:)), name: NSNotification.Name(rawValue: "UserDeleted"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    func getDataStaticFromController(){
        StaticPositionController.shared.verifData { (verif) in
            if verif {
                print("verif:",StaticPositionController.shared.UsersPos.count)
                self.map_table2 = []
                if StaticPositionController.shared.UsersPos.count != 0 {
                    //let temp = StaticPositionController.shared.UsersPos
                    let results = StaticPositionController.shared.UsersPos
                    print("results:",results)
                    for (key2, value2) in results {
                        if StaticPositionController.shared.ListeNoir.contains(key2) == false {
                            print("key2 \(key2) value3 \(value2)")
                            let distance = self.calculeDistanceFromOtherUser(location: CLLocationCoordinate2D(latitude: Double(value2["latitude"].stringValue)!, longitude: Double( value2["longitude"].stringValue)!))
                            self.map_table2.append(MapStruct(idUser: value2["user_id"].stringValue, coordinate: CLLocationCoordinate2D(latitude: Double(value2["latitude"].stringValue)!, longitude: Double( value2["longitude"].stringValue)!), username:  value2["prenom"].stringValue + " " +  value2["nom"].stringValue, url_img:  value2["photo"].stringValue, distance: distance, unit: "m", pratique:  value2["pratique"].stringValue, niveau:  value2["niveau"].stringValue))
                        }
                    }
                    self.map_table2 =  self.map_table2.sorted(by: { (a:MapStruct, b:MapStruct) -> Bool in
                       return b.distance >= a.distance
                    })
                    self.table.reloadData()
                    
                }
            }else {
                if StaticPositionController.shared.count == -1 {
                    self.getDataStaticFromController()
                }
            }
        }
    }
    /*
     *notification received when the actual user location did change
     *
     */
    @objc func changeDistance(notification:NSNotification) {
        if finishCells {
            if self.map_table2.count != 0 {
                
                for i in 0 ... self.map_table2.count - 1 {
                    
                    let distance = self.calculeDistanceFromOtherUser(location: CLLocationCoordinate2D(latitude: Double(self.map_table2[i].coordinate.latitude), longitude: Double( self.map_table2[i].coordinate.longitude)))
                    if self.map_table2[i].distance != distance {
                        self.table.beginUpdates()
                        self.table.reloadRows(at: [IndexPath(row: i, section: 0)], with: .none)
                        self.table.endUpdates()
                    }
                    
                }
            }
        }
    }
    /*
     *notification received when new user has been added
     *
     */
    @objc func AddNewUser(notification:NSNotification) {
        if finishCells {
            let id = notification.object as! String
            var i = 0
            if self.map_table2.count != 0 {
                i = self.map_table2.count
            }else{
                i = 0
            }
            
            self.map_table2[i].coordinate = CLLocationCoordinate2D(latitude: Double(StaticPositionController.shared.UsersPos[id]!["latitude"].stringValue)!, longitude: Double( StaticPositionController.shared.UsersPos[id]!["longitude"].stringValue)!)
            self.map_table2[i].distance = self.calculeDistanceFromOtherUser(location: CLLocationCoordinate2D(latitude: Double(StaticPositionController.shared.UsersPos[id]!["latitude"].stringValue)!, longitude: Double( StaticPositionController.shared.UsersPos[id]!["longitude"].stringValue)!))
            self.map_table2[i].niveau = StaticPositionController.shared.UsersPos[id]!["niveau"].stringValue
            self.map_table2[i].pratique = StaticPositionController.shared.UsersPos[id]!["pratique"].stringValue
            self.map_table2[i].url_img = StaticPositionController.shared.UsersPos[id]!["photo"].stringValue
            self.map_table2[i].username = StaticPositionController.shared.UsersPos[id]!["prenom"].stringValue + " " + StaticPositionController.shared.UsersPos[id]!["nom"].stringValue
            self.map_table2[i].unit = "m"
            self.map_table2 =  self.map_table2.sorted(by: { (a:MapStruct, b:MapStruct) -> Bool in
                return b.distance >= a.distance
            })
            self.table.beginUpdates()
            self.table.insertRows(at: [IndexPath(row: i, section: 0)], with: .none)
            self.table.endUpdates()
        }
        
    }
    /*
     *notification received when another user location did change
     *
     */
    @objc func changeDistanceFromOtherUser(notification:NSNotification) {
        if finishCells {
            let id = notification.object as! String
            if self.map_table2.count != 0 {
                var x = 0
                for i in 0 ... self.map_table2.count - 1 {
                    
                    if self.map_table2[i].idUser == id {
                        x = i
                        self.map_table2[i].coordinate = CLLocationCoordinate2D(latitude: Double(StaticPositionController.shared.UsersPos[id]!["latitude"].stringValue)!, longitude: Double( StaticPositionController.shared.UsersPos[id]!["longitude"].stringValue)!)
                        self.map_table2[i].distance = self.calculeDistanceFromOtherUser(location: CLLocationCoordinate2D(latitude: Double(StaticPositionController.shared.UsersPos[id]!["latitude"].stringValue)!, longitude: Double( StaticPositionController.shared.UsersPos[id]!["longitude"].stringValue)!))
                        self.map_table2[i].niveau = StaticPositionController.shared.UsersPos[id]!["niveau"].stringValue
                        self.map_table2[i].pratique = StaticPositionController.shared.UsersPos[id]!["pratique"].stringValue
                        self.map_table2[i].url_img = StaticPositionController.shared.UsersPos[id]!["photo"].stringValue
                        self.map_table2[i].username = StaticPositionController.shared.UsersPos[id]!["prenom"].stringValue + " " + StaticPositionController.shared.UsersPos[id]!["nom"].stringValue
                        
                        break
                    }
                    
                }
                self.map_table2 =  self.map_table2.sorted(by: { (a:MapStruct, b:MapStruct) -> Bool in
                    return b.distance >= a.distance
                })
                //self.table.beginUpdates()
               // self.table.reloadRows(at: [IndexPath(row: x, section: 0)], with: .fade)
                //self.table.endUpdates()
                UIView.performWithoutAnimation {
                     self.table.reloadData()
                }
               
                
            }
        }
    }
    @objc func RemoveUser(notification:NSNotification) {
        if finishCells {
            var i = -1
            let id = notification.object as! String
            if self.map_table2.count != 0 {
                
                for j in 0 ... self.map_table2.count - 1 {
                    if self.map_table2[j].idUser == id {
                        i = j
                    }
                }
                if i != -1 {
                    self.map_table2.remove(at: i)
                    self.table.beginUpdates()
                    self.table.deleteRows(at: [IndexPath(row: i, section: 0)], with: .none)
                    self.table.endUpdates()
                }
                
            }
        }
    }
    
    /*
     *to know if a push notif has been received
     *
     */
    @objc func DeleteNotifVar(notification:NSNotification) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.NotifReceive =  false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("appear")
        LoadAllStation()
        print("mapsaif:",map_table2)
        getDataStaticFromController()
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("map:",map_table.count)
        return map_table2.count
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            let img = cell.viewWithTag(30) as! UIImageView
            img.contentMode = .scaleAspectFill
            let radius : CGFloat = img.bounds.size.width / 2
            img.layer.cornerRadius = radius
        }
        if indexPath.row == self.map_table2.count - 1 {
            self.finishCells = true
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "liste", for: indexPath)
        UIView.performWithoutAnimation {
        let labelName : UILabel = cell.viewWithTag(1) as! UILabel
        labelName.text = map_table2[indexPath.row].username
        let labelDistance : UILabel = cell.viewWithTag(4) as! UILabel
        
        if ( map_table2[indexPath.row].distance > 1000 )
        {
            labelDistance.text = String(format: "%.2f",Measurement(value: map_table2[indexPath.row].distance / 1000 , unit: UnitLength.kilometers).value) + " km"
        }
        else
        {
            
            labelDistance.text = String(Int(map_table2[indexPath.row].distance.truncatingRemainder(dividingBy: 1000))) + " m"
        }
        let img : UIImageView = cell.viewWithTag(30) as! UIImageView
        
        let placeholder =  UIImage(named: "user_placeholder.png")
        
        img.image = placeholder
        if  map_table2[indexPath.row].url_img.hasPrefix("h") {
            // img.setImage(withUrl: urlImgUser , placeholder: placeholder,cacheScaled: true )
            
            /*ImageCache.sharedInstance.findOrLoadAsync(imageUrl: map_table[indexPath.row].url_img, completionHandler: { (image) -> Void in
             // do something with image (UIImage)
             
             img.image = image?.circleMasked
             
             }) */
            let URL = NSURL(string: map_table2[indexPath.row].url_img)
            img.setImage(withUrl: URL! as URL, placeholder: placeholder,cacheScaled: true)
            // let AlamofireSourcex = AlamofireSource(url: urlImgUser, placeholder: placeholder)
            //  AlamofireSourcex.load(to: img, with: { (image) in
            
            
            //  })
            
            /* img.af_setImage(withURL: URL(string : map_table[indexPath.row].url_img)!, placeholderImage: placeholder, filter: nil, progress: nil) { (response) in
             
             
             } */
            
            //
            // })
        }
        
        
        let labelSport : UILabel = cell.viewWithTag(2) as! UILabel
        //let labelNiveau : UILabel = cell.viewWithTag(3) as! UILabel
        if Localisator.sharedInstance.currentLanguage == "French"  || Localisator.sharedInstance.currentLanguage == "Français"  || Localisator.sharedInstance.currentLanguage == "French_fr" {
            labelSport.text = map_table2[indexPath.row].pratique + " - " + map_table2[indexPath.row].niveau
            
            //labelNiveau.text =
        }else {
            switch map_table2[indexPath.row].pratique {
            case SportSaifFR[1] :
                labelSport.text = SportSaifEN[1]
            case SportSaifFR[2] :
                labelSport.text = SportSaifEN[2]
            case SportSaifFR[3] :
                labelSport.text = SportSaifEN[3]
            case SportSaifFR[4] :
                labelSport.text = SportSaifEN[4]
            case SportSaifFR[5] :
                labelSport.text = SportSaifEN[5]
            case SportSaifFR[6] :
                labelSport.text = SportSaifEN[6]
            default :
                labelSport.text = ""
            }
            switch map_table2[indexPath.row].niveau {
            case NiveauSaifFR[1] :
                labelSport.text = labelSport.text! + " - " + NiveauSaifEN[1]
            case NiveauSaifFR[2] :
                labelSport.text = labelSport.text! + " - " + NiveauSaifEN[2]
            case NiveauSaifFR[3] :
                labelSport.text = labelSport.text! + " - " + NiveauSaifEN[3]
            case NiveauSaifFR[4] :
                labelSport.text = labelSport.text! + " - " + NiveauSaifEN[4]
            case NiveauSaifFR[5] :
                labelSport.text = labelSport.text! + " - " + NiveauSaifEN[5]
            default :
                labelSport.text = ""
                
            }
        }
            var attributedString : NSMutableAttributedString = NSMutableAttributedString()
            
        
        if ( map_table2[indexPath.row].niveau == "Confirmé" )
        {
            attributedString = NSMutableAttributedString(string: labelSport.text!)
            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor("#FF3E53"), range: NSMakeRange((labelSport.text?.indexOfCharacter(char: "-"))!,((labelSport.text?.count)! - (labelSport.text?.indexOfCharacter(char: "-"))!) ))
            
        }
        else if (map_table2[indexPath.row].niveau == "Débutant")
        {
            attributedString = NSMutableAttributedString(string: labelSport.text!)
            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor("#D3D3D3"), range: NSMakeRange((labelSport.text?.indexOfCharacter(char: "-"))!,((labelSport.text?.count)! - (labelSport.text?.indexOfCharacter(char: "-"))!) ))
          
        }
        else if (map_table2[indexPath.row].niveau == "Intermédiaire")
        {
            attributedString = NSMutableAttributedString(string: labelSport.text!)
            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor("#75A7FF"), range: NSMakeRange((labelSport.text?.indexOfCharacter(char: "-"))!,((labelSport.text?.count)! - (labelSport.text?.indexOfCharacter(char: "-"))!) ))
           
        }
        else if (map_table2[indexPath.row].niveau == "Expert")
        {
            attributedString = NSMutableAttributedString(string: labelSport.text!)
            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor("#4D4D4D"), range: NSMakeRange((labelSport.text?.indexOfCharacter(char: "-"))!,((labelSport.text?.count)! - (labelSport.text?.indexOfCharacter(char: "-"))!) ))
            
        }
        else {
            attributedString = NSMutableAttributedString(string: labelSport.text!)
            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value:  UIColor("#76EC9E"), range: NSMakeRange((labelSport.text?.indexOfCharacter(char: "-"))!,((labelSport.text?.count)! - (labelSport.text?.indexOfCharacter(char: "-"))!) ))
          
        }
            labelSport.text = ""
            labelSport.attributedText = attributedString
        let btnMessages : CustomButton = cell.viewWithTag(10) as! CustomButton
        btnMessages.index = indexPath.row
        btnMessages.indexPath = indexPath
        btnMessages.addTarget(self, action: #selector(self.messageAction), for: .touchUpInside)
        
        let btnRdv : CustomButton = cell.viewWithTag(20) as! CustomButton
        btnRdv.index = indexPath.row
        btnRdv.indexPath = indexPath
        if (self.StationEnabled)
        {
            btnRdv.isEnabled = true
            btnRdv.addTarget(self, action: #selector(self.invitRdv), for: .touchUpInside)
        }
          }
        return cell
      
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // self.locationManager.stopUpdatingLocation()
        self.first = true
        self.table.reloadData()
        
    }
    
    func calculeDistanceFromOtherUser(location:CLLocationCoordinate2D) -> CLLocationDistance  {
        let user = StaticPositionController.shared.locationUser
        let meters: CLLocationDistance = CLLocation(latitude: location.latitude, longitude: location.longitude).distance(from: CLLocation(latitude: user.latitude, longitude: user.longitude))
        return meters
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // print ("tenzel 3la ami")
        let view = self.storyboard?.instantiateViewController(withIdentifier: "visiteur_profil") as! visiteur_profil
        view.id = self.map_table2[indexPath.row].idUser
        view.parentNavigationController = self.parentNavigationController
        parentNavigationController?.pushViewController(view, animated: true)
    }
    /*
     *message button for each user
     *
     */
    @objc func messageAction(sender : CustomButton) {
        LoaderAlert.show()
        let params: Parameters = [
            "amiId": self.map_table2[sender.index!].idUser
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.addConvPriv , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                // print ("add conv "  , b)
                if b["status"].boolValue {
                    
                    let ConvId = b["data"]["id"].stringValue
                    SocketIOManager.sharedInstance.connectToServerWithNickname(nickname: ConvId, userid: a["user"]["id"].stringValue, first: false)
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.NotifAhmed =  true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToMessageForomNoWhere2"), object: ConvId )
                }
                
                
        }
    }
    /*
     *load all the stations
     *
     */
    func LoadAllStation ()
    {
        LoaderAlert.show()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.Stations , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                let b = JSON(response.data)
                
                if (b["status"].boolValue)
                {
                    
                    self.stations = b["data"]
                    self.StationEnabled = true
                    self.table.isUserInteractionEnabled = true
                }
                else {
                    //print ("nooooo")
                }
                
                
        }
    }
    /*
     *invite button for each user
     *
     */
    @objc func invitRdv(sender : CustomButton) {
        // print ("tenzel 3la rdv")
        if StationEnabled {
            let view = self.storyboard?.instantiateViewController(withIdentifier: "InviterRdvViewController") as! InviterRdvViewController
            view.idUser = self.map_table2[sender.index!].idUser
            //print("HedhaListView")
            view.stations = self.stations
            parentNavigationController?.pushViewController(view, animated: true)
        }
    }
    
    
}

