//
//  InviterMemberViewController.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 16/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit

class InviterMemberViewController: InternetCheckViewControllers ,MKMapViewDelegate{
    
    @IBOutlet weak var map: MKMapView!
    /*
     *validate button
     *
     */
    @IBOutlet weak var btnValider: UIButton!
    /*
     *revert button
     *
     */
    @IBOutlet weak var btnAnnuler: UIButton!
    /*
     *title of the view
     *
     */
    @IBOutlet weak var titre: UILabel!
    /*
     *station name
     *
     */
    @IBOutlet weak var nomStation: UILabel!
    /*
     *validate/dissmiss view
     *
     */
    @IBOutlet weak var validerAnnulerView: UIView!
    /*
     *id_user received from the preview view
     *
     */
    var idUser = ""
    /*
     *hangout data
     *
     */
    var sortie : JSON = []
    /*
     *to know what view did called this controller
     *
     */
    var chkoun = ""
    /*
     *latitude recevied from the preview view s
     *
     */
    var latitude  = ""
    /*
     *longitude recevied from the preview view s
     *
     */
    var longitiude = ""
    override func viewDidLoad() {
        super.viewDidLoad()
       // getSortie()
       self.nomStation.numberOfLines = 3
        self.nomStation.font = UIFont.systemFont(ofSize: 15.0)
        if chkoun != "" {
            self.longitiude = sortie["point"]["longitude"].stringValue
            self.latitude = sortie["point"]["latitude"].stringValue
            self.nomStation.text = "Massif : " + sortie["point"]["station"]["massif"].stringValue + "\n"
            self.nomStation.text =  self.nomStation.text! + "Department : " + sortie["point"]["station"]["dept"].stringValue + "-" + sortie["point"]["station"]["nom"].stringValue
        
        }else {
            self.longitiude = sortie["sortie"]["point"]["longitude"].stringValue
            self.latitude = sortie["sortie"]["point"]["latitude"].stringValue
            self.nomStation.text = "Massif : " + sortie["sortie"]["massif"].stringValue + "\n"
            self.nomStation.text =  self.nomStation.text! + "Department : " + sortie["sortie"]["departement"].stringValue + "-" + sortie["sortie"]["station"].stringValue
        }
        map.delegate = self
        validerAnnulerView.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDisconnectedUserUpdateNotification(notification:)), name: NSNotification.Name(rawValue: "PinView"), object: nil)
        let Report = ImageAnnotation()
        if longitiude.prefix(1) != " " {
        longitiude = String(longitiude.dropFirst())
        }
        print ("lon //// " , longitiude)
        Report.coordinate = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!)
        Report.image1 = "default2"
        StaticImageAnnotationRDV.sharedInstance.add(im:Report)
        map.addAnnotation(Report)
        for annotation in map.annotations {
            
            if annotation.isKind(of: MKPointAnnotation.self) {
                let pinView : ImageAnnotationView = map.view(for: annotation) as! ImageAnnotationView
                formatAnnotationView(pinView, for: map)
                
            }
        }
        map.zoomIn(coordinate: CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!))
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        
        // Do any additional setup after loading the view.
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let tileOverlay = overlay as? MKTileOverlay {
            return MKTileOverlayRenderer(tileOverlay: tileOverlay)
        } else {
            return MKOverlayRenderer(overlay: overlay)
        }
    }
    /*
     *to put a cutom annotaion on the map
     *
     */
    @objc func handleDisconnectedUserUpdateNotification(notification: NSNotification){
        let coord = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitiude)!)
        let annot = Artwork(title: "", locationName: "", discipline: "", coordinate: coord,color: UIColor.blue)
        
        self.map.addAnnotation(annot)
        self.validerAnnulerView.isHidden = false
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        
        
        if annotation.isKind(of: MKUserLocation.self){
            return nil
        }else if annotation is ImageAnnotation {
            reuseId = "Pin"
            var pinView = map.dequeueReusableAnnotationView(withIdentifier: reuseId) as? ImageAnnotationViewRDV
            
            
            pinView = ImageAnnotationViewRDV(annotation: annotation, reuseIdentifier: reuseId)
            /*  switch now_image  {
             case 0 : pinView?.image = UIImage(named:"exemple")
             break
             case  1 : pinView?.image = UIImage(named:"aymen")
             break
             default : pinView?.image = UIImage(named:"haddar")
             break
             } */
            for x in StaticImageAnnotationRDV.sharedInstance.AImages {
                if isEqual(withCoordinate: x.coordinate,withAnotherCoordinate: annotation.coordinate) {
                    pinView?.image1 = x.image1
                    pinView?.setimageview()
                }
            }
            
            
            
            
            return pinView
            
        }else {
            reuseId = "Pin2"
            let saifPin =  MKAnnotationView(annotation: annotation, reuseIdentifier: "PINA")
            saifPin.isEnabled = true
            saifPin.canShowCallout = true
            
            saifPin.image = UIImage(named: "Pin_saif")
            
            // saifPin.tintColor = MKAnnotationView.blueColor()
            // saifPin.tintAdjustmentMode = .normal
            return saifPin
        }
        
    }
    func isEqual(withCoordinate location1: CLLocationCoordinate2D, withAnotherCoordinate location2: CLLocationCoordinate2D) -> Bool {
        let locationString1 = "\(location1.latitude), \(location1.longitude)"
        let locationString2 = "\(location2.latitude), \(location2.longitude)"
        if (locationString1 == locationString2) {
            return true
        }
        else {
            return false
        }
    }
    /*
     *not in use yet
     *
     */
    func formatAnnotationView(_ pinView: ImageAnnotationView, for aMapView: MKMapView) {
        if pinView != nil {
            
            let zoomLevel: Double = aMapView.zoomLevel
            let scale = Double(-1 * sqrt(Double(1 - pow((zoomLevel / 20.0), 2.0))) + 1.1)
            // This is a circular scale function where at zoom level 0 scale is 0.1 and at zoom level 20 scale is 1.1
            // Option #1
            pinView.transform = CGAffineTransform(scaleX: CGFloat(scale), y: CGFloat(scale))
            // Option #2
            let pinImage = UIImage(named: "exemple")
            let pinImage2 = UIImage(named: "skii")
            /* pinView.image = resizeImage(image: pinImage!,targetSize: CGSize(width: Double((pinImage?.size.width)!) * scale, height:  Double((pinImage?.size.height)!) * scale)) */
            /*pinView.image2 = resizeImage(image: pinImage2!,targetSize: CGSize(width: Double(((pinImage?.size.width)! / 3)) * scale, height:  Double(((pinImage?.size.height)! / 3)) * scale)) */
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     *back action
     *
     */
    @IBAction func back(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    /*
     *configure texts
     *
     */
    func configureViewFromLocalisation() {
        titre.text = Localization("InviterMembreTitre")
        btnValider.setTitle(Localization("btnValider"), for: .normal)
        btnAnnuler.setTitle(Localization("BtnAnnuler"), for: .normal)
        
        //Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /*
     *validate action
     *
     */
    @IBAction func validerInvtation(_ sender: Any) {
        
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        var params: Parameters = [:]
         if chkoun != "" {
        params = [
            "idSortie": self.sortie["id"].stringValue ,
            "idParticipant" : self.idUser
        ]
        }
         else {
            params = [
                "idSortie": self.sortie["sortie"]["id"].stringValue ,
                "idParticipant" : self.idUser
            ]
        }
        print ("***********" , params)
        Alamofire.request(ScriptBase.sharedInstance.invitParticipant , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                
                let b = JSON(response.data)
                print ("invite friends : " , response)
                if b["status"].boolValue {
                    if ( b["message"].stringValue == "user already participating")
                    {
                        _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("alreadyUser"), style: AlertStyle.error)
                    }
                    else {
                        _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("invit"), style: AlertStyle.success , buttonTitle: "OK" , action: { (otherButton) in
                            let ab = UserDefaults.standard.value(forKey: "User") as! String
                            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                            var a = JSON(data: dataFromString!)
                            let header: HTTPHeaders = [
                                "Content-Type" : "application/json",
                                "x-Auth-Token" : a["value"].stringValue
                            ]
                            
                            let params: Parameters = [
                                "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " vous a invité a une sortie" ,
                                "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " invited you to join sortie" ,
                                "userids" : ["\(self.idUser)"] ,
                                "type" : "rdv" ,
                                "titre" : "Invitation" ,
                                "photo" : "default" ,
                                "activite" : self.sortie["sortie"]["id"].stringValue ,
                                "sender" : a["user"]["id"].stringValue ,
                                "rdv" : self.sortie["pointrdv"]["id"].stringValue
                            ]
                            Alamofire.request(ScriptBase.sharedInstance.creeNotif , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                                .responseJSON { response in
                                    print (response)
                            }
                            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                        })
                    }
                    
                    
                }else{
                    
                    _ = SweetAlert().showAlert(Localization("inviteFriends"), subTitle: Localization("erreur"), style: AlertStyle.error)
                    
                }
                
                
        }
        
    }
    /*
     *revert action
     *
     */
    @IBAction func annulerInvitation(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
   /* func getSortie () {
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        
        Alamofire.request(ScriptBase.sharedInstance.getSortieParId + self.idSortie  , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                print (response)
                let b = JSON(response.data)
                print ( "sortie :" , b["data"])
                if b["status"].boolValue {
                    
                    print ("c bon")
                    self.nomStation.text = "Massif : " + b["data"]["sortie"]["massif"].stringValue + "\n"
                    self.nomStation.text =  self.nomStation.text! + "Department : " + b["data"]["sortie"]["departement"].stringValue + "-" + b["data"]["sortie"]["station"].stringValue
                    self.longitiude = b["data"]["pointrdv"]["longitude"].stringValue
                    self.latitude = b["data"]["pointrdv"]["latitude"].stringValue
                    print ( self.longitiude , " ////// " , self.latitude)
                }
                else {
                    _ = SweetAlert().showAlert("Sortie", subTitle: Localization("erreur"), style: AlertStyle.error)
                }
        }
    }*/
    
}

