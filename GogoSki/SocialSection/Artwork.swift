//
//  Artwork.swift
//  Arislia
//
//  Created by Bouzid saif on 18/05/2017.
//  Copyright © 2017 Bouzid saif. All rights reserved.
//

import Foundation
import MapKit
import UIKit
/*
 *utility of the MKAnnotation used byt the map in the map
 *
 */
class Artwork: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let discipline: String
    let coordinate: CLLocationCoordinate2D
    let pinTintColor: UIColor
    init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D,color:UIColor) {
        self.title = title
        self.locationName = locationName
        self.discipline = discipline
        self.coordinate = coordinate
        self.pinTintColor = color
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}
