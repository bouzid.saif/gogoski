//
//  ConnexionUser.swift
//  GogoSki
//
//  Created by Bouzid saif on 21/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import FBSDKLoginKit
import Google
import GoogleSignIn
import LinkedinSwift
import WebKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
import OneSignal
class ConnexionUser : InternetCheckViewControllers, GIDSignInDelegate,GIDSignInUIDelegate,UIWebViewDelegate, OSSubscriptionObserver {
    /*
     * array of all languages available
     */
    //*** change langue
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    //****
    
    /*
     * title of this page
     */
    @IBOutlet weak var titre: UILabel!
    /*
     * or label
     */
    @IBOutlet weak var ou: UILabel!
    
    /*
     * password forgot button
     */
    @IBOutlet weak var btnMotDePasseOublié: UIButton!
    
    /*
     * email label
     */
    @IBOutlet weak var labelMail: UILabel!
    
    /*
     * connexion label
     */
    @IBOutlet weak var btnConnexion: UIButton!
    /*
     * password textfield
     */
    @IBOutlet weak var password: PaddingTextField!
    /*
     * email textfield
     */
    @IBOutlet weak var email: PaddingTextField!
    
    /*
     * function of google sign in
     */
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if user != nil {
            print(user.profile.email)
            guard let authentication = user.authentication else { return }
            authentication.idToken
            //let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
            // accessToken: authentication.accessToken)
            let params: Parameters = [
                "googleId": user.userID ,
                "email" : user.profile.email
            ]
            let header: HTTPHeaders = [
                "Content-Type" : "application/json"
            ]
            print("pramas:", params.description)
            Alamofire.request(ScriptBase.sharedInstance.ConnexionUserGoogle , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    LoaderAlert.shared.dismiss()
                    //   self.btnConnexion.isEnabled = true
                    let a = JSON(response.data)
                    print("saif:",a)
                    GIDSignIn.sharedInstance().signOut()
                    if a["status"].boolValue == false {
                        
                        
                        if a["message"].stringValue == "Email existe deja "{
                            let err = a["data"].stringValue
                            if err == "sql" {
                                _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionAutreMail") + "GogoSki", style: AlertStyle.error)
                            }else if err == "facebook" {
                                _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                            }else if err == "google" {
                                _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                            }else if err == "linkedin" {
                                _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                            }
                        } else {
                            _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionExistSaif"), style: AlertStyle.error)
                        }
                        
                        
                    }else{
                        
                        if (a["message"].stringValue == "Utlisateur existe deja" ) ||  (a["message"].stringValue == "Authentification successful")
                        {
                            if (a["data"]["user"]["enabled"].boolValue)
                            {
                                SocketIOManager.sharedInstance.establishConnection()
                                if (a["data"]["user"]["sports"].arrayObject?.count == 0)
                                {
                                    
                                    UserDefaults.standard.setValue("NO", forKey: "ModifOnce")
                                    UserDefaults.standard.synchronize()
                                    UserDefaults.standard.setValue(a["data"].rawString(), forKey: "User")
                                    UserDefaults.standard.setValue(a["Blocked"].rawString(), forKey: "UserBlocked")
                                    
                                    UserDefaults.standard.synchronize()
                                    let a = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "modifierprofilOnce") as! ModifierProfilUser
                                    a.Connex = "gm"
                                    self.navigationController?.pushViewController(a, animated: true)
                                    
                                }else{
                                    UserDefaults.standard.setValue("YES", forKey: "ModifOnce")
                                    UserDefaults.standard.synchronize()
                                    UserDefaults.standard.setValue(a["data"].rawString(), forKey: "User")
                                    UserDefaults.standard.setValue(a["Blocked"].rawString(), forKey: "UserBlocked")
                                    
                                    UserDefaults.standard.synchronize()
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userConnection"), object: nil)
                                    self.performSegue(withIdentifier: "connexion_home", sender: self)
                                }
                                
                            }else{
                                _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("ConnexionExist"), style: AlertStyle.warning)
                            }
                        }
                        
                    }
                    
            }
        }
    }
    /*
     * function of linkedin webview
     */
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url = request.url!
        print(url)
        
        if url.host == "com.appcoda.linkedin.oauth" {
            
            if url.absoluteString.range(of: "code") != nil {
                // Extract the authorization code.
                let urlParts = url.absoluteString.components(separatedBy: "?")
                let code = urlParts[1].components(separatedBy: "=")[1]
                
                requestForAccessToken(authorizationCode: code)
            }
        }
        
        return true
    }
    /*
     * function to initialize linkedin permission and tocken
     */
    func requestForAccessToken(authorizationCode: String) {
        let grantType = "authorization_code"
        let redirectURL = "https://com.appcoda.linkedin.oauth/oauth".addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        let linkedInKey = "86em0va3cx3dpw"
        let linkedInSecret = "ZEyvrZJPq61Rt2WX"
        let accessTokenEndPoint = "https://www.linkedin.com/uas/oauth2/accessToken"
        var postParams = "grant_type=\(grantType)&"
        postParams += "code=\(authorizationCode)&"
        postParams += "redirect_uri=\(redirectURL!)&"
        postParams += "client_id=\(linkedInKey)&"
        postParams += "client_secret=\(linkedInSecret)"
        let postData = postParams.data(using: String.Encoding.utf8)
        let request = NSMutableURLRequest(url: NSURL(string: accessTokenEndPoint)! as URL)
        // Indicate that we're about to make a POST request.
        request.httpMethod = "POST"
        
        // Set the HTTP body using the postData object created above.
        request.httpBody = postData
        request.addValue("application/x-www-form-urlencoded;", forHTTPHeaderField: "Content-Type")
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        // Make the request.
        let task: URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            let statusCode = (response as! HTTPURLResponse).statusCode
            
            if statusCode == 200 {
                // Convert the received JSON data into a dictionary.
                do {
                    let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String : Any]
                    
                    let accessToken = dataDictionary["access_token"] as! String
                    UserDefaults.standard.set(accessToken, forKey: "LIAccessToken")
                    UserDefaults.standard.synchronize()
                    self.getProfile()
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.webView.removeFromSuperview()
                        //self.dismiss(animated: true, completion: nil)
                    })
                }
                catch {
                    print("Could not convert JSON data into a dictionary.")
                }
            }
        }
        
        task.resume()
    }
    /*
     * get all profile information of linkedin
     */
    func getProfile(){
        if let accessToken = UserDefaults.standard.object(forKey: "LIAccessToken") {
            // Specify the URL string that we'll get the profile info from.
            let targetURLString = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,email-address)?format=json"
            let request = NSMutableURLRequest(url: NSURL(string: targetURLString)! as URL)
            // Indicate that we're about to make a POST request.
            request.httpMethod = "GET"
            
            // Set the HTTP body using the postData object created above.
            
            request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
            let session = URLSession(configuration: URLSessionConfiguration.default)
            let task: URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
                let statusCode = (response as! HTTPURLResponse).statusCode
                
                if statusCode == 200 {
                    // Convert the received JSON data into a dictionary.
                    do {
                        let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)  as! [String : Any]
                        print(dataDictionary)
                        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as? UITabBarController {
                            if let navigator = self.navigationController {
                                DispatchQueue.main.async(execute: {
                                    
                                    navigator.pushViewController(viewController, animated: true)
                                })
                                
                            }
                        }
                        //let profileURLString = dataDictionary["publicProfileUrl"] as! String
                        //print(profileURLString)
                    }
                    catch {
                        print("Could not convert JSON data into a dictionary.")
                    }
                }
                
            }
            
            task.resume()
            
            
        }
    }
    /*
     * check if there is access tocken of your linkedin or not
     */
    func checkForExistingAccessToken() {
        if UserDefaults.standard.object(forKey: "LIAccessToken") != nil {
            print("we got this")
        }
    }
    /*
     * variable contains information of your linkedin
     */
    private var linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "86em0va3cx3dpw", clientSecret: "ZEyvrZJPq61Rt2WX", state: "\(arc4random())", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://com.appcoda.linkedin.oauth/oauth"))
    /*
     * linkedin button
     */
    @IBOutlet weak var linkedin: UIButton!
    /*
     * gmail button
     */
    @IBOutlet weak var gmail: UIButton!
    /*
     * facebook button
     */
    @IBOutlet weak var facebook: UIButton!
    /*
     * initialize webview
     */
    var webView = UIWebView()
    /*
     * variable contains facebook information
     */
    var dict : [String : AnyObject]!
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().clientID = "876794620181-b7fs583arl17d0nf6gaju0fg0blhaj6t.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*
     * start linkedin authorization
     */
    func startAuthorization() {
        let linkedInKey = "86em0va3cx3dpw"
        let linkedInSecret = "ZEyvrZJPq61Rt2WX"
        let authorizationEndPoint = "https://www.linkedin.com/uas/oauth2/authorization"
        let accessTokenEndPoint = "https://www.linkedin.com/uas/oauth2/accessToken"
        let responseType = "code"
        
        // Set the redirect URL. Adding the percent escape characthers is necessary.
        let redirectURL = "https://com.appcoda.linkedin.oauth/oauth".addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        // Create a random string based on the time interval (it will be in the form linkedin12345679).
        let state = "linkedin\(Int(NSDate().timeIntervalSince1970))"
        
        // Set preferred scope.
        let scope = "r_basicprofile,r_emailaddress"
        
        // Create the authorization URL string.
        var authorizationURL = "\(authorizationEndPoint)?"
        authorizationURL += "response_type=\(responseType)&"
        authorizationURL += "client_id=\(linkedInKey)&"
        authorizationURL += "redirect_uri=\(redirectURL!)&"
        authorizationURL += "state=\(state)&"
        authorizationURL += "scope=\(scope)"
        
        print(authorizationURL)
        let request = NSURLRequest(url: NSURL(string: authorizationURL)! as URL )
        // let webView = WebViewController(url: NSURL(string: authorizationURL)! as URL)
        //  self.definesPresentationContext = true
        //self.present( UINavigationController(rootViewController: webView), animated: true, completion: nil)
        webView = UIWebView(frame: self.view.bounds)
        webView.delegate = self
        self.view.addSubview(webView)
        webView.loadRequest(request as URLRequest)
        
        
    }
    /*
     * sign in google dismiss
     */
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        LoaderAlert.shared.show(withStatus: Localization("recup"))
    }
    /*
     * back action
     */
    @IBAction func  go_back(_ sender :Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     * connect to linkedin
     */
    @IBAction func linkedin_connect(_ sender: Any) {
        //SweetAlert
        //SVProgressHUD
        LoaderAlert.shared.show(withStatus: Localization("Chargement"))
        let appName = "Linkedin"
        let appScheme = "\(appName)://"
        let appUrl = URL(string: appScheme)
        if UIApplication.shared.canOpenURL(appUrl! as URL){
            if LinkedinNativeClient.validSession() {
                print("clearing")
                LinkedinNativeClient.clearSession()
                linkedinHelper =  LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "86em0va3cx3dpw", clientSecret: "ZEyvrZJPq61Rt2WX", state: "\(arc4random())", permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "https://com.appcoda.linkedin.oauth/oauth"))
                
            }
            LoaderAlert.shared.dismiss()
            linkedinHelper.authorizeSuccess({ (token) in
                print("saif")
                print(token)
                
                LoaderAlert.shared.show(withStatus: Localization("recup"))
                self.linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original),positions,date-of-birth,phone-numbers,location)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
                    print("saif")
                    print(response)
                    let q = JSON (response.jsonObject)
                    let params: Parameters = [
                        "LinkedInId" : q["id"].stringValue ,
                        "email" : q["emailAddress"].stringValue
                    ]
                    
                    let header: HTTPHeaders = [
                        "Content-Type" : "application/json"
                    ]
                    
                    Alamofire.request(ScriptBase.sharedInstance.ConnexionUserLinkedin , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                        .responseJSON { response in
                            LoaderAlert.shared.dismiss()
                            //   self.btnConnexion.isEnabled = true
                            let a = JSON(response.data)
                            //  print("saif:",a)
                            
                            if a["status"].boolValue == false {
                                
                                
                                if a["message"].stringValue == "Email existe deja "{
                                    let err = a["data"].stringValue
                                    if err == "sql" {
                                        _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionAutreMail") + "GogoSki", style: AlertStyle.error)
                                    }else if err == "facebook" {
                                        _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                                    }else if err == "google" {
                                        _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                                    }else if err == "linkedin" {
                                        _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                                    }
                                } else {
                                    _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionExistSaif"), style: AlertStyle.error)
                                }
                                
                            }else{
                                if (a["message"].stringValue == "Utlisateur existe deja" ) ||  (a["message"].stringValue == "Authentification successful")
                                {
                                    if (a["data"]["user"]["enabled"].boolValue)
                                    {
                                        if (a["data"]["user"]["sports"].arrayObject?.count == 0)
                                        {
                                            
                                            UserDefaults.standard.setValue("NO", forKey: "ModifOnce")
                                            UserDefaults.standard.synchronize()
                                            UserDefaults.standard.setValue(a["data"].rawString(), forKey: "User")
                                            UserDefaults.standard.setValue(a["Blocked"].rawString(), forKey: "UserBlocked")
                                            
                                            UserDefaults.standard.synchronize()
                                            let a = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "modifierprofilOnce") as! ModifierProfilUser
                                            a.Connex = "ln"
                                            self.navigationController?.pushViewController(a, animated: true)
                                            
                                        }else{
                                            UserDefaults.standard.setValue("YES", forKey: "ModifOnce")
                                            UserDefaults.standard.synchronize()
                                            UserDefaults.standard.setValue(a["data"].rawString(), forKey: "User")
                                            UserDefaults.standard.setValue(a["Blocked"].rawString(), forKey: "UserBlocked")
                                            
                                            UserDefaults.standard.synchronize()
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userConnection"), object: nil)
                                            self.performSegue(withIdentifier: "connexion_home", sender: self)
                                        }
                                        
                                    }else{
                                        _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("ConnexionExist"), style: AlertStyle.warning)
                                    }
                                }
                                
                            }
                            
                            
                    }
                    
                    // self.dismiss(animated: true, completion: nil)
                    //parse this response which is in the JSON format
                }) {(error) -> Void in
                    
                    print(error.localizedDescription)
                    //handle the error
                }
                //This token is useful for fetching profile info from LinkedIn server
                
            }, error: { (error) in
                print("saif")
                print(error.localizedDescription)
                //show respective error
            }, cancel:  {
                //show sign in cancelled event
                print("cancled")
            })
            
        }else{
            startAuthorization()
        }
        
        
    }
    
    /*
     * connect to gmail
     */
    @IBAction func gmail_connect(_ sender: Any) {
        
        GIDSignIn.sharedInstance().signIn()
    }
    /*
     * connect to facebook
     */
    @IBAction func facebook_connect(_ sender: Any) {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        if FBSDKAccessToken.current() == nil {
            fbLoginManager.logIn(withReadPermissions: ["public_profile","email"], from: self) { (result, error) in
                if (error == nil){
                    let fbloginresult : FBSDKLoginManagerLoginResult = result!
                    if fbloginresult.grantedPermissions != nil {
                        if(g)
                        {
                            self.getFBUserData()
                            //fbLoginManager.logOut()
                            /*  SharedPreferences.sharedpref.setSharedprefs(email: result["data"]["email"].stringValue, id: result["data"]["user_id"].stringValue, pseudoName: "", fullName: "", gender: "", birthday: "", description: "", address: "", picture: "", phone: "", premium: "", coins: "" , isLoggedin: 1)
                             */
                            /*  if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as? UITabBarController {
                             if let navigator = self.navigationController {
                             navigator.pushViewController(viewController, animated: true)
                             }
                             } */
                            
                            //   self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }
        }else{
            self.getFBUserData()
            
        }
    }
    /*
     * get facebook information
     */
    func getFBUserData(){
        
        LoaderAlert.shared.show(withStatus: Localization("recup"))
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email , gender , birthday" ]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let q = JSON(result as Any)
                    self.dict = result as! [String : AnyObject]
                    print(result!)
                    print("dict:",self.dict)
                    let params: Parameters = [
                        "facebookId": q["id"].stringValue ,
                        "email" :  q["email"].stringValue
                        
                    ]
                    let header: HTTPHeaders = [
                        "Content-Type" : "application/json"
                    ]
                    print("params:",params.description)
                    Alamofire.request(ScriptBase.sharedInstance.ConnexionUser , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                        .responseJSON { response in
                            LoaderAlert.shared.dismiss()
                            //   self.btnConnexion.isEnabled = true
                            let a = JSON(response.data)
                            print("saif:",a)
                            
                            if a["status"].boolValue == false {
                                
                                
                                if a["message"].stringValue == "Email existe deja "{
                                    let err = a["data"].stringValue
                                    if err == "sql" {
                                        _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionAutreMail") + "GogoSki", style: AlertStyle.error)
                                    }else if err == "facebook" {
                                        _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                                    }else if err == "google" {
                                        _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                                    }else if err == "linkedin" {
                                        _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionAutreMail") + err, style: AlertStyle.error)
                                    }
                                } else {
                                    _ = SweetAlert().showAlert(Localization("ConnexionTitre"), subTitle: Localization("InscriptionExistSaif"), style: AlertStyle.error)
                                }
                                
                                
                            }else{
                                if (a["message"].stringValue == "Token créé" )
                                {
                                    if (a["data"]["user"]["enabled"].boolValue)
                                    {
                                        SocketIOManager.sharedInstance.establishConnection()
                                        if (a["data"]["user"]["sports"].arrayObject?.count == 0)
                                        {
                                            
                                            UserDefaults.standard.setValue("NO", forKey: "ModifOnce")
                                            UserDefaults.standard.synchronize()
                                            UserDefaults.standard.setValue(a["data"].rawString(), forKey: "User")
                                            UserDefaults.standard.setValue(a["Blocked"].rawString(), forKey: "UserBlocked")
                                            
                                            UserDefaults.standard.synchronize()
                                            let a = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "modifierprofilOnce") as! ModifierProfilUser
                                            a.Connex = "fb"
                                            self.navigationController?.pushViewController(a, animated: true)
                                            
                                        }else{
                                            UserDefaults.standard.setValue("YES", forKey: "ModifOnce")
                                            UserDefaults.standard.synchronize()
                                            UserDefaults.standard.setValue(a["data"].rawString(), forKey: "User")
                                            UserDefaults.standard.setValue(a["Blocked"].rawString(), forKey: "UserBlocked")
                                            
                                            
                                            UserDefaults.standard.synchronize()
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userConnection"), object: nil)
                                            self.performSegue(withIdentifier: "connexion_home", sender: self)
                                        }
                                        
                                    }else{
                                        _ = SweetAlert().showAlert(Localization("BtnConex"), subTitle: Localization("ConnexionExist"), style: AlertStyle.warning)
                                    }
                                }
                                
                            }
                            
                            
                    }
                    
                }
            })
        }
    }
    /*
     * connect to push notification (onesignal)
     */
    func ConnectTopush(){
        
        OneSignal.add(self as OSSubscriptionObserver)
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.setSubscription(false)
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            OneSignal.setSubscription(true)
            let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
            let userID = status.subscriptionStatus.userId
            print("userID = \(userID)")
            let pushToken = status.subscriptionStatus.pushToken
            print("pushToken = \(pushToken)")
        })
        
    }
    /*
     * disconnect from push notification
     */
    func DisconnectFromPush(){
        OneSignal.remove(self as OSSubscriptionObserver)
    }
    /*
     * subscribe to push notification
     */
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
            // get player ID
            print("userId:" ,  stateChanges.to.userId)
            if stateChanges.to.userId != nil {
                let ab = UserDefaults.standard.value(forKey: "User") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let params : Parameters = [
                    "os" : "ios" ,
                    "pid" : stateChanges.to.userId
                ]
                let header: HTTPHeaders = [
                    "Content-Type" : "application/json"
                    ,"x-Auth-Token" : a["value"].stringValue
                ]
                Alamofire.request(ScriptBase.sharedInstance.subscribePush , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        //      LoaderAlert.shared.dismiss()
                        
                        let b = JSON(response.data!)
                        print(b)
                        
                }
                //  else {
                //      _ = SweetAlert().showAlert("Conexion", subTitle: "Veuillez vous connecter", style: AlertStyle.error)
                //}
            }
        }
        stateChanges.to.userId
    }
    
    /*
     * normal connect to gogoski
     */
    @IBAction func conexionSQL(_ sender: Any) {
        if self.email.text != "" && self.password.text != ""{
            LoaderAlert.shared.show(withStatus: Localization("ChargementConnextion"))
            self.btnConnexion.isEnabled = false
            let params: Parameters = [
                "email": self.email.text!,
                "password" : self.password.text!
            ]
            let header: HTTPHeaders = [
                "Content-Type" : "application/json"
            ]
            
            Alamofire.request(ScriptBase.sharedInstance.ConnexionUser , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                .responseString { response in
                    LoaderAlert.shared.dismiss()
                    self.btnConnexion.isEnabled = true
                    let a = JSON(response.data)
                    print("saif:",a)
                    
                    if a["status"].boolValue == false {
                        if a["message"].stringValue == "L utilisateur n existe pas" || a["message"].stringValue == "Mot de passe invalide" {
                            self.email.text = ""
                            self.password.text = ""
                            _ = SweetAlert().showAlert(Localization("ConnexionChargement"), subTitle: Localization("ConnexionErreur"), style: AlertStyle.error)
                        }
                    }else{
                        if a["data"]["user"]["enabled"].boolValue {
                            //***** change langue
                            if a["data"]["user"]["langue"].stringValue == "English" || a["data"]["user"]["langue"].stringValue == "Anglais" {
                                
                                SetLanguage(self.arrayLanguages[1])
                            }else if a["data"]["user"]["langue"].stringValue == "French" || a["data"]["user"]["langue"].stringValue == "Français"  {
                                SetLanguage(self.arrayLanguages[2])
                            }
                            Localisator.sharedInstance.saveInUserDefaults = true
                            
                            //*****
                            SocketIOManager.sharedInstance.establishConnection()
                            if a["data"]["user"]["nom"].description == "null"{
                                UserDefaults.standard.setValue("NO", forKey: "ModifOnce")
                                UserDefaults.standard.synchronize()
                                UserDefaults.standard.setValue(a["data"].rawString(), forKey: "User")
                                UserDefaults.standard.setValue(a["Blocked"].rawString(), forKey: "UserBlocked")
                                
                                UserDefaults.standard.synchronize()
                            }else{
                                UserDefaults.standard.setValue("YES", forKey: "ModifOnce")
                                UserDefaults.standard.synchronize()
                                UserDefaults.standard.setValue(a["data"].rawString(), forKey: "User")
                                UserDefaults.standard.setValue(a["Blocked"].rawString(), forKey: "UserBlocked")
                                
                                UserDefaults.standard.synchronize()
                            }
                            let x = a["data"]["user"]["nom"].description
                            self.ConnectTopush()
                            if x != "null" {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userConnection"), object: nil)
                                self.performSegue(withIdentifier: "connexion_home", sender: self)
                            }else{
                                
                                let a = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "modifierprofilOnce") as! ModifierProfilUser
                                a.Connex = "no"
                                self.navigationController?.pushViewController(a, animated: true)
                            }
                        }else{
                            _ = SweetAlert().showAlert(Localization("BtnConex"), subTitle: Localization("ConnexionExist"), style: AlertStyle.warning)
                        }
                    }
                    
                    
            }
            
        }
    }
    
    /*
     * language configure
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("ConnexionTitre")
        ou.text = Localization("ou")
        btnMotDePasseOublié.setTitle(Localization("btnMotDePasseOublié"), for: .normal)
        labelMail.text = Localization("labelMail")
        btnConnexion.setTitle(Localization("BtnConex"), for: .normal)
        //  email_password.text = Localization("enterEmail")
        //btnSend.setTitle(Localization("send"), for: .normal)
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    
    
}

