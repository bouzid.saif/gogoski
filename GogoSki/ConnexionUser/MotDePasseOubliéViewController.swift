//
//  MotDePasseOubliéViewController.swift
//  GogoSki
//
//  Created by Ahmed Haddar on 21/12/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MotDePasseOublie_ViewController: InternetCheckViewControllers {
    
    /*
     * cancel button
     */
    @IBOutlet weak var btnCancel: UIButton!
    /*
     * password forgot popup
     */
    @IBOutlet weak var popup: UIView!
    /*
     * email label
     */
    @IBOutlet weak var email_password: UILabel!
    /*
     * send button
     */
    @IBOutlet weak var btnSend: UIButton!
    /*
     * email textfield
     */
    @IBOutlet weak var email_password_textfield: PaddingTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        popup.layer.cornerRadius = 10
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     * function that send you an email with link to reset your password
     */
    @IBAction func motDePasseOubliéAction(_ sender: Any) {
        LoaderAlert.show()
        let params: Parameters = [
            "email": self.email_password_textfield.text
        ]
        
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            
            ]
        Alamofire.request(ScriptBase.sharedInstance.motDePasseOublié , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                print (response)
                LoaderAlert.dismiss()
                if b["status"].boolValue {
                    _ = SweetAlert().showAlert(Localization("pwd") , subTitle: Localization("pwd1"), style: AlertStyle.success)
                    self.dismiss(animated: true, completion: nil)
                }
                else {
                    _ = SweetAlert().showAlert(Localization("pwd") , subTitle: Localization("pwd5"), style: AlertStyle.error)
                }
                
                
        }
    }
    
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        btnCancel.setTitle(Localization("cancel"), for: .normal)
        email_password.text = Localization("enterEmail")
        btnSend.setTitle(Localization("send"), for: .normal)
        // Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    
    /*
     * cancel action
     */
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

