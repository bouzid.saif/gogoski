//
//  PrivacyViewController.swift
//  GogoSki
//
//  Created by Ahmed Haddar on 05/01/2018.
//  Copyright © 2018 Velox-IT. All rights reserved.
//

import UIKit

class PrivacyViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     * back action
     */
    @IBAction func Back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

