//
//  ListeNoire_saif.swift
//  GogoSki
//
//  Created by Bouzid saif on 21/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import MapleBacon

class ListeNoire_saif: InternetCheckViewControllers,UITableViewDelegate,UITableViewDataSource {
    
    /*
     * text appear if there is no user in blacklist
     */
    @IBOutlet weak var default_noire: UILabel!
    /*
     * title of this page
     */
    @IBOutlet weak var titre: UILabel!
    /*
     * blacklist tableview
     */
    @IBOutlet weak var table : UITableView!
    /*
     * variable of users in blacklist
     */
    var listeNoir : JSON = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (listeNoir.arrayObject?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let imgUser : UIImageView = cell.viewWithTag(1) as! UIImageView
        let labelName : UILabel = cell.viewWithTag(2) as! UILabel
        let labelDate : UILabel = cell.viewWithTag(3) as! UILabel
        let btnSupp : CustomButton = cell.viewWithTag(40) as! CustomButton
        btnSupp.index = indexPath.row
        btnSupp.indexPath = indexPath
        btnSupp.addTarget(self, action: #selector(self.RemoveListNoire), for: .touchUpInside)
        labelName.text = self.listeNoir[indexPath.row]["prenom"].stringValue + " " + self.listeNoir[indexPath.row]["nom"].stringValue
        labelDate.text = self.listeNoir[indexPath.row]["date"].stringValue.prefix(10) + ""
        if let urlImgUser = URL(string : self.listeNoir[indexPath.row]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imgUser.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        
        
        return cell
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        table.allowsMultipleSelection = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        getFavoris ()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func go_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("ListNoirTitre")
        default_noire.text = Localization("default_noire")
        
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    /*
     * get all users in blacklist
     */
    func getFavoris ()
    {
        LoaderAlert.shared.show()
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.afficherListeNoir , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.shared.dismiss()
                
                let b = JSON(response.data!)
                
                if b["status"].boolValue {
                    if (b["message"].stringValue == "List vide")
                    {
                        print ("fera8")
                        self.table.isHidden = true
                        self.default_noire.isHidden = false
                        self.listeNoir = []
                    }
                    else {
                        self.table.isHidden = false
                        self.default_noire.isHidden = true
                        self.listeNoir = b["data"]
                        self.table.reloadData()
                        print (b["data"])
                    }
                    
                    
                    
                }
                
        }
    }
    /*
     * remove user from blacklist
     */
    @objc func RemoveListNoire (sender : CustomButton )
    {
        let params: Parameters = [
            "idUserNoire": self.listeNoir[sender.index!]["id"].stringValue
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.removeListNoire , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                
                let b = JSON(response.data)
                print ("supp ListNoire : " , b)
                if b["status"].boolValue {
                    
                    _ = SweetAlert().showAlert(Localization("removeListNoir"), subTitle: Localization("removeListNoir2"), style: AlertStyle.success)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RemoveFromBlackList"), object:  ["\(self.listeNoir[sender.index!]["id"].stringValue)"])
                    if (self.listeNoir.arrayObject?.count != 1)
                    {
                        self.listeNoir.arrayObject?.remove(at: sender.index!)
                        
                    }
                    else {
                        self.listeNoir = []
                    }
                    
                    self.table.reloadData()
                }else{
                    
                    _ = SweetAlert().showAlert(Localization("removeListNoir"), subTitle: Localization("erreur"), style: AlertStyle.error)
                    
                }
                
                
        }
    }
}

