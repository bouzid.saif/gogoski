//
//  Favoris_user.swift
//  GogoSki
//
//  Created by Bouzid saif on 20/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class Favoris_user: UIViewController,UITableViewDelegate,UITableViewDataSource {
    /*
     * array of all users connected
     */
    var Connected : [String] = []
    /*
     * array for levels in french
     */
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     * array for levels in English
     */
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     * array for sports in French
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     * array for sports in English
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     * navigation controller of this controller
     */
    var parentNavigationController : UINavigationController?
    /*
     * text appear when user has no favorite
     */
    @IBOutlet weak var default_favoris: UILabel!
    /*
     * favorite tableview
     */
    @IBOutlet weak var table : UITableView!
    /*
     * variable of user's favorites
     */
    var Favoris : JSON = []
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Favoris.arrayObject?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let imgUser : UIImageView = cell.viewWithTag(1) as! UIImageView
        
        let labelNom : UILabel = cell.viewWithTag(2) as! UILabel
        labelNom.text = self.Favoris[indexPath.row]["prenom"].stringValue + " " + self.Favoris[indexPath.row]["nom"].stringValue
        let labelSport : UILabel = cell.viewWithTag(3) as! UILabel
        let labelNiveau : UILabel = cell.viewWithTag(4) as! UILabel
        let labelNiveau2 : UILabel = cell.viewWithTag(10000) as! UILabel
        print ("langueeeeeee" , Localisator.sharedInstance.currentLanguage)
        labelNiveau2.isHidden = true
        labelNiveau.isHidden = false
        if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr" {
            switch self.Favoris[indexPath.row]["pratique"].stringValue {
            case SportSaifFR[0] :
                
                labelSport.text = SportSaifEN[0]
            case SportSaifFR[1] :
                labelSport.text = SportSaifEN[1]
                
            case SportSaifFR[2] :
                labelSport.text = SportSaifEN[2]
                
            case SportSaifFR[3] :
                labelSport.text  = SportSaifEN[3]
                
            case SportSaifFR[4] :
                labelSport.text = SportSaifEN[4]
            case SportSaifFR[5] :
                labelSport.text = SportSaifEN[5]
                labelNiveau2.isHidden = false
                labelNiveau.isHidden = true
            default :
                
                labelSport.text  = SportSaifEN[0]
                
            }
            
            switch self.Favoris[indexPath.row]["niveau"].stringValue {
            case NiveauSaifFR[0] :
                labelNiveau2.text = NiveauSaifEN[0]
                labelNiveau.text = "- " + NiveauSaifEN[0]
            case NiveauSaifFR[1] :
                labelNiveau.text = "- " + NiveauSaifEN[1]
                labelNiveau2.text = NiveauSaifEN[1]
            case NiveauSaifFR[2] :
                labelNiveau.text = "- " + NiveauSaifEN[2]
                labelNiveau2.text = NiveauSaifEN[2]
            case NiveauSaifFR[3] :
                labelNiveau.text = "- " + NiveauSaifEN[3]
                labelNiveau2.text = NiveauSaifEN[3]
            case NiveauSaifFR[4] :
                labelNiveau.text = "- " + NiveauSaifEN[4]
                labelNiveau2.text = NiveauSaifEN[4]
                
            default :
                labelNiveau2.text = " "
                labelNiveau.text = " "
            }
            
        }
        else {
            labelSport.text = self.Favoris[indexPath.row]["pratique"].stringValue
            labelNiveau.text = " - " + self.Favoris[indexPath.row]["niveau"].stringValue
            labelNiveau2.text = self.Favoris[indexPath.row]["niveau"].stringValue
            if self.Favoris[indexPath.row]["pratique"].stringValue == "Ski de randonnée"
            {
                labelNiveau2.isHidden = false
                labelNiveau.isHidden = true
            }
        }
        
        if ( self.Favoris[indexPath.row]["niveau"].stringValue == "Confirmé" )
        {
            labelNiveau.textColor = UIColor("#FF3E53")
        }
        else if (self.Favoris[indexPath.row]["niveau"].stringValue == "Débutant")
        {
            labelNiveau.textColor = UIColor("#D3D3D3")
        }
        else if (self.Favoris[indexPath.row]["niveau"].stringValue == "Intermédiaire")
        {
            labelNiveau.textColor = UIColor("#75A7FF")
        }
        else if (self.Favoris[indexPath.row]["niveau"].stringValue == "Expert")
        {
            labelNiveau.textColor = UIColor("#4D4D4D")
        }
        else {
            labelNiveau.textColor = UIColor("#76EC9E")
        }
        
        
        
        
        if let urlImgUser = URL(string : self.Favoris[indexPath.row]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imgUser.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        //  let labelDistance : UILabel = cell.viewWithTag(5) as! UILabel
        let btnSupp : CustomButton = cell.viewWithTag(40) as! CustomButton
        btnSupp.index = indexPath.row
        btnSupp.indexPath = indexPath
        if self.Favoris[indexPath.row]["type"].stringValue == "ami" {
            btnSupp.setBackgroundImage(UIImage(named:"etoile"), for: .normal)
        }else {
            btnSupp.setBackgroundImage(UIImage(named:"coeur_saif"), for: .normal)
        }
        btnSupp.addTarget(self, action: #selector(self.RemoveFavoris), for: .touchUpInside)
        let btnMessages : CustomButton = cell.viewWithTag(100) as! CustomButton
        btnMessages.index = indexPath.row
        btnMessages.indexPath = indexPath
        btnMessages.addTarget(self, action: #selector(self.messageAction), for: .touchUpInside)
        let Badge : UIImageView = cell.viewWithTag(10) as! UIImageView
        Badge.isHidden = false
        var ab : Bool = false
        var temp = self.Favoris[indexPath.row]
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Connected = appDelegate.ConnectedUser
        let abc = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = abc.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var accc = JSON(data: dataFromString!)
        if temp.arrayObject?.count != 0
        {
            
            
            for a in Connected{
                
                if a == temp["id"].stringValue && temp["id"].stringValue != accc["user"]["id"].stringValue {
                    ab = true
                }
                
                
                
            }
            
            
            
            Badge.image = (ab == false) ? UIImage(named:"offline_ahmed") : UIImage(named:"online_ahmed")
            if ab {
                cell.backgroundColor = UIColor("#EFEFEF")
            }else {
                cell.backgroundColor = UIColor("#F2F2F2")
            }
        }
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getFavoris()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        table.allowsMultipleSelection = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Connected = appDelegate.ConnectedUser
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleReload(notification:)), name: NSNotification.Name(rawValue: "ReloadConv"), object: nil)
        if #available(iOS 11.0, *) {
            table.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        
    }
    /*
     * function to notify if there is user disconnected or connected
     */
    @objc func handleReload(notification: NSNotification) {
        self.table.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*
     * get all favorites
     */
    func getFavoris ()
    {
        
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.afficherFavoris , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                
                let b = JSON(response.data)
                
                
                if b["status"].boolValue {
                    if (b["message"].stringValue == "List vide")
                    {
                        print ("fera8")
                        self.table.isHidden = true
                        self.default_favoris.isHidden = false
                        self.Favoris = []
                    }
                    else {
                        self.table.isHidden = false
                        self.default_favoris.isHidden = true
                        self.Favoris = b["data"]
                        self.table.reloadData()
                        print (b["data"])
                    }
                    
                }
                
        }
    }
    /*
     * remove from your favorites action
     */
    @objc func RemoveFavoris (sender : CustomButton )
    {
        let params: Parameters = [
            "idUserFav": self.Favoris[sender.index!]["id"].stringValue
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.removeFavoris , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                
                let b = JSON(response.data)
                print ("supp favoris : " , b)
                if b["status"].boolValue {
                    
                    _ = SweetAlert().showAlert(Localization("removeFavoris"), subTitle: Localization("removeFavoris2"), style: AlertStyle.success)
                    if (self.Favoris.arrayObject?.count != 1)
                    {
                        self.Favoris.arrayObject?.remove(at: sender.index!)
                    }
                    else {
                        self.Favoris = []
                    }
                    self.table.reloadData()
                }else{
                    
                    _ = SweetAlert().showAlert(Localization("removeFavoris"), subTitle: Localization("erreur"), style: AlertStyle.error)
                    
                }
                
                
        }
    }
    /*
     * go to conversation with user
     */
    @objc func messageAction(sender : CustomButton) {
        LoaderAlert.show()
        let params: Parameters = [
            "amiId": self.Favoris[sender.index!]["id"].stringValue
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.addConvPriv , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                print ("add conv "  , b)
                if b["status"].boolValue {
                    
                    let ConvId = b["data"]["id"].stringValue
                    SocketIOManager.sharedInstance.connectToServerWithNickname(nickname: ConvId, userid: a["user"]["id"].stringValue, first: false)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToMessageForomNoWhere2"), object: ConvId )
                }
                
                
        }
    }
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        
        default_favoris.text = Localization("default_favoris")
        
        //Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let view = self.storyboard?.instantiateViewController(withIdentifier: "visiteur_profil") as! visiteur_profil
        view.id = self.Favoris[indexPath.row]["id"].stringValue
        view.parentNavigationController = self.parentNavigationController
        parentNavigationController?.pushViewController(view, animated: true)
    }
}

