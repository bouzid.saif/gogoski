//
//  Rechercher_user.swift
//  GogoSki
//
//  Created by Bouzid saif on 20/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

class Rechercher_user: UIViewController,UITableViewDelegate,UITableViewDataSource , UITextFieldDelegate {
    /*
     * array for levels in french
     */
    let NiveauSaifFR = ["Débutant","Débrouillard","Intermédiaire","Confirmé","Expert"]
    /*
     * array for levels in English
     */
    let NiveauSaifEN = ["Beginner","Resourceful","Intermediate","Confirmed","Expert"]
    /*
     * array for sports in French
     */
    let SportSaifFR = ["Ski Alpin","Snowboard","Ski de randonnée","Handiski","Raquettes","Ski de fond"]
    /*
     * array for sports in English
     */
    let SportSaifEN = ["Alpine skiing","Snowboard","Nordic skiing","Handiski","Racket","Cross-country skiing"]
    /*
     * user tableview
     */
    @IBOutlet weak var table : UITableView!
    /*
     * search textfield
     */
    @IBOutlet weak var rechercheTextField: UITextField!
    /*
     * button invite user
     */
    @IBOutlet weak var btnInviter: UIButton!
    /*
     * variable of users found
     */
    var users : JSON = []
    /*
     * navigation controller of this controller
     */
    var parentNavigationController : UINavigationController?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (users.arrayObject?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let labelName : UILabel = cell.viewWithTag(2) as! UILabel
        let labelSport : UILabel = cell.viewWithTag(3) as! UILabel
        let labelNiveau : UILabel = cell.viewWithTag(4) as! UILabel
        let imgUser : UIImageView = cell.viewWithTag(40) as! UIImageView
        let btnAjout : CustomButton = cell.viewWithTag(50) as! CustomButton
        btnAjout.index = indexPath.row
        btnAjout.indexPath = indexPath
        btnAjout.addTarget(self, action: #selector(self.AjoutFavoris), for: .touchUpInside)
        labelName.text = self.users[indexPath.row]["prenom"].stringValue + " " + self.users[indexPath.row]["nom"].stringValue
        let labelNiveau2 : UILabel = cell.viewWithTag(10000) as! UILabel
        labelNiveau2.isHidden = true
        labelNiveau.isHidden = false
        if Localisator.sharedInstance.currentLanguage != "French"  && Localisator.sharedInstance.currentLanguage != "Français"  && Localisator.sharedInstance.currentLanguage != "French_fr" {
            switch self.users[indexPath.row]["sports"][0]["pratique"].stringValue {
            case SportSaifFR[0] :
                
                labelSport.text = SportSaifEN[0]
            case SportSaifFR[1] :
                labelSport.text = SportSaifEN[1]
                
            case SportSaifFR[2] :
                labelSport.text = SportSaifEN[2]
                
            case SportSaifFR[3] :
                labelSport.text  = SportSaifEN[3]
                
            case SportSaifFR[4] :
                labelSport.text = SportSaifEN[4]
            case SportSaifFR[5] :
                labelSport.text = SportSaifEN[5]
                labelNiveau2.isHidden = false
                labelNiveau.isHidden = true
            default :
                
                labelSport.text  = ""
                
            }
            
            switch self.users[indexPath.row]["sports"][0]["niveau"].stringValue {
            case NiveauSaifFR[0] :
                labelNiveau2.text = NiveauSaifEN[0]
                labelNiveau.text = "- " + NiveauSaifEN[0]
            case NiveauSaifFR[1] :
                labelNiveau.text = "- " + NiveauSaifEN[1]
                labelNiveau2.text = NiveauSaifEN[1]
            case NiveauSaifFR[2] :
                labelNiveau.text = "- " + NiveauSaifEN[2]
                labelNiveau2.text = NiveauSaifEN[2]
            case NiveauSaifFR[3] :
                labelNiveau.text = "- " + NiveauSaifEN[3]
                labelNiveau2.text = NiveauSaifEN[3]
            case NiveauSaifFR[4] :
                labelNiveau.text = "- " + NiveauSaifEN[4]
                labelNiveau2.text = NiveauSaifEN[4]
                
            default :
                
                labelNiveau.text = " "
            }
        }
        else {
            labelSport.text = self.users[indexPath.row]["sports"][0]["pratique"].stringValue
            labelNiveau.text = "- " + self.users[indexPath.row]["sports"][0]["niveau"].stringValue
            labelNiveau2.text = self.users[indexPath.row]["sports"][0]["niveau"].stringValue
            if self.users[indexPath.row]["sports"][0]["pratique"].stringValue == "Ski de randonnée"
            {
                labelNiveau2.isHidden = false
                labelNiveau.isHidden = true
            }
        }
        
        if ( self.users[indexPath.row]["sports"][0]["niveau"].stringValue == "Confirmé" )
        {
            labelNiveau.textColor = UIColor("#FF3E53")
        }
        else if (self.users[indexPath.row]["sports"][0]["niveau"].stringValue == "Débutant")
        {
            labelNiveau.textColor = UIColor("#D3D3D3")
        }
        else if (self.users[indexPath.row]["sports"][0]["niveau"].stringValue == "Intermédiaire")
        {
            labelNiveau.textColor = UIColor("#75A7FF")
        }
        else if (self.users[indexPath.row]["sports"][0]["niveau"].stringValue == "Expert")
        {
            labelNiveau.textColor = UIColor("#4D4D4D")
        }
        else {
            labelNiveau.textColor = UIColor("#76EC9E")
        }
        
        let btnMessages : CustomButton = cell.viewWithTag(100) as! CustomButton
        btnMessages.index = indexPath.row
        btnMessages.indexPath = indexPath
        btnMessages.addTarget(self, action: #selector(self.messageAction), for: .touchUpInside)
        if let urlImgUser = URL(string : self.users[indexPath.row]["photo"].stringValue) , let placeholder =  UIImage(named: "user_placeholder.png") {
            imgUser.setImage(withUrl: urlImgUser , placeholder: placeholder)
        }
        return cell
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        rechercheTextField.delegate = self
        rechercheTextField.addTarget(self, action: #selector(self.TextfiedDidChange(TextField:)), for: .editingChanged)
        table.allowsMultipleSelection = false
        if #available(iOS 11.0, *) {
            table.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
    }
    /*
     * invite to my friends with email
     */
    @IBAction func go_next(_ sender: Any) {
        let Inviter = self.storyboard?.instantiateViewController(withIdentifier: "InviterAmiUser") as! InviterAmi_saif
        parentNavigationController?.pushViewController(Inviter, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*
     * configure language
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        btnInviter.setTitle(Localization("btnInviterAmi"), for: .normal)
        rechercheTextField.placeholder = Localization("startSearch")
        
        //  Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    
    
    /*
     * action to do when textfield search did change
     */
    @objc func TextfiedDidChange(TextField : UITextField)
    {
        //users = []
        //  LoaderAlert.shared.show()
        if TextField.text != "" {
            let ab = UserDefaults.standard.value(forKey: "User") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let header: HTTPHeaders = [
                "Content-Type" : "application/json",
                "x-Auth-Token" : a["value"].stringValue
            ]
            Alamofire.request(ScriptBase.sharedInstance.searchUser + TextField.text! , method: .get, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    
                    let b = JSON(response.data)
                    
                    if b["status"].boolValue {
                        if (b["data"].arrayObject?.count == 0)
                        {
                            self.users = b["data"]
                            self.table.reloadData()
                            
                        }
                        else {
                            self.users = b["data"]
                            self.table.reloadData()
                            print (b["data"])
                        }
                        
                        
                        
                    }
                    
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.rechercheTextField.text = ""
        print ("tenzel 3la ami")
        let view = self.storyboard?.instantiateViewController(withIdentifier: "visiteur_profil") as! visiteur_profil
        view.id = self.users[indexPath.row]["id"].stringValue
        view.parentNavigationController = self.parentNavigationController
        parentNavigationController?.pushViewController(view, animated: true)
    }
    /*
     * add user to your favorite
     */
    @objc func AjoutFavoris (sender : CustomButton )
    {
        let params: Parameters = [
            "idUserFav": self.users[sender.index!]["id"].stringValue
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.addFavoris , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                
                let b = JSON(response.data)
                print ("add Favoris "  , b)
                if b["status"].boolValue {
                    if ( b["message"].stringValue == "Already friend")
                    {
                        _ = SweetAlert().showAlert(Localization("addFavorite"), subTitle: Localization("alreadyExist"), style: AlertStyle.error)
                    }
                    else {
                        _ = SweetAlert().showAlert(Localization("addFavorite"), subTitle: Localization("addFavorite2"), style: AlertStyle.success)
                        let ab = UserDefaults.standard.value(forKey: "User") as! String
                        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                        var a = JSON(data: dataFromString!)
                        let header: HTTPHeaders = [
                            "Content-Type" : "application/json",
                            "x-Auth-Token" : a["value"].stringValue
                        ]
                        
                        let params: Parameters = [
                            "content_fr": a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " vous a ajouté a ses favoris" ,
                            "content_en" : a["user"]["prenom"].stringValue + " " + a["user"]["nom"].stringValue + " added you to his favorites" ,
                            "userids" : ["\(self.users[sender.index!]["id"].stringValue)"] ,
                            
                            //  "\(self.users[sender.index!]["id"].stringValue)"
                            "type" : "ajout" ,
                            "titre" : "Invitation" ,
                            "photo" : a["user"]["photo"].stringValue ,
                            "activite" : "" ,
                            "sender" : a["user"]["id"].stringValue ,
                            "rdv" : ""
                        ]
                        print(params.description)
                        Alamofire.request(ScriptBase.sharedInstance.creeNotif , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                            .responseJSON { response in
                                print (response)
                        }
                    }
                }else{
                    
                    _ = SweetAlert().showAlert(Localization("addFavorite"), subTitle: Localization("erreur"), style: AlertStyle.error)
                    
                }
                
                
        }
    }
    /*
     * go to conversation with this user
     */
    @objc func messageAction(sender : CustomButton) {
        LoaderAlert.show()
        let params: Parameters = [
            "amiId": self.users[sender.index!]["id"].stringValue
        ]
        let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Content-Type" : "application/json",
            "x-Auth-Token" : a["value"].stringValue
        ]
        Alamofire.request(ScriptBase.sharedInstance.addConvPriv , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                LoaderAlert.dismiss()
                let b = JSON(response.data)
                print ("add conv "  , b)
                if b["status"].boolValue {
                    
                    let ConvId = b["data"]["convId"].stringValue
                    SocketIOManager.sharedInstance.connectToServerWithNickname(nickname: ConvId, userid: a["user"]["id"].stringValue, first: false)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToMessageForomNoWhere"), object: ConvId as! String)
                }
                
                
        }
    }
}

