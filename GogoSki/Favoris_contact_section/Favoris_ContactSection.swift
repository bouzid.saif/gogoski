//
//  Favoris_ContactSection.swift
//  GogoSki
//
//  Created by Bouzid saif on 20/10/2017.
//  Copyright © 2017 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import PageMenu
class Favoris_ContactSection: InternetCheckViewControllers {
    /*
     * the view who contains the controllers favorite and search
     */
    @IBOutlet weak var container: UIView!
    /*
     * title of this page
     */
    @IBOutlet weak var titre: UILabel!
    /*
     * search title
     */
    var titleSearch = ""
    /*
     * favorite title
     */
    var titleFavoris = ""
    /*
     * the menu used for segmented controller
     */
    var pageMenu : CAPSPageMenu?
    /*
     * test if first enter or not
     */
    var first = true
    /*
     * parametres of the page menu
     */
    var parameters : [CAPSPageMenuOption] = []
    /*
     * controllers of the page menu
     */
    var controllerArray : [UIViewController] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()
        let mapcontroller = self.storyboard?.instantiateViewController(withIdentifier: "Favoris_user") as! Favoris_user
        mapcontroller.title = titleFavoris
        mapcontroller.parentNavigationController = self.navigationController
        controllerArray.append(mapcontroller)
        let messangerController = self.storyboard?.instantiateViewController(withIdentifier: "Rechercher_user") as! Rechercher_user
        messangerController.parentNavigationController = self.navigationController
        messangerController.title = titleSearch
        
        controllerArray.append(messangerController)
        parameters  = [
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(UIColor(red: 18.0/255.0, green: 150.0/255.0, blue: 225.0/255.0, alpha: 1.0)),
            .menuMargin(20),
            .menuHeight(50.0),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor(red: 40.0/255.0, green: 40.0/255.0, blue: 40.0/255.0, alpha: 1.0)),
            .menuItemSeparatorWidth(0)
        ]
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if first == false {
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: (container?.frame.width)!, height: (container?.frame.height)! ), pageMenuOptions: parameters)
            
            container?.addSubview(pageMenu!.view)
            pageMenu?.view.frame = container.bounds
            container?.updateConstraintsIfNeeded()
            pageMenu?.viewDidLayoutSubviews()
            // pageMenu.
            // pageMenu?.addBadgeAtView(0, badge: "0")
            //  pageMenu?.addBadgeAtView(1, badge: "2")
        }
        first = false
    }
    /*
     * back action
     */
    @IBAction func go_back (_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*
     * language configure
     */
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    func configureViewFromLocalisation() {
        titre.text = Localization("FavorisContactTitre")
        titleSearch = Localization("Search")
        titleFavoris = Localization("Favoris")
        Localisator.sharedInstance.saveInUserDefaults = true
        // label.text = Localization("label")
    }
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
}

