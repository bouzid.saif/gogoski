//
//  BadgeClass.swift
//  PageMenu
//
//  Created by Bouzid saif on 02/12/2017.
//

import UIKit

class BadgeClass: UIView {

    @IBOutlet var ContentView: UIView!
    @IBOutlet var NumBadge: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override init(frame: CGRect) {
        super.init(frame: frame)
        commoninit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commoninit()
    }
    private func commoninit(){
        Bundle.main.loadNibNamed("Badge", owner: nil, options: nil)?.first
        addSubview(ContentView)
        ContentView.frame = self.bounds
        ContentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        
        
    }

}
